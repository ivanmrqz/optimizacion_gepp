# language: es
# encoding: utf-8
Característica: GEPP - TEST Login

  @regresion @login @CP001_L
  Escenario: CONFIGURACIÓN INICIAL
  Dado abro la aplicacion
    Entonces valido estar en la pantalla de Configuracion Inicial
    Cuando valido atributos de la primer configuracion
    Cuando Obtengo los datos de prueba desde "dt_test_v1" data pool con filtros "id = 1"
    Entonces ingreso bodega
    Y I press back button
    Entonces dar clic en el boton sincronizar
    Entonces que estoy en la pagina de inicio '1420'


  @regresion @login @CP002L
  Escenario: CARGA DE INFORMACION
    Dado abro la aplicacion
    Cuando valido atributos de inicio
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 1"
    Entonces ingreso bodega y ruta
    Y I press back button
    Y selecciono el boton tcomm local
    Y acepto la popup
    Entonces valido estar en la pantalla de cargas
    Y selecciono una carga disponible
    Cuando Seleccionamos siguiente
    Entonces valido estar en la pantalla de Login



  @regresion @login @CP003L
  Escenario: INICIO DE SESION Y CONFIGURACION DE TCOMM
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 1"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y doy clic en 'Siguiente'
    Entonces validar estar en la ventana 1000 Menu
    Y doy click en inventario
    Cuando imprimo el inventario
    Y acepto la popup
    Y acepto la popup
    Entonces doy click en el tercer tcomm
    Y Seleccionamos siguiente en la pagina "1507"
    Entonces selecciono el boton tcomm local
    Y valido el icono de pepsi
    Entonces ingreso datos de control de km
    Y I press back button
    Entonces confirmo kilometraje
    Y I press back button
    Entonces doy click en el icono de confirmacion
    Y doy click en el boton etiqueta
    Y acepto la popup
    Entonces valido el boton servicio al cliente




