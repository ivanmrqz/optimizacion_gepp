# language: es
# encoding: utf-8
Característica:  VENTAS - CREDITO REVOLVENTE + TRANSFERENCIA + OTRAS MONEDAS GRB0


  @regresion @ventas @CP045 @Credito_Revolvente+Transferencia+Otras_monedas @grb0 @ruta19
  Escenario: Venta Efectivo+Cheque+CréditoRevolvente
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 45"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=45"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=45-1"
    Entonces busco segundo combo
    Entonces Se muestra el pedido del dia
    Y obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces  acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces  se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup