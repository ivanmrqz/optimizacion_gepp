# language: es
# encoding: utf-8
Característica: CREDITO REVOLVENTE + TRANSFERENCIA

  @regresion @ventas @CP002 @Credito_Revolvente+Transferencia @grb2
  Escenario: Venta Credito_Revolvente+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 2"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Cuando calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

  @regresion @login @CP059 @Credito_Revolvente+Transferencia @grb2
  Escenario: Venta Credito_Revolvente+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 59"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 59"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Entonces selecciono el descuento y valido que se aplique
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @login @CP153 @Credito_Revolvente+Transferencia @grb2
  Escenario: Venta Credito_Revolvente+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 153"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Y busco productos y agrego productos"primer grb"
  Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 153"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

