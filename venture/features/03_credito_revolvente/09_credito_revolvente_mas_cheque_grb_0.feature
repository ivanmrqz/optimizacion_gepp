# language: es
# encoding: utf-8
Característica: VENTAS - CREDITO REVOLVENTE MAS CHEQUE GRB0

  @regresion @ventas @grb0 @ruta19 @CP096 @Credito_Revolvente+cheque
  Escenario: venta Credito Revolvente + cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=96"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @grb0 @ruta19 @CP165 @Credito_Revolvente+cheque
  Escenario: venta Credito Revolvente + cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=165"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 165"
    Y busco tipo de combo
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

  @regresion @ventas @grb0 @ruta19 @CP173 @Credito_Revolvente+cheque
  Escenario: venta Credito Revolvente + cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=173"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @grb0 @ruta19 @CP174 @Credito_Revolvente+cheque
  Escenario: venta Credito Revolvente + cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=174"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=174-1"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

  @regresion @ventas @grb0 @ruta19 @CP175 @Credito_Revolvente+cheque
  Escenario: venta Credito Revolvente + cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=175"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 175"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
