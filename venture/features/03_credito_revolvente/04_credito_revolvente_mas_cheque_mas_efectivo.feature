# language: es
# encoding: utf-8
Característica: CREDITO REVOLVENTE + CHEQUE + EFECTIVO
##Efectivo+Cheque+CréditoRevolvente, Refrescos GRB +No Carbonatados, Agrupado Bandera GRB 2, Cobro de Envase GRB, Préstamo de Cajilla, Vende Envase, Saldo de Prestamo,Descuento Escalera, Descuento Condición Comercial
  @regresion @ventas @C125 @Credito_Revolvente+Cheque+Efectivo @ruta3 @grb2
  Escenario: Venta Efectivo+Cheque+CréditoRevolvente
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 125"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 125"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar "cajilla"
    Y Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y cr
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

