# language: es
# encoding: utf-8
Característica: CREDITO REVOLVENTE + TRANSFERENCIA + EFECTIVO + OTRAS MONEDAS

##Efectivo+Transferencia+Crédito Revolvente+Otras Monedas, Refrescos GRB +Agua Embotellada, Préstamo de Envase GRB, Cobro de Envase GRB, No Vendibles, Descuento Condición Comercial
  @regresion @login @CP030 @Efectivo+Transferencia+Crédito_Revolvente+Otras_Monedas @grb2
  Escenario: CP030 Venta Efectivo+Transferencia+Crédito_Revolvente+Otras_Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=30"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Entonces liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    #Y Seleccionamos siguiente en la pantalla movimientos del dia
    #Entonces acepto la popup
