# language: es
# encoding: utf-8
Característica:  VENTAS - CREDITO REVOLVENTE + TRANSFERENCIA + OTRAS MONEDAS


  @regresion @ventas @CP137 @Credito_Revolvente+Transferencia+Otras_monedas @ruta4 @grb2
  Escenario: Venta Efectivo+Cheque+CréditoRevolvente
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 137"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=137"
    Entonces busco tipo de combo
    Entonces Se muestra el pedido del dia
    Y obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces  se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup





  @regresion @ventas @CP049 @Credito_Revolvente+Transferencia+Otras_monedas @ruta3 @grb2
  Escenario: Venta Efectivo+Cheque+CréditoRevolvente
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=49"
    Cuando Inicio sesion como usuario "valido"
    Entonces press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 49"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB y garrafon
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces  se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup