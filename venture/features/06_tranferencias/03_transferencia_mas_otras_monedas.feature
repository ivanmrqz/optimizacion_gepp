# language: es
# encoding: utf-8
Característica: VENTA TRANSFERENCIA MAS OTRAS MONEDAS


  @regresion  @CP035 @ventas @ruta1 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 35"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Entonces busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=35"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=35-1"
    Entonces busco segundo combo
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "cajilla"
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @CP038 @ruta2 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=38"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Entonces busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 38"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @ruta2 @CP042 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=42"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @CP047 @ruta1 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=47"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=47"
    Entonces busco tipo de combo
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "cajilla"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

  @regresion @ventas @CP048 @ruta2 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=48"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=48"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ventas @CP151 @ruta1 @Transferencia+Otras_monedas @grb2
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 151"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Entonces busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=151"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=151-1"
    Entonces busco segundo combo
    ##OBTENGO IMPORTE
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB y garrafon
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup






