# language: es
# encoding: utf-8
Característica: VENTA TRANSFERENCIA MAS OTRAS MONEDAS

  @regresion  @CP036 @grb0 @ventas @ruta19 @Transferencia+Otras_monedas
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 36"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 36"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion  @CP037 @grb0 @ventas @ruta19 @Transferencia+Otras_monedas
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 37"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=37"
    Entonces busco tipo de combo
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup




  @regresion @ventas @ruta19 @CP043 @Transferencia+Otras_monedas
  Escenario: venta transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=43"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 43"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup





