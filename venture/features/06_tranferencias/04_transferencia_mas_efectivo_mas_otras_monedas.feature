# language: es
# encoding: utf-8
Característica: [VENTA] - [TRANSFERENCIA + EFECTIVO + OTRAS MONEDAS]
 ##Efectivo+Transferencia+Otras Monedas, Refrescos GRB +Agua Embotellada, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, No Vendibles, Descuento Escalera, Descuento Condición Comercial
  @regresion @login @CP023 @Efectivo+Transferencia+Otras_Monedas @grb2
  Escenario: CP024 Venta Efectivo+Transferencia+Otras_Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 23"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=23"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=23-1"
    Entonces busco segundo combo
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y selecciono el descuento y valido que se aplique
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y Se muestra la pantalla movimientos del dia
    Entonces valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup






    ##Efectivo+Transferencia+Otras Monedas, Refrescos GRB +Agua Embotellada, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, No Vendibles, Descuento Escalera, Descuento Condición Comercial
  @regresion @login @CP024 @Efectivo+Transferencia+Otras_Monedas @grb2
  Escenario: CP024 Venta Efectivo+Transferencia+Otras_Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 24"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 24"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido datos de cobro de GRB
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y Se muestra la pantalla movimientos del dia
    Entonces valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup





    ##Efectivo+Transferencia+Otras Monedas, Refrescos GRB + Refrescos PET+Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Cobro de Envase GRB, Préstamo de Cajilla, Vende Envase, No Vendibles+Lista de Precio, Descuento Escalera, Descuento Condición Comercial
  @regresion @ventas @CP134 @Efectivo+Transferencia+Otras_Monedas @ruta1 @grb2
  Escenario: CP134 Venta Efectivo+Transferencia+Otras_Monedas
    Dado abro la aplicacion
  #  Entonces valido estar en la pantalla de Login
  #  Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=134"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 134"
    #Cuando Obtengo los datos de inventario desde  "dt_inventario" data pool con filtros  "id= 134"
    Cuando I goto home device
    Entonces busco ticket
   # Cuando Inicio sesion como usuario "valido"
   # Y press_enter_button
   # Y Seleccionamos siguiente
   # Cuando Selecciono menu "Servicio Cliente"
   # Entonces Se muestra la pantalla programados
   # Cuando Busco un cliente por NUD
   # Y Selecciono el submenu "VENTA"
   # Y Confirmo el mensaje emergente
  #  Entonces se muestra la auditoria de enfriadores
  #  Y selecciono el boton siguiente de la pagina de enfriadores
  #  Entonces acepto la popup
   # Entonces Se muestra el pedido del dia
   # Y busco productos y agrego productos"primer pet"
   # Y busco productos y agrego productos"segundo pet"
   # Y busco productos y agrego productos"primer grb"
   # Y busco productos y agrego productos"segundo grb"
   # Y busco productos y agrego productos"primer garrafon"
   # Y busco productos y agrego productos"segundo garrafon"
   # Y busco productos y agrego productos"primer no carbonatado"
   # Y busco productos y agrego productos"segundo no carbonatado"
   # Y busco productos y agrego productos"primer agua embotellada"
   # Y busco productos y agrego productos"segunda agua embotellada"
   # Y busco un producto no vendible
   # Entonces Se muestra el pedido del dia
    #Cuando damos clic en el boton "Paquetes"
   # Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 134"
    #Y busco tipo de combo
   # Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=134"
    #Entonces busco tipo de combo
   # Y valido datos de cobro de GRB
  #  Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=134-1"
   # Entonces busco segundo combo
   #--- Y seleccionamos siguiente en la pantalla pedido del dia
   #--- Entonces valido estar en la pantalla de descuentos
   # Y calculo descuento eventual
    #Y calculo descuento del CSV
   # Y selecciono el descuento y valido que se aplique
   # Y seleccionamos siguiente en la pantalla descuentos
   # Entonces Se muestra la pantalla de Devolucion Retornables
   # Cuando damos clic en el boton de cajilla
   # Y valido datos de cobro de GRB
   # Cuando acepto el mensaje de cobro de envase GRB
   # Y buscar cajilla para prestamo
   # Y Seleccionamos siguiente
   # Entonces valido pagos y si existen los liquido
   # Entonces se muestra la pantalla cantidad de cheques
   # Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
   # Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
   # Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
   # Y Seleccionamos siguiente en la pagina cantidad de cheques
   # Entonces se muestra la pantalla cantidad de cupones
   # Y busco la moneda "primer moneda"
    #Y busco la moneda "segunda moneda"
    #Y busco la moneda "tercer moneda"
   # Entonces Seleccionamos siguiente en la pagina cantidad de cheques
   # Y Se muestra la pantalla movimientos del dia
   # Entonces valido que el resumen de cuenta sea correcto
   # Y Seleccionamos siguiente en la pantalla movimientos del dia
   # Entonces acepto la popup
