# language: es
# encoding: utf-8

Característica: VENTAS - EFECTIVO GRB0

  @regresion @ruta19 @grb0 @ventas @CP082 @efectivo
  Escenario: CP082 Venta Efectivo+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =82"
    Cuando Inicio sesion como usuario "valido"
  #  Y press_enter_button
  #  Y Seleccionamos siguiente
  #  Cuando Selecciono menu "Servicio Cliente"
  #  Entonces Se muestra la pantalla programados
  #  Cuando Busco un cliente por NUD
  #  Y Selecciono el submenu "VENTA"
  #  Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
  #  Entonces Se muestra el pedido del dia
  #  Y busco productos y agrego productos"primer pet"
  #  Y busco productos y agrego productos"segundo pet"
  #  Y busco productos y agrego productos"primer no carbonatado"
  #  Y busco productos y agrego productos"segundo no carbonatado"
  #  Entonces Se muestra el pedido del dia
  #  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 82"
  #  Y busco tipo de combo
  #  Entonces obtenemos el importe del pedido
  #  Y seleccionamos siguiente en la pantalla pedido del dia
  #  Entonces valido pagos y si existen los liquido
  #  Entonces se muestra la pantalla cantidad de cupones
  #  Y Seleccionamos siguiente
  #  Entonces Se muestra la pantalla movimientos del dia
  #  Y valido que el resumen de cuenta sea correcto
  #  Y Seleccionamos siguiente
  #  Entonces acepto la popup
    Cuando I goto home device
    Entonces busco ticket
    Y regreso a la ruta principal


  @regresion @ruta19 @grb0 @ventas @CP083 @efectivo
  Escenario: CP083 Venta Efectivo+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =83"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
  #  Entonces se muestra la auditoria de enfriadores
  #  Y selecciono el boton siguiente de la pagina de enfriadores
  #  Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=83"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Y acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Entonces Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
    Cuando I goto home device
    Entonces busco ticket


  @regresion @ruta19 @grb0 @ventas @CP086 @efectivo
  Escenario: CP086 Venta Efectivo+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =86"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Cuando I goto home device
    Entonces busco ticket


    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=86"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Y acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Entonces Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
    Cuando I goto home device
    Entonces busco ticket

