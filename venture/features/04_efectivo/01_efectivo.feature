# language: es
# encoding: utf-8
Característica: VENTAS - EFECTIVO


  ##Efectivo, Refrescos PET +Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Cobro de Envase Garrafón, Préstamo de Envase Garrafón, Vende Envase, No Vendibles, Descuento Condición Comercial, Combo Dinámico
  @regresion @ventas @CP078 @Efectivo @ruta3@grb2
  Escenario: CP078 Venta efectivo
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=78"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=78"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup

## EFECTIVO, Refrescos GRB +Refrescos PET , Agrupado Bandera GRB 2, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, Lista de Precio Especial, Descuento Escalera, Descuento Condición Comercial / fijo
  ##CLIENTE 1120
  @regresion @ventas @CP081 @Efectivo @ruta3 @grb2
  Escenario: CP087 Venta efectivo
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=81"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Y acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup


    ## EFECTIVO, Refrescos GRB +Refrescos PET , Agrupado Bandera GRB 2, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, Lista de Precio Especial, Descuento Escalera, Descuento Condición Comercial / fijo
  ##CLIENTE 1120
  @regresion @ventas @CP087 @Efectivo @ruta3 @grb2
  Escenario: CP087 Venta efectivo
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 87"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    #Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces Se muestra el pedido del dia
    #Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 87"
    #Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    #Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y Se muestra la pantalla de Prestamos Retornables
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup




   ##Efectivo+Transferencia, Refrescos GRB +Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Cobro de Envase Garrafón, Vende Envase, Cajas Mínimas, Descuentos Eventuales, Descuento Escalera
  @regresion @login @CP129 @Efectivo+Transferencia @grb2
  Escenario: CP129 Venta Efectivo+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 129"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces Se muestra el pedido del dia
     ##OBTENGO IMPORTE
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup




  @regresion @ventas @IVAN @Efectivo @ruta3@grb2
  Escenario: CP129 Venta Efectivo+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 87"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando doy click en inventario
    Entonces obtengo el inventario Final
