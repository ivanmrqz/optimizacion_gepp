# language: es
# encoding: utf-8
Característica: GEPP - TEST POC


  @regresion @Entrega_Revolvente_01
  Escenario: Entrega_Revolvente_01
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 1"
    Cuando Inicio sesion como usuario "valido"
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD "414790"
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de pagos
    Y Realizo el pago
    Cuando Seleccionamos siguiente
    Cuando Agrego el monto del credito revolvente "301"
    Y Selecciono el motivo "EVENTUAL (7 DIAS)"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla otras monedas
    Cuando Agrego la cantidad otras modenas "2"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia

  #@regresion @Recuperación_envase_03
  Escenario: Recuperación_envase_03
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 3"
    Cuando Inicio sesion como usuario "valido"
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD "6232"
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Agrego cantidad de envases para su devolucion
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla otras monedas
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    #Cuando Seleccionamos siguiente
    #Y Confirmo el mensaje emergente


  #@regresion @Venta_envase_cajilla_04
  Escenario: Venta_envase_cajilla_04
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 5"
    Cuando Inicio sesion como usuario "valido"
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD "251465"
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos prestamo de caja
    Y Confirmo el mensaje emergente
    Entonces Se muestra la pantalla de Prestamos Retornables
    Cuando agrego el numero de cajas prestadas "1"
    Cuando Seleccionamos siguiente
    Y Confirmo el mensaje emergente
    Entonces Se muestra la pantalla otras monedas
    Cuando Agrego la cantidad otras modenas "2"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    #Cuando Seleccionamos siguiente
    #Y Confirmo el mensaje emergente


  #@regresion @login_invalido_07
  Escenario: login_invalido_07
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 5"
    Cuando Inicio sesion como usuario "valido"
    Entonces Veo Login


  @regresion @Credito_Revolvente_Venta_envase_cajilla_05
  Escenario: Credito_Revolvente_Venta_envase_cajilla_05
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 5"
    Cuando Inicio sesion como usuario "valido"
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD "7701"
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos la opcion "+ Prod."
    Cuando Agregamos cajas
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos prestamo de caja
    Y Confirmo el mensaje emergente
    Entonces Se muestra la pantalla de Prestamos Retornables
    Cuando agrego el numero de cajas prestadas "2"
    Cuando Seleccionamos siguiente
    Y Confirmo el mensaje emergente
    Cuando Agrego el monto del credito revolvente "500"
    Y Selecciono el motivo "EVENTUAL (7 DIAS)"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla otras monedas
    Cuando Agrego la cantidad otras modenas "20"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    #Cuando Seleccionamos siguiente
    #Y Confirmo el mensaje emergente


  @regresion @Pago_Nota_CreditoRevolvente_Venta_06
  Escenario: Pago_Nota_CreditoRevolvente_Venta_06
    Dado abro la aplicacion
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 6"
    Cuando Inicio sesion como usuario "valido"
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD "414790"
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos la opcion "+ Prod."
    Cuando Agregamos cajas
    Y Confirmo el mensaje emergente
    Entonces Se muestra el pedido del dia
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de pagos
    Y Realizo el pago
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando Seleccionamos prestamo de caja
    Y Confirmo el mensaje emergente
    Entonces Se muestra la pantalla de Prestamos Retornables
    Cuando agrego el numero de cajas prestadas "2"
    Cuando Seleccionamos siguiente
    Y Confirmo el mensaje emergente
    Cuando Agrego el monto del credito revolvente "500"
    Y Selecciono el motivo "EVENTUAL (7 DIAS)"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla otras monedas
    Cuando Agrego la cantidad otras modenas "20"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    #Cuando Seleccionamos siguiente
    Y Confirmo el mensaje emergente




