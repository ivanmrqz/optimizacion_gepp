# language: es
# encoding: utf-8
Característica: RECARGA INVENTARIO

  @regresion @recarga_inventario @CP061_inventario
  Escenario: Recarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Cuando Obtengo los datos de prueba desde "dt_recarga_inventario" data pool con filtros "id=61"
    Cuando Inicio sesion como verificador
    Y Seleccionamos siguiente
    #PANTALLA 2310
    Cuando valido y selecciono el boton Recarga
    Entonces valido estar en la pantalla de recarga y descarga
    Y busco y agrego el producto para inventario "1"
    Y busco y agrego el producto para inventario "2"
    Y busco y agrego el producto para inventario "3"
    Y busco y agrego el producto para inventario "4"
    Y busco y agrego el producto para inventario "5"
    Y busco y agrego el producto para inventario "6"
    Y busco y agrego el producto para inventario "7"
    Y busco y agrego el producto para inventario "8"
    Y busco y agrego el producto para inventario "9"
    Y busco y agrego el producto para inventario "10"
    Cuando  imprimo la recarga de inventario
    Y acepto la popup
    Y acepto la popup
    Entonces valido estar en la pagina de login para supervisor
    Cuando Inicio sesion como supervisor
    Y Seleccionamos siguiente en la pantalla de supervisor
    Y acepto la popup
    Entonces valido estar en la pantalla de Login
    Cuando Inicio sesion como vendedor
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton inventario
    Y valido el producto agregado en inventario "1"
    Y valido el producto agregado en inventario "2"
    Y valido el producto agregado en inventario "3"
    Y valido el producto agregado en inventario "4"
    Y valido el producto agregado en inventario "5"
    Y valido el producto agregado en inventario "6"
    Y valido el producto agregado en inventario "7"
    Y valido el producto agregado en inventario "8"
    Y valido el producto agregado en inventario "9"
    Y valido el producto agregado en inventario "10"




  @regresion @Descarga_inventario @CP062_inventario
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Cuando Obtengo los datos de prueba desde "dt_recarga_inventario" data pool con filtros "id=61"
    Cuando Inicio sesion como verificador
    Y Seleccionamos siguiente
     #PANTALLA 2310
    Cuando valido y selecciono el boton Recarga
    Entonces valido estar en la pantalla de recarga y descarga
    Y busco y descargo el producto para inventario "1"
    Y busco y descargo el producto para inventario "2"
    Y busco y descargo el producto para inventario "3"
    Y busco y descargo el producto para inventario "4"
    Y busco y descargo el producto para inventario "5"
    Y busco y descargo el producto para inventario "6"
    Y busco y descargo el producto para inventario "7"
    Y busco y descargo el producto para inventario "8"
    Y busco y descargo el producto para inventario "9"
    Y busco y descargo el producto para inventario "10"
    Cuando  imprimo la recarga de inventario
    Y acepto la popup
    Y acepto la popup
    Entonces valido estar en la pagina de login para supervisor
    Cuando Inicio sesion como supervisor
    Y Seleccionamos siguiente en la pantalla de supervisor
    Y acepto la popup
    Entonces valido estar en la pantalla de Login
    Cuando Inicio sesion como vendedor
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton inventario
    Y valido el producto descargado en inventario "1"
    Y valido el producto descargado en inventario "2"
    Y valido el producto descargado en inventario "3"
    Y valido el producto descargado en inventario "4"
    Y valido el producto descargado en inventario "5"
    Y valido el producto descargado en inventario "6"
    Y valido el producto descargado en inventario "7"
    Y valido el producto descargado en inventario "8"
    Y valido el producto descargado en inventario "9"
    Y valido el producto descargado en inventario "10"

  @regresion @recarga_inventario @CP062_inventario
  Escenario: Recarga de inventario Sin confirmación de supervisor
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Cuando Obtengo los datos de prueba desde "dt_recarga_inventario" data pool con filtros "id=61"
    Cuando Inicio sesion como verificador
    Y Seleccionamos siguiente
   #PANTALLA 2310
    Cuando valido y selecciono el boton Recarga
    Entonces valido estar en la pantalla de recarga y descarga
    Y busco y agrego el producto para inventario "1"
    Y busco y agrego el producto para inventario "2"
    Y busco y agrego el producto para inventario "3"
    Y busco y agrego el producto para inventario "4"
    Y busco y agrego el producto para inventario "5"
    Y busco y agrego el producto para inventario "6"
    Y busco y agrego el producto para inventario "7"
    Y busco y agrego el producto para inventario "8"
    Y busco y agrego el producto para inventario "9"
    Y busco y agrego el producto para inventario "10"
    Cuando  imprimo la recarga de inventario
    Y acepto la popup
    Y acepto la popup
    Entonces valido estar en la pantalla de Login
    Cuando Inicio sesion como vendedor
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton inventario
    Y valido el producto agregado en inventario "1"
    Y valido el producto agregado en inventario "2"
    Y valido el producto agregado en inventario "3"
    Y valido el producto agregado en inventario "4"
    Y valido el producto agregado en inventario "5"
    Y valido el producto agregado en inventario "6"
    Y valido el producto agregado en inventario "7"
    Y valido el producto agregado en inventario "8"
    Y valido el producto agregado en inventario "9"
    Y valido el producto agregado en inventario "10"









































