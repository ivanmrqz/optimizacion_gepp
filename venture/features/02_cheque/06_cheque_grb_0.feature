# language: es
# encoding: utf-8

Característica: VENTAS - CHEQUE GRB0

  @regresion @ventas @CP005 @Cheque @grb0 @ruta19
  Escenario: CP148 Venta Cheque+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =5"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Cuando acepto la popup
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Cuando ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup




  @regresion @ventas @CP079 @Cheque @grb0 @ruta19
  Escenario: CP148 Venta Cheque+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=79"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=79"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=79-1"
    Entonces busco segundo combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Cuando ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup


  @regresion @ventas @CP080 @Cheque @grb0 @ruta19
  Escenario: CP148 Venta Cheque+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =80"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=80"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=80-1"
    Entonces busco segundo combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar "garrafon 2"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Cuando ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques

    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup


  @regresion @ventas @CP096 @Cheque @grb0 @ruta19
  Escenario: CP148 Venta Cheque+Transferencia
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id =96"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 96"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Cuando ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques

    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
