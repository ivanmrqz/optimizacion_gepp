# language: es
# encoding: utf-8
Característica: VENTA - [CHEQUE + OTRAS MONEDAS]

  ##Cheque+Otras Monedas,
  @regresion @ventas @CP008 @Cheque+Otras_Monedas @ruta3 @grb2
  Escenario: CP008 Venta Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=8"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=8"
    Entonces busco tipo de combo
    Y seleccionamos el combo de descuentos dinamico
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Cuando damos clic en el boton de cajilla
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup


   ##Cheque+Otras Monedas, Garrafón+Agua Embotellada+Refrescos GRB, Agrupado Bandera GRB 2, Devolución de Envase GRB, Cobro de Envase Garrafón, Vende Envase, Cajas Mínimas+Lista de Precio Especial+Saldo de Envases, Combo Estático, Descuento Condición Comercial
  @regresion @ventas @CP010 @Cheque+Otras_Monedas @ruta4 @grb2
  Escenario: CP010 Venta Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 10"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup

     ##Cheque+Otras Monedas, Refrescos GRB + Refrescos PET+Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Cobro de Envase GRB, Préstamo de Cajilla, Vende Envase, Cajas Mínimas+No Vendibles, Combo Dinámico, Descuento Condición Comercial
  @regresion @ventas @CP146 @Cheque+Otras_Monedas @ruta1 @grb2
  Escenario: CP146 Venta Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 146"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=146"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar "cajilla"
    Y Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto


      ##Cheque+Otras Monedas, Refrescos GRB + Refrescos PET+Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Préstamo de Envase GRB, Cobro de Envase Garrafón, Vende Envase, No Vendibles+Lista de Precio, Descuento Escalera
  @regresion @ventas @CP147 @Cheque+Otras_Monedas @ruta2 @grb2
  Escenario: CP147 Venta Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 147"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 147"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup


