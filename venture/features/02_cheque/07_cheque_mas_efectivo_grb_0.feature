# language: es
# encoding: utf-8
Característica: VENTA - EFECTIVO + CHEQUE

   ##Efectivo+Cheque, Refrescos GRB, Agrupado Bandera GRB 2, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, No Vendibles, Descuento Escalera, Combo Estático
  @regresion @ventas @grb0 @CP116 @Cheque+Efectivo @ruta19
  Escenario: CP116 Venta Efectivo+Cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 116"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces  valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    Cuando I goto home device
    Entonces busco ticket

     ##Efectivo+Cheque, Refrescos GRB, Agrupado Bandera GRB 2, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, No Vendibles, Descuento Escalera, Combo Estático
  @regresion @ventas @grb0 @CP117 @Cheque+Efectivo @ruta19
  Escenario: CP117 Venta Efectivo+Cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 117"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=117"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=117-1"
    Entonces busco segundo combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces  valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    Cuando I goto home device
    Entonces busco ticket


  @regresion @ventas @grb0 @CP122 @Cheque+Efectivo @ruta19
  Escenario: CP122 Venta Efectivo+Cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 122"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 122"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y seleccionamos siguiente en la pantalla descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces  valido que el resumen de cuenta sea correcto
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    Cuando I goto home device
    Entonces busco ticket
