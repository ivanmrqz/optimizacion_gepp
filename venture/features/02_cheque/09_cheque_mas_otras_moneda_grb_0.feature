# language: es
# encoding: utf-8
Característica: VENTA - CHEQUE + OTRAS MONEDAS GRB0

  ##Cheque+Otras Monedas,
  @regresion @ventas @CP009 @Cheque+Otras_Monedas @ruta19 @grb0
  Escenario: CP008 Venta Cheque+Otras Monedas
  Dado abro la aplicacion
  Entonces valido estar en la pantalla de Login
  Entonces valido atributos
  Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 9"
  Cuando Inicio sesion como usuario "valido"
  Y press_enter_button
  Y Seleccionamos siguiente
  Cuando Selecciono menu "Servicio Cliente"
  Entonces Se muestra la pantalla programados
  Cuando Busco un cliente por NUD
  Y Selecciono el submenu "VENTA"
  Y Confirmo el mensaje emergente
  Entonces se muestra la auditoria de enfriadores
  Y selecciono el boton siguiente de la pagina de enfriadores
  Entonces acepto la popup
  Entonces Se muestra el pedido del dia
  Y busco productos y agrego productos"primer pet"
  Y busco productos y agrego productos"segundo pet"
  Y busco productos y agrego productos"primer agua embotellada"
  Y busco productos y agrego productos"segunda agua embotellada"
  Y busco productos y agrego productos"primer garrafon"
  Y busco productos y agrego productos"segundo garrafon"
  Y busco productos y agrego productos"primer no carbonatado"
  Y busco productos y agrego productos"segundo no carbonatado"
  Entonces Se muestra el pedido del dia
  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=9"
  Entonces busco tipo de combo
  Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=9-1"
  Entonces busco segundo combo
  Entonces obtenemos el importe del pedido
  Y seleccionamos siguiente en la pantalla pedido del dia
  Entonces valido estar en la pantalla de descuentos
  Y selecciono el descuento y valido que se aplique
  Y seleccionamos siguiente en la pantalla descuentos
  Entonces Se muestra la pantalla de Devolucion Retornables
  Cuando Seleccionamos siguiente en la pantalla devolucion de retornables
  Entonces valido pagos y si existen los liquido
  Entonces se muestra la pantalla cantidad de cheques
  Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
  Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
  Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
  Y Seleccionamos siguiente en la pagina cantidad de cheques
  Entonces se muestra la pantalla cantidad de cupones
  Y busco la moneda "primer moneda"
  Y busco la moneda "segunda moneda"
  Y busco la moneda "tercer moneda"
  Cuando Seleccionamos siguiente en la pantalla cupones
  Entonces Se muestra la pantalla movimientos del dia
  Y valido que el resumen de cuenta sea correcto


  ##Cheque+Otras Monedas,
  @regresion @ventas @CP012 @Cheque+Otras_Monedas @ruta19 @grb0
  Escenario: CP012 Venta Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 12"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y buscar "GRB"
    Y buscar el grb 2
    Y buscar "garrafon"
    Y buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
