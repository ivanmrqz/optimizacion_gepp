# language: es
# encoding: utf-8
Característica: [VENTA] - [EFECTIVO + CHEQUE]

   ##Efectivo+Cheque, Refrescos GRB, Agrupado Bandera GRB 2, Cobro de Cajilla, Cobro de Envase GRB, Vende Envase, No Vendibles, Descuento Escalera, Combo Estático
  @regresion @ventas @CP121 @Cheque+Efectivo @ruta1 @grb2
  Escenario: CP121 Venta Efectivo+Cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 121"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=121"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=121-1"
    Entonces busco segundo combo
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces  valido que el resumen de cuenta sea correcto


    ##Efectivo+Cheque, Refrescos GRB + Refrescos PET+Garrafón+No Carbonatados+Agua Embotellada, Agrupado Bandera GRB 2, Cobro de Envase GRB, Préstamo de Cajilla, Vende Envase,No Vendibles+Lista de Precio, Combo Estático, Descuento Condición Comercial
  @regresion @ventas @CP144 @Cheque+Efectivo @ruta3 @grb2
  Escenario: CP144 Efectivo+Cheque
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 144"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco un producto no vendible
    Entonces Se muestra el pedido del dia
    Cuando damos clic en el boton "Paquetes"
    Y damos clic en el combo "Estático"
    Y busco combo estatico y realizo calculo
    Y dar clic en la palomita para confirmar combo
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 144"
    Y busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar "cajilla"
    Y Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces  valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup

