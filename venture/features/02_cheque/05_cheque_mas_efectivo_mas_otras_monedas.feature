# language: es
# encoding: utf-8
Característica: [CARACTERISTICA] - [CINTILLO EN MAYUS]



  ##Efectivo+Cheque+Otras Monedas, Refrescos GRB +Refrescos PET, Agrupado Bandera GRB 2, Cobro de Envase GRB, Préstamo de Envase GRB, Vende Envase, Lista de Precio Especial, Descuentos Eventuales, Combo Dinámico
  @regresion @ventas @CP100 @Efectivo+Cheque+Otras_Monedas @ruta3 @grb2
  Escenario: CP100 Venta Efectivo+Cheque+Otras Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id= 100"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=100"
    Entonces busco tipo de combo
    Entonces obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup



  ##Efectivo+Cheque+Otras Monedas, Garrafón+Agua Embotellada, Agrupado Bandera GRB 2, Préstamo de Envase Garrafón, Cobro de Envase Garrafón, Vende Envase, Cajas Mínimas+Lista de Precio Especial, Descuento Condición Comercial, Combo Estático
  @regresion @ventas @CP109 @Cheque+Efectivo+Otras_Monedas @ruta4 @grb2
  Escenario: CP109 Venta Efectivo+Cheque+Otras_Monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id = 109"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   # Entonces se muestra la auditoria de enfriadores
   # Y selecciono el boton siguiente de la pagina de enfriadores
   # Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 109"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup