class TicketPedidoDelDia
  attr_accessor :importeDeVenta, :descuento, :netoAPagar, :totalDeAbonos


  def initialize(importeDeVenta, descuento, netoAPagar, totalDeAbonos)
    @importeDeVenta = importeDeVenta
    @descuento = descuento
    @netoAPagar = netoAPagar
    @totalDeAbonos = totalDeAbonos
  end


end