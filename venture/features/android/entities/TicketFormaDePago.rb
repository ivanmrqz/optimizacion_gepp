class TicketFormaDePago
  attr_accessor :notaDePrestamo, :efectivo, :totalMovimientoDeDia, :totalCupones, :totalCheques


  def initialize(notaDePrestamo, efectivo, totalMovimientoDeDia, totalCupones, totalCheques)
    @notaDePrestamo = notaDePrestamo
    @efectivo = efectivo
    @totalMovimientoDeDia = totalMovimientoDeDia
    @totalCupones = totalCupones
    @totalCheques = totalCheques

  end
end