# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class ChequesPage< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_cheques'
  @@txt_cheques= get_or_data(@or_file, 'txt_cheques')
  @@importe = get_or_data(@or_file, 'importe')
  @@txt_importe_cuenta=get_or_data(@or_file, 'txt_importe_cuenta')
  @@txt_numero_de_cheque=get_or_data(@or_file, 'txt_numero_de_cheque')
  @@txt_monto=get_or_data(@or_file, 'txt_monto')
  @@btn_siguiente_cheque=get_or_data(@or_file, 'btn_siguiente_cheque')
  @@monto_cheque=get_or_data(@or_file, 'monto_cheque')
  @@descripcionEnvase = get_or_data(@or_file, 'descripcionEnvase')
  @@ent = get_or_data(@or_file, 'ent')
  @@dev = get_or_data(@or_file, 'dev')
  @@sdo = get_or_data(@or_file, 'sdo')
  @@rec = get_or_data(@or_file, 'rec')
  @@inv = get_or_data(@or_file, 'inv')
  @@elementoCajilla = get_or_data(@or_file, 'elementoCajilla')
  @mensaje = get_or_data(@or_file, 'mensaje')
  @@lbl_envase_a_buscar = get_or_data(@or_file, 'lbl_envase_a_buscar')
  @@txt_data_envase= get_or_data(@or_file, 'txt_data_envase')
  @@nombre_empresa= get_or_data(@or_file, 'nombre_empresa')
  @@descripcionCajilla= get_or_data(@or_file, 'descripcionCajilla')



  #############################################################################
  # METODOS
  #############################################################################


  def realiza_pagos_parciales_y_otras_monedas(empresa,moneda,cuenta,cheque)

    monto=query(@@monto_cheque[:locator], :text)
    sleep 1
    r_monto=monto[1..-1].map{|r_importe| r_importe.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
    puts "el monto disponible es"
    puts r_monto
    r_empresa_2=empresa.sub"-",""
    nombre_empresa=query(@@nombre_empresa[:locator],:text)
    nombre_empresa=nombre_empresa[1..-1]
    #se hace la validacion si estan vacios los campos en el csv para la empresa 2
    if empresa != nil
      regex=Regexp.new(r_empresa_2)
      r_nombre_empresa=nombre_empresa.find{|e|regex=~e}
      posicion_empresa= nombre_empresa.index(r_nombre_empresa)

      puts "el monto es ",r_monto[posicion_empresa]
      puts
      puts "la moneda es",moneda
      puts

      resta=r_monto[posicion_empresa].to_f-moneda
      puts"la resta es", resta

      touch("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{posicion_empresa}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{posicion_empresa}",cuenta)
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{posicion_empresa}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{posicion_empresa}",cheque)
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{posicion_empresa}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{posicion_empresa}",resta)
      press_user_action_button('search')
   end

  end

  def realiza_pagos_parciales_otras_monedas_y_efectivo(empresa,moneda)
    monto=query(@@monto_cheque[:locator], :text)
    sleep 1
    r_monto=monto[1..-1].map{|r_importe| r_importe.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
    puts "el monto disponible es"
    puts r_monto
    r_empresa_1=empresa.sub"-",""
    nombre_empresa=query(@@nombre_empresa[:locator],:text)
    nombre_empresa=nombre_empresa[1..-1]

    regex=Regexp.new(r_empresa_1)
    r_nombre_empresa=nombre_empresa.find{|e|regex=~e}
    posicion_empresa= nombre_empresa.index(r_nombre_empresa)

    puts "el monto es ",r_monto[posicion_empresa]
    puts
    puts "la moneda es",moneda
    puts

    resta=r_monto[posicion_empresa].to_f/2-moneda
    puts"la resta es", resta
    touch("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{posicion_empresa}")
    enter_text("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{posicion_empresa}",$dt_row[:numero_cuenta])
    press_user_action_button('search')

    touch("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{posicion_empresa}")
    enter_text("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{posicion_empresa}",$dt_row[:numero_cheque])
    press_user_action_button('search')

    touch("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{posicion_empresa}")
    enter_text("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{posicion_empresa}",resta)
    press_user_action_button('search')


  end






  #############################################################################
  # ACCIONES
  #############################################################################



  def send_datos_de_cheque
    monto=query(@@monto_cheque[:locator], :text)
    r_monto=monto[1..-1].map{|r_monto| r_monto.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
    puts "el monto es"
    puts r_monto
    $r_monto=r_monto.each.map(&:to_f)
    puts "el monto es "
    puts $r_monto
    index_monto=r_monto.length
    sleep 2
    i=0
    while i<index_monto do

      touch("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{i}",$dt_row[:numero_cuenta])
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{i}",$dt_row[:numero_cheque])
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{i}",$r_monto[i])
      press_user_action_button('search')

      i+=1
    end

  end

  def sed_datos_cheque_pago_parcial
    monto=query(@@monto_cheque[:locator], :text)
    r_monto=monto[1..-1].map{|r_monto| r_monto.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
    puts "el monto es"
    puts r_monto
    $r_monto=r_monto.each.map(&:to_f)
    puts "el monto es "
    puts $r_monto
    index_monto=r_monto.length

    sleep 2
    i=0
    while i<index_monto do

      touch("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{i}",$dt_row[:numero_cuenta])
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{i}",$dt_row[:numero_cheque])
      press_user_action_button('search')

      touch("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{i}",$r_monto[i].to_f/2)
      press_user_action_button('search')

      i+=1
      end


  end

  def pago_parcial_cheque_y_otras_monedas_empresa_1

    if  $empresa_1== nil
      puts
    elsif $empresa_1==$empresa_2 and $empresa_1==$empresa_3
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      suma_moneda=$moneda_1+$moneda_2+$moneda_3
      puts suma_moneda
      realiza_pagos_parciales_y_otras_monedas($empresa_1,suma_moneda,cuenta,cheque)
    elsif $empresa_1==$empresa_2 and $empresa_1!=$empresa_3
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      suma_moneda=$moneda_1+$moneda_2
      realiza_pagos_parciales_y_otras_monedas($empresa_1,suma_moneda,cuenta,cheque)
    elsif $empresa_2==$empresa_3 and $empresa_2 != $empresa_1
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      suma_moneda=$moneda_2+$moneda_3
      realiza_pagos_parciales_y_otras_monedas($empresa_2,suma_moneda,cuenta,cheque)
    elsif $empresa_1==$empresa_3 and $empresa_1 != $empresa_2
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      suma_moneda=$moneda_1+$moneda_3
      realiza_pagos_parciales_y_otras_monedas($empresa_1,suma_moneda,cuenta,cheque)

    else
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      realiza_pagos_parciales_y_otras_monedas($empresa_1,$moneda_1,cuenta,cheque)

    end
    else
    puts "no se agrego cuenta de cheque"

  end

  def pago_parcial_cheque_y_otras_monedas_empresa_2
    sleep 1
    x="(//*[contains(@text,'"+$empresa_2+"')]/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@resource-id,'cuenta_edittext')])[1]"
    dt_x=(get_mobile_element_attribute x,"text").to_i
    if $empresa_2== nil
      elsif dt_x.to_i>0
      else
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      realiza_pagos_parciales_y_otras_monedas($empresa_2,$moneda_2,cuenta,cheque)
    end

  end


  def pago_parcial_cheque_y_otras_monedas_empresa_3
    sleep 1
    x="(//*[contains(@text,'"+$empresa_3+"')]/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@resource-id,'cuenta_edittext')])[1]"
    dt_x=(get_mobile_element_attribute x,"text").to_i
    puts dt_x
    if $empresa_3==nil
      elsif dt_x.>0
      else
      cuenta=$dt_row[:numero_cuenta]
      cheque=$dt_row[:numero_cheque]
      realiza_pagos_parciales_y_otras_monedas($empresa_3,$moneda_3,cuenta,cheque)
    end
  end


  def pago_parcial_cheque_otras_monedas_efectivo_empresa_1
   if $empresa_1==nil
     puts
   elsif $empresa_1==$empresa_2 and $empresa_1==$empresa_3
     suma_moneda=$moneda_1+$moneda_2+$moneda_3
     puts suma_moneda
     realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_1,suma_moneda )
   elsif $empresa_1==$empresa_2 and $empresa_1!=$empresa_3
     suma_moneda=$moneda_1+$moneda_2
     realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_1,suma_moneda )
   elsif $empresa_2==$empresa_3 and $empresa_2 != $empresa_1
     suma_moneda=$moneda_2+$moneda_3
     realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_1,suma_moneda )
   elsif $empresa_1==$empresa_3 and $empresa_1 != $empresa_2
     suma_moneda=$moneda_1+$moneda_3
     realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_1,suma_moneda )

   else
     realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_1,$moneda_1)
   end
  end

  def pago_parcial_cheque_otras_monedas_efectivo_empresa_2
    sleep 1
    x="(//*[contains(@text,'"+$empresa_2+"')]/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@resource-id,'cuenta_edittext')])[1]"
    dt_x=(get_mobile_element_attribute x,"text").to_i
    if $empresa_2==nil
      puts
    elsif dt_x.>0
    else
      realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_2,$moneda_2)
    end
  end

  def pago_parcial_cheque_otras_monedas_efectivo_empresa_3
    sleep 1
    x="(//*[contains(@text,'"+$empresa_3+"')]/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@resource-id,'cuenta_edittext')])[1]"
    dt_x=(get_mobile_element_attribute x,"text").to_i
    if $empresa_3==nil
      puts "no se agregan monedas"
    elsif dt_x.>0
    else
      puts "si se agregan monedas"
      realiza_pagos_parciales_otras_monedas_y_efectivo($empresa_3,$moneda_3)
    end


  end


  def pago_parcial_cheque_y_cr
    monto=query(@@monto_cheque[:locator], :text)
    sleep 1
    r_monto=monto[1..-1].map{|r_importe| r_importe.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
    puts "el monto es"
    puts r_monto
    $r_monto=r_monto.each.map(&:to_f)
    index_monto=r_monto.length
    r_saldo_cheque=$saldo_cheque/index_monto
    sleep 2
    i=0
    while i<index_monto do
      $cuenta =("android.widget.EditText id:'cheque_captura_row_3045_cuenta_edittext' index:#{i}")
      $cheque= ("android.widget.EditText id:'cheque_captura_row_3045_cheque_edittext' index:#{i}")
      $monto=("android.widget.EditText id:'cheque_captura_row_3045_monto_edittext' index:#{i}")
      touch($cuenta )
      enter_text($cuenta,$dt_row[:numero_cuenta])
      press_user_action_button('search')

      touch($cheque)
      enter_text($cheque,$dt_row[:numero_cheque])
      press_user_action_button('search')

      touch($monto)
      enter_text($monto,$r_monto[i]-r_saldo_cheque)
      press_user_action_button('search')


      i+=1
    end
  end



  def touch_btn_siguiente_cheque
    sleep 2
    or_exec_uielement(@@btn_siguiente_cheque)
  rescue Exception => e
    raise("seleccionar_siguiente_cheque => No se pudo dar tap en siguiente")
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def   assert_pagina_cheques_displayed?
    assert_for_element_exist(@@txt_cheques[:locator], 10,
                             "Error => no se cargo la pantalla 'cantidad cheques'")
  end


  def obtener_importe


    $DATOS.sort_by! { |h | h[:inventario_final] }
    $DATOS.uniq! {|e| e[:sku] }
     $DTH_INVENTARIO = {
        :id=>nil,
        :sku=> nil,
        :inventario_inicial=> nil,
        :inventario_final => nil,
        :monto_combo1=>nil,
        :monto_combo2=>nil,
        :descuento_total =>nil,
        :importe => nil,
        :efectivo=>nil,
        :cheques=>nil,
        :cupones=>nil,
        :pagos_cr=>nil,
        :monto_cr=>nil,
        :neto => nil,
        :nombre_ticket=>nil,
        :devolucion_envase_grb=>nil,
        :devolucion_garrafon=>nil,
        :prestamo_envase_grb=>nil,
        :prestamo_envase_garrafon=>nil,
        :cobro_envase_grb=>nil,
        :cobro_envase_garrafon=>nil,
        :cobro_envase_grb_y_garrafon=>nil,
        :cobro_cajilla=>nil,
    }
    $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
    $DTH_INVENTARIO[:sku] =nil
    $DTH_INVENTARIO[:inventario_inicial]=nil
    $DTH_INVENTARIO[:inventario_final] =nil
    #PUSHEAR PRODUCTO A DATA
    $DATOS.push($DTH_INVENTARIO)


    #VAlIDO QUE EL IMPORTE ESTA CORRECTO
    primer_precio=$dt_row[:precio_1].to_f*$dt_row[:cantidad_cajas_1].to_f
    segundo_precio=$dt_row[:precio_2].to_f*$dt_row[:cantidad_cajas_1].to_f
    tercer_precio=$dt_row[:precio_3].to_f*$dt_row[:caja_refresco_grb_1].to_f
    cuarto_precio=$dt_row[:precio_4].to_f*$dt_row[:caja_refresco_grb_2].to_f
    quinto_precio=$dt_row[:precio_5].to_f*$dt_row[:caja_garrafon_1].to_f
    sexto_precio=$dt_row[:precio_6].to_f*$dt_row[:caja_garrafon_2].to_f
    septimo_precio=$dt_row[:precio_7].to_f*$dt_row[:cantidad_cajas_1].to_f
    octavo_precio=$dt_row[:precio_8].to_f*$dt_row[:cantidad_cajas_1].to_f
    noveno_precio=$dt_row[:precio_9].to_f*$dt_row[:cantidad_cajas_1].to_f
    decimo_precio=$dt_row[:precio_10].to_f*$dt_row[:cantidad_cajas_1].to_f


    importe_calculado=primer_precio+segundo_precio+tercer_precio+cuarto_precio+quinto_precio+sexto_precio+septimo_precio+octavo_precio+noveno_precio+decimo_precio
    importe_calculado=importe_calculado+$monto_total_combo_dinamico.to_f+$totalAcumulado.to_f+$restaMonto.to_f
    importe = query(@@importe[:locator],:text).map{|importe| importe.delete('$')}
    r_importe=importe.map{|r_importe| r_importe.delete(',')}
    $importe=r_importe[0].to_f
    puts "El importe calculado es: "
    puts importe_calculado.round(2)
    puts"el importe de la handheld es",$importe
      if importe_calculado.round(2) ==$importe
        puts "el importe es correcto"
      else
       puts("Error=> El importe no es correcto ")
      end
    $DTH_INVENTARIO[:importe] = $importe.to_s.strip
    #OBTENGO LA DATA DEL CSV Y PARA EL CASO DE LAS MONEDAS LAS MULTIPLICO POR SU VALOR
    $moneda_1=$dt_row[:cantidad_moneda_1].to_f*$dt_row[:valor_moneda_1].to_f
    $moneda_2=$dt_row[:cantidad_moneda_2].to_f*$dt_row[:valor_moneda_2].to_f
    $moneda_3=$dt_row[:cantidad_moneda_3].to_f*$dt_row[:valor_moneda_3].to_f
    $empresa_1=$dt_row[:nombre_empresa_1_m]
    $empresa_2=$dt_row[:nombre_empresa_2_m]
    $empresa_3=$dt_row[:nombre_empresa_3_m]
    #REDONDEO LAS MONEDAS
    r_moneda_1=$moneda_1.round(2)
    r_moneda_2=$moneda_2.round(2)
    r_moneda_3=$moneda_3.round(2)



    $Total_monedas=r_moneda_1+r_moneda_2+r_moneda_3
    puts"el total de monedas es ",$Total_monedas

    $new_importe=$importe-$Total_monedas

    if $df_row_revisita == nil
      puts
    else
      importe_revisitas=$dt_row_revisita[:precio_sku_nueva_venta].to_f*$dt_row_revisita[:precio_sku_nueva_venta]
      importe_rp=$dt_row_revisita[:precio_sku_rechazo_parcial].to_f
      $new_importe=$importe-$Total_monedas+importe_revisitas+importe_rp
    end
    x=$new_importe/3
    $saldo_cr=x.round(2)
    $saldo_cheque=x.round(2)


    if $importe<$Total_monedas
      raise("Error => se agregaron demasiadas monedas, el importe es menor")
    end
    #PUSHEAR PRODUCTO A DATA

  end





  def buscar_retornable(retornable)
    retornable = retornable.to_s #GRB, GARRAFON Y CAJILLA
    tipo_retornable = case retornable
                        when "GRB"
                          nombreRetornable = $dt_row[:nombre_envase_grb_1]
                          caja_retornable=$dt_row[:devolucion_grb_1]
                        when "GRB 2"
                          nombreRetornable = $dt_row[:nombre_envase_grb_2]
                          caja_retornable=$dt_row[:devolucion_grb_2]
                        when "garrafon"
                          nombreRetornable = $dt_row[:nombre_envase_garrafon_1]
                          caja_retornable=$dt_row[:devolucion_garrafon_1]
                        when "garrafon 2"
                          nombreRetornable = $dt_row[:nombre_envase_garrafon_2]
                          caja_retornable=$dt_row[:devolucion_garrafon_2]
                        when "cajilla"
                          nombreRetornable = $dt_row[:nombre_cajilla]
                          caja_retornable=$dt_row[:devolucion_cajilla]


                      end
    if nombreRetornable.nil?
      raise ("No se encontró el retornable en el csv")
    end
    if caja_retornable.nil?
      raise("NO se encontró la cantidad del retornable en el csv")
    end
    x="//*[contains(@text,'"+nombreRetornable+"')]/following-sibling::*[@class='android.widget.EditText']"
    #tap_element(x)
    scroll_down_to_uielement(x,
                             10,
                             'mate'
    )
   # assert_for_mobile_element(x,15,"Error=>No se encontro el retornable")

    enter_text_uielement(x,
                         caja_retornable,
                         'mate')
    sleep 5
    press_user_action_button('search')

  end

 def buscar_grb_2

    nombreRetornableCSV = $dt_row[:nombre_envase_grb_2]
    cantidadRetornableCSV = $dt_row[:devolucion_grb_2]
    if nombreRetornableCSV.nil?
      raise ("No se encontró el retornable en el csv")
    end
    if cantidadRetornableCSV.nil?
      raise("No se encontró la cantidad del retornable en el csv")
    end

      x="//*[contains(@text,'"+nombreRetornableCSV+"')]/following-sibling::*[@class='android.widget.EditText']"
      #tap_element(x)
      scroll_down_to_uielement(x,
                               10,
                               'mate')

    enter_text_uielement(x,
                         cantidadRetornableCSV,
                         'mate')

      sleep 1
      press_user_action_button('search')
 end


  def buscar_garrafon_2
    nombreRetornableGarrafonCSV = $dt_row[:nombre_envase_garrafon_2]
    cantidadRetornableGarrafonCSV = $dt_row[:devolucion_garrafon_2]
    if nombreRetornableGarrafonCSV.nil?
      raise ("No se encontró el retornable en el csv")
    end
    if cantidadRetornableGarrafonCSV.nil?
      raise("No se encontró la cantidad del retornable en el csv")
    end
      x="//*[contains(@text,'"+nombreRetornableGarrafonCSV+"')]/following-sibling::*[@class='android.widget.EditText']"
      #tap_element(x)
      scroll_down_to_uielement(x,
                               10,
                               'mate')
     # assert_for_mobile_element(x,15,"Error=>No se encontro el garrafon")
      enter_text_uielement(x,
                           cantidadRetornableGarrafonCSV,
                           'mate')
      sleep 1
      press_user_action_button('search')
  end













  #####################################################################################################
## METODO QUE AGREGA EN INPUT DEVOLUCION CUANDO SE TIENE SALDO DE UN PRODUCTO QUE NO SE ESTÁ COMPRANDO
  #####################################################################################################

  def ingresar_envases_saldo

  #  data_nombre_retornable=query(@@'data_nombre_retornable'[:locator],:text)
  #  data_entregado=query(@@data_entregado[:locator],:text).each.map(&:to_i)
  #  data_devolucion= $dt_row[:devolucion_envase_1].to_s
  #  array_envases=data_nombre_retornable.zip(data_entregado)


    sdo = Array.new
    dev = Array.new

    sdo = query(@@sdo[:locator], :text)
    dev = query(@@dev[:locator])

 #   nombreEnvase = query("android.widget.TextView id:'retornable_row_edit_0000_nombre_textview'  {text CONTAINS '#{retornable}'}",:text  )
 #   while nombreEnvase.empty?
  #    scroll_down
   #   nombreEnvase = query("android.widget.TextView id:'retornable_row_edit_0000_nombre_textview'  {text CONTAINS '#{retornable}'}",:text  )
    #  assert_for_element_exist(nombreEnvase,100)
   # end


    sdo.each do |itemSaldoMayorACero|
      if itemSaldoMayorACero.to_i > 0
        posicionSaldo = sdo.index(itemSaldoMayorACero)
        puts "El valor de posicionSaldo es: ", posicionSaldo

        itemTemporalDev = dev[posicionSaldo]

        devTemporal = Array.new
        devTemporal = query(@@dev[:locator], :text)

        puts "El valor de devTemporal es: ", devTemporal

        if itemTemporalDev.empty?
        cantidadAIngresar = itemSaldoMayorACero.to_i
          touch(itemTemporalDev)
          keyboard_enter_text(cantidadAIngresar)
          adb_keys("KEYCODE_BACK")
        end

      end



    end

#    posicionDescripcionEnvase= descripcionEnvases.index(retornable)
 #   itemDev = dev[posicionDescripcionEnvase]
  #  touch(itemDev)
   # ingresar_texto(posicionInput, "56")
   # keyboard_enter_text($dt_row[:devolucion_envase_1].to_s)
  end

  #####################################################################################################

  def adb_keys(key, id_device = nil)

    #device = id_device.nil? ? $device : id_device
    #system("adb -s #{device} shell input keyevent #{key}")

    require 'open3'

    if !ENV['DEVICE'].nil?
      #si tenemos la 'variable de entorno DEVICE ' entonces ejecutaremos ADB SHELL desde consola
      #   ESTO EN CASO DE NO ESTAR USANDO CALABASH
      device = id_device.nil? ? $device : id_device
      adb_cmd = "adb -s #{device} shell input keyevent #{key}"

    elsif !default_device.nil?
      #si default device no es nulo, entonces se esta usando 'CALABASH'
      adb_cmd = "#{default_device.adb_command} shell input keyevent #{key}"

    else
      raise "Se necesita definir la VARIABLE DE ENTORNO 'DEVICE'. O ejecutar desde 'CALABASH CONSOLE'"

    end

    #puts "adb_cmd => #{adb_cmd}"
    stdout, stderr, status = Open3.capture3(adb_cmd)

    unless status.success?
      raise "Adb failed: #{adb_cmd} Returned #{stdout} :: #{stderr}"
    end

    [stdout, stderr, status]

  end




 # def buscar_cajilla(cajilla)
  #  sleep 10
   # descripcionCajilla = Array.new
    #elementoCajilla = Array.new
    #descripcionCajilla = query(@@descripcionEnvase[:locator], :text)
    #elementoCajilla = query(@@elementoCajilla[:locator])
    #cantidadCajilla = $dt_row[:cantidad_prestamo_cajilla_1]

#    descripcionCajilla.each do |itemCajilla|
 #     if itemCajilla.to_s.include? cajilla.to_s
  #      textoCajilla = descripcionCajilla.index(itemCajilla)
   #     puts "la posicion de la cajilla es: ", textoCajilla

    #    indexElementoCajilla = elementoCajilla[textoCajilla]
     #   sleep(2)
      #  touch(indexElementoCajilla)
       # sleep(2)
        #keyboard_enter_text(cantidadCajilla)
        #sleep(2)
        #press_user_action_button('search')


    #    break
   #   end

  #  end

 # end


  def buscar_cajilla_prestamo
    cantidadCajilla = $dt_row[:cantidad_prestamo_cajilla_1]
    sleep 5
    nombreCajila = $dt_row[:nombre_prestamo_cajilla]
    descripcionCajilla = Array.new
    elementoCajilla = Array.new
    descripcionCajilla = query(@@descripcionCajilla[:locator], :text)

    puts "Nombre de la cajila en csv es: ", nombreCajila
    elementoCajilla = query(@@elementoCajilla[:locator])


    descripcionCajilla.each do |itemCajilla|
      if itemCajilla.to_s.include? nombreCajila.to_s
        textoCajilla = descripcionCajilla.index(itemCajilla)
        puts "la posicion de la cajilla es: ", textoCajilla

        indexElementoCajilla = elementoCajilla[textoCajilla]
        sleep(2)
        touch(indexElementoCajilla)
        sleep(3 )
        keyboard_enter_text(cantidadCajilla)
        sleep(2)
        press_user_action_button('search')

        break
      end

    end

  end


end


