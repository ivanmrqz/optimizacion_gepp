# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class CombosPage< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_combos'
  @@btn_paquetes= get_or_data(@or_file, 'btn_paquetes')
  @@btn_cajilla= get_or_data(@or_file, 'btn_cajilla')
  @@btn_combo= get_or_data(@or_file, 'btn_combo')
  @@btn_combo_escalera= get_or_data(@or_file, 'btn_combo_escalera')
  @@btn_combo_dinamico= get_or_data(@or_file, 'btn_combo_dinamico')
  @@btn_opcion_combo= get_or_data(@or_file, 'btn_opcion_combo')
  @@lbl_producto= get_or_data(@or_file, 'lbl_producto')
  @@txt_cantidad= get_or_data(@or_file, 'txt_cantidad')
  @@txt_nivel= get_or_data(@or_file, 'txt_nivel')
  @@txt_minimo= get_or_data(@or_file, 'txt_minimo')
  @@txt_maximo= get_or_data(@or_file, 'txt_maximo')
  @@txt_descuento= get_or_data(@or_file, 'txt_descuento')
  @@txt_total= get_or_data(@or_file, 'txt_total')
  @@txt_cDescuento= get_or_data(@or_file, 'txt_cDescuento')
  @@btn_confirmar_combo= get_or_data(@or_file, 'btn_confirmar_combo')
  @@lbl_max= get_or_data(@or_file, 'lbl_max')
  @@txt_requerido= get_or_data(@or_file, 'txt_requerido')
  @@combosDinamicos= get_or_data(@or_file, 'combosDinamicos')
  @@txt_maximo_dinamico= get_or_data(@or_file, 'txt_maximo_dinamico')
  @@txt_subtotal= get_or_data(@or_file, 'txt_subtotal')
  @@btn_tipo_combo_escalera= get_or_data(@or_file, 'btn_tipo_combo_escalera')
  @@btn_nombre_combo_escalera= get_or_data(@or_file, 'btn_nombre_combo_escalera')
  @@check_nombre_combo_escalera= get_or_data(@or_file, 'check_nombre_combo_escalera')
  @@txt_skus_combo_escalera= get_or_data(@or_file, 'txt_skus_combo_escalera')
  @@input_cantidad_sku_combo_escalera= get_or_data(@or_file, 'input_cantidad_sku_combo_escalera')
  @@rd_btn_combo_dinamico= get_or_data(@or_file, 'rd_btn_combo_dinamico')
  @@txt_elemento_combo= get_or_data(@or_file, 'txt_elemento_combo')
  @@btn_combo_dinamico_a_buscar= get_or_data(@or_file, 'btn_combo_dinamico_a_buscar')
  @@lbl_cantmax_dinamico= get_or_data(@or_file, 'lbl_cantmax_dinamico')
  @@txt_input_cantmax_dinamico=get_or_data(@or_file, 'txt_input_cantmax_dinamico')
  @@txt_cajas= get_or_data(@or_file, 'txt_cajas')
  @@dto_total_dinamico= get_or_data(@or_file, 'dto_total_dinamico')
#  @@btn_check_descuento= get_or_data(@or_file, 'btn_check_descuento')
  @@txt_descripcion_descuento= get_or_data(@or_file, 'txt_descripcion_descuento')
  @@txt_cantidad_minima_descuento= get_or_data(@or_file, 'txt_cantidad_minima_descuento')
  @@txt_cantidad_maxima_descuento= get_or_data(@or_file, 'txt_cantidad_maxima_descuento')
  @@txt_tipo_de_descuento= get_or_data(@or_file, 'txt_tipo_de_descuento')
  @@txt_descuento_valor_descuento= get_or_data(@or_file, 'txt_descuento_valor_descuento')
  @@importeTotalDispositivo= get_or_data(@or_file, 'importeTotalDispositivo')
  @@btn_check_descuento= get_or_data(@or_file, 'btn_check_descuento')
  @@txt_descripcion_producto_aplicable= get_or_data(@or_file, 'txt_descripcion_producto_aplicable')
  @@txt_disponible_producto_aplicable= get_or_data(@or_file, 'txt_disponible_producto_aplicable')
  @@txt_cantidad_producto_aplicable= get_or_data(@or_file, 'txt_cantidad_producto_aplicable')
  @@txt_importe_producto_aplicable= get_or_data(@or_file, 'txt_importe_producto_aplicable')
  @@txt_descripcion_producto_aplicado= get_or_data(@or_file, 'txt_descripcion_producto_aplicado')
  @@txt_aplicado_producto_aplicado= get_or_data(@or_file, 'txt_aplicado_producto_aplicado')
  @@txt_importe_producto_aplicado= get_or_data(@or_file, 'txt_importe_producto_aplicado')
  @@check_nombre_combo_estatico= get_or_data(@or_file, 'check_nombre_combo_estatico')
  @@txt_producto= get_or_data(@or_file, 'txt_producto')
  @@txt_maximo= get_or_data(@or_file, 'txt_maximo')
  @@txt_cantidad_combo= get_or_data(@or_file, 'txt_cantidad_combo')
  @@txt_disponible= get_or_data(@or_file, 'txt_disponible')
  @@subTotalAplicacion= get_or_data(@or_file, 'subTotalAplicacion')
  @@totalAplicacion= get_or_data(@or_file, 'totalAplicacion')
  @@txt_nombre_sku= get_or_data(@or_file, 'txt_nombre_sku')

#############################################################################
#############################################################################
#METODOS
##############################################################################
###############################################################################

  def agrega_inventario(sku, i_inicial, i_final)

    $DTH_INVENTARIO = {
        :id => nil,
        :sku => nil,
        :inventario_inicial => nil,
        :inventario_final => nil,
        :monto_combo1 => nil,
        :monto_combo2 => nil,
        :descuento_total => nil,
        :importe => nil,
        :efectivo => nil,
        :cheques => nil,
        :cupones => nil,
        :pagos_cr => nil,
        :monto_cr => nil,
        :neto => nil,
        :nombre_ticket => nil,
        :devolucion_envase_grb => nil,
        :devolucion_garrafon => nil,
        :prestamo_envase_grb => nil,
        :prestamo_envase_garrafon => nil,
        :cobro_envase_grb => nil,
        :cobro_envase_garrafon => nil,
        :cobro_envase_grb_y_garrafon => nil,
        :cobro_cajilla => nil,
    }
    $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
    $DTH_INVENTARIO[:sku] =sku.gsub!("  ", "")
    $DTH_INVENTARIO[:inventario_inicial]=i_inicial
    $DTH_INVENTARIO[:inventario_final] =i_final.to_i
    #PUSHEAR PRODUCTO A DATA
    $DATOS.push($DTH_INVENTARIO)
    puts $DATOS
  end

  def agrega_productos_estaticos_inventario_inicial(nombre_combo, sku)
    nombre_combo = nombre_combo.to_s
    sku = sku.to_s

    x2="(//*[contains(@text,'"+nombre_combo+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.TextView'])[2]"
    scroll_down_to_uielement(x2,
                             10,
                             'mate')
    inventario_estatico = get_mobile_element_attribute x2, "text"
    puts "El inventario inicial del sku ", sku, " es: ", inventario_estatico

    return inventario_estatico
  end

  def calcula_inventario_final_estatico(inventario_inicial, sku, cajas)
    i_final = inventario_inicial.to_i - cajas.to_i
    puts "inventario final es", i_final
    agrega_inventario(sku, inventario_inicial, i_final)

    # puts "Inventario final en metodo calcula_inventario_final_estatico es: ", i_final
  end

  def inventario_final_escalera(nombre_combo, sku, cajas)
    nombre_combo = nombre_combo.to_s
    sku = sku.to_s
    cajas = cajas.to_i

    x = "(//*[contains(@text,'"+nombre_combo+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.EditText'])[1]"
    x2 = "(//*[contains(@text,'"+nombre_combo+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.TextView'])[1]"
    scroll_down_to_uielement(x,
                             10,
                             'mate'
    )

    i_inicial = get_mobile_element_attribute x2, "text"
    i_final = i_inicial.to_i - cajas
    puts "inventario inicial es ", i_inicial
    puts
    puts
    puts "cajas", cajas
    puts
    puts
    puts "inventario final es", i_final
    agrega_inventario(sku, i_inicial, i_final)
  end


  def busca_y_agrega_productos_dinamicos(nivel, sku, cantidad)
    nivel=nivel.to_s
    sku=sku.to_s
    cantidad=cantidad.to_s
    caja=cantidad.to_i
    if sku.empty?
      puts "no hay producto"
    else
      x="(//*[contains(@text,'"+nivel+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.EditText'])[1]"
      x2="(//*[contains(@text,'"+nivel+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.TextView'])[1]"
      scroll_down_to_uielement(x,
                               10,
                               'mate'
      )
      if mobile_element_exists x
        i_inicial=get_mobile_element_attribute x2, "text"
        i_final=i_inicial.to_i-caja
        puts "inventario inicial es ", i_inicial
        puts
        puts
        puts "cajas", caja
        puts
        puts
        puts "inventario final es", i_final
        agrega_inventario(sku, i_inicial, i_final)


        enter_text_uielement(x,
                             cantidad,
                             'mate')
        sleep 5
        press_user_action_button('search')
      else
        puts "no se encontro el  producto"
      end
    end


  end


  def calcula_el_inventario_final(nivel, sku, cajas)
    nivel=nivel.to_s
    sku=sku.to_s
    cajas=cajas.to_i
    x="(//*[contains(@text,'"+nivel+"')]/ancestor::*[contains(@resource-id, 'header_linearlayout')]/following-sibling::*//*[@text='"+sku+"']/following-sibling::*[@class='android.widget.TextView'])[1]"
    i_inicial=get_mobile_element_attribute x, "text"
    i_final=i_inicial.to_i-cajas
    puts "inventario inicial es ", i_inicial
    puts
    puts
    puts "cajas", cajas
    puts
    puts
    puts "inventario final es", i_final
  end

  def calculo_dto_porcentaje(cajas1, precio1, cajas2, precio2, cajas3, precio3, dto)
    #SE  MULTIPLICAN LAS CAJAS POR SU PRECIO UNITARIO
    primer_producto=cajas1.to_f*precio1.to_f
    segundo_producto=cajas2.to_f*precio2.to_f
    tercer_producto=cajas3.to_f*precio3.to_f
    #SE REALIZA LA SUMA DE LOS PRODUCTOS
    suma_productos=primer_producto+segundo_producto+tercer_producto
    r_suma_dinamico=suma_productos.round(2)
    #EN CASO DE QUE EL PORCENTAJE SEA 0 SE RESTA AL IMPORTE DINAMICO, DE LO CONTRARIO SE MULTIPLICA
    if dto.to_f==0
      resultado_dinamico=r_suma_dinamico-dto.to_f
    else
      resultado_dinamico=r_suma_dinamico*dto.to_f
    end
    #SE INGRESA EL MONTO DEL DESCUENTO  AL ARREGLO CADA QUE SE MANDA A LLAMAR EL METODO
    $R_DTO_COMBOS.push(resultado_dinamico)
    $R_SUMA_DINAMICO.push(suma_productos)
  end


  def calculo_dto_monto(cajas1, precio1, cajas2, precio2, cajas3, precio3, dto)
    #SE  MULTIPLICAN LAS CAJAS POR SU PRECIO UNITARIO
    primer_producto=cajas1.to_f*precio1.to_f
    segundo_producto=cajas2.to_f*precio2.to_f
    tercer_producto=cajas3.to_f*precio3.to_f
    #SE REALIZA LA SUMA DE LOS PRODUCTOS
    suma_productos=primer_producto+segundo_producto+tercer_producto
    r_suma_dinamico=suma_productos.round(2)
    # SE RESTA EL DESCUENTO MENOS EL IMPORTE DEL COMBO DINAMICO
    resultado=r_suma_dinamico-dto.to_f
    #SE INGRESA EL MONTO DEL DESCUENTO  AL ARREGLO CADA QUE SE MANDA A LLAMAR EL METODO
    $R_DTO_COMBOS.push(resultado)
    $R_SUMA_DINAMICO.push(suma_productos)

  end


#############################################################################
# ACCIONES
#############################################################################

  def seleccionar_btn(paquetes)
    #convertir string menu a utf8, compatibilidad con encoding
    paquetes = paquetes.to_s.force_encoding(Encoding::UTF_8)
    sleep 5
    or_exec_uielement(@@btn_paquetes, paquetes)
  rescue Exception => e
    raise("seleccionar_menus => Error no se pudo seleccionar el boton paquetes '#{paquetes}'.
             \nException => #{e.message}")
    sleep 8
  end

  def seleccionar_btn_cajilla
    sleep 5
    or_exec_uielement(@@btn_cajilla)
  rescue Exception => e
    raise("seleccionar_menus => Error no se pudo seleccionar el boton cajilla.
             \nException => #{e.message}")
  end

  def seleccionar_btn_combo(combo)
    #convertir string menu a utf8, compatibilidad con encoding
    combo = combo.to_s.force_encoding(Encoding::UTF_8)
    or_exec_uielement(@@btn_combo, combo)
    sleep 5

  rescue Exception => e
    raise("seleccionar_menus => Error no se pudo seleccionar el boton paquetes '#{combo}'.
             \nException => #{e.message}")
  end


  def buscar_tipo_combo # CAMEL buscarTipoCombo

    #convertir string menu a utf8, compatibilidad con encoding
    sleep 3
    or_exec_uielement(@@btn_paquetes)
    sleep 3
    tipoCombo = $dt_row_combo[:tipo_combo] #GRB, GARRAFON Y CAJILLA

    tipoComboApp = case tipoCombo
                     when "estatico"

                       touchEnTipoCombo1()
                       #  or_exec_uielement(@@check_nombre_combo_estatico)
                       #sleep 2
                       sleep 3

                       ##NOMBRE DEL SKU
                       nombreSku = query(@@txt_nombre_sku[:locator], :text)

                       ##DESCUENTOS POR PORCENTAJE POR NIVEL

                       descuentoDinamicoPorcentajeNivel1 = $dt_row_combo[:descuento_dinamico_porcentaje_nivel_1]
                       descuentoDinamicoPorcentajeNivel2 = $dt_row_combo[:descuento_dinamico_porcentaje_nivel_2]
                       descuentoDinamicoPorcentajeNivel3= $dt_row_combo[:descuento_dinamico_porcentaje_nivel_3]

                       ##DESCUENTOS POR PESOS POR NIVEL

                       descuentoDinamicoMontoNivel1 = $dt_row_combo[:descuento_dinamico_monto_nivel_1]
                       descuentoDinamicoMontoNivel2 = $dt_row_combo[:descuento_dinamico_monto_nivel_2]
                       descuentoDinamicoMontoNivel3 = $dt_row_combo[:descuento_dinamico_monto_nivel_3]


                       ##PRODUCTOS PRIMER NIVEL
                       nombreSku1ComboEstaticoNivel1 = $dt_row_combo[:sku_1_nivel_1]
                       precioSku1ComboEstaticoNivel1 = $dt_row_combo[:precio_sku_1_nivel_1]

                       nombreSku2ComboEstaticoNivel1 = $dt_row_combo[:sku_2_nivel_1]
                       precioSku2ComboEstaticoNivel1 = $dt_row_combo[:precio_sku_2_nivel_1]

                       nombreSku3ComboEstaticoNivel1 = $dt_row_combo[:sku_3_nivel_1]
                       precioSku3ComboEstaticoNivel1 = $dt_row_combo[:precio_sku_3_nivel_1]


                       ##PRODUCTOS SEGUNDO NIVEL
                       nombreSku1ComboEstaticoNivel2 = $dt_row_combo[:sku_1_nivel_2]
                       precioSku1ComboEstaticoNivel2 = $dt_row_combo[:precio_sku_1_nivel_2]

                       nombreSku2ComboEstaticoNivel2 = $dt_row_combo[:sku_2_nivel_2]
                       precioSku2ComboEstaticoNivel2 = $dt_row_combo[:precio_sku_2_nivel_2]

                       nombreSku3ComboEstaticoNivel2 = $dt_row_combo[:sku_3_nivel_2]
                       precioSku3ComboEstaticoNivel2 = $dt_row_combo[:precio_sku_3_nivel_2]


                       ##PRODUCTOS TERCER NIVEL
                       nombreSku1ComboEstaticoNivel3 = $dt_row_combo[:sku_1_nivel_3]
                       precioSku1ComboEstaticoNivel3 = $dt_row_combo[:precio_sku_1_nivel_3]

                       nombreSku2ComboEstaticoNivel3 = $dt_row_combo[:sku_2_nivel_3]
                       precioSku2ComboEstaticoNivel3 = $dt_row_combo[:precio_sku_2_nivel_3]

                       nombreSku3ComboEstaticoNivel3 = $dt_row_combo[:sku_3_nivel_3]
                       precioSku3ComboEstaticoNivel3 = $dt_row_combo[:precio_sku_3_nivel_3]


                       nombreCombo = $dt_row_combo[:nombre_combo]
                       puts "LINEA 357"
                       unless nombreSku1ComboEstaticoNivel1.nil?
                         inventarioInicialSku1Nivel1 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku1ComboEstaticoNivel1)
                         puts "LINEA 360"
                       end
                       puts "LINEA 362"

                       unless nombreSku2ComboEstaticoNivel1.nil?
                         inventarioInicialSku2Nivel1 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku2ComboEstaticoNivel1)
                         puts "LINEA 366"
                       end
                       puts "LINEA 368"
                       unless nombreSku3ComboEstaticoNivel1.nil?

                         inventarioInicialSku3Nivel1 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku3ComboEstaticoNivel1)
                         puts "LINEA 372"
                       end
                       puts "LINEA 374"
                       unless nombreSku1ComboEstaticoNivel2.nil?
                         inventarioInicialSku1Nivel2 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku1ComboEstaticoNivel2)
                         puts "LINEA 377"
                       end
                       puts "LINEA 379"
                       unless nombreSku2ComboEstaticoNivel2.nil?
                         inventarioInicialSku2Nivel2 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku2ComboEstaticoNivel2)
                         puts "LINEA 382"
                       end
                       puts "LINEA 384"
                       unless nombreSku3ComboEstaticoNivel2.nil?
                         inventarioInicialSku3Nivel2 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku3ComboEstaticoNivel2)
                         puts "LINEA 387"
                       end
                       puts "LINEA 389"
                       unless nombreSku1ComboEstaticoNivel3.nil?
                         inventarioInicialSku1Nivel3 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku1ComboEstaticoNivel3)
                         puts "LINEA 392"
                       end
                       puts "LINEA 394"
                       unless nombreSku2ComboEstaticoNivel3.nil?
                         inventarioInicialSku2Nivel3 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku2ComboEstaticoNivel3)
                         puts "LINEA 397"
                       end
                       puts "LINEA 399"
                       unless nombreSku3ComboEstaticoNivel3.nil?
                         inventarioInicialSku3Nivel3 = agrega_productos_estaticos_inventario_inicial(nombreCombo, nombreSku3ComboEstaticoNivel3)
                         puts "LINEA 402"
                       end
                       puts "LINEA 404"
                       obtenerNombreComboEstatico(nombreCombo)

                       puts "LINEA 407"


                       subtotalAcumuladoPesos = 0
                       descuentoAcumulado = 0
                       subtotalAcumulado1 = 0
                       totalAcumulado = 0
                       descuentoPorMontoAcumulado = 0
                       puts "LINEA 405"
                       unless descuentoDinamicoPorcentajeNivel1.nil? && descuentoDinamicoPorcentajeNivel2.nil? && descuentoDinamicoPorcentajeNivel3.nil?
                         puts "LINEA 407"
                         unless nombreSku1ComboEstaticoNivel1.nil?
                           puts "LINEA 409"
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel1)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel1, nombreSku1ComboEstaticoNivel1, cantidad)

                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel1)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel1 #92
                           # puts "El sub total del sku 1 nivel 1 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel1)
                           # puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)

                           puts "El total del sku 1 nivel 1 es: ", total #496.8
                           totalAcumulado = totalAcumulado + total
                           # puts "El total es: ", total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado

                         end
                         unless nombreSku2ComboEstaticoNivel1.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel1)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel1, nombreSku2ComboEstaticoNivel1, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel1)
                           #  puts "El precio del sku es: ", precioSku2ComboEstaticoNivel1

                           # puts "El sub total del sku 2 nivel 1 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel1)
                           # puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           #puts "El total del sku 2 nivel 1 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #  subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel1, descuentoDinamicoPorcentajeNivel1)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel1.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel1)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel1, nombreSku3ComboEstaticoNivel1, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel1)
                           # puts "El precio del sku es: ", precioSku3ComboEstaticoNivel1

                           # puts "El sub total del sku 3 nivel 1 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel1)
                           #  puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 3 nivel 1 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel1, descuentoDinamicoPorcentajeNivel1)
                           #subtotalAcumulado = subtotalAcumulado + subTotal
                         end
                         unless nombreSku1ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel2)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel2, nombreSku1ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel2)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel2

                           #  puts "El sub total del sku 1 nivel 2 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel2)
                           #  puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 1 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku1ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku2ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel2)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel2, nombreSku2ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel2)
                           # puts "El precio del sku es: ", precioSku2ComboEstaticoNivel2

                           # puts "El sub total del sku 2 nivel 2 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel2)
                           #  puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 2 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           #subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel2)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel2, nombreSku3ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel2)
                           #  puts "El precio del sku es: ", precioSku3ComboEstaticoNivel2

                           # puts "El sub total del sku 3 nivel 2 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel2)
                           # puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 3 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku1ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel3)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel3, nombreSku1ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel3

                           #  puts "El sub total del sku 1 nivel 3 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel3)
                           #  puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 1 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku1ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           #subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku2ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel3)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel3, nombreSku2ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku2ComboEstaticoNivel3

                           #  puts "El sub total del sku 2 nivel 3 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel3)
                           # puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 2 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel3)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel3, nombreSku3ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku3ComboEstaticoNivel3

                           # puts "El sub total del sku 3 nivel 3 es: ", subTotal
                           subtotalAcumulado1 = subtotalAcumulado1 + subTotal
                           descuentoCSV = calculoDescuentoComboEstaticoPorPorcentaje(subTotal, descuentoDinamicoPorcentajeNivel3)
                           # puts "El descuento total es: ", descuentoCSV
                           descuentoAcumulado = descuentoAcumulado + descuentoCSV
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR PORCENTAJE
                           puts "El descuento acumulado es: ", descuentoAcumulado
                           total = calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
                           # puts "El total del sku 3 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         # subtotalAcumulado
                         # totalAcumulado

                         #subtotalAcumulado = subtotalAcumulado + subTotal
                         puts "El total calculado es: ", subtotalAcumulado1
                         puts "El descuento calculado es: ", totalAcumulado

                         subTotalAplicacion = query(@@subTotalAplicacion[:locator], :text)
                         subTotalAplicacion = quitarCaracteres(subTotalAplicacion.to_s.split("$")[1])
                         subTotalAplicacion = subTotalAplicacion[0].to_f
                         totalAplicacion = query(@@totalAplicacion[:locator], :text)
                         totalAplicacion = quitarCaracteres(totalAplicacion.to_s.split("$")[1])
                         totalAplicacion = totalAplicacion[0].to_f

                         if subTotalAplicacion.to_f == subtotalAcumulado1.to_f.round(2)
                           puts "El total en la aplicacion es correcto ", subtotalAcumulado1
                         else
                           $DTH_INVENTARIO[:monto_combo1] =subtotalAcumulado1
                           puts "subTotalAplicacion es: ", subTotalAplicacion, " subtotalAcumulado1 es: ", subtotalAcumulado1
                           raise("Error=> El total de la aplicacion deberia ser: '#{subtotalAcumulado1}'")
                         end

                         $totalAcumulado=totalAcumulado.to_f.round(2)
                         if totalAplicacion.to_f == totalAcumulado.to_f.round(2)
                           puts "El descuento es correcto ", totalAcumulado

                           puts "LINEA 674"
                           sleep 2
                           or_exec_uielement(@@btn_confirmar_combo)
                           sleep 5
                           puts "LINEA 678"

                         else
                           raise("Error=> El descuento de la aplicacion deberia ser: '#{totalAcumulado}'")
                         end
                       end

                       #######PARA PESOS
                       unless descuentoDinamicoMontoNivel1.nil? && descuentoDinamicoMontoNivel2.nil? && descuentoDinamicoMontoNivel3.nil?

                         unless nombreSku1ComboEstaticoNivel1.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel1)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel1, nombreSku1ComboEstaticoNivel1, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel1)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel1
                           # puts "El sub total del sku 1 nivel 1 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel1, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 1 nivel 1 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El total es: ", total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                         end
                         unless nombreSku2ComboEstaticoNivel1.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel1)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel1, nombreSku2ComboEstaticoNivel1, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel1)
                           # puts "El precio del sku es: ", precioSku2ComboEstaticoNivel1

                           # puts "El sub total del sku 2 nivel 1 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel1, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 2 nivel 1 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #  subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel1, descuentoDinamicoPorcentajeNivel1)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel1.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel1)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel1, " nivel 1 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel1, nombreSku3ComboEstaticoNivel1, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel1)
                           # puts "El precio del sku es: ", precioSku3ComboEstaticoNivel1

                           # puts "El sub total del sku 3 nivel 1 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel1, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 3 nivel 1 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel1, descuentoDinamicoPorcentajeNivel1)
                           #subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku1ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel2)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel2, nombreSku1ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel2)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel2

                           #  puts "El sub total del sku 1 nivel 2 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel2, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 1 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku1ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku2ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel2)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel2, nombreSku2ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel2)
                           # puts "El precio del sku es: ", precioSku2ComboEstaticoNivel2

                           # puts "El sub total del sku 2 nivel 2 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel2, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 2 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           #subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel2.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel2)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel2, " nivel 2 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel2, nombreSku3ComboEstaticoNivel2, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel2)
                           # puts "El precio del sku es: ", precioSku3ComboEstaticoNivel2

                           # puts "El sub total del sku 3 nivel 2 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel2, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 3 nivel 2 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel2, descuentoDinamicoPorcentajeNivel2)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku1ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku1ComboEstaticoNivel3)
                           puts "La cantidad del sku 1 ", nombreSku1ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku1Nivel3, nombreSku1ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku1ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku1ComboEstaticoNivel3

                           # puts "El sub total del sku 1 nivel 3 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel3, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 1 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           # puts "El subtotal acumulado es: ", subtotalAcumulado


                           #subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku1ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           #subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku2ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku2ComboEstaticoNivel3)
                           puts "La cantidad del sku 2 ", nombreSku2ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku2Nivel3, nombreSku2ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku2ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku2ComboEstaticoNivel3

                           #  puts "El sub total del sku 2 nivel 3 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel3, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 2 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku2ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         unless nombreSku3ComboEstaticoNivel3.nil?
                           cantidad = obtenerCantidadPorNombreDelProductoDelCombo(nombreCombo, nombreSku3ComboEstaticoNivel3)
                           puts "La cantidad del sku 3 ", nombreSku3ComboEstaticoNivel3, " nivel 3 es: ", cantidad
                           calcula_inventario_final_estatico(inventarioInicialSku3Nivel3, nombreSku3ComboEstaticoNivel3, cantidad)


                           subTotal = calculoSubTotalComboEstatico(cantidad, precioSku3ComboEstaticoNivel3)
                           # puts "El precio del sku es: ", precioSku3ComboEstaticoNivel3

                           # puts "El sub total del sku 3 nivel 3 es: ", subTotal
                           subtotalAcumuladoPesos = subtotalAcumuladoPesos + subTotal

                           descuentoPorMonto = calculoDescuentoComboEstaticoPorMonto(descuentoDinamicoMontoNivel3, cantidad)
                           descuentoPorMontoAcumulado = descuentoPorMontoAcumulado + descuentoPorMonto
                           #IVAN AQUI ESTA EL DESCUENTO ACUMULADO DEL COMBO ESTATICO POR MONTO
                           puts "El descuento acumulado es: ", descuentoPorMontoAcumulado

                           total = calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorMonto)
                           puts "El total del sku 3 nivel 3 es: ", total
                           totalAcumulado = totalAcumulado + total
                           #  puts "El subtotal acumulado es: ", subtotalAcumulado


                           # subTotal = calculoComboEstaticoPorPorcentaje(cantidad, precioSku3ComboEstaticoNivel3, descuentoDinamicoPorcentajeNivel3)
                           # subtotalAcumulado = subtotalAcumulado + subTotal

                         end
                         #subtotalAcumulado
                         #totalAcumulado

                         #subtotalAcumulado = subtotalAcumulado + subTotal
                         puts "El total calculado es: ", subtotalAcumuladoPesos
                         puts "El descuento calculado es: ", totalAcumulado

                         subTotalAplicacion = query(@@subTotalAplicacion[:locator], :text)
                         subTotalAplicacion = quitarCaracteres(subTotalAplicacion.to_s.split("$")[1])
                         subTotalAplicacion = subTotalAplicacion[0].to_f
                         totalAplicacion = query(@@totalAplicacion[:locator], :text)
                         totalAplicacion = quitarCaracteres(totalAplicacion.to_s.split("$")[1])
                         totalAplicacion = totalAplicacion[0].to_f

                         if subTotalAplicacion.to_f == subtotalAcumuladoPesos.to_f.round(2)
                           puts "El total en la aplicacion es correcto ", subtotalAcumuladoPesos
                         else
                           raise("Error=> El total de la aplicacion deberia ser: '#{subtotalAcumuladoPesos}'")
                         end
                         $DESCUENTO_TOTAL=$DESCUENTO_TOTAL+totalAcumulado
                         if totalAplicacion.to_f == totalAcumulado.to_f.round(2)
                           puts "LINEA 943"
                           puts "El descuento es correcto ", totalAcumulado

                           puts "LINEA 949"

                           puts "LINEA 954"
                           sleep 2
                           or_exec_uielement(@@btn_confirmar_combo)
                           sleep 5
                         else
                           raise("Error=> El descuento de la aplicacion deberia ser: '#{totalAcumulado}'")
                         end
                       end
                     #    touch_confirmar_combo

                     when "escalera"
                       ##COMIENZA COMB ESCALERA
                       ##elegir el radio del combo
                       or_exec_uielement(@@btn_tipo_combo_escalera)
                       sleep 2

                       or_exec_uielement(@@check_nombre_combo_escalera)
                       sleep 2

                       ##elegir el check del combo
                       nombreCombo = $dt_row_combo[:nombre_combo].to_s.strip


                       #puts nombreCombo
                       el = val_or_uielement(@@btn_nombre_combo_escalera, nombreCombo)
                       # puts el["element"]
                       scroll_down_to_uielement_adr el["element"]
                       assert_for_element_exist(el["element"], 20,
                                                "Error => no se encontro el combo")
                       sleep 2
                       #elementoNombreCombo = el

                       touch(el["element"])


                       skusComboEscalera = Array.new
                       skusComboEscalera = query(@@txt_skus_combo_escalera[:locator], :text)
                       inputComboEscalera = Array.new
                       inputComboEscalera = query(@@input_cantidad_sku_combo_escalera[:locator])
                       nivelEscalera= $dt_row_combo[:nivel_escalera]


                       nombreSku1ComboEscaleraNivel1 = $dt_row_combo[:sku_1_nivel_1]
                       cantidadSku1ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_1_nivel_1]
                       precioSku1ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_1_nivel_1]
                       descuentoPorcentajeSku1ComboEscaleraNivel1 = $dt_row_combo[:descuento_dinamico_porcentaje_nivel_1]

                       nombreSku2ComboEscaleraNivel1 = $dt_row_combo[:sku_2_nivel_1]
                       cantidadSku2ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_2_nivel_1]
                       precioSku2ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_2_nivel_1]
                       descuentoPorcentajeSku2ComboEscaleraNivel1 = $dt_row_combo[:descuento_dinamico_porcentaje_nivel_2]

                       nombreSku3ComboEscaleraNivel1 = $dt_row_combo[:sku_3_nivel_1]
                       cantidadSku3ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_3_nivel_1]
                       precioSku3ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_3_nivel_1]
                       descuentoPorcentajeSku3ComboEscaleraNivel1 = $dt_row_combo[:descuento_dinamico_porcentaje_nivel_3]

                       unless descuentoPorcentajeSku1ComboEscaleraNivel1.nil? && descuentoPorcentajeSku2ComboEscaleraNivel1.nil? && descuentoPorcentajeSku3ComboEscaleraNivel1.nil?


                         unless nombreSku1ComboEscaleraNivel1.nil?


                           skusComboEscalera.each do |itemNombreSkuComboEscalera|

                             if itemNombreSkuComboEscalera.to_s.include? nombreSku1ComboEscaleraNivel1.to_s
                               #  puts "El nombre del primer sku del combo es: ", itemNombreSkuComboEscalera
                               posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
                               #   puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
                               posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
                               #  puts "La posicion del input de cantidad es: ", posicionCantidadSkuComboEscalera
                               #  puts "La cantidad de cajas de nombreSku1ComboEscaleraNivel1 es: ", cantidadSku1ComboEscaleraNivel1
                               inventario_final_escalera(nombreCombo, nombreSku1ComboEscaleraNivel1, cantidadSku1ComboEscaleraNivel1)
                               touch(posicionCantidadSkuComboEscalera)
                               keyboard_enter_text(cantidadSku1ComboEscaleraNivel1)
                               press_user_action_button('search')
                               sleep 2

                               $montoProducto1 = cantidadSku1ComboEscaleraNivel1.to_f * precioSku1ComboEscaleraNivel1.to_f
                               puts "El monto a pagar por el producto 1 es: ", $montoProducto1.to_f
                               sleep 2

                             end
                           end
                         end


                         unless nombreSku2ComboEscaleraNivel1.nil?


                           skusComboEscalera.each do |itemNombreSkuComboEscalera|

                             if itemNombreSkuComboEscalera.to_s.include? nombreSku2ComboEscaleraNivel1.to_s
                               #    puts "El nombre del segundo sku del combo es: ", itemNombreSkuComboEscalera
                               posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
                               #   puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
                               posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
                               #  puts "La posicion del input de cantidad es: ", posicionCantidadSkuComboEscalera
                               #ivam1018
                               puts "La cantidad de cajas de nombreSku2ComboEscaleraNivel1 es: ", cantidadSku2ComboEscaleraNivel1

                               inventario_final_escalera(nombreCombo, nombreSku2ComboEscaleraNivel1, cantidadSku2ComboEscaleraNivel1)
                               touch(posicionCantidadSkuComboEscalera)
                               keyboard_enter_text(cantidadSku2ComboEscaleraNivel1)
                               press_user_action_button('search')
                               sleep 2

                               $montoProducto2 = cantidadSku2ComboEscaleraNivel1.to_i * precioSku2ComboEscaleraNivel1.to_f
                               puts "El monto a pagar por el producto 2 es: ", $montoProducto2.to_f

                               sleep 2
                             end
                           end
                         end

                         unless nombreSku3ComboEscaleraNivel1.nil?


                           skusComboEscalera.each do |itemNombreSkuComboEscalera|

                             if itemNombreSkuComboEscalera.to_s.include? nombreSku3ComboEscaleraNivel1.to_s
                               #   puts "El nombre del segundo sku del combo es: ", itemNombreSkuComboEscalera
                               posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
                               #  puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
                               posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
                               # puts "La posicion del input de cantidad es: ", posicionCantidadSkuComboEscalera
                               puts "La cantidad de cajas de nombreSku3ComboEscaleraNivel1 es: ", cantidadSku3ComboEscaleraNivel1
                               inventario_final_escalera(nombreCombo, nombreSku3ComboEscaleraNivel1, cantidadSku3ComboEscaleraNivel1)


                               touch(posicionCantidadSkuComboEscalera)
                               keyboard_enter_text(cantidadSku3ComboEscaleraNivel1)
                               press_user_action_button('search')
                               sleep 2

                               $montoProducto3 = cantidadSku3ComboEscaleraNivel1.to_i * precioSku3ComboEscaleraNivel1.to_f
                               puts "El monto a pagar por el producto 3 es: ", $montoProducto3.to_f

                               sleep 2


                             end
                           end
                         end

                         sumaMontosProductos = $montoProducto1.to_f.round(2) + $montoProducto2.to_f.round(2) + $montoProducto3.to_f.round(2)
                         puts "La sumatoria de los montos es: ", sumaMontosProductos.to_f.round(2)
                         sleep 5

                         totalApp = query(@@txt_total[:locator], :text)
                         # puts "Total de la app es: ", totalApp
                         totalSeparado = totalApp.map {|totalSeparado| totalSeparado.delete('  $,Tabcdefghijklmnopqr¤stuvwxyz')}
                         totalSeparado2 = totalSeparado.to_s.split("n")
                         total = totalSeparado2[1].to_f

                         # puts "Total es: ", total

                         if total.to_f.round(2) == sumaMontosProductos.to_f.round(2)
                           puts "El total es correcto", sumaMontosProductos
                         else
                           $DTH_INVENTARIO[:monto_combo1] =sumaMontosProductos

                           puts "El total no es correcto, debería ser: ", sumaMontosProductos
                         end

                         numeroDeProductos = cantidadSku1ComboEscaleraNivel1.to_f.round(2) + cantidadSku2ComboEscaleraNivel1.to_f.round(2) + cantidadSku3ComboEscaleraNivel1.to_f.round(2)
                         puts "Numero de productos es: ", numeroDeProductos.to_f

                         unless descuentoPorcentajeSku1ComboEscaleraNivel1.nil?
                           porcentaje = descuentoPorcentajeSku1ComboEscaleraNivel1.to_f.round(2) / 100
                           puts "Descuento nivel 1 ", porcentaje.to_f.round(2)
                         end

                         unless descuentoPorcentajeSku2ComboEscaleraNivel1.nil?
                           porcentaje1 = descuentoPorcentajeSku2ComboEscaleraNivel1.to_f.round(2) / 100
                           porcentaje = porcentaje1.to_f.round(2)
                           puts "Descuento nivel 2 ", porcentaje
                         end

                         unless descuentoPorcentajeSku3ComboEscaleraNivel1.nil?
                           porcentaje = descuentoPorcentajeSku3ComboEscaleraNivel1.to_f.round(2) / 100
                           puts "Descuento nivel 3 ", porcentaje.to_f.round(2)
                         end

                         multiplicacionPorDescuento = sumaMontosProductos.to_f.round(2) * porcentaje.to_f.round(2)
                         #puts "IVAN AQUI ESTA EL MONTO A DESCONTAR DEL COMBO ESCALERA POR PORCENTAJE, LINEA 1837"
                         puts "El monto a descontar es: ", multiplicacionPorDescuento.to_f.round(2)

                         restaMonto = total.to_f.round(2) - multiplicacionPorDescuento.to_f.round(2)
                         puts "El monto total con decuento es: ", restaMonto.round(2)

                         descuentoApp = query(@@txt_cDescuento[:locator], :text)
                         #  puts "Descuento de la app es: ", descuentoApp
                         descuentoSeparado = descuentoApp.map {|descuentoSeparado| descuentoSeparado.delete('  $,Tabcdefghijklmnopqr¤stuvwxyz')}
                         descuentoSeparado2 = descuentoSeparado.to_s.split("n")
                         descuento = descuentoSeparado2[1].to_f
                         $DESCUENTO_TOTAL=$DESCUENTO_TOTAL+restaMonto
                         $restaMonto=restaMonto.round(2)

                         if descuento.to_f.round(2) == restaMonto.to_f.round(2)
                           puts "El descuento es correcto"
                         else
                           puts "El descuento no es correcto, debería ser: ", restaMonto.round(2)
                         end

                         sleep 2

                         busca_nivel ="//*[contains(@text,'"+nivelEscalera+"')]"
                         scroll_down_to_uielement(busca_nivel,
                                                  5,
                                                  'mate')
                         boleanoNivel = mobile_element_exists(busca_nivel)
                         if boleanoNivel == true
                           puts "El nivel es correcto ", nivelEscalera
                         else
                           puts "El nivel es incorrecto, deberia ser: ", nivelEscalera
                         end

                         cajas1 = query(@@txt_cajas[:locator], :text)
                         cajas2 = cajas1.to_s.split("n")
                         cajas = cajas2[1].to_i
                         puts "Las cajas en la app son: ", cajas.to_i

                         if cajas.to_i == numeroDeProductos.to_i
                           puts "El numero de cajas en la aplicacion es correcta"

                         else
                           puts "El numero de cajas en la aplicacion es incorrecta, deberia ser: ", numeroDeProductos.to_i
                         end
                       else
                         combo_escalera_por_monto

                       end
                       # unless descuentoPorcentajeSku1ComboEscaleraNivel1.nil? && descuentoPorcentajeSku2ComboEscaleraNivel1.nil? && descuentoPorcentajeSku3ComboEscaleraNivel1.nil?

                       # end
                       #                       touch_confirmar_combo
                       sleep 2
                       or_exec_uielement(@@btn_confirmar_combo)
                       sleep 2

                     when "dinamico"
                       $R_DTO_COMBOS=Array.new
                       $R_SUMA_DINAMICO=Array.new

                       ##elegir el check del combo
                       nombrecombo =$dt_row_combo[:nombre_combo].to_s
                       #elegir el check_combo dinamico y abrir el menu
                       or_exec_uielement(@@txt_elemento_combo)
                       sleep 3
                       #SE DESPLIEGA EL MENU Y SE SELECCIONA EL NOMBRE DEL COMBO
                       el = val_or_uielement(@@btn_combo_dinamico_a_buscar, nombrecombo)
                       nombre_combo="//*[@text='"+nombrecombo+"']"

                       scroll_down_to_uielement(nombre_combo,
                                                10,
                                                'mate'
                       )
                       tap_element(nombre_combo)

                       #se ingresa la cantidad del combo
                       cantidad_combo=$dt_row_combo[:cantidad_combo].to_i
                       assert_for_element_exist(@@lbl_cantmax_dinamico[:locator], 10, "Error=>no se cargo el combo ")
                       cantidad_max_dinamico=query(@@lbl_cantmax_dinamico[:locator], :text)[0].to_i
                       if cantidad_max_dinamico < cantidad_combo
                         raise("Error=> La cantidad maxima es'#{cantidad_max_dinamico}', validar el csv, e ingresar una cantidad menor ")
                       else
                         touch (@@txt_input_cantmax_dinamico[:locator])
                         or_exec_uielement(@@txt_input_cantmax_dinamico, cantidad_combo)
                         press_user_action_button('search')
                       end
                       #SE BUSCAN LOS PRODUCTOS DEL PRIMER NIVEL
                       nivel_1=$dt_row_combo[:nivel_1].to_s
                       #se ingresa el primer articulo
                       primer_sku=$dt_row_combo[:sku_1_nivel_1].to_s
                       cantidad_primer_sku=$dt_row_combo[:cantidad_sku_1_nivel_1].to_s
                       precio_primer_sku=$dt_row_combo[:precio_sku_1_nivel_1].to_s
                       if primer_sku == nil
                         raise("Error=>Se debe agregar por lo menos un producto, validar el csv")
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, primer_sku, cantidad_primer_sku)


                       end

                       #SE INGRESA EL SEGUNDO PRODUCTO
                       segundo_sku=$dt_row_combo[:sku_2_nivel_1]
                       cantidad_segundo_sku=$dt_row_combo[:cantidad_sku_2_nivel_1].to_s
                       precio_segundo_sku=$dt_row_combo[:precio_sku_2_nivel_1].to_s
                       if segundo_sku == nil
                         cantidad_segundo_sku=0
                         precio_segundo_sku=0
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, segundo_sku, cantidad_segundo_sku)
                       end
                       #SE INGRESA EL TERCER PRODUCTO
                       tercer_sku=$dt_row_combo[:sku_3_nivel_1]
                       cantidad_tercer_sku=$dt_row_combo[:cantidad_sku_3_nivel_1].to_s
                       precio_tercer_sku=$dt_row_combo[:precio_sku_3_nivel_1].to_s
                       if tercer_sku==nil
                         cantidad_tercer_sku=0
                         precio_tercer_sku=0

                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, tercer_sku, cantidad_primer_sku)
                       end
                       #VALIDO SI EL DESCUENTO ES POR % O $
                       monto=$dt_row_combo[:descuento_dinamico_monto_nivel_1]
                       porcentaje=$dt_row_combo[:descuento_dinamico_porcentaje_nivel_1]
                       if monto == nil
                       else
                         calculo_dto_monto(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, monto)
                       end
                       if porcentaje == nil
                       else
                         r_porcentaje=(100-porcentaje.to_f)/100.to_f
                         calculo_dto_porcentaje(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, r_porcentaje)
                       end
                       if monto==nil
                       elsif porcentaje == nil?
                         raise("Error=>no se ingreso ningun tipo de descuento en el csv")
                       end

                       #SE BUSCAN LOS PRODUCTOS DEL SEGUNDO NIVEL

                       nivel_1=$dt_row_combo[:nivel_2].to_s
                       #se ingresa el primer articulo
                       primer_sku=$dt_row_combo[:sku_1_nivel_2]
                       cantidad_primer_sku=$dt_row_combo[:cantidad_sku_1_nivel_2].to_s
                       precio_primer_sku=$dt_row_combo[:precio_sku_1_nivel_2].to_s
                       if primer_sku == nil
                         raise("Error=> Faltan productos del Nivel 2")
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, primer_sku, cantidad_primer_sku)
                       end

                       #SE INGRESA EL SEGUNDO PRODUCTO
                       segundo_sku=$dt_row_combo[:sku_2_nivel_2]
                       cantidad_segundo_sku=$dt_row_combo[:cantidad_sku_2_nivel_2].to_s
                       precio_segundo_sku=$dt_row_combo[:precio_sku_2_nivel_2].to_s
                       if segundo_sku == nil
                         cantidad_segundo_sku=0
                         precio_segundo_sku=0
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, segundo_sku, cantidad_primer_sku)

                       end
                       #SE INGRESA EL TERCER PRODUCTO
                       tercer_sku=$dt_row_combo[:sku_3_nivel_2]
                       cantidad_tercer_sku=$dt_row_combo[:cantidad_sku_3_nivel_2].to_f
                       precio_tercer_sku=$dt_row_combo[:precio_sku_3_nivel_2].to_f
                       if tercer_sku==nil
                         cantidad_tercer_sku=0
                         precio_tercer_sku=0
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, tercer_sku, cantidad_tercer_sku)
                       end
                       #VALIDO SI EL DESCUENTO ES POR % O $
                       monto=$dt_row_combo[:descuento_dinamico_monto_nivel_2]
                       porcentaje=$dt_row_combo[:descuento_dinamico_porcentaje_nivel_2]
                       if monto == nil
                       else
                         puts monto
                         calculo_dto_monto(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, monto)
                       end
                       if porcentaje == nil
                       else
                         r_porcentaje=(100-porcentaje.to_f)/100.to_f
                         calculo_dto_porcentaje(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, r_porcentaje)
                       end
                       if monto==nil
                       elsif porcentaje == nil?
                         raise("Error=>no se ingreso ningun tipo de descuento en el csv")
                       end

                       #SE BUSCAN PRODUCTOS DEL TERCER NIVEL
                       nivel_1=$dt_row_combo[:nivel_3].to_s
                       #se ingresa el primer articulo
                       primer_sku=$dt_row_combo[:sku_1_nivel_3]
                       cantidad_primer_sku=$dt_row_combo[:cantidad_sku_1_nivel_3].to_s
                       precio_primer_sku=$dt_row_combo[:precio_sku_1_nivel_3].to_s
                       if primer_sku == nil
                         raise("Error=> Faltan productos del Nivel 2")
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, primer_sku, cantidad_primer_sku)
                       end

                       #SE INGRESA EL SEGUNDO PRODUCTO
                       segundo_sku=$dt_row_combo[:sku_2_nivel_3]
                       cantidad_segundo_sku=$dt_row_combo[:cantidad_sku_2_nivel_3].to_s
                       precio_segundo_sku=$dt_row_combo[:precio_sku_2_nivel_3].to_s
                       if segundo_sku == nil
                         cantidad_segundo_sku=0
                         precio_segundo_sku=0
                       else
                         busca_y_agrega_productos_dinamicos(nivel_1, segundo_sku, cantidad_primer_sku)

                       end
                       #SE INGRESA EL TERCER PRODUCTO
                       tercer_sku=$dt_row_combo[:sku_3_nivel_3]
                       cantidad_tercer_sku=$dt_row_combo[:cantidad_sku_3_nivel_3].to_f
                       precio_tercer_sku=$dt_row_combo[:precio_sku_3_nivel_3].to_f
                       if tercer_sku==nil
                         cantidad_tercer_sku=0
                         precio_tercer_sku=0
                       else

                         busca_y_agrega_productos_dinamicos(nivel_1, tercer_sku, cantidad_tercer_sku)

                       end
                       #VALIDO SI EL DESCUENTO ES POR % O $
                       monto=$dt_row_combo[:descuento_dinamico_monto_nivel_3]
                       porcentaje=$dt_row_combo[:descuento_dinamico_porcentaje_nivel_3]
                       if monto == nil
                       else
                         puts monto
                         calculo_dto_monto(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, monto)
                       end
                       if porcentaje == nil
                       else
                         r_porcentaje=(100-porcentaje.to_f)/100.to_f
                         calculo_dto_porcentaje(cantidad_primer_sku, precio_primer_sku, cantidad_segundo_sku, precio_segundo_sku, cantidad_tercer_sku, precio_tercer_sku, r_porcentaje)
                       end
                       if monto==nil
                       elsif porcentaje == nil?
                       end
                       descuento_total_dinamico=$R_DTO_COMBOS.inject(0, :+)
                       puts "el total de los descuentos es,", descuento_total_dinamico
                       dto_total=query(@@dto_total_dinamico[:locator], :text).map {|r_importe| r_importe.delete('  $Descuento,')}
                       r_dto=dto_total[0].to_f
                       monto_total_combo_dinamico=$R_SUMA_DINAMICO.inject(0, :+)
                       importe_final_combo=monto_total_combo_dinamico-descuento_total_dinamico
                       $DESCUENTO_TOTAL=$DESCUENTO_TOTAL+importe_final_combo
                       puts "monto_total_combo_dinamico", monto_total_combo_dinamico.to_f.round(2)
                       puts
                       puts
                       puts "descuento_total_dinamico", $monto_total_combo_dinamico =descuento_total_dinamico
                       puts
                       puts
                       puts "$descuento_total", $DESCUENTO_TOTAL=$DESCUENTO_TOTAL.round(2)

                       if descuento_total_dinamico == r_dto
                         puts "el descuento es correcto"

                       else
                         raise ("Error==>El descuento no es correcto por monto")
                         save_evidence_execution
                       end
                       $DTH_INVENTARIO[:monto_combo1] = r_dto.to_s.strip
                       sleep 2
                       or_exec_uielement(@@btn_confirmar_combo)
                       sleep 2
                     #aqui acaba el combo dinamico

                     when nil
                       raise("Error=>")
                   end
  end

  def calculoTotalDescuentoComboEstaticoPorMonto(subTotal, descuentoPorCantidad)
    resultado = subTotal.to_f - descuentoPorCantidad.to_f

    return resultado
  end

  def calculoDescuentoComboEstaticoPorMonto(monto, cantidad)
    descuentoCantidad = monto.to_f * cantidad.to_f

    return descuentoCantidad
  end

  def calculoTotalConDescuentoComboEstatico(subTotal, descuentoCSV)
    resultado = subTotal - descuentoCSV

    return resultado
  end

  def calculoDescuentoComboEstaticoPorPorcentaje(subTotal, porcentaje)

    #inversoPorcentaje = (100 - porcentaje.to_f) / 100
    #resultado = inversoPorcentaje * subTotal

    descuentoCSV = porcentaje.to_f / 100 #25
    descuentoCSV = descuentoCSV.to_f * subTotal.to_f
    ##IVAN AQUI ESTÁ EL DESCUENTO DE COMBO ESTATICO POR PORCENTAJE
    # puts "El descuento a restar es: ", descuentoCSV

    return descuentoCSV
  end

  def calculoSubTotalComboEstatico(cantidad, precio)
    montoSubtotal = precio.to_f * cantidad.to_f
    return montoSubtotal
  end

  def combo_escalera_por_monto


    skusComboEscalera = Array.new
    skusComboEscalera = query(@@txt_skus_combo_escalera[:locator], :text)
    inputComboEscalera = Array.new
    inputComboEscalera = query(@@input_cantidad_sku_combo_escalera[:locator])
    nivelEscalera= $dt_row_combo[:nivel_escalera]


    nombreSku1ComboEscaleraNivel1 = $dt_row_combo[:sku_1_nivel_1]
    cantidadSku1ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_1_nivel_1]
    precioSku1ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_1_nivel_1]
    descuentoMontoSkuComboEscaleraNivel1 = $dt_row_combo[:descuento_dinamico_monto_nivel_1]

    nombreSku2ComboEscaleraNivel1 = $dt_row_combo[:sku_2_nivel_1]
    cantidadSku2ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_2_nivel_1]
    precioSku2ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_2_nivel_1]
    descuentoMontoSkuComboEscaleraNivel2 = $dt_row_combo[:descuento_dinamico_monto_nivel_2]


    nombreSku3ComboEscaleraNivel1 = $dt_row_combo[:sku_3_nivel_1]
    cantidadSku3ComboEscaleraNivel1 = $dt_row_combo[:cantidad_sku_3_nivel_1]
    precioSku3ComboEscaleraNivel1 = $dt_row_combo[:precio_sku_3_nivel_1]
    descuentoMontoSkuComboEscaleraNivel3 = $dt_row_combo[:descuento_dinamico_monto_nivel_3]


    unless descuentoMontoSkuComboEscaleraNivel1.nil? && descuentoMontoSkuComboEscaleraNivel2.nil? && descuentoMontoSkuComboEscaleraNivel3.nil?


      unless nombreSku1ComboEscaleraNivel1.nil?


        skusComboEscalera.each do |itemNombreSkuComboEscalera|

          if itemNombreSkuComboEscalera.to_s.include? nombreSku1ComboEscaleraNivel1.to_s
            #  puts "El nombre del primer sku del combo es: ", itemNombreSkuComboEscalera
            posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
            #   puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
            posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
            #  puts "La posicion del input de cantidad   es: ", posicionCantidadSkuComboEscalera

            touch(posicionCantidadSkuComboEscalera)
            keyboard_enter_text(cantidadSku1ComboEscaleraNivel1)
            press_user_action_button('search')
            sleep 2

            $montoProducto1 = cantidadSku1ComboEscaleraNivel1.to_f * precioSku1ComboEscaleraNivel1.to_f
            puts "El monto a pagar por el producto 1 es: ", $montoProducto1.to_f
            sleep 2

          end
        end
      end


      unless nombreSku2ComboEscaleraNivel1.nil?


        skusComboEscalera.each do |itemNombreSkuComboEscalera|

          if itemNombreSkuComboEscalera.to_s.include? nombreSku2ComboEscaleraNivel1.to_s
            #    puts "El nombre del segundo sku del combo es: ", itemNombreSkuComboEscalera
            posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
            #   puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
            posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
            #  puts "La posicion del input de cantidad es: ", posicionCantidadSkuComboEscalera

            touch(posicionCantidadSkuComboEscalera)
            keyboard_enter_text(cantidadSku2ComboEscaleraNivel1)
            press_user_action_button('search')
            sleep 2

            $montoProducto2 = cantidadSku2ComboEscaleraNivel1.to_i * precioSku2ComboEscaleraNivel1.to_f
            puts "El monto a pagar por el producto 2 es: ", $montoProducto2.to_f

            sleep 2
          end
        end
      end

      unless nombreSku3ComboEscaleraNivel1.nil?


        skusComboEscalera.each do |itemNombreSkuComboEscalera|

          if itemNombreSkuComboEscalera.to_s.include? nombreSku3ComboEscaleraNivel1.to_s
            #   puts "El nombre del segundo sku del combo es: ", itemNombreSkuComboEscalera
            posicionNombreSkuComboEscalera = skusComboEscalera.index(itemNombreSkuComboEscalera)
            #  puts "La posicion del primer nombre es: ", posicionNombreSkuComboEscalera
            posicionCantidadSkuComboEscalera = inputComboEscalera[posicionNombreSkuComboEscalera-2]
            # puts "La posicion del input de cantidad es: ", posicionCantidadSkuComboEscalera

            touch(posicionCantidadSkuComboEscalera)
            keyboard_enter_text(cantidadSku3ComboEscaleraNivel1)
            press_user_action_button('search')
            sleep 2

            $montoProducto3 = cantidadSku3ComboEscaleraNivel1.to_i * precioSku3ComboEscaleraNivel1.to_f
            puts "El monto a pagar por el producto 3 es: ", $montoProducto3.to_f

            sleep 2


          end
        end
      end

      sumaMontosProductos = $montoProducto1.to_f.round(2) + $montoProducto2.to_f.round(2) + $montoProducto3.to_f.round(2)
      puts "La sumatoria de los montos es: ", sumaMontosProductos.to_f.round(2)
      sleep 5

      totalApp = query(@@txt_total[:locator], :text)
      # puts "Total de la app es: ", totalApp
      totalSeparado = totalApp.map {|totalSeparado| totalSeparado.delete('  $,Tabcdefghijklmnopqr¤stuvwxyz')}
      totalSeparado2 = totalSeparado.to_s.split("n")
      total = totalSeparado2[1].to_f

      # puts "Total es: ", total

      if total.to_f.round(2) == sumaMontosProductos.to_f.round(2)
        puts "El total es correcto", sumaMontosProductos
      else
        puts "El total no es correcto, debería ser: ", sumaMontosProductos
      end
      #      $DTH_INVENTARIO[:monto_combo1] =sumaMontosProductos
      numeroDeProductos = cantidadSku1ComboEscaleraNivel1.to_f.round(2) + cantidadSku2ComboEscaleraNivel1.to_f.round(2) + cantidadSku3ComboEscaleraNivel1.to_f.round(2)
      puts "Numero de productos es: ", numeroDeProductos.to_f


      unless descuentoMontoSkuComboEscaleraNivel1.nil?
        montoARestar = descuentoMontoSkuComboEscaleraNivel1.to_f.round(2) * numeroDeProductos
        #puts "IVAN AQUI ESTA EL MONTO A DESCONTAR DEL COMBO ESCALERA POR MONTO NIVEL 1, LINEA 2246"
        puts "Descuento nivel 1 ", montoARestar.to_f.round(2)
      end

      unless descuentoMontoSkuComboEscaleraNivel2.nil?
        montoARestar = descuentoMontoSkuComboEscaleraNivel2.to_f.round(2) * numeroDeProductos
        #puts "IVAN AQUI ESTA EL MONTO A DESCONTAR DEL COMBO ESCALERA POR MONTO NIVEL 2, LINEA 2252"

        puts "Descuento nivel 2 ", montoARestar
      end

      unless descuentoMontoSkuComboEscaleraNivel3.nil?
        montoARestar = descuentoMontoSkuComboEscaleraNivel3.to_f.round(2) * numeroDeProductos
        #puts "IVAN AQUI ESTA EL MONTO A DESCONTAR DEL COMBO ESCALERA POR MONTO NIVEL 3, LINEA 2259"

        puts "Descuento nivel 3 ", montoARestar.to_f.round(2)
      end

      restaMonto = total.to_f.round(2) - montoARestar.to_f.round(2)
      puts "El monto total con decuento es: ", restaMonto.round(2)

      descuentoApp = query(@@txt_cDescuento[:locator], :text)
      #  puts "Descuento de la app es: ", descuentoApp
      descuentoSeparado = descuentoApp.map {|descuentoSeparado| descuentoSeparado.delete('  $,Tabcdefghijklmnopqr¤stuvwxyz')}
      descuentoSeparado2 = descuentoSeparado.to_s.split("n")
      descuento = descuentoSeparado2[1].to_f

      if descuento.to_f.round(2) == restaMonto.to_f.round(2)
        puts "El descuento es correcto"
      else
        puts "El descuento no es correcto, deberia ser: ", restaMonto.round(2)
      end
      $DESCUENTO_TOTAL=$DESCUENTO_TOTAL+restaMonto
      sleep 2

      busca_nivel ="//*[contains(@text,'"+nivelEscalera+"')]"
      scroll_down_to_uielement(busca_nivel,
                               5,
                               'mate')
      boleanoNivel = mobile_element_exists(busca_nivel)
      if boleanoNivel == true
        puts "El nivel es correcto ", nivelEscalera
      else
        puts "El nivel es incorrecto, deberia ser: ", nivelEscalera
      end

      cajas1 = query(@@txt_cajas[:locator], :text)
      cajas2 = cajas1.to_s.split("n")
      cajas = cajas2[1].to_i
      puts "Las cajas en la app son: ", cajas.to_i


      if cajas.to_i == numeroDeProductos.to_i
        puts "El numero de cajas en la aplicacion es correcta"

      else
        puts "El numero de cajas en la aplicacion es incorrecta, deberia ser: ", numeroDeProductos.to_i
      end
    end
  end

##TERMINA COMB ESCALERA

#################################################################################################################

#################################################################################################################

  def obtenerCantidadPorNombreDelProductoDelCombo(nombreComboEstatico, nombreProducto)
    nombreComboEstatico = nombreComboEstatico.to_s
    nombreProducto = nombreProducto.to_s


    nombreProductoMapeo="(//*[@text='"+nombreComboEstatico+"']/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@text,'"+nombreProducto+"')]/following-sibling::*[@class='android.widget.EditText'])[1]"

    get_mobile_element_attribute nombreProductoMapeo, "text"
    ##agrega_productos_estaticos_inventario(nombreComboEstatico, nombreProducto, nombreProductoMapeo)
  end

  def clicAComboEstatico(nombreComboEstatico)

    clic = obtenerNombreComboEstatico(nombreComboEstatico)
    get_mobile_element clic
    tap_element clic

    return clic
  end

  def obtenerNombreComboEstatico(nombreComboEstatico)

    mapeoDeNombreCombo="//*[@text='"+nombreComboEstatico+"']/following-sibling::*[@class='android.widget.EditText']"


    get_mobile_element mapeoDeNombreCombo
    tap_element mapeoDeNombreCombo
    cantidadCombo = $dt_row_combo[:cantidad_combo]

    scroll_down_to_uielement(mapeoDeNombreCombo,
                             5,
                             'mate')
    sleep 1
    clear_text(mapeoDeNombreCombo)
    #  tap_element(mapeoDeNombreCombo)
    enter_text_uielement(mapeoDeNombreCombo, cantidadCombo, 'mate')
    press_user_action_button('search')

    return mapeoDeNombreCombo

  end

  def touchEnTipoCombo1()

    or_exec_uielement(@@check_nombre_combo_estatico)
    sleep 2

  end

  def validarDescuentosEnElCSV()
    sleep 5
    descuentoHHC1 = $dt_row_combo.select {|e| e.to_s == 'hhc_descuento'}.first
    descuentoHHC = descuentoHHC1[1]
    # puts "El descuentoHHC : ", descuentoHHC[1]

    if descuentoHHC == 'eventual'
      calcularDescuentoEVT()
    else
      if descuentoHHC == 'condicion comercial'
        calcularDescuentoCND()
      else
        if descuentoHHC == 'combinado'
          validarDosDescuentos()
        end
      end
    end
  end

  def validarDosDescuentos()

    sleep 5
    descuentos = Array.new
    descuentos = obtenerDescuentos()
    arrayContador = obtenerNumeroDeDescuentos()

    if existeDescuentoTipo(descuentos) == false
      raise("seleccionar_menus => Error no se encontró el valor.
             \nException => #{e.message}")
    end

    numeroDeAplicadosEVT = 0
    descuentoAcumulado = 0.0
    importeFinal = 0.0
    importeInicial = query(@@importeTotalDispositivo[:locator], :text)[0]
    importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])


    puts "El importe inicial es: ", importeInicial
    for i in (0..1)

      nombreDescuentoConfigurado = obtenerValorCsvPorColumna(
          i == 0 ? 'nombre_descuento_eventual'
          : 'nombre_descuento_condicion_comercial')

      posicionDescuentoDescripcion = seleccionarDescuentoPorDescripcion(nombreDescuentoConfigurado)

      productosAplicables = Array.new
      productosAplicables = obtenerProductosAplicables(arrayContador)

      marcarDescuentoPorPosicion(posicionDescuentoDescripcion)

      productosAplicados = Array.new

      if i == 0
        productosAplicados = obtenerProductosAplicados(0)
        numeroDeAplicadosEVT = productosAplicados.count
        puts "El numero de aplicados EVT es: ", numeroDeAplicadosEVT
      else
        productosAplicados = obtenerProductosAplicados(numeroDeAplicadosEVT)
        numeroDeAplicadosCND = productosAplicados.count
        puts "El numero de aplicados de CND es: ", numeroDeAplicadosCND
      end


      descuentoObj = descuentos.select {|des| des.descripcion == nombreDescuentoConfigurado}.first


      columnaTipoDescuento = obtenerValorCsvPorColumna(
          i == 0 ? 'tipo_descuento_eventual'
          : 'tipo_descuento_condicion_comercial')

      if columnaTipoDescuento == '%'

        descuentoAcumulado = descuentoAcumulado +
            validarDescuentosPorProductosAplicablesYAplicadosPorPorcentaje(productosAplicables,
                                                                           productosAplicados,
                                                                           descuentoObj,
                                                                           i == 0 ? 'descuento_eventual'
                                                                           : 'descuento_condicion_comercial')
      else
        if columnaTipoDescuento == '$'

          descuentoAcumulado = descuentoAcumulado +
              validarDescuentosPorProductosAplicablesYAplicadosPorPesos(productosAplicables,
                                                                        productosAplicados,
                                                                        descuentoObj,
                                                                        i == 0 ? 'descuento_eventual'
                                                                        : 'descuento_condicion_comercial')
        end
      end
    end

    #IVAN AQUÍ ESTÁ EL DESCUENTO ACUMULADO CUANDO LOS DOS DESCUENTOS SON ELEGIDOS LINEA 1599
    puts "El IMPORTE INCIAL ES: ", importeInicial, " EL DESCUENTO ACUMULADO ES: ", descuentoAcumulado
    importeFinal = importeInicial[0].to_f + descuentoAcumulado
    puts "El importe inicial menos el descuento acumulado es: ", importeFinal

    importeTotalDispositivoConDescuentos = query(@@importeTotalDispositivo[:locator], :text)
    importeTotalDispositivoConDescuentos = quitarCaracteres(importeTotalDispositivoConDescuentos.to_s.split("$")[1])


    #puts "El importe inicial es: ", importeInicial

    if importeFinal == importeTotalDispositivoConDescuentos[0].to_f
      puts "El importe final es correcto ", importeFinal
    else
      puts "El importe final es INCORRECTO, debería ser ", importeTotalDispositivoConDescuentos, " pero es: ", importeFinal
    end
  end

  def calcularDescuentoCND()
    sleep 5
    descuentos = Array.new
    descuentos = obtenerDescuentos()
    arrayContador = obtenerNumeroDeDescuentos()


    if existeDescuentoTipo(descuentos) == false
      raise("seleccionar_menus => Error no se encontró el valor.
             \nException => #{e.message}")
    end

    nombreDescuentoConfigurado = obtenerValorCsvPorColumna('nombre_descuento_condicion_comercial')
    posicionDescuentoDescripcion = seleccionarDescuentoPorDescripcion(nombreDescuentoConfigurado)

    productosAplicables = Array.new
    productosAplicables = obtenerProductosAplicables(arrayContador)

    importeInicial = query(@@importeTotalDispositivo[:locator], :text)
    #puts "El importe inicial es: ", importeInicial

    marcarDescuentoPorPosicion(posicionDescuentoDescripcion)

    productosAplicados = Array.new
    productosAplicados = obtenerProductosAplicados(0)

    descuentoObj = descuentos.select {|des| des.descripcion == nombreDescuentoConfigurado}.first

    columnaTipoDescuento = obtenerValorCsvPorColumna('tipo_descuento_condicion_comercial')

    if columnaTipoDescuento == '%'

      descuentoAcumulado =
          descuentoAcumulado =
              validarDescuentosPorProductosAplicablesYAplicadosPorPorcentaje(productosAplicables,
                                                                             productosAplicados,
                                                                             descuentoObj,
                                                                             'descuento_condicion_comercial'

              )
      importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])
      validarImporteTotal(descuentoAcumulado, importeInicial[0].to_f)

    else
      if columnaTipoDescuento == '$'

        descuentoAcumulado =
            validarDescuentosPorProductosAplicablesYAplicadosPorPesos(productosAplicables,
                                                                      productosAplicados,
                                                                      descuentoObj,
                                                                      'descuento_condicion_comercial'
            )
        importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])
        validarImporteTotal(descuentoAcumulado, importeInicial[0].to_f)

      end
    end
  end

  def calcularDescuentoEVT()
    sleep 5
    descuentos = Array.new
    descuentos = obtenerDescuentos()
    arrayContador = obtenerNumeroDeDescuentos()


    if existeDescuentoTipo(descuentos) == false
      raise("seleccionar_menus => Error no se encontró el valor.
             \nException => #{e.message}")
    end

    nombreDescuentoConfigurado = obtenerValorCsvPorColumna('nombre_descuento_eventual')
    posicionDescuentoDescripcion = seleccionarDescuentoPorDescripcion(nombreDescuentoConfigurado)

    productosAplicables = Array.new
    productosAplicables = obtenerProductosAplicables(arrayContador)

    importeInicial = query(@@importeTotalDispositivo[:locator], :text)
    #puts "El importe inicial es: ", importeInicial

    marcarDescuentoPorPosicion(posicionDescuentoDescripcion)

    productosAplicados = Array.new
    productosAplicados = obtenerProductosAplicados(0)

    descuentoObj = descuentos.select {|des| des.descripcion == nombreDescuentoConfigurado}.first
    #  puts "***************descuentoObj es: ", descuentoObj

    columnaTipoDescuento = obtenerValorCsvPorColumna('tipo_descuento_eventual')

    if columnaTipoDescuento == '%'

      descuentoAcumulado =
          validarDescuentosPorProductosAplicablesYAplicadosPorPorcentaje(productosAplicables,
                                                                         productosAplicados,
                                                                         descuentoObj,
                                                                         'descuento_eventual'
          )
      #importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])
      #validarImporteTotal(descuentoAcumulado, importeInicial[0].to_f)

    else
      if columnaTipoDescuento == '$'

        descuentoAcumulado =
            validarDescuentosPorProductosAplicablesYAplicadosPorPesos(productosAplicables,
                                                                      productosAplicados,
                                                                      descuentoObj,
                                                                      'descuento_eventual'
            )
        # importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])
        # validarImporteTotal(descuentoAcumulado, importeInicial[0].to_f)

      end
    end
    importeInicial = quitarCaracteres(importeInicial.to_s.split("$")[1])
    validarImporteTotal(descuentoAcumulado, importeInicial[0].to_f)
  end

  def obtenerNumeroDeDescuentos()

    checkDescuento = Array.new
    checkDescuento = query(@@btn_check_descuento[:locator])
    # puts "El elemento de los checks es: ", checkDescuento

    countArray = checkDescuento.count

    return countArray
  end

  def validarImporteTotal(descuentoAcumulado, importeInicial)
    importeTotalDispositivoLocalizado = query(@@importeTotalDispositivo[:locator], :text)
    importeTotalDispositivoSeparado = quitarCaracteres(importeTotalDispositivoLocalizado[0])
    importeTotalDispositivo = importeTotalDispositivoSeparado[0]
    importeFinal = importeInicial - descuentoAcumulado.abs

    #   puts "El importe inicial es: ", importeInicial, " EL importe del dispositivo es: ", importeTotalDispositivo.to_f, " el importe final es: ", importeFinal, " el importe total es: ", importeTotal
    #IVAN AQUI ESTA EL DESCUENTO DE LOS PRODUCTOS EN DESCUENTO EVENTUAL Y CONDICION COMERCIAL, VARIABLE descuentoAcumulado linea 1708
    puts "El importe inicial es: ", importeInicial, " menos el descuento acumulado es: ",
         descuentoAcumulado, " El importe final es: ", importeFinal
    puts "El importe es correcto"

    unless importeTotalDispositivo.to_f == importeFinal
      raise("validarImporteTotal => Error el valor no es correcto.")
    end

  end

  def validarDescuentosPorProductosAplicablesYAplicadosPorPorcentaje(productosAplicables,
                                                                     productosAplicados,
                                                                     descuentoObj,
                                                                     columaDescuento)


    descuentoConfigurado = obtenerValorCsvPorColumna(columaDescuento)
    contador = 0
    descuentoAcumulado = 0.0
    cantidadMaximaRestante = 0
    productosAplicables.each do |itemProductoAplicable|
      if contador == 0
        cantidadMaximaRestante = descuentoObj.cantidadMaxima.to_i
      #  puts " ********------------- cantidadMaximaRestante es: ", cantidadMaximaRestante
      end
      #puts "LINEA 1970 cantidadMaximaRestante es: ", cantidadMaximaRestante
      if cantidadMaximaRestante == 0
       # puts "------descuentoAcumulado es: ", descuentoAcumulado
        return descuentoAcumulado
      end

#puts "[[[[[[[[[[[[ cantidadMaximaRestante es: ", cantidadMaximaRestante, " productosAplicados[contador].aplicado.to_i es: ", productosAplicados[contador].aplicado.to_i

cantidadMaximaRestante = cantidadMaximaRestante - productosAplicados[contador].aplicado.to_i

#puts "[[[[[[[[[[[[ cantidadMaximaRestante es: ", cantidadMaximaRestante, " productosAplicados[contador].aplicado.to_i es: ", productosAplicados[contador].aplicado.to_i


      importeTotalAplicable = itemProductoAplicable.importe.to_s
      puts "El importe APLICABLE es: ", importeTotalAplicable
      importeTotalAplicable = quitarCaracteres(importeTotalAplicable)
      importeTotalAplicableF = importeTotalAplicable[0].to_f

      importePorProducto = importeTotalAplicableF / itemProductoAplicable.cantidad.to_i
      importeTotalCalculado = 0.0

      puts "El importe por producto es: ", importePorProducto


      if itemProductoAplicable.cantidad.to_i > cantidadMaximaRestante
        importeTotalCalculado = importePorProducto * cantidadMaximaRestante
        puts "CUANDO LA CANTIDAD ES MAYOR AL MAXIMO. La cantidad es:  ", itemProductoAplicable.cantidad.to_i,
             " el maximo es: ", cantidadMaximaRestante
      else
        importeTotalCalculado = importePorProducto * itemProductoAplicable.cantidad.to_i
        puts "CUANDO LA CANTIDAD ES MENOR AL MAXIMO"
      end
      #
      # #  if itemProductoAplicable.cantidad.to_i > descuentoObj.cantidadMaxima.to_i
      #   importeTotalCalculado = importePorProducto* descuentoObj.cantidadMaxima.to_i
      #  puts "CUANDO LA CANTIDAD ES MAYOR AL MAXIMO. La cantidad es:  ", itemProductoAplicable.cantidad.to_i,
      #      " el maximo es: ", descuentoObj.cantidadMaxima.to_i
      # itemProductoAplicable.cantidad.to_i = descuentoObj.cantidadMaxima.to_i
      # puts "itemProductoAplicable.cantidad.to_i asignación es: ", itemProductoAplicable.cantidad.to_i
      # else
      #  importeTotalCalculado = importePorProducto * itemProductoAplicable.cantidad.to_i
      #  puts "CUANDO LA CANTIDAD ES MENOR AL MAXIMO"
      # end
      puts "El importe Total calculado es: ", importeTotalCalculado

      #  descuentoARestar = (descuentoConfigurado.to_f / 100) * importeTotalCalculado
      #  puts "El descuento a restar es", descuentoARestar
      # importeCalculado = importeTotalCalculado - descuentoARestar.to_f


      inversoDescuento = (100 - descuentoConfigurado.to_f) * 0.01
      puts "El inverso descuento es: ", inversoDescuento

      importeCalculado = importePorProducto * inversoDescuento

      importeAplicado = quitarCaracteres(productosAplicados[contador].importe.to_s)
      puts "El importe aplicado auxiliar es: ", importeAplicado
      importeTotalAplicado = importeAplicado[0].to_f
    #  puts "**********importeTotalAplicado es: ", importeTotalAplicado

      importeTotalPorProductoConDescuento = importeCalculado * productosAplicados[contador].aplicado.to_i
     # puts "productosAplicados[contador].aplicado.to_i es: ", productosAplicados[contador].aplicado.to_i
     # puts "****************El importe total por producto con descuento es: ", importeTotalPorProductoConDescuento

      # puts "El importe total es: ", importeCalculado,
      # "El importe aplicado es: ", importeTotalAplicado,
      #"El importe calculado es: con round", importeCalculado.round(2)

      unless importeTotalPorProductoConDescuento.round(2) == importeTotalAplicado
        #  raise("validarDescuentosPorProductosAplicablesYAplicadosPorPorcentaje => Error el valor no es correcto.")
      end
      #  puts "El importe aplicado CON CICLO es: ", importeAplicado.to_f
      descuentoAcumulado = descuentoAcumulado - (importeTotalCalculado - importeTotalPorProductoConDescuento.to_f)
      contador = contador + 1
    end
    puts "El total acumulado es: ", descuentoAcumulado
    return descuentoAcumulado
  end

  def validarDescuentosPorProductosAplicablesYAplicadosPorPesos(productosAplicables,
                                                                productosAplicados,
                                                                descuentoObj,
                                                                columnaDescuento)

    descuentoConfigurado = obtenerValorCsvPorColumna(columnaDescuento)
    puts "El descuento configurado en el csv es: ", descuentoConfigurado, " la columna descuento es: ", columnaDescuento
    contador = 0
    descuentoAcumulado = 0.0
    productosAplicables.each do |itemProductoAplicable|

      importeTotalAplicable = itemProductoAplicable.importe.to_s
      puts "El importe APLICABLE es: ", importeTotalAplicable
      importeTotalAplicable = quitarCaracteres(importeTotalAplicable)
      importeTotalAplicableF = importeTotalAplicable[0].to_f

      importePorProducto = importeTotalAplicableF / itemProductoAplicable.cantidad.to_i
      importeTotalCalculado = 0.0

      puts "El precio del producto es: ", importePorProducto


      if itemProductoAplicable.cantidad.to_i > descuentoObj.cantidadMaxima.to_i
        importeTotalCalculado = importePorProducto * descuentoObj.cantidadMaxima.to_i
        puts "CUANDO LA CANTIDAD ES MAYOR AL MAXIMO. La cantidad es:  ", itemProductoAplicable.cantidad.to_i,
             " el maximo es: ", descuentoObj.cantidadMaxima.to_i
      else
        importeTotalCalculado = importePorProducto * itemProductoAplicable.cantidad.to_i
        puts "CUANDO LA CANTIDAD ES MENOR AL MAXIMO"

      end
      puts "El importe importeTotalCalculado es: ", importeTotalCalculado, " la cantidad de productos es: ", itemProductoAplicable.cantidad.to_i

      descuentoPorProducto = importePorProducto - descuentoConfigurado.to_f
      puts "El nuevo precio por producto con descuento es: ", descuentoPorProducto
      descuentoPorProductoPorCantidad = descuentoPorProducto * itemProductoAplicable.cantidad.to_i
      puts "El importe total con descuento es descuentoPorProductoPorCantidad: ", descuentoPorProductoPorCantidad

      importeAplicado = quitarCaracteres(productosAplicados[contador].importe.to_s)
      puts "El importe aplicado auxiliar es: ", importeAplicado
      importeTotalAplicado = importeAplicado[0].to_f
     # puts "importeTotalAplicado es: ", importeTotalAplicado

      # puts "El importe calculado es: con round", inversoDescuento.round(2),
      #     "El importe calculado es: ", inversoDescuento,
      #    "el importe aplicado es: ", importeTotalAplicado
      unless descuentoPorProductoPorCantidad.round(2) == importeTotalAplicado
        raise("validarDescuentosPorProductosAplicablesYAplicados => Error el valor no es correcto.")
      end
      #        puts "El importe aplicado CON CICLO es: ", importeAplicado.to_f
      descuentoAcumulado = descuentoAcumulado - (importeTotalCalculado - descuentoPorProductoPorCantidad.to_f)
      contador = contador + 1
    end
    puts "El total acumulado es: ", descuentoAcumulado
    return descuentoAcumulado
  end


  def seleccionarDescuentoPorDescripcion(descripcion)

    descuentoDescripcionElemento = Array.new
    descuentoDescripcionElemento = query(@@txt_descripcion_descuento[:locator])

    descuentoDescripcion = Array.new
    descuentoDescripcion = query(@@txt_descripcion_descuento[:locator], :text)

    contador = 0
    descuentoDescripcion.each do |itemDescuentoDescripcion|
      if itemDescuentoDescripcion.to_s.include? descripcion
        posicionDescuentoDescripcion = contador
        #   puts "La posicion del descuento es: ", posicionDescuentoDescripcion
        touch(descuentoDescripcionElemento[posicionDescuentoDescripcion])
        return posicionDescuentoDescripcion

      end
      contador = contador + 1
    end
  end

  def marcarDescuentoPorPosicion(posicion)
    #  puts "La posicion es: ", posicion
    checkDescuento = Array.new
    checkDescuento = query(@@btn_check_descuento[:locator])
    touch(checkDescuento[posicion])
  end

  def obtenerValorCsvPorColumna(columna)
    puts "El NOMBRE DE LA COLUMNA A BUSCAR ES:  ", columna
    valorObtenido = $dt_row_combo.select {|e| e.to_s == columna}.first

    #puts "EL ARRAY VALOR OBTENIDO ES :", valorObtenido
    puts "EL VALOR DE LA COLUMNA ES : ", valorObtenido[1]
    return valorObtenido[1]
  end

  def existeDescuentoTipo(descuentos)
    descuentos.each do |itemDescuento|
      if itemDescuento.tipo.nil?
        return false
      end
    end
    return true
  end

  def obtenerDescuentos()

    checkDescuento = Array.new
    checkDescuento = query(@@btn_check_descuento[:locator])
    puts "El elemento de los checks es: ", checkDescuento

    descripcionDescuento = Array.new
    descripcionDescuento = query(@@txt_descripcion_descuento[:locator], :text)
    puts "Los descuentos son: ", descripcionDescuento

    cantidadMinimaDescuento = Array.new
    cantidadMinimaDescuento = query(@@txt_cantidad_minima_descuento[:locator], :text)
    #puts "La cantidad minima del decuento es: ", cantidadMinimaDescuento

    cantidadMaximaDescuento = Array.new
    cantidadMaximaDescuento = query(@@txt_cantidad_maxima_descuento[:locator], :text)
    puts "La cantidad maxima del descuento es: ", cantidadMaximaDescuento

    tipoDescuento = Array.new
    tipoDescuento = query(@@txt_tipo_de_descuento[:locator], :text)
    #puts "El tipo de descuento es: ", tipoDescuento

    descuentoDescuento = Array.new
    descuentoDescuento = query(@@txt_descuento_valor_descuento[:locator], :text)
    #puts "El valor del descuento es: ", descuentoDescuento


    if cantidadMaximaDescuento == 'â^z'
      cantidadMaximaDescuento = '999999'
    end

    begin
      #  puts "Se puede castear el maximo a Integer"
      cantidadMaximaDescuento.to_i
    rescue
      # puts "No se puede castear el maximo a Integer, se convierte en '999999'"
      #  cantidadMaximaDescuento = '999999'
    end

    countArray = checkDescuento.count
    puts "Hay ", countArray, " descuentos para elegir"
    descuentos = Array.new
    for i in (0..countArray - 1)
      descuento = Descuento.new(checkDescuento[i], descripcionDescuento[i],
                                cantidadMinimaDescuento[i], cantidadMaximaDescuento[i],
                                tipoDescuento[i], descuentoDescuento[i])
      descuentos[i] = descuento
      # puts "Descripcion descuentos en el metodo obtenerDescuentos es: ", descripcionDescuento[i]
      # return countArray
      puts "La cantidad maxima del descuento en el metodo obtenerDescuentos es: ", cantidadMaximaDescuento[i]
    end
    return descuentos
  end

  def obtenerProductosAplicables(arrayContador)

    descripcionDeProductosAplicables = Array.new
    descripcionDeProductosAplicables = query(@@txt_descripcion_producto_aplicable[:locator], :text)
    # puts "La descripcion del producto aplicable es: ", descripcionDeProductosAplicables

    disponibleDeProductosAplicables = Array.new
    disponibleDeProductosAplicables = query(@@txt_disponible_producto_aplicable[:locator], :text)
    # puts "El disponible de productos aplicables es: ", disponibleDeProductosAplicables

    cantidadDeProductosAplicables = Array.new
    cantidadDeProductosAplicables = query(@@txt_cantidad_producto_aplicable[:locator], :text)
    # puts "La cantidad de productos aplicables es: ", cantidadDeProductosAplicables

    importeDeProductosAplicables = Array.new
    importeDeProductosAplicables = query(@@txt_importe_producto_aplicable[:locator], :text)
    #    puts "El importe de productos aplicables es: ", importeDeProductosAplicables

    contadorInicial = arrayContador
    #puts "El contador inicial en el metodo obtenerProductosAplicables es: ", contadorInicial

    countCantidad = cantidadDeProductosAplicables.count
    puts "Hay ", countCantidad, " productos aplicables"
    productosAplicables = Array.new


    for i in (0..countCantidad - 1)
      productoAplicable = ProductoAplicable.new(descripcionDeProductosAplicables[contadorInicial], disponibleDeProductosAplicables[i],
                                                cantidadDeProductosAplicables[i], importeDeProductosAplicables[i])
      productosAplicables[i] = productoAplicable

      puts "Los productos aplicables son : ", descripcionDeProductosAplicables[contadorInicial]
      contadorInicial = contadorInicial + 1
    end

    return productosAplicables
  end

  def obtenerProductosAplicados(arrayContador)

    descripcionDeProductosAplicados = Array.new
    descripcionDeProductosAplicados = query(@@txt_descripcion_producto_aplicado[:locator], :text)
    # puts "La descripcion del producto aplicado es: ", descripcionDeProductosAplicados

    aplicadoDeProductosAplicados = Array.new
    aplicadoDeProductosAplicados = query(@@txt_aplicado_producto_aplicado[:locator], :text)
    # puts "El producto aplicado es: ", aplicadoDeProductosAplicados

    importeDeProductosAplicados = Array.new
    importeDeProductosAplicados = query(@@txt_importe_producto_aplicado[:locator], :text)
    #    puts "El importe de productos aplicados es: ", importeDeProductosAplicados

    puts "Array contador en onbtenerProductosAplicados es: ", arrayContador
    if arrayContador == 0

      countAplicado = aplicadoDeProductosAplicados.count
      puts "Hay ", countAplicado, " productos aplicados"

      productosAplicados = Array.new

      for i in (0..countAplicado - 1)
        productoAplicado = ProductoAplicado.new(descripcionDeProductosAplicados[i], aplicadoDeProductosAplicados[i],
                                                importeDeProductosAplicados[i])
        productosAplicados[i] = productoAplicado
      end
    else
      productosAplicados = Array.new

      contadorAplicado = 0
      countAplicado = arrayContador
      # for i in (countAplicado.. aplicadoDeProductosAplicados.count - countAplicado)
      while countAplicado <= aplicadoDeProductosAplicados.count - countAplicado
        productoAplicado = ProductoAplicado.new(descripcionDeProductosAplicados[countAplicado],
                                                aplicadoDeProductosAplicados[countAplicado],
                                                importeDeProductosAplicados[countAplicado])
        productosAplicados[contadorAplicado] = productoAplicado

        puts "Los productos aplicados son : ", descripcionDeProductosAplicados[countAplicado]

        countAplicado += 1
        contadorAplicado += 1
      end
    end
    return productosAplicados
  end

  def quitarCaracteres(cadena)

    #  rgx = '$,'
    #  cadenaSeparada = cadena.gsub!(rgx)
    #  puts 'Cadena separada ', cadenaSeparada

    cadenaSeparada = cadena.split.map {|x| x.delete(' $,Tabcdefghijklmnopqr¤stuvwxyz')}

    return cadenaSeparada
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################
end



