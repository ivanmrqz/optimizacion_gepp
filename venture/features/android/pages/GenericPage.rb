# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class GenericPage < Calabash::ABase


  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_generic'
  @@btn_siguiente = get_or_data(@or_file, 'btn_siguiente')
  @@btn_popup_confirmacion_si = get_or_data(@or_file, 'btn_popup_confirmacion_si')
  @@btn_permitir= get_or_data(@or_file, 'btn_permitir')
  @@btn_sig= get_or_data(@or_file, 'btn_sig')
  @@mensaje_cobro_embase= get_or_data(@or_file, 'mensaje_cobro_embase')
  @@mensaje_cobro_cajilla= get_or_data(@or_file, 'mensaje_cobro_cajilla')
  @@btn_cobro_envase= get_or_data(@or_file, 'btn_cobro_envase')
  @@btn_cobro_envase_garrafon= get_or_data(@or_file, 'btn_cobro_envase_garrafon')
  @@btn_cobro_cajilla= get_or_data(@or_file, 'btn_cobro_cajilla')
  @@lbl_prestamo_retornables= get_or_data(@or_file, 'lbl_prestamo_retornables')
  @@txt_sdo= get_or_data(@or_file, 'txt_sdo')
  @@txt_descripcionEnvase= get_or_data(@or_file, 'txt_descripcionEnvase')
  @@btn_popup= get_or_data(@or_file, 'btn_popup')
  @@txt_ticket= get_or_data(@or_file, 'txt_ticket')
  #############################################################################
  # ACCIONES
  #############################################################################

  #HACE CLICK EN BTN "PERMITIR" PERMISO DE LA APP SI ESTE APARECE
  def permisos_aplicacion
    exec_adb "shell pm grant com.sismov.hebadomandroid android.permission.ACCESS_FINE_LOCATION"
    exec_adb "shell pm grant com.sismov.hebadomandroid android.permission.ACCESS_COARSE_LOCATION"
    if wait_for_mobile_element($gen_permisos_btn_permitir, 2000)
      tap_element($gen_permisos_btn_permitir)
    end
  end


  def touch_btn_siguiente
    assert_for_element_exist(@@btn_siguiente[:locator], 10)
    sleep 4
    or_exec_uielement(@@btn_siguiente)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'SIGUIENTE'.
             \nException => #{e.message}")
  end


  def popup_confirmar_si
    or_exec_uielement(@@btn_popup_confirmacion_si)

  rescue Exception => e
    raise("popup_confirmar_si => Error al confirmar mensaje emergente.
             \nException => #{e.message}")
  end


  def touch_btn_permitir
    or_exec_uielement(@@btn_permitir)
  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'Permitir'.")

    def touch_btn_siguiente
      or_exec_uielement(@@btn_siguiente)
    rescue Exception => e
      raise("touch_btn_siguiente => Error al hacer touch en boton 'SIGUIENTE'.
             \nException => #{e.message}")
    end
  end

  def touch_btn_sig
    or_exec_uielement(@@btn_sig)
  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'SIGUIENTE'.
             \nException => #{e.message}")
  end

  def btn_cobro_envase
    or_exec_uielement(@@btn_cobro_envase)
  rescue Exception => e
    raise("btn_cobro_envase => Error al hacer touch en boton 'SI.
             \nException => #{e.message}")
    sleep 2
  end

  def btn_cobro_envase_garrafon
    or_exec_uielement(@@btn_cobro_envase_garrafon)
  rescue Exception => e
    raise("btn_cobro_envase_garrafon => Error al hacer touch en boton 'SI.
             \nException => #{e.message}")
    sleep 2
  end

  def btn_cobro_cajilla
    or_exec_uielement(@@btn_cobro_cajilla)
  rescue Exception => e
    raise("btn_cobro_cajilla => Error al hacer touch en boton 'SI.
             \nException => #{e.message}")
    sleep 2
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################


  def obtener_cobro_embase_garrafon
    sleep 1
    # assert_for_element_exist(@@mensaje_cobro_embase, 5)

    montoGRBq=query(@@mensaje_cobro_embase[:locator], :text)
    montoGRBA = montoGRBq[0].split("$")
    #  montoGRBSeparado =montoGRBA.map{|montoGRBSeparado| montoGRBSeparado.delete(',')}
    $DTH_INVENTARIO[:envase] = $montoGRB
    $montoGRB = montoGRBA[1].to_f
    $importe=$importe+$montoGRB


    puts "El cobro de envase es de:  ", $montoGRB
    puts "El nuevo importe es de:  ", $importe


  end

  def obtener_cobro_cajilla
    sleep 1
    # assert_for_element_exist(@@mensaje_cobro_embase, 5)

    montoGRBq=query(@@mensaje_cobro_cajilla[:locator], :text)
    montoGRBA = montoGRBq[0].split("$")
    montoGRBSeparado =montoGRBA.map {|montoGRBSeparado| montoGRBSeparado.delete(',')}

    $montoGRB = montoGRBSeparado[1].to_f
    $importe=$montoGRB+$importe

    puts "El cobro de envase es de: ", $montoGRB


  end

  def suma_cobros
    sleep 1
    # assert_for_element_exist(@@mensaje_cobro_embase, 5)

    montos=query(@@mensaje_cobro_embase[:locator], :text)
    split1 = montos.to_s.split("\\n")
    split2 = split1[1].to_s.split("$")
    split3 = split1[2].to_s.split("$")
    split2F = split2[1].to_f
    split3F = split3[1].to_f
    $sumaMontosPrestamos = split2F.to_f + split3F.to_f
    puts "La suma de los cobros del mensaje son: ", $sumaMontosPrestamos
    $importe=$importe+$sumaMontosPrestamos
    puts $importe

  end


  def lbl_prestamo_retornable
    sleep 2
    assert_for_element_exist(@@lbl_prestamo_retornables[:locator], 10)
  end

  def txt_mensaje_grb
    sleep 2

    disponibleGRB1 = $dt_row[:disponible_envase_grb_1]
    disponibleGRB = disponibleGRB1.to_i #0
    #  puts "El límite de GRB del cliente es: ", disponibleGRB #0
    cantidadGRB1 = $dt_row[:caja_refresco_grb_1] #3
    cantidadGRB2 = $dt_row[:caja_refresco_grb_2] #2
    sumaCantidadGRB = cantidadGRB1.to_i + cantidadGRB2.to_i #5
    cantidadGRB = sumaCantidadGRB.to_i #5
    # puts "Los productos de GRB son: ", cantidadGRB
    mensajeGRB = query(@@mensaje_cobro_embase[:locator], :text)

    excedidoGRB1 = cantidadGRB.to_i - disponibleGRB.to_i #5-0 =5
    excedidoGRB = excedidoGRB1.to_i #5
    #   puts "EL excedido de GRB del cliente es: ", excedidoGRB #5

    mensajeSeparado1 = mensajeGRB[0].split("mite de ")
    mensajeSeparado2 = mensajeSeparado1[1].split(" en ")
    mensajeLimiteGRB = mensajeSeparado2[0].to_i
    #   puts "El límite de GRB es: ", mensajeLimiteGRB.to_i #0 disponible
    mensajeSeparado3 = mensajeSeparado2[2].split(" y ")
    mensajeExcedidoGRB = mensajeSeparado3[0].to_i
    #   puts "El excedido de GRB es: ", mensajeExcedidoGRB.to_i #5 cobrado
    mensajeSeparado4 = mensajeSeparado3[1].split("$")
    $mensajeCobroGRB = mensajeSeparado4[1].to_i
    #   puts "El cobro de GRB es: ", $mensajeCobroGRB.to_i #320


    if mensajeLimiteGRB.to_i == disponibleGRB.to_i #ok
      puts "El limite de GRB coincide: ", disponibleGRB.to_i

    else
      puts "El limite de GRB no coincide, debería ser: ", disponibleGRB.to_i
    end

    if mensajeExcedidoGRB.to_i == excedidoGRB.to_i

      puts "El excedido de GRB coincide: ", excedidoGRB.to_i

    else
      puts "El excedido de GRB no coincide, debería ser: ", excedidoGRB.to_i
    end


    $DTH_INVENTARIO[:cobro_envase_grb] = $mensajeCobroGRB
    $importe=$importe + $mensajeCobroGRB
    puts "El cobro de envase es de: ", $mensajeCobroGRB
    puts "el nuevo importe es  #{$importe}"


  end


  def txt_mensaje_cajilla
    sleep 2

    disponibleCajilla1 = $dt_row[:disponible_cajilla]
    disponibleCajilla = disponibleCajilla1.to_i #0
    #  puts "El límite de GRB del cliente es: ", disponibleGRB #4
    cantidadCajilla1 = $dt_row[:cantidad_prestamo_cajilla_1] #5
    #  cantidadCajilla2 = $dt_row[:caja_garrafon_2]#5
    sumaCantidadCajilla = cantidadCajilla1.to_i #10
    cantidadCajilla = sumaCantidadCajilla.to_i #10
    # puts "Los productos de GRB son: ", cantidadGRB
    mensajeCajilla = query(@@mensaje_cobro_cajilla[:locator], :text)

    excedidoCajilla1 = cantidadCajilla.to_i - disponibleCajilla.to_i #6
    excedidoCajilla = excedidoCajilla1.to_i #6
    #   puts "EL excedido de GRB del cliente es: ", excedidoGRB #6

    mensaje1 = mensajeCajilla.to_s.split("Caja ")
    mensaje2 = mensaje1[1].to_s.split(",")
    mensajeLimiteCajilla = mensaje2[0].to_i
    # puts "el limite de cajilla es: ", mensajeLimiteCajilla
    mensaje3 = mensaje2[1].to_s.split("en ")
    mensaje4 = mensaje3[1].to_s.split(" debe")
    mensajeExcedidoCajilla = mensaje4[0].to_i
    # puts "el excedido de cajilla es: ", mensajeExcedidoCajilla
    mensaje5= mensaje4[1].to_s.split("$")
    $mensajeCobroCajilla = mensaje5[1].to_f
    # puts "el cobro del mensja es: ", $mensajeCobroCajilla


    if mensajeLimiteCajilla.to_i == disponibleCajilla.to_i
      puts "El limite de cajilla coincide: ", disponibleCajilla.to_i

    else
      puts "El limite de cajilla no coincide, debería ser: ", disponibleCajilla.to_i
    end

    if mensajeExcedidoCajilla.to_i == excedidoCajilla.to_i

      puts "El excedido de cajilla coincide: ", excedidoCajilla.to_i

    else
      puts "El excedido de cajilla no coincide, debería ser: ", excedidoCajilla.to_i
    end


    $DTH_INVENTARIO[:cobro_cajilla] = $mensajeCobroCajilla
    $importe=$importe+$mensajeCobroCajilla
    puts "El cobro de envase es de: ", $mensajeCobroCajilla
    puts "el nuevo importe es  #{$importe}"


  end

  def txt_mensaje_garrafon
    sleep 2

    disponibleGarrafon1 = $dt_row[:disponible_envase_garrafon]
    disponibleGarrafon = disponibleGarrafon1.to_i #4
    #  puts "El límite de GRB del cliente es: ", disponibleGRB #4
    cantidadGarrafon1 = $dt_row[:caja_garrafon_1] #5
    cantidadGarrafon2 = $dt_row[:caja_garrafon_2] #5
    sumaCantidadGarrafon = cantidadGarrafon1.to_i + cantidadGarrafon2.to_i #10
    cantidadGarrafon = sumaCantidadGarrafon.to_i #10
    # puts "Los productos de GRB son: ", cantidadGRB
    mensajeGarrafon = query(@@mensaje_cobro_embase[:locator], :text)

    excedidoGarrafon1 = cantidadGarrafon.to_i - disponibleGarrafon.to_i #6
    excedidoGarrafon = excedidoGarrafon1.to_i #6
    #   puts "EL excedido de GRB del cliente es: ", excedidoGRB #6

    mensajeSeparado1 = mensajeGarrafon[0].split("mite de ")
    mensajeSeparado2 = mensajeSeparado1[1].split(" en ")
    mensajeLimiteGarrafon = mensajeSeparado2[0].to_i
    #   puts "El límite de GRB es: ", mensajeLimiteGRB.to_i #4 disponible
    mensajeSeparado3 = mensajeSeparado2[2].split(" y ")
    mensajeExcedidoGarrafon = mensajeSeparado3[0].to_i
    #   puts "El excedido de GRB es: ", mensajeExcedidoGRB.to_i #6 cobrado
    mensajeSeparado4 = mensajeSeparado3[1].split("$")
    $mensajeCobroGarrafon = mensajeSeparado4[1].to_i
    #   puts "El cobro de GRB es: ", $mensajeCobroGRB.to_i #900


    if mensajeLimiteGarrafon.to_i == disponibleGarrafon.to_i
      puts "El limite de garrafon coincide: ", disponibleGarrafon.to_i

    else
      puts "El limite de garrafon no coincide, debería ser: ", disponibleGarrafon.to_i
    end

    if mensajeExcedidoGarrafon.to_i == excedidoGarrafon.to_i

      puts "El excedido de garrafon coincide: ", excedidoGarrafon.to_i

    else
      puts "El excedido de garrafon no coincide, debería ser: ", excedidoGarrafon.to_i
    end


    $DTH_INVENTARIO[:cobro_envase_garrafon] = $mensajeCobroGarrafon
    $importe=$importe+$mensajeCobroGarrafon
    puts "El cobro de envase es de: ", $mensajeCobroGarrafon
    puts "el nuevo importe es  #{$importe}"


  end


  def txt_mensaje_grb_y_garrafon

    ##COBRO CAJILLA

    sleep 2

    disponibleGRB1 = $dt_row[:disponible_envase_grb_1]
    disponibleGRB = disponibleGRB1.to_i #0
    #  puts "El límite de GRB del cliente es: ", disponibleGRB #0
    cantidadGRB1 = $dt_row[:caja_refresco_grb_1] #3
    cantidadGRB2 = $dt_row[:caja_refresco_grb_2] #2
    sumaCantidadGRB = cantidadGRB1.to_i + cantidadGRB2.to_i #5
    cantidadGRB = sumaCantidadGRB.to_i #5
    # puts "Los productos de GRB son: ", cantidadGRB
    mensajeGRB = query(@@mensaje_cobro_embase[:locator], :text)

    excedidoGRB1 = cantidadGRB.to_i - disponibleGRB.to_i #5-0 =5
    excedidoGRB = excedidoGRB1.to_i #5
    #   puts "EL excedido de GRB del cliente es: ", excedidoGRB #5

    mensaje1 = mensajeGRB.to_s.split("de ")
    mensaje2 = mensaje1[1].to_s.split(" en")
    mensajeLimiteGRB = mensaje2[0].to_i
    # puts "El límite de GRB es: ", mensajeLimiteGRB #0
    mensaje3 = mensaje2[2].to_s.split(" ")
    mensajeExcedidoGRB = mensaje3[0].to_i
    # puts "El excedido de GRB es: ", mensajeExcedidoGRB #20
    mensaje3[4].to_s.split("$")
    mensaje5 = mensaje3[4].to_s.split("$")
    $mensajeCobroGRB = mensaje5[1].to_f
    # puts "El cobro de GRB es: ", $mensajeCobroGRB #600


    if mensajeLimiteGRB.to_i == disponibleGRB.to_i #ok
      puts "El limite de GRB coincide: ", disponibleGRB.to_i

    else
      puts "El limite de GRB no coincide, debería ser: ", disponibleGRB.to_i
    end

    if mensajeExcedidoGRB.to_i == excedidoGRB.to_i

      puts "El excedido de GRB coincide: ", excedidoGRB.to_i

    else
      puts "El excedido de GRB no coincide, debería ser: ", excedidoGRB.to_i
    end

    ##COBRO GARRAFON

    sleep 2

    disponibleGarrafon1 = $dt_row[:disponible_envase_garrafon]
    disponibleGarrafon = disponibleGarrafon1.to_i #0
    #  puts "El límite de GRB del cliente es: ", disponibleGRB #0
    cantidadGarrafon1 = $dt_row[:caja_garrafon_1] #10
    cantidadGarrafon2 = $dt_row[:caja_garrafon_2] #5
    sumaCantidadGarrafon = cantidadGarrafon1.to_i + cantidadGarrafon2.to_i #15
    cantidadGarrafon = sumaCantidadGarrafon.to_i #15
    # puts "Los productos de GRB son: ", cantidadGRB
    #  mensajeGarrafon = query(@@mensaje_cobro_embase[:locator], :text)

    excedidoGarrafon1 = cantidadGarrafon.to_i - disponibleGarrafon.to_i #15
    excedidoGarrafon = excedidoGarrafon1.to_i #15
    #   puts "EL excedido de GRB del cliente es: ", excedidoGRB #15

    mensaje6 = mensaje1[2].to_s.split(" en")
    mensajeLimiteGarrafon = mensaje6[0].to_i
    # puts "El limite de Garrafon es: ", mensajeLimiteGarrafon #0
    mensaje7 = mensaje6[2].to_s.split(" y")
    mensajeExcedidoGarrafon = mensaje7[0].to_i
    # puts "El excedido de garrafon es: ", mensajeExcedidoGarrafon #15
    mensaje8 = mensaje7[1].to_s.split("$")
    $mensajeCobroGarrafon = mensaje8[1].to_f
    # puts "El cobro de Garrafon es: ", $mensajeCobroGarrafon #960


    if mensajeLimiteGarrafon.to_i == disponibleGarrafon.to_i
      puts "El limite de garrafon coincide: ", disponibleGarrafon.to_i

    else
      puts "El limite de garrafon no coincide, debería ser: ", disponibleGarrafon.to_i
    end

    if mensajeExcedidoGarrafon.to_i == excedidoGarrafon.to_i

      puts "El excedido de garrafon coincide: ", excedidoGarrafon.to_i

    else
      puts "El excedido de garrafon no coincide, debería ser: ", excedidoGarrafon.to_i
    end

    $sumaCobrosGRByGarrafon = $mensajeCobroGRB.to_f + $mensajeCobroGarrafon.to_f
    puts "La suma de los cobros de GRB y Garrafon es: ", $sumaCobrosGRByGarrafon

    $importe = $importe + $sumaCobrosGRByGarrafon
    puts "El nuevo importe es de #{$importe}"

    $DTH_INVENTARIO[:cobro_envase_grb_y_garrafon] = $mensajeCobroGRB
    $importe=$importe + $mensajeCobroGRB
    puts "El cobro de envase es de: ", $mensajeCobroGRB
    puts "el nuevo importe es  #{$importe}"


    # $DTH_INVENTARIO[:envase] = $mensajeCobroGarrafon
    # $importe=$importe+$mensajeCobroGarrafon
    # puts "El cobro de envase es de: ", $mensajeCobroGarrafon
    # puts "el nuevo importe es  #{$importe}"


  end

  def validar_recuperacion_retornable
    grb1=$dt_row[:saldo_envase_grb_1].to_i
    grb2=$dt_row[:saldo_envase_grb_2].to_i
    garrafon1=$dt_row[:saldo_envase_garrafon].to_i
    suma_saldo=grb1+grb2+garrafon1

    if suma_saldo>0
      assert_for_element_exist(@@btn_popup[:locator], 10)
      or_exec_uielement(@@btn_popup)
    else
      puts "No se cuenta con saldo, no hay recuperacion de retornables"
    end


  end
end

