# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class Movimiento_diaPage< Calabash::ABase

  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_movimiento_dia'
  @@txt_pago_efectivo = get_or_data(@or_file, 'txt_pago_efectivo')
  @@txt_importe_en_cheques= get_or_data(@or_file, 'txt_importe_en_cheques')
  @@txt_importe_en_cupones= get_or_data(@or_file, 'txt_importe_en_cupones')
  @@txt_total_pagado= get_or_data(@or_file, 'txt_total_pagado')
  @@txt_ticket= get_or_data(@or_file, 'txt_ticket')
  #############################################################################
  # ACCIONES
  #############################################################################
  def validar_resumen_de_cuenta



    assert_for_element_exist(@@txt_pago_efectivo[:locator], 10, "Error => No se mostro la cantidad de pago en efectivo")
    assert_for_element_exist(@@txt_importe_en_cheques[:locator], 10, "Error => No se mostro la cantidad de importe en cheques")
    assert_for_element_exist(@@txt_importe_en_cupones[:locator], 10, "Error => No se mostro la cantidad de importe en cupones")
    assert_for_element_exist(@@txt_total_pagado[:locator], 10, "Error => No se mostro la cantidad de pago total")

    pago_efectivo=query(@@txt_pago_efectivo[:locator], :text).map {|importe| importe.delete('$,')}
    r_pago_efectivo=pago_efectivo[0].to_f
    importe_en_cheques=query(@@txt_importe_en_cheques[:locator], :text).map {|importe| importe.delete('$,')}
    r_importe_en_cheques=importe_en_cheques[0].to_f
    importe_en_cupones=query(@@txt_importe_en_cupones[:locator], :text).map {|importe| importe.delete('$,')}
    r_importe_en_cupones=importe_en_cupones[0].to_f
    total_pagado=query(@@txt_total_pagado[:locator], :text).map {|importe| importe.delete('$,')}
    r_total_pagado=total_pagado[0].to_f
    resultado=r_pago_efectivo+r_importe_en_cheques+r_importe_en_cupones

    #REAlIZO LA VALIDACIÓN DE PRESTAMO DE GRB Y GARRAFON
    disponible_grb=$dt_row[:disponible_envase_grb_1].to_i
    disponible_garrafon=$dt_row[:disponible_envase_garrafon].to_i
    cajas_grb=$dt_row[:caja_refresco_grb_2].to_i+$dt_row[:caja_refresco_grb_1].to_i
    cajas_garrafon=$dt_row[:caja_garrafon_1].to_i+$dt_row[:caja_garrafon_2].to_i
    bandera_grb=$dt_row[:bandera_grb].to_i


    tipo_bandera= case bandera_grb
                    when bandera_grb == 0
                      $DTH_INVENTARIO[:prestamo_envase_grb]=cajas_grb
                      $DTH_INVENTARIO[:prestamo_envase_garrafon]=cajas_grb
                    when bandera_grb > 0
                      prestamo_grb = cajas_grb - disponible_grb
                      prestamo_garrafon=cajas_garrafon-disponible_garrafon
                      if disponible_grb== 0
                        puts " se cobra el grb"
                      elsif prestamo_grb < 0
                        prestamo_grb_r=prestamo_grb*-1
                        $DTH_INVENTARIO[:prestamo_envase_grb]=prestamo_grb_r
                      else
                        $DTH_INVENTARIO[:prestamo_envase_grb]=prestamo_grb
                      end

                      if disponible_garrafon == 0
                        puts " se cobra el garrafon"
                      elsif prestamo_garrafon < 0
                        prestamo_garrafon_r=prestamo_garrafon*-1
                        $DTH_INVENTARIO[:prestamo_envase_garrafon]=prestamo_garrafon_r
                      else
                        $DTH_INVENTARIO[:prestamo_envase_garrafon]=prestamo_garrafon
                      end
                  end


    resultado=resultado.to_f+$pagos_anteriores_cr.to_f

    $DTH_INVENTARIO[:descuento_total]=$DESCUENTO_TOTAL.to_s.strip
    $DTH_INVENTARIO[:efectivo] = r_pago_efectivo.to_s.strip
    $DTH_INVENTARIO[:cheques] = r_importe_en_cheques.to_s.strip
    $DTH_INVENTARIO[:cupones] = r_importe_en_cupones.to_s.strip
    $DTH_INVENTARIO[:neto] = resultado.to_s.strip
    $DTH_INVENTARIO[:nombre_ticket]=$nombre_ticket.to_s
    $DTH_INVENTARIO[:devolucion_envase_grb]=$dt_row[:devolucion_grb_1].to_i+$dt_row[:devolucion_grb_2].to_i
    $DTH_INVENTARIO[:devolucion_garrafon]=$dt_row[:devolucion_garrafon_1].to_i+$dt_row[:devolucion_garrafon_2].to_i


    puts "el total neto  es de $#{resultado}"
    if r_total_pagado==resultado
      puts "El movimiento del dia es correcto"
    else
      raise "Error el resumen de cuenta no es correcto"
    end

    dt= get_csv_data 'dt_inventario'


    #Leer cada nuevo registro y agregarlo al CSV Original
    $DATOS.each_with_index do |fila, index|
      dt.push fila
      #ESCRIBIR CSV INVENTARIO
    end


    #ESCRIBIR CSV INVENTARIO
    exportHashToCsv(dt, 'dt_inventario')
    #Elimino la ultima fila de la hash para usar los mismos productos en las revisitas
    puts "hash-pop"
    $DATOS.pop()

  end

  def validar_resumen_de_cuenta_con_cr
    nud=$dt_row[:nud].to_s
    ticket=$dt_row[:ticket].to_s
    visita=$dt_row[:visita].to_s
    $nombre_ticket=nud+ticket+visita


    assert_for_element_exist(@@txt_pago_efectivo[:locator], 10, "Error => No se mostro la cantidad de pago en efectivo")
    assert_for_element_exist(@@txt_importe_en_cheques[:locator], 10, "Error => No se mostro la cantidad de importe en cheques")
    assert_for_element_exist(@@txt_importe_en_cupones[:locator], 10, "Error => No se mostro la cantidad de importe en cupones")
    assert_for_element_exist(@@txt_total_pagado[:locator], 10, "Error => No se mostro la cantidad de pago total")

    pago_efectivo=query(@@txt_pago_efectivo[:locator], :text).map {|importe| importe.delete('$,')}
    r_pago_efectivo=pago_efectivo[0].to_f
    importe_en_cheques=query(@@txt_importe_en_cheques[:locator], :text).map {|importe| importe.delete('$,')}
    r_importe_en_cheques=importe_en_cheques[0].to_f
    importe_en_cupones=query(@@txt_importe_en_cupones[:locator], :text).map {|importe| importe.delete('$,')}
    r_importe_en_cupones=importe_en_cupones[0].to_f
    total_pagado=query(@@txt_total_pagado[:locator], :text).map {|importe| importe.delete('$,')}
    r_total_pagado=total_pagado[0].to_f


    #REAlIZO LA VALIDACIÓN DE PRESTAMO DE GRB Y GARRAFON
    disponible_grb=$dt_row[:disponible_envase_grb_1].to_i
    disponible_garrafon=$dt_row[:disponible_envase_garrafon].to_i
    cajas_grb=$dt_row[:caja_refresco_grb_2].to_i+$dt_row[:caja_refresco_grb_1].to_i
    cajas_garrafon=$dt_row[:caja_garrafon_1].to_i+$dt_row[:caja_garrafon_2].to_i

    puts "cajas grb", cajas_grb
    puts "disponible garrafon", cajas_garrafon
    puts "disponible grb", disponible_grb
    bandera_grb=$dt_row[:bandera_grb].to_s
    prestamo_grb=cajas_grb-disponible_grb
    puts "prestamo ggrb es ", prestamo_grb
    tipo_bandera = case bandera_grb

                      when"0"
                        $DTH_INVENTARIO[:prestamo_envase_grb]=cajas_grb
                        $DTH_INVENTARIO[:prestamo_envase_garrafon] = cajas_garrafon
                     when"2"
                       if cajas_grb > disponible_grb
                         $DTH_INVENTARIO[:prestamo_envase_grb]=disponible_grb
                       elsif cajas_grb < disponible_grb
                         $DTH_INVENTARIO[:prestamo_envase_grb]=cajas_grb
                       end
                   end
    r_total_pagado=r_total_pagado.to_f+ $pagos_anteriores_cr.to_f

    bandera_grb=$dt_row[:bandera_grb].to_i
    if bandera_grb ==0
      $DTH_INVENTARIO[:prestamo_envase_grb]=cajas_grb
      $DTH_INVENTARIO[:prestamo_envase_garrafon]=cajas_grb
    else
      prestamo_grb = cajas_grb - disponible_grb
      prestamo_garrafon=cajas_garrafon-disponible_garrafon
      if disponible_grb== 0
        puts " se cobra el grb"
      elsif prestamo_grb < 0
        puts "prestamo_grb", prestamo_grb

        prestamo_grb_r=prestamo_grb*-1
        $DTH_INVENTARIO[:prestamo_envase_grb]=prestamo_grb_r
      else
        $DTH_INVENTARIO[:prestamo_envase_grb]=prestamo_grb
      end

      if disponible_garrafon == 0
        puts " se cobra el garrafon"
      elsif prestamo_garrafon < 0
        prestamo_garrafon_r=prestamo_garrafon*-1
        $DTH_INVENTARIO[:prestamo_envase_garrafon]=prestamo_garrafon_r
      else
        $DTH_INVENTARIO[:prestamo_envase_garrafon]=prestamo_garrafon
      end
    end

    $DTH_INVENTARIO[:descuento_total]=$DESCUENTO_TOTAL.to_s.strip
    $DTH_INVENTARIO[:efectivo] = r_pago_efectivo.to_s.strip
    $DTH_INVENTARIO[:cheques] = r_importe_en_cheques.to_s.strip
    $DTH_INVENTARIO[:cupones] = r_importe_en_cupones.to_s.strip
    $DTH_INVENTARIO[:neto] =r_total_pagado.to_s.strip
    $DTH_INVENTARIO[:nombre_ticket]=$nombre_ticket.to_s
    $DTH_INVENTARIO[:devolucion_envase_grb]=$dt_row[:devolucion_grb_1].to_i+$dt_row[:devolucion_grb_2].to_i
    $DTH_INVENTARIO[:devolucion_garrafon]=$dt_row[:devolucion_garrafon_1].to_i+$dt_row[:devolucion_garrafon_2].to_i


    puts "el total neto  es de $#{r_total_pagado}"
    if $importe==r_total_pagado
      puts "El movimiento del dia es correcto"

    else
      raise "Error el resumen de cuenta no es correcto"
    end

    dt= get_csv_data 'dt_inventario'


    #Leer cada nuevo registro y agregarlo al CSV Original
    $DATOS.each_with_index do |fila, index|
      dt.push fila
      #ESCRIBIR CSV INVENTARIO
    end

    exportHashToCsv(dt, 'dt_inventario')
    $DATOS.pop()
  end

  def validarTicketAR()
    irAlTicket()

    buscarNud()

    validarProductosAR()

    sleep 5

    # sumarNetosAPagar()
    puts "********** Comienzo de validacion de los Netos a Pagar **********"
    validarNetoAPagarARConCSV()
    puts "********** Termino de validacion de los Netos a Pagar **********"
    puts


    puts "********** Comienzo de validacion de los Descuentos **********"
    validarDescuentosConCSV()
    puts "********** Termino de validacion de los Descuentos **********"
    puts

    sleep 1
    puts "********** Comienzo de validacion del IVA**********"
    validarIVAConCSV()
    puts "********** Termino de validacion del IVA **********"
    puts

  end

  def  validarTicket()
    irAlTicket()

    buscarNud()

    sleep 5

    hash = get_csv_data 'dt_combos'
    # puts "hash es: ", hash
    # puts "Hash en su posicion id es: ", hash[:id]
    id = $dt_row[:id].to_s
    #  puts "El id en dt test es: ", id

    hash.each do |fila|
      #  puts "fila en posicion id es: ", fila[:id]
      if fila[:id] == id
        if fila[:nombre_combo].nil?
          puts "No hay combos para validar"

        else
          buscarCombosEnElTicket()
        end


      end
    end

    validarProductos()

    sleep 5

   # sumarNetosAPagar()
    puts "********** Comienzo de validacion de los Netos a Pagar **********"
    validarNetoAPagarConCSV()
    puts "********** Termino de validacion de los Netos a Pagar **********"
    puts

    hash = get_csv_data 'dt_inventario'
    # puts "hash es: ", hash
    # puts "Hash en su posicion id es: ", hash[:id]
    id = $dt_row[:id].to_s
    #  puts "El id en dt test es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash
    puts "********** Comienzo de validacion del Efectivo **********"
    selectDeHash.each do |efectivo|
      # puts "El importe a pagar en dt_inventario es: ", itemEfectivo[:efectivo]

      unless efectivo[:efectivo].nil?
        validarEfectivoConCSV()

      end
    end
    puts "********** Termino de validacion del Efectivo **********"
    puts

    puts "********** Comienzo de validacion de Cheques **********"
    selectDeHash.each do |cheques|
      # puts "El importe a pagar en dt_inventario es: ", cheques[:cheques]

      unless cheques[:cheques].nil?
        validarChequesConCSV()

      end
    end
    puts "********** Termino de validacion de Cheques **********"
    puts


    puts "********** Comienzo de validacion de Cupones **********"
    selectDeHash.each do |cupones|
      # puts "El importe a pagar en dt_inventario es: ", cheques[:cheques]

      unless cupones[:cupones].nil?
        validarCuponesConCSV()

      end
    end
    puts "********** Termino de validacion de Cupones **********"
    puts

    puts "********** Comienzo de validacion de Nuevo Saldo **********"
    selectDeHash.each do |nuevoSaldo|
      # puts "El importe a pagar en dt_inventario es: ", nuevoSaldo[:monto_cr]

      unless nuevoSaldo[:monto_cr].nil?
        validarNuevosSaldosConCSV()

      end
    end
    puts "********** Termino de validacion de Nuevo Saldo **********"
    puts


    puts "********** Comienzo de validacion de Total de Abonos **********"
    selectDeHash.each do |totalDeAbonos|
      # puts "El importe a pagar en dt_inventario es: ", nuevoSaldo[:monto_cr]

      unless totalDeAbonos[:pagos_cr].nil?
        validarTotalDeAbonosConCSV()

      end
    end

    puts "********** Termino de validacion de Total de Abonos **********"
    puts

  #  puts "********** Comienzo de validacion de Prestamo de Envase Garrafon **********"
  #  validarCantidadDePrestamoConCSV()
  #  puts "********** Termino de validacion de Prestamo de Envase Garrafon **********"
  #  puts

  #  puts "********** Comienzo de validacion de Prestamo de Envase GRB **********"
  #  validarCantidadDePrestamoGRBConCSV()
  #  puts "********** Termino de validacion de Prestamo de Envase GRB **********"
  #  puts

    puts "********** Comienzo de validacion de Devolucion de Envase Garrafon **********"
      nombre_envase_garrafon_1 = $dt_row[:nombre_envase_garrafon_1].to_s
      # puts "El importe a pagar en dt_inventario es: ", nuevoSaldo[:monto_cr]

      unless nombre_envase_garrafon_1.nil?
    validarCantidadDeDevolucionConCSV()
      end

    puts "********** Termino de validacion de Devolucion de Envase Garrafon **********"
    puts

    puts "********** Comienzo de validacion de Devolucion de Envase GRB **********"
      nombre_envase_grb_1 = $dt_row[:nombre_envase_grb_1].to_s
      # puts "El importe a pagar en dt_inventario es: ", nuevoSaldo[:monto_cr]

      unless nombre_envase_grb_1.nil?
  #      validarCantidadDeDevolucionGRBConCSV()
      end

    puts "********** Termino de validacion de Devolucion de Envase GRB **********"
    puts

  end

  def validarCantidadDeDevolucionGRBConCSV()
    cantidadDeDevolucionGRB = obtenerCantidadDeDevolucionDeEnvaseGRB()


    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    nombre_envase_grb_1 = $dt_row[:nombre_envase_grb_1]

    #  puts "El id es: ", id
    # puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1


    selectDeHash = hash.select {|fila| fila[:id] == id}
    #  puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemDevolucionGRB|
      unless itemDevolucionGRB[:devolucion_envase_garrafon].nil?
           puts "La devolucion de GRB en dt_inventario es: ", itemDevolucionGRB[:devolucion_envase_garrafon]

        #   puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1, " el nombre_envase_grb_2 es: ", nombre_envase_grb_2
        unless nombre_envase_grb_1.nil?
          if itemDevolucionGRB[:devolucion_envase_garrafon].to_f == cantidadDeDevolucionGRB.to_f
            puts "La cantidad de pestramo es correcta. En dt_inventario es: ", cantidadDeDevolucionGRB, " la suma de la devolucion de GRB en el ticket son: ", cantidadDeDevolucionGRB
          else
            raise("validarCantidadDeDevolucionGRBConCSV => La cantidad de la devolucion no es correcto.")
            #    puts "El itemDevolucionGRB[:devolucion_envase_garrafon] es: ", itemDevolucionGRB[:devolucion_envase_garrafon], " el cantidadDeDevolucionGRB es: ", cantidadDeDevolucionGRB
          end

        else
          #   puts "No hay envase GRB para validar 1"
        end
      else
        #  puts "No hay envase GRB para validar 2"
      end
    end


  end

  def validarCantidadDePrestamoGRBConCSV()
    cantidadDePrestamoGRB = obtenerCantidadDePestamoDeEnvaseGRB()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    nombre_envase_grb_1 = $dt_row[:nombre_envase_grb_1]

    #  puts "El id es: ", id
    # puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1, " el nombre_envase_grb_2 es: ", nombre_envase_grb_2


    selectDeHash = hash.select {|fila| fila[:id] == id}
    #   puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemPrestamoGRB|
      unless itemPrestamoGRB[:prestamo_envase_grb].nil?
        #   puts "El prestamo de GRB en dt_inventario es: ", itemPrestamoGRB[:prestamo_envase_grb]

        #   puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1, " el nombre_envase_grb_2 es: ", nombre_envase_grb_2
        unless nombre_envase_grb_1.nil?
          if itemPrestamoGRB[:prestamo_envase_grb].to_f == cantidadDePrestamoGRB.to_f
            puts "La cantidad de pestramo es correcta. En dt_inventario es: ", itemPrestamoGRB[:prestamo_envase_grb], " la suma de los prestamos de GRB en el ticket son: ", cantidadDePrestamoGRB
          else
            raise("validarCantidadDePrestamoGRBConCSV => La cantidad de los prestamos no es correcto.")
            #    puts "El itemPrestamoGRB[:prestamo_envase_grb] es: ", itemPrestamoGRB[:prestamo_envase_grb], " el cantidadDePrestamoGRB es: ", cantidadDePrestamoGRB
          end

        else
          #   puts "No hay envase GRB para validar 1"
        end
      else
        #  puts "No hay envase GRB para validar 2"
      end
    end


  end

  def validarCantidadDeDevolucionConCSV()
    cantidadDeDevolucionGarrafon = obtenerCantidadDeDevolucionDeEnvaseGarrafon()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    nombre_envase_garrafon_1 = $dt_row[:nombre_envase_garrafon_1]

    #  puts "El id es: ", id
    # puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1, " el nombre_envase_grb_2 es: ", nombre_envase_garrafon_2


    selectDeHash = hash.select {|fila| fila[:id] == id}
    #   puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemDevolucionGarrafon|
      unless itemDevolucionGarrafon[:devolucion_envase_garrafon].nil?
        #   puts "La devolucion de garrafon en dt_inventario es: ", itemDevolucionGarrafon[:devolucion_envase_garrafon]

        #   puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1, " el nombre_envase_grb_2 es: ", nombre_envase_garrafon_2
        unless nombre_envase_garrafon_1.nil?
          if itemDevolucionGarrafon[:devolucion_envase_garrafon].to_f == cantidadDeDevolucionGarrafon.to_f
            puts "La cantidad de la devolucion es correcta. En dt_inventario es: ", itemDevolucionGarrafon[:devolucion_envase_garrafon], " la suma de los prestamos de garrafon en el ticket son: ", cantidadDeDevolucionGarrafon
          else
            raise("validarCantidadDeDevolucionConCSV => La cantidad de la devolucion no es correcto.")
            #    puts "El itemDevolucionGarrafon[:devolucion_envase_garrafon] es: ", itemDevolucionGarrafon[:devolucion_envase_garrafon], " el cantidadDeDevolucionGarrafon es: ", cantidadDeDevolucionGarrafon
          end

        else
          #   puts "No hay envase garrafon para validar 1"
        end
      else
        #  puts "No hay envase garrafon para validar 2"
      end
    end


  end

  def validarCantidadDePrestamoConCSV()
    cantidadDePrestamoGarrafon = obtenerCantidadDePrestamoDeEnvaseGarrafon()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    nombre_envase_garrafon_1 = $dt_row[:nombre_envase_garrafon_1]

    #  puts "El id es: ", id
   # puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1, " el nombre_envase_grb_2 es: ", nombre_envase_garrafon_2


    selectDeHash = hash.select {|fila| fila[:id] == id}
  #   puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemPrestamoGarrafon|
      unless itemPrestamoGarrafon[:prestamo_envase_garrafon].nil?
    #   puts "El prestamo de garrafon en dt_inventario es: ", itemPrestamoGarrafon[:prestamo_envase_garrafon]

    #   puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1, " el nombre_envase_grb_2 es: ", nombre_envase_garrafon_2
      unless nombre_envase_garrafon_1.nil?
        if itemPrestamoGarrafon[:prestamo_envase_garrafon].to_f == cantidadDePrestamoGarrafon.to_f
          puts "La cantidad de pestramo es correcta. En dt_inventario es: ", itemPrestamoGarrafon[:prestamo_envase_garrafon], " la suma de los prestamos de garrafon en el ticket son: ", cantidadDePrestamoGarrafon
        else
          raise("validarCantidadDePrestamoConCSV => La cantidad de los prestamos no es correcto.")
          #    puts "El itemPrestamoGarrafon[:prestamo_envase_garrafon] es: ", itemPrestamoGarrafon[:prestamo_envase_garrafon], " el cantidadDePrestamoGarrafon es: ", cantidadDePrestamoGarrafon
        end

      else
     #   puts "No hay envase garrafon para validar 1"
      end
      else
      #  puts "No hay envase garrafon para validar 2"
      end
    end


  end

  def obtenerCantidadDeDevolucionDeEnvaseGRB()

    envaseDevolucion = obtenerEnvasesDeDevolucionConSaltosDeLinea()
    encabezadoInventarioActualDevolucion = separarInventarioActualDevolucion()


    nombre_envase_grb_11 = $dt_row[:nombre_envase_grb_1]

    nombre_envase_grb_22 = $dt_row[:nombre_envase_grb_2]


    cantidadDeDevolucionAcumulado = 0

    if nombre_envase_grb_11.nil?
      #  puts "No hay envase garrafon 1 para validar"
    else
      nombre_envase_grb_1 = nombre_envase_grb_11.sub("-","")
      nombre_envase_grb_1 = nombre_envase_grb_1.sub(" ","")
      nombre_envase_grb_1 = nombre_envase_grb_1.split(" ")
      nombre_envase_grb_1 = nombre_envase_grb_1[0]

      # puts "Algo garrafon 1"
      puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1
      envaseDevolucion.each do|itemEnvaseGRB1|
        # puts "El item antes del if es: ", itemEnvaseGarrafon1

        if itemEnvaseGRB1.include? nombre_envase_grb_1
          #  puts "El item despues del if es: ", itemEnvaseGarrafon1
          quantity = itemEnvaseGRB1[34..36]
          #  puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDeDevolucion = cadenaSeparada[0].to_i
          puts "La cantidad del envase GRB es: ", cantidadDeDevolucion
          cantidadDeDevolucionAcumulado = cantidadDeDevolucionAcumulado + cantidadDeDevolucion

        else
          #  puts "No se encontro el envase garrafon"
        end
      end

    end

    if nombre_envase_grb_22.nil?
      #  puts "No hay envase garrafon 2 para validar"
    else
      nombre_envase_grb_2 = nombre_envase_grb_22.sub("-","")
      nombre_envase_grb_2 = nombre_envase_grb_2.sub(" ","")
      nombre_envase_grb_2 = nombre_envase_grb_2.split(" ")
      nombre_envase_grb_2 = nombre_envase_grb_2[0]
    #  puts " nombre_envase_grb_2 es: ", nombre_envase_grb_2

      #  puts "Algo garrafon 2"
      puts "El nombre_envase_grb_2 es: ", nombre_envase_grb_2
      envaseDevolucion.each do|itemEnvaseGRB2|
        if itemEnvaseGRB2.include? nombre_envase_grb_2
           # puts "El item despues del if es: ", itemEnvaseGRB2
          quantity = itemEnvaseGRB2[34..36]
          # puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDeDevolucion = cadenaSeparada[0].to_i
          puts "La cantidad del envase GRB es: ", cantidadDeDevolucion
          cantidadDeDevolucionAcumulado = cantidadDeDevolucionAcumulado + cantidadDeDevolucion
        end
      end
    end

    puts "La cantidad de la devolucion acumulada es: ", cantidadDeDevolucionAcumulado

    return cantidadDeDevolucionAcumulado
  end

  def obtenerCantidadDePrestamoDeEnvaseGRB()

    envasePrestamo = obtenerEnvasesDePrestamoConSaltosDeLinea()
    encabezadoInventarioActual = separarInventarioActual()


    nombre_envase_grb_11 = $dt_row[:nombre_envase_grb_1]

    nombre_envase_grb_22 = $dt_row[:nombre_envase_grb_2]


    cantidadDePrestamoAcumulado = 0

    if nombre_envase_grb_11.nil?
      #  puts "No hay envase garrafon 1 para validar"
    else
      nombre_envase_grb_1 = nombre_envase_grb_11.sub("-","")
      nombre_envase_grb_1 = nombre_envase_grb_1.sub(" ","")
      nombre_envase_grb_1 = nombre_envase_grb_1.split(" ")
      nombre_envase_grb_1 = nombre_envase_grb_1[0]

      # puts "Algo garrafon 1"
      puts "El nombre_envase_grb_1 es: ", nombre_envase_grb_1
      envasePrestamo.each do|itemEnvaseGarrafon1|
        # puts "El item antes del if es: ", itemEnvaseGarrafon1

        if itemEnvaseGarrafon1.include? nombre_envase_grb_1
          #  puts "El item despues del if es: ", itemEnvaseGarrafon1
          quantity = encabezadoInventarioActual[38..39]
          #  puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDePrestamo = cadenaSeparada[0].to_i
          puts "La cantidad del envase garrafon es: ", cantidadDePrestamo
          cantidadDePrestamoAcumulado = cantidadDePrestamoAcumulado + cantidadDePrestamo

        else
          #  puts "No se encontro el envase garrafon"
        end
      end

    end

    if nombre_envase_grb_22.nil?
      #  puts "No hay envase garrafon 2 para validar"
    else
      nombre_envase_grb_2 = nombre_envase_grb_22.sub("-","")
      nombre_envase_grb_2 = nombre_envase_grb_2.sub(" ","")
      nombre_envase_grb_2 = nombre_envase_grb_2.split(" ")
      nombre_envase_grb_2 = nombre_envase_grb_2[0]

      #  puts "Algo garrafon 2"
      puts "El nombre_envase_grb_2 es: ", nombre_envase_grb_2
      envasePrestamo.each do|itemEnvaseGarrafon2|
        if itemEnvaseGarrafon2.include? nombre_envase_grb_2
          #  puts "El item despues del if es: ", itemEnvaseGarrafon2
          quantity = encabezadoInventarioActual[38..39]
          # puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDePrestamo = cadenaSeparada[0].to_i
          puts "La cantidad del envase garrafon es: ", cantidadDePrestamo
          cantidadDePrestamoAcumulado = cantidadDePrestamoAcumulado + cantidadDePrestamo
        end
      end
    end

    puts "La cantidad de prestamo acumulado es: ", cantidadDePrestamoAcumulado

    return cantidadDePrestamoAcumulado
  end

  def obtenerCantidadDeDevolucionDeEnvaseGarrafon()

  #  envaseDevolucion = separarInventarioActualDevolucion()
    encabezadoInventarioActualDevolucion = separarInventarioActualDevolucion()


    nombre_envase_garrafon_11 = $dt_row[:nombre_envase_garrafon_1]

    nombre_envase_garrafon_22 = $dt_row[:nombre_envase_garrafon_2]


    cantidadDeDevolucionAcumulado = 0

    if nombre_envase_garrafon_11.nil?
      #  puts "No hay envase garrafon 1 para validar"
    else
      nombre_envase_garrafon_1 = nombre_envase_garrafon_11.sub("-","")
      nombre_envase_garrafon_1 = nombre_envase_garrafon_1.sub(" ","")
      nombre_envase_garrafon_1 = nombre_envase_garrafon_1.split(" ")
      nombre_envase_garrafon_1 = nombre_envase_garrafon_1[0]

      # puts "Algo garrafon 1"
      puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1
      encabezadoInventarioActualDevolucion.each do|itemEnvaseGarrafon1|
        # puts "El item antes del if es: ", itemEnvaseGarrafon1

        if itemEnvaseGarrafon1.include? nombre_envase_garrafon_1
            puts "El item despues del if es: ", itemEnvaseGarrafon1
          quantity = encabezadoInventarioActualDevolucion[38..39]
            puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDeDevolucion = cadenaSeparada[0].to_i
          puts "La cantidad del envase garrafon es: ", cantidadDeDevolucion
          cantidadDeDevolucionAcumulado = cantidadDeDevolucionAcumulado + cantidadDeDevolucion

        else
          #  puts "No se encontro el envase garrafon"
        end
      end

    end

    if nombre_envase_garrafon_22.nil?
      #  puts "No hay envase garrafon 2 para validar"
    else
      nombre_envase_garrafon_2 = nombre_envase_garrafon_22.sub("-","")
      nombre_envase_garrafon_2 = nombre_envase_garrafon_2.sub(" ","")
      nombre_envase_garrafon_2 = nombre_envase_garrafon_2.split(" ")
      nombre_envase_garrafon_2 = nombre_envase_garrafon_2[0]

      #  puts "Algo garrafon 2"
      puts "El nombre_envase_grb_2 es: ", nombre_envase_garrafon_2
      envaseDevolucion.each do|itemEnvaseGarrafon2|
        if itemEnvaseGarrafon2.include? nombre_envase_garrafon_2
          #  puts "El item despues del if es: ", itemEnvaseGarrafon2
          quantity = encabezadoInventarioActualDevolucion[38..39]
          # puts "quantity es: ", quantity
          cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
          cantidadDeDevolucion = cadenaSeparada[0].to_i
          puts "La cantidad del envase garrafon es: ", cantidadDeDevolucion
          cantidadDeDevolucionAcumulado = cantidadDeDevolucionAcumulado + cantidadDeDevolucion
        end
      end
    end

    puts "La cantidad de devolucion acumulado es: ", cantidadDeDevolucionAcumulado

    return cantidadDeDevolucionAcumulado
  end

  def obtenerCantidadDePrestamoDeEnvaseGarrafon()

   envasePrestamo = obtenerEnvasesDePrestamoConSaltosDeLinea()
   envasePrestamo = envasePrestamo.split("wD")
   puts "*****************envasePrestamo es: ", envasePrestamo

   nombre_envase_garrafon_11 = $dt_row[:nombre_envase_garrafon_1]

   nombre_envase_garrafon_22 = $dt_row[:nombre_envase_garrafon_2]


  cantidadDePrestamoAcumulado = 0

  if nombre_envase_garrafon_11.nil?
  #  puts "No hay envase garrafon 1 para validar"
  else
    nombre_envase_garrafon_1 = nombre_envase_garrafon_11.sub("-","")
    nombre_envase_garrafon_1 = nombre_envase_garrafon_1.sub(" ","")
    nombre_envase_garrafon_1 = nombre_envase_garrafon_1.split(" ")
    nombre_envase_garrafon_1 = nombre_envase_garrafon_1[0]

    # puts "Algo garrafon 1"
    puts "El nombre_envase_grb_1 es: ", nombre_envase_garrafon_1
    envasePrestamo.each do|itemEnvaseGarrafon1|
     # puts "El item antes del if es: ", itemEnvaseGarrafon1

      if itemEnvaseGarrafon1.include? nombre_envase_garrafon_1
      #  puts "El item despues del if es: ", itemEnvaseGarrafon1
        quantity = encabezadoInventarioActual[38..39]
      #  puts "quantity es: ", quantity
        cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
        cantidadDePrestamo = cadenaSeparada[0].to_i
        puts "La cantidad del envase garrafon es: ", cantidadDePrestamo
        cantidadDePrestamoAcumulado = cantidadDePrestamoAcumulado + cantidadDePrestamo

      else
      #  puts "No se encontro el envase garrafon"
      end
    end

  end

  if nombre_envase_garrafon_22.nil?
  #  puts "No hay envase garrafon 2 para validar"
  else
    nombre_envase_garrafon_2 = nombre_envase_garrafon_22.sub("-","")
    nombre_envase_garrafon_2 = nombre_envase_garrafon_2.sub(" ","")
    nombre_envase_garrafon_2 = nombre_envase_garrafon_2.split(" ")
    nombre_envase_garrafon_2 = nombre_envase_garrafon_2[0]

    #  puts "Algo garrafon 2"
    puts "El nombre_envase_grb_2 es: ", nombre_envase_garrafon_2
    envasePrestamo.each do|itemEnvaseGarrafon2|
      if itemEnvaseGarrafon2.include? nombre_envase_garrafon_2
      #  puts "El item despues del if es: ", itemEnvaseGarrafon2
        quantity = encabezadoInventarioActual[38..39]
       # puts "quantity es: ", quantity
        cadenaSeparada = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
        cantidadDePrestamo = cadenaSeparada[0].to_i
        puts "La cantidad del envase garrafon es: ", cantidadDePrestamo
        cantidadDePrestamoAcumulado = cantidadDePrestamoAcumulado + cantidadDePrestamo
      end
    end
  end

  puts "La cantidad de prestamo acumulado es: ", cantidadDePrestamoAcumulado

  return cantidadDePrestamoAcumulado
  end

  def obtenerEnvasesDeDevolucionConSaltosDeLinea()
    encabezadoInventarioActualDevolucion = separarInventarioActualDevolucion()
    envaseDevolucion = encabezadoInventarioActualDevolucion.split("\n")
    #envaseDevolucion = envaseDevolucion.split("wD")

    return envaseDevolucion
  end

  def obtenerEnvasesDePrestamoConSaltosDeLinea()
    encabezadoInventarioActual = separarInventarioActual()
    envasePrestamo = encabezadoInventarioActual.split("\n")

    return envasePrestamo
  end

  def separarInventarioActualDevolucion()
    encabezadoDevolucion = separarDevolucion()
    encabezadoInventarioActual1 = encabezadoDevolucion[1].split("Inventario Actual")
    encabezadoInventarioActual = encabezadoInventarioActual1[0]
    encabezadoInventarioActual = encabezadoInventarioActual.split("\\n")
    return encabezadoInventarioActual
  end

  def separarInventarioActual()
    encabezadoPrestamo = separarPrestamo()
    encabezadoInventarioActual1 = encabezadoPrestamo[1].split("Inventario Actual")

    encabezadoInventarioActual = encabezadoInventarioActual1[0]
    return encabezadoInventarioActual
  end

  def separarDevolucion()
    encabezadoAvisoDePrivacidad = separarAvisoDePrivacidad()
    encabezadoDevolucion = encabezadoAvisoDePrivacidad[0].split("Devolucion")

    return encabezadoDevolucion
  end

  def separarPrestamo()
    encabezadoAvisoDePrivacidad = separarAvisoDePrivacidad()
    encabezadoPrestamo = encabezadoAvisoDePrivacidad[0].split("Prestamo")

    return encabezadoPrestamo
  end

  def separarAvisoDePrivacidad()
    encabezadoEnvase = separarEnvase()
    encabezadoAvisoDePrivacidad = encabezadoEnvase[1].split("AVISO DE PRIVACIDAD")

    return encabezadoAvisoDePrivacidad
  end

  def separarEnvase()
    tSplit = obtenerTicketOriginal()

    encabezadoEnvase = tSplit[0].split("ENVASE")

    return encabezadoEnvase
  end

  def validarIVAConCSV()
    ivaAcumulado = sumarIVA()

    hash = get_csv_data 'dt_ar_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemIvaTotal|
      # puts "El importe a pagar en dt_inventario es: ", itemDescuentoTotal[:descuento_total]

      unless itemIvaTotal[:iva].nil?

        if itemIvaTotal[:iva].to_f == ivaAcumulado.to_f
          puts "El importe es correcto. El en dt_ar_inventario es: ", itemIvaTotal[:iva], " la suma de los montos de los Descuentos en el ticket son: ", ivaAcumulado
        else
          puts "itemIvaTotal[:iva] es: ", itemIvaTotal[:iva] , "ivaAcumulado es: ", ivaAcumulado
          raise("validarIVAConCSV => El monto de los Iva a pagar no es correcto.")
          #    puts "El itemIvaTotal[:iva] es: ", itemIvaTotal[:iva], " el ivaAcumulado es: ", ivaAcumulado
        end
      end
    end
  end

  def sumarIVA()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    ivaAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemDescuentos|

      if itemDescuentos.include? "IVA "
        puts "El item despues del if es: ", itemDescuentos
        quantity = itemDescuentos[35..45]
        puts "quantity es: ", quantity
        cadenaSeparada  = quantity.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}
        cantidadDePrestamo = cadenaSeparada[0].to_f
        puts "La cantidad de los descuentos es: ", cantidadDePrestamo
        ivaAcumulado = ivaAcumulado + cantidadDePrestamo
        puts "El iva acumulado es: ", ivaAcumulado
      end
    end
    return ivaAcumulado

  end

  def validarDescuentosConCSV()
    descuentoAcumulado = sumarDescuentos()

    hash = get_csv_data 'dt_ar_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemDescuentoTotal|
      # puts "El importe a pagar en dt_inventario es: ", itemDescuentoTotal[:descuento_total]

      unless itemDescuentoTotal[:descuento_total].nil?

        if itemDescuentoTotal[:descuento_total].to_f == descuentoAcumulado.to_f
          puts "El importe es correcto. El en dt_ar_inventario es: ", itemDescuentoTotal[:descuento_total], " la suma de los montos de los Descuentos en el ticket son: ", descuentoAcumulado
        else
          raise("validarDescuentossConCSV => El monto de los Descuentos a pagar no es correcto.")
          #    puts "El itemDescuentoTotal[:descuento_total] es: ", itemDescuentoTotal[:descuento_total], " el descuentoAcumulado es: ", descuentoAcumulado
        end
      end
    end
  end

  def sumarDescuentos()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    descuentoAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemDescuentos|
     # ticketSaltosDeLinea.each do |itemDescuentos|
      if itemDescuentos.to_s.include? "Descuento "
     # puts "itemDescuentos es: ", itemDescuentos
        cadenaSeparada = itemDescuentos.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnopqr¤stuvwxyz')}
      #  puts "Cadena Separada es: ", cadenaSeparada
       # puts "Los montos de los Descuentos son: ", cadenaSeparada[1].to_f

        descuentoAcumulado = descuentoAcumulado + cadenaSeparada[1].to_f

        puts "La suma de los montos de los Descuentos son: ", descuentoAcumulado
      end
    end
    return descuentoAcumulado

  end

  def validarTotalDeAbonosConCSV()
    totalDeAbonosAcumulado = sumarTotalDeAbonos()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemPagosCR|
      # puts "El importe a pagar en dt_inventario es: ", itemCheques[:cheques]

      unless itemPagosCR[:pagos_cr].nil?

        if itemPagosCR[:pagos_cr].to_f == totalDeAbonosAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemPagosCR[:pagos_cr], " la suma de los montos de los Nuevos Saldos en el ticket son: ", totalDeAbonosAcumulado
        else
          raise("validarNuevosSaldosConCSV => El monto de los Nuevos Saldos a pagar no es correcto.")
          #    puts "El itemPagosCR[:pagos_cr] es: ", itemNuevoSaldo[:monto_cr], " el nuevoSaldoAcumulado es: ", totalDeAbonosAcumulado
        end
      end
    end
  end

  def sumarTotalDeAbonos()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    totalDeAbonosAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemTotalDeAbonos|
      if itemTotalDeAbonos.to_s.include? "Total de Abonos"

        cadenaSeparada = itemTotalDeAbonos.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos del Total de Abono son: ", cadenaSeparada[3].to_f

        totalDeAbonosAcumulado = totalDeAbonosAcumulado + cadenaSeparada[3].to_f

        puts "La suma de los montos de los Nuevos Saldos son: ", totalDeAbonosAcumulado
      end
    end
    return totalDeAbonosAcumulado

  end

  def validarNuevosSaldosConCSV()
    nuevoSaldoAcumulado = sumarNuevoSaldo()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemNuevoSaldo|
      # puts "El importe a pagar en dt_inventario es: ", itemCheques[:cheques]

      unless itemNuevoSaldo[:monto_cr].nil?

        if itemNuevoSaldo[:monto_cr].to_f == nuevoSaldoAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemNuevoSaldo[:monto_cr], " la suma de los montos de los Nuevos Saldos en el ticket son: ", nuevoSaldoAcumulado
        else
          raise("validarNuevosSaldosConCSV => El monto de los Nuevos Saldos a pagar no es correcto.")
          #    puts "El itemNuevoSaldo[:monto_cr] es: ", itemNuevoSaldo[:monto_cr], " el nuevoSaldoAcumulado es: ", nuevoSaldoAcumulado
        end
      end
    end
  end

  def sumarNuevoSaldo()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    nuevoSaldoAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemNuevoSaldo|
      if itemNuevoSaldo.to_s.include? "Nuevo Saldo"

        cadenaSeparada = itemNuevoSaldo.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos de los Nuevos Saldos son: ", cadenaSeparada[2].to_f

        nuevoSaldoAcumulado = nuevoSaldoAcumulado + cadenaSeparada[2].to_f

        puts "La suma de los montos de los Nuevos Saldos son: ", nuevoSaldoAcumulado
      end
    end
    return nuevoSaldoAcumulado

  end

  def validarCuponesConCSV()
    cuponesAcumulado = sumarCupones()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemCupones|
      # puts "El importe a pagar en dt_inventario es: ", itemCheques[:cheques]

      unless itemCupones[:cupones].nil?

        if itemCupones[:cupones].to_f == cuponesAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemCupones[:cupones], " la suma de los montos en cupones en el ticket son son: ", cuponesAcumulado
        else
          raise("validarCuponesConCSV => El monto en cupones a pagar no es correcto.")
          #    puts "El itemCheques[:cheques] es: ", itemCheques[:cheques], " el chequesAcumulado es: ", chequesAcumulado
        end
      end
    end
  end

  def sumarCupones()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    cuponesAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemCupon|
      if itemCupon.to_s.include? "Total Cupones"

        cadenaSeparada = itemCupon.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos de Cupones son: ", cadenaSeparada[2].to_f

        cuponesAcumulado = cuponesAcumulado + cadenaSeparada[2].to_f

        puts "La suma de los montos en Cupones son: ", cuponesAcumulado
      end
    end
    return cuponesAcumulado

  end

  def validarChequesConCSV()
    chequesAcumulado = sumarCheques()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemCheques|
      # puts "El importe a pagar en dt_inventario es: ", itemCheques[:cheques]

      unless itemCheques[:cheques].nil?

        if itemCheques[:cheques].to_f == chequesAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemCheques[:cheques], " la suma de los montos en cheques en el ticket son son: ", chequesAcumulado
        else
          raise("validarChequesConCSV => El monto en cheques a pagar no es correcto.")
        #    puts "El itemCheques[:cheques] es: ", itemCheques[:cheques], " el chequesAcumulado es: ", chequesAcumulado
        end
      end
    end
  end

  def sumarCheques()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    chequesoAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemCheque|
      if itemCheque.to_s.include? "Total Cheques"

        cadenaSeparada = itemCheque.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos de Cheques son: ", cadenaSeparada[2].to_f

        chequesoAcumulado = chequesoAcumulado + cadenaSeparada[2].to_f

        puts "La suma de los montos en Cheques son: ", chequesoAcumulado
      end
    end
    return chequesoAcumulado

  end

  def validarEfectivoConCSV()
    efectivoAcumulado = sumarEfectivo()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemEfectivo|
      # puts "El importe a pagar en dt_inventario es: ", itemEfectivo[:efectivo]

      unless itemEfectivo[:efectivo].nil?

        if itemEfectivo[:efectivo].to_f == efectivoAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemEfectivo[:efectivo], " la suma de los montos en efectivo en el ticket son son: ", efectivoAcumulado
        else
          raise("validarEfcetivoConCSV => El monto en efectivo a pagar no es correcto.")
          #  puts "El itemImporte[:importe] es: ", itemImporte[:importe], " el netoAPagarAcumulado es: ", netoAPagarAcumulado
        end
      end
    end
  end

  def sumarEfectivo()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    efectivoAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemEfectivo|
      if itemEfectivo.to_s.include? "Efectivo"

        cadenaSeparada = itemEfectivo.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos de Efectivo son: ", cadenaSeparada[1].to_f

        efectivoAcumulado = efectivoAcumulado + cadenaSeparada[1].to_f

        puts "La suma de los montos en Efectivo son: ", efectivoAcumulado
      end
    end
    return efectivoAcumulado

  end

  def validarNetoAPagarARConCSV()
    netoAPagarAcumulado = sumarNetosAPagar()

    hash = get_csv_data 'dt_ar_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
    # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemImporte|
      # puts "El importe a pagar en dt_inventario es: ", itemImporte[:importe]

      unless itemImporte[:neto].nil?

        if itemImporte[:neto].to_f == netoAPagarAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemImporte[:neto], " la suma de los netos a pagar en el ticket son son: ", netoAPagarAcumulado
        else
          raise("validarNetoAPagarConCSV => El Importe a pagar no es correcto.")
          #  puts "El itemImporte[:importe] es: ", itemImporte[:importe], " el netoAPagarAcumulado es: ", netoAPagarAcumulado
        end
      end
    end
  end

  def validarNetoAPagarConCSV()
    netoAPagarAcumulado = sumarNetosAPagar()

    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    selectDeHash = hash.select {|fila| fila[:id] == id}
   # puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemImporte|
     # puts "El importe a pagar en dt_inventario es: ", itemImporte[:importe]

      unless itemImporte[:neto].nil?

        if itemImporte[:neto].to_f == netoAPagarAcumulado.to_f
          puts "El importe es correcto. El en dt_inventario es: ", itemImporte[:neto], " la suma de los netos a pagar en el ticket son son: ", netoAPagarAcumulado
        else
          raise("validarNetoAPagarConCSV => El Importe a pagar no es correcto.")
        #  puts "El itemImporte[:importe] es: ", itemImporte[:importe], " el netoAPagarAcumulado es: ", netoAPagarAcumulado
        end
      end
    end
  end

  def sumarNetosAPagar()

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
   # puts "Ticket separado es: ", ticketOriginalConSaltosDeLinea
    netoAPagarAcumulado = 0.0
    ticketOriginalConSaltosDeLinea.each do |itemNetoAPagar|
      # puts "item neto a pagar es: ", itemNetoAPagar
      if itemNetoAPagar.to_s.include? "Neto a Pagar"
        # puts "Los netos a pagar son: ", itemNetoAPagar

        cadenaSeparada = itemNetoAPagar.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

        puts "Los montos de los Netos a Pagar son: ", cadenaSeparada[3].to_f

        netoAPagarAcumulado = netoAPagarAcumulado + cadenaSeparada[3].to_f

        puts "La suma de los Netos a Pagar son: ", netoAPagarAcumulado
      end
    end
    return netoAPagarAcumulado
  end

  def validarProductosAR()
    puts "********** Comienzo de validacion de productos **********"
    hash = get_csv_data 'dt_ar_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "El ticket original con saltos de linea es: ", ticketOriginalConSaltosDeLinea


    selectDeHash = hash.select {|fila| fila[:id] == id}
    #puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemProductos|
      puts "El producto en dt_inventario es: ", itemProductos[:sku]
      unless itemProductos[:sku].nil?
        productoEncontrado = ticketOriginalConSaltosDeLinea.select {|ticket| ticket.to_s.include? itemProductos[:sku]}.first
        puts "El producto encontrado es: ", productoEncontrado
        if productoEncontrado.nil?

          raise("validarProductos => No se encontro el producto en el ticket")

        end
      else
        puts "Sin productgo en dt_inventario"
      end
    end
    puts "********** Termino de validacion de productos **********"
    puts
  end

  def validarProductos()
    puts "********** Comienzo de validacion de productos **********"
    hash = get_csv_data 'dt_inventario'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id

    ticketOriginalConSaltosDeLinea = separarTicketOriginalPorSaltosDeLinea()
    # puts "El ticket original con saltos de linea es: ", ticketOriginalConSaltosDeLinea


    selectDeHash = hash.select {|fila| fila[:id] == id}
    #puts "El selectDeHash es ", selectDeHash

    selectDeHash.each do |itemProductos|
      puts "El producto en dt_inventario es: ", itemProductos[:sku]
      unless itemProductos[:sku].nil?
        productoEncontrado = ticketOriginalConSaltosDeLinea.select {|ticket| ticket.to_s.include? itemProductos[:sku]}.first
        puts "El producto encontrado es: ", productoEncontrado
        if productoEncontrado.nil?

          raise("validarProductos => No se encontro el producto en el ticket")

        end
      else
        puts "Sin productgo en dt_inventario"
      end
    end
    puts "********** Termino de validacion de productos **********"
    puts
  end

  def buscarCombosEnElTicket()
    puts "********** Comienzo de validación de Combos **********"
    hash = get_csv_data 'dt_combos'
    id = $dt_row[:id].to_s
    #  puts "El id es: ", id


    ticketPorEmpresa = obtenerContenidoDeEmpresas()
    # ticketPorEmpresa

    hash.each_with_index do |fila|
      #  puts "la fila es: ", fila[:id]
      if fila[:id] == id
        #  puts "ID del caso primer combo ", fila[:id]

        n = 0
        unless fila[:nombre_combo].nil?

          ticketPorEmpresa.each do |itemCombo|
            n= n+1
            # textoConCombo = itemCombo.select {|x| x.to_s.include? fila[:nombre_combo]}
            # puts textoConCombo

            if itemCombo.to_s.include? fila[:nombre_combo]
              puts "El combo ", fila[:nombre_combo], " existe en el ticket de la empresa ", n, " y es correcto"
              true
            else
              puts "El combo ", fila[:nombre_combo], " no existe en el ticket de la empresa ", n
            end
          end

        end
      end


      segundoCombo = id+"-1" #134-1
      # id1 = fila[:id]+"-1" #134-1
      if fila[:id] == segundoCombo.to_s

        #  puts "el id en dt_combos es: ", fila[:id]
        #  puts "El ID en dt_inventario es: ", segundoCombo
        #      puts "El nombre del segundo combo es: ", fila[:nombre_combo]

        n = 0

        unless fila.nil?

          ticketPorEmpresa.each do |itemCombo|
            n= n+1
            textoConCombo = itemCombo.select {|x| x.to_s.include? fila[:nombre_combo]}
            #  puts textoConCombo

            if itemCombo.to_s.include? fila[:nombre_combo]
              puts "El combo ", fila[:nombre_combo], " existe en el ticket de la empresa ", n, " y es correcto"
            else
              puts "El combo ", fila[:nombre_combo], " no existe en el ticket de la empresa ", n
            end
          end
        end
      end
    end

    puts "********** Termino de validación de Combos **********"
    puts
  end

  def buscarNud()
    puts "********** Comienzo de validacion de Nud **********"
    ticketPorEmpresa = obtenerContenidoDeEmpresas()

    nud=$dt_row[:nud].to_s

    ticketPorEmpresa.each do |itemNud|
      textoConNud = itemNud.select {|x| x.to_s.include? 'NUD:'}
      puts textoConNud
      if textoConNud.to_s.include? nud
        puts "El nud existe en el ticket de la empresa y es correcto"
      else
        puts "El nud no existe en el ticket o no es correcto"
      end
    end
    puts "********** Termino de validacion de Nud **********"
    puts
  end

  def obtenerContenidoDeEmpresas()

    encabezado = eliminarEncabezadoDeLaCopia()

    ticketPorEmpresa = encabezado.map {|encabezado| encabezado.split("\n")}

    return ticketPorEmpresa
  end

  def eliminarEncabezadoDeLaCopia()
    encabezado = obtenerEmpresasDelTicket()
    encabezado.pop
    return encabezado
  end

  def obtenerEmpresasDelTicket()

    tSplit = obtenerTicketOriginal()
    encabezado = tSplit[0].split("AVISO DE PRIVACIDAD")

    return encabezado
  end

  def separarTicketOriginalPorSaltosDeLinea()
    ticketOriginal = obtenerTicketOriginal()
  #  puts "El ticket original en separarTicketOriginalPorSaltosDeLinea es: ", ticketOriginal
    ticketSaltosDeLinea = ticketOriginal[0].split("\n")

    return ticketSaltosDeLinea
  end

  def obtenerTicketOriginal()

    ticketLimpio = obtenerTextoDelTicket()
 #   puts "El ticket limpio en  obtenerTicketOriginal es: ", ticketLimpio
    tSplit = ticketLimpio.split("COPIA")


 #   puts "El ticket original es: ", tSplit
    return tSplit
  end

  def obtenerTextoDelTicket()
    ticketString = "//*[@class= 'android.widget.EditText' and @resource-id= 'com.cyanogenmod.filemanager:id/editor']"
    textoTicket = get_mobile_element_attribute ticketString, "text"
    # puts "EL TICKET ", textoTicket

    ticketLimpio = textoTicket.gsub! 'wD', '\n'

    return ticketLimpio

  end

  def irAlTicket()
    nud=$dt_row[:nud].to_s
    ticket=$dt_row[:ticket].to_s
    visita=$dt_row[:visita].to_s
    $nombre_ticket=nud+ticket+visita

    # Tap en el menú
    tap_element("//*[@class= 'android.widget.ImageView' and @resource-id= 'com.android.launcher3:id/all_apps_handle']")
    sleep 2
    # Tap en el buscador y colocar texto File Manager
    carpetaAndroid = obtenerValorCsvPorColumna('carpeta_android')
    set_text_element("//*[@class='android.widget.EditText' and @resource-id='com.android.launcher3:id/search_box_input']", carpetaAndroid)
    sleep 2
    # Tap en File Manager
    tap_element("//*[@class= 'android.widget.TextView' and @resource-id= 'com.android.launcher3:id/icon']")
    sleep 2
    # Tap en Busqueda de carpeta y colocar texto MGOutputWorking
    carpeta = obtenerValorCsvPorColumna('carpeta')
    set_text_element("//*[@class= 'android.widget.ImageButton' and @resource-id= 'com.cyanogenmod.filemanager:id/ab_search']", carpeta)
    sleep 2
    # Tap en la lupa de buscar en el teclado
    tap_element("//*[@class= 'android.widget.ImageButton' and @resource-id= 'com.cyanogenmod.filemanager:id/ab_search']")
    sleep 2
    # Tap en MGOutputWorking
    tap_element("//*[@class= 'android.widget.RelativeLayout' and @resource-id= 'com.cyanogenmod.filemanager:id/search_item']")
    sleep 2
    # Tap en Busqueda de ticket y colocar texto del ticket 22374
    puts "El nombre del ticket es: ", $nombre_ticket
    set_text_element("//*[@class= 'android.widget.ImageButton' and @resource-id= 'com.cyanogenmod.filemanager:id/ab_search']", $nombre_ticket)
    sleep 2
    # Tap en la lupa de buscar en el teclado
    tap_element("//*[@class= 'android.widget.ImageButton' and @resource-id= 'com.cyanogenmod.filemanager:id/ab_search']")
    sleep 2
    # Tap en el Ticket
    tap_element("//*[@class= 'android.widget.RelativeLayout' and @resource-id= 'com.cyanogenmod.filemanager:id/search_item']")
    sleep 2

    # ticketString = query(@@txt_ticket[:locator], :text)
  end

  def obtenerValorCsvPorColumna(columna)
    puts "El NOMBRE DE LA COLUMNA A BUSCAR ES:  ", columna
    valorObtenido = $dt_row.select {|e| e.to_s == columna}.first

    #puts "EL ARRAY VALOR OBTENIDO ES :", valorObtenido
    puts "EL VALOR DE LA COLUMNA ES : ", valorObtenido[1]
    return valorObtenido[1]
  end

  def quitarCaracteres(cadena)

    #  rgx = '$,'
    #  cadenaSeparada = cadena.gsub!(rgx)
    #  puts 'Cadena separada ', cadenaSeparada

    cadenaSeparada = cadena.split.map {|x| x.delete('  $,ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnopqr¤stuvwxyz')}

    return cadenaSeparada
  end
  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################


end