
# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/csv_data'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class Mas_producto_page< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_mas_producto'
  @@sku_producto=get_or_data(@or_file, 'sku_producto')
  @@data_nombre=get_or_data(@or_file,'data_nombre')
  @@data_disponible=get_or_data(@or_file,'data_disponible')
  @@data_cantidad=get_or_data(@or_file,'data_cantidad')
  @@btn_agrega_producto_1=get_or_data(@or_file,'btn_agrega_producto_1')
  @@btn_agrega_producto_2=get_or_data(@or_file,'btn_agrega_producto_2')
  @@btn_agrega_producto_3=get_or_data(@or_file,'btn_agrega_producto_3')
  @@btn_mas_producto=get_or_data(@or_file,'btn_mas_producto')
  @@txt_nombre_pedido_del_dia=get_or_data(@or_file,'txt_nombre_pedido_del_dia')
  @@txt_cantidad_pedido_del_dia=get_or_data(@or_file,'txt_cantidad_pedido_del_dia')
  @@txt_cantidad_a_agregar_pedido_del_dia=get_or_data(@or_file,'txt_cantidad_a_agregar_pedido_del_dia')
  @@btn_agregar_productos_confirmar=get_or_data(@or_file,'btn_agregar_productos_confirmar')
  @@btn_popup=get_or_data(@or_file,'btn_popup')
  @@lbl_producto_a_buscar=get_or_data(@or_file,'lbl_producto_a_buscar')


  #############################################################################
  # ACCIONES
  #############################################################################

  #Metodo que hace scroll down  y scroll up N intentos y de lo contrario realiza otra accion
  def scroll_to_elementos(element, intentos = nil,sku,cajas)
    element =  element.to_s.strip
    count = 0
    result = false
    #si no se manda timeout por default se envia 5
    intentos = intentos.nil? ? 8 : intentos

    while count < intentos

      if element_exists(element)
        #puts "element exist => #{element}"
        el = query element
        if el.first["visible"]
          result = true
          nombre=query(@@txt_nombre_pedido_del_dia[:locator],:text)
          cantidad=query(@@txt_cantidad_pedido_del_dia[:locator],:text).each.map(&:to_i)
          regex=Regexp.new(sku)
          r_sku=nombre.find{|e|regex=~e}
          posicion= nombre.index(r_sku)
          sleep 1

          #VALIDO que la posicion del articulo en pedido sugerido sea correcta
          inventario_i=cantidad[posicion]
          cajas= cajas.to_i
          inventario_f=inventario_i-cajas
          #creo la data con el sku del producto, inventario incial, cajas e inventario final
          r=Array.new
          r.push nombre[posicion]
          r.push inventario_i
          r.push cajas
          r.push inventario_f
          #Se ingresa la data en la HASH
          if inventario_f < cajas
            raise("Error=> Las cajas a agregar superan al inventario. Es necesario agregar menos cajas")

          end
          $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
          $DTH_INVENTARIO[:sku] = r[0].to_s.strip
          $DTH_INVENTARIO[:inventario_inicial] = r[1].to_s.strip
          $DTH_INVENTARIO[:inventario_final] = r[3].to_i

          x="//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.EditText']"

          tap_element(x)
          tap_element(x)
          sleep 1
          clear_text()
          enter_text_uielement(x, cajas,'mate')
          press_user_action_button('search')
          #PUSHEAR PRODUCTO A DATA
          $DATOS.push($DTH_INVENTARIO)
          break
        end
      end

      #scroll si el elemento no existe
      scroll_down
      count =count + 1

    end

    unless result
      sleep 1
      or_exec_uielement(@@btn_mas_producto)
      #LO BUSCA EN MAS PRODUCTOS
      sleep 1
      busco_e_ingreso_productos(sku,cajas)
    end

    return result
  end

def scroll_to_elementos(element, intentos = nil,sku,cajas)
  sleep 3
    element =  element.to_s.strip
    count = 0
    result = false
    #si no se manda timeout por default se envia 5
    intentos = intentos.nil? ? 8 : intentos

    while count < intentos

      if element_exists(element)
        #puts "element exist => #{element}"
        el = query element
        if el.first["visible"]
          result = true
          nombre=query(@@txt_nombre_pedido_del_dia[:locator],:text)
          cantidad=query(@@txt_cantidad_pedido_del_dia[:locator],:text).each.map(&:to_i)
          regex=Regexp.new(sku)
          r_sku=nombre.find{|e|regex=~e}
          posicion= nombre.index(r_sku)
          sleep 1

          #VALIDO que la posicion del articulo en pedido sugerido sea correcta
          inventario_i=cantidad[posicion]
          cajas= cajas.to_i
          inventario_f=inventario_i-cajas
          #creo la data con el sku del producto, inventario incial, cajas e inventario final
          r=Array.new
          r.push nombre[posicion]
          r.push inventario_i
          r.push cajas
          r.push inventario_f
          #Se ingresa la data en la HASH
          if inventario_f < cajas
            raise("Error=> Las cajas a agregar superan al inventario. Es necesario agregar menos cajas")

          end

          clave_producto=sku
          espacio=" - "
          clave_producto=clave_producto+espacio
          sku=r[0].to_s
          sku.gsub!(clave_producto,"")
          $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
          $DTH_INVENTARIO[:sku] = sku
          puts  $DTH_INVENTARIO[:sku] =sku
          $DTH_INVENTARIO[:inventario_inicial] = r[1].to_s.strip
          $DTH_INVENTARIO[:inventario_final] = r[3].to_i

          x="//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.EditText']"

          tap_element(x)
          tap_element(x)
          sleep 1
          clear_text()
          enter_text_uielement(x, cajas,'mate')
          press_user_action_button('search')
          #PUSHEAR PRODUCTO A DATA
          $DATOS.push($DTH_INVENTARIO)
          scroll_up
          scroll_up
          scroll_up
          scroll_up
          scroll_up
          scroll_up
          break
        end
      end

      #scroll si el elemento no existe
      scroll_down
      count =count + 1
    end

    unless result
      #LO BUSCA EN MAS PRODUCTOS
      sleep 1

      sleep 2
      or_exec_uielement(@@btn_mas_producto)
      busco_e_ingreso_productos(sku,cajas)


    end

    return result
  end









  def buscar_y_agregar_productos(tipo_sku)
  sleep 5

  $DTH_INVENTARIO = {
      :id=>nil,
      :sku=> nil,
      :inventario_inicial=> nil,
      :inventario_final => nil,
      :monto_combo1=>nil,
      :monto_combo2=>nil,
      :descuento_total =>nil,
      :importe => nil,
      :efectivo=>nil,
      :cheques=>nil,
      :cupones=>nil,
      :pagos_cr=>nil,
      :monto_cr=>nil,
      :neto => nil,
      :nombre_ticket=>nil,
      :devolucion_envase_grb=>nil,
      :devolucion_garrafon=>nil,
      :prestamo_envase_grb=>nil,
      :prestamo_envase_garrafon=>nil,
      :cobro_envase_grb=>nil,
      :cobro_envase_garrafon=>nil,
      :cobro_envase_grb_y_garrafon=>nil,
      :cobro_cajilla=>nil,
  }
    #OBTENGO EL SKU DEL PRODUCTO A BUSCAR
    tipo_sku=tipo_sku.to_s
    tipo_producto = case tipo_sku
                      when"primer pet"
                        sku=$dt_row[:refresco_pet_1]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"segundo pet"
                        sku=$dt_row[:refresco_pet_2]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"primer grb"
                        sku=$dt_row[:refresco_grb_1]
                        cajas=$dt_row[:caja_refresco_grb_1].to_s.strip
                      when"segundo grb"
                        sku=$dt_row[:refresco_grb_2]
                        cajas=$dt_row[:caja_refresco_grb_2].to_s.strip
                      when"primer garrafon"
                        sku=$dt_row[:garrafon_1]
                        cajas=$dt_row[:caja_garrafon_1].to_s.strip
                      when"segundo garrafon"
                        sku=$dt_row[:garrafon_2]
                        cajas=$dt_row[:caja_garrafon_2].to_s.strip
                      when"primer no carbonatado"
                        sku=$dt_row[:no_carbonatado_1]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"segundo no carbonatado"
                        sku=$dt_row[:no_carbonatado_2]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"primer agua embotellada"
                        sku=$dt_row[:agua_embotellada_1]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"segunda agua embotellada"
                        sku=$dt_row[:agua_embotellada_2]
                        cajas=$dt_row[:cantidad_cajas_1].to_s.strip
                      when"revisita"
                        sku=$dt_row_revisita[:sku_nueva_venta_revisita]
                        cajas=$dt_row_revisita[:cajas_revisita].to_s.strip
                      when"pet"
                        sku=$dt_row[:pet]
                        cajas=$dt_row[:cajas_1].to_s.strip
                      when"garrafon"
                        sku=$dt_row[:garrafon]
                        cajas=$dt_row[:cajas_2].to_s.strip
                      when"no_carbonatado"
                        sku=$dt_row[:no_carbonatado]
                        cajas=$dt_row[:cajas_3].to_s.strip
                      when"agua_embotellada"
                        sku=$dt_row[:agua_embotellada]
                        cajas=$dt_row[:cajas_4].to_s.strip

                    end


      if cajas==nil
        raise("Error=> no se encontro una caja a agregar")
      end

      if sku==nil
        raise("Error=> no se encontro un sku, favor de validar csv")
      end
      el = val_or_uielement(@@lbl_producto_a_buscar, sku)
 # puts "EL ES: ", el, el["element"]
 # puts "SKU ES: ", sku
      scroll_to_elementos el["element"],sku,cajas
  end


  def   busco_e_ingreso_productos(sku,cajas)
    sleep 1

    el = val_or_uielement(@@lbl_producto_a_buscar, sku)
    scroll_down_to_uielement_adr el["element"]
    assert_for_element_exist(el["element"], 20,
                             "Error => no se encontro el  producto")
    sleep 2
    data_nombre=query(@@data_nombre[:locator],:text)
    data_disponible=query(@@data_disponible[:locator],:text).each.map(&:to_i)
    data_cantidad= cajas.to_i
    array_productos=data_nombre.zip(data_disponible)
    longitud=array_productos.length
    r=Array.new
    i=0
    while i<longitud  do
      if array_productos[i].to_s.include? sku
        r.push array_productos[i]
      end
      i+=1
    end
    data_r=r[0][0].to_s
    r[0].push data_cantidad
    resta=r[0][1].to_i
    inventario_final=resta-data_cantidad
    r[0].push inventario_final
    sleep 2
    index_nombre=data_nombre.index(data_r)
    data_boton=query(@@btn_agrega_producto_1[:locator])
    r_posicion =data_boton[index_nombre]
    #data_boton=("android.widget.EditText id:'act3205RowCantidadLabel' index:#{index_nombre}")
    #touch(data_boton)
    #enter_text(data_boton,cajas)
    #press_user_action_button('search')
    $Data=r[0]
    if $Data[1].to_i<data_cantidad
      raise("Error=> Las cajas a agregar superan al inventario. Es necesario agregar menos cajas ")

    end
    e=0
    while e< data_cantidad do
      touch(r_posicion)
      e+=1
    end
    sleep 1
  or_exec_uielement(@@btn_agregar_productos_confirmar )
  sleep 1
  assert_for_element_exist(@@btn_popup[:locator], 20)
  or_exec_uielement(@@btn_popup)
  sleep 1

    clave_producto=sku
    espacio=" - "
    clave_producto=clave_producto+espacio
    sku=$Data[0].to_s
    sku.gsub!(clave_producto,"")
  $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
  $DTH_INVENTARIO[:sku] =sku
  $DTH_INVENTARIO[:inventario_inicial]=$Data[1]
  $DTH_INVENTARIO[:inventario_final] =$Data[3].to_i
    $DATOS.push($DTH_INVENTARIO)

  end








  def buscar_producto_no_vendible
    sku=$dt_row[:sku_no_vendible].to_s.strip
    nombre=query(@@txt_nombre_pedido_del_dia[:locator],:text)
    cantidad=query(@@txt_cantidad_pedido_del_dia[:locator],:text)
    regex=Regexp.new(sku)
    r_sku=nombre.find{|e|regex=~e}
    posicion= nombre.index(r_sku)
    if posicion ==nil
      puts"no se encontro un producto no vendible en pedido del dia "
      or_exec_uielement(@@btn_mas_producto)
      sleep 2
      q = query("android.widget.TextView id:'act3205RowProductoLabel'  {text CONTAINS '#{sku}'}",:text )
    if q.empty?
      puts"no se encontro un producto no vendible en + productos "
      sleep 1
      adb_keys("KEYCODE_BACK")
    end
    else
      raise("Error=>se encontro un producto no vendible ")

    end
  end
  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES


  #############################################################################
  #############################################################################
end


