# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'
require_relative '../../../../helpers/data_db/data_sql'


class LoginPage < Calabash::ABase

  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_login'
  @@txt_usuario = get_or_data(@or_file, 'txt_usuario')
  @@txt_password = get_or_data(@or_file, 'txt_password')
  @@txt_inicio_page= get_or_data(@or_file, 'txt_inicio_page')
  @@img_logo_gepp= get_or_data(@or_file, 'img_logo_gepp')
  @@fecha_entrega_siguiente= get_or_data(@or_file, 'fecha_entrega_siguiente')
  @@txt_version_app= get_or_data(@or_file, 'txt_version_app')
  @@txt_bodega= get_or_data(@or_file, 'txt_bodega')
  @@txt_ruta= get_or_data(@or_file, 'txt_ruta')
  @@btn_tcomm_local= get_or_data(@or_file, 'btn_tcomm_local')
  @@btn_intelli_load= get_or_data(@or_file, 'btn_intelli_load')
  @@view_cargas_page= get_or_data(@or_file, 'view_cargas_page')
  @@lbl_version = get_or_data(@or_file, 'lbl_version')
  @@img_logo = get_or_data(@or_file, 'lbl_logo')
  @@lbl_fecha = get_or_data(@or_file, 'lbl_fecha')
  @@lbl_ruta = get_or_data(@or_file, 'lbl_ruta')
  @@lbl_etiqueta_clave = get_or_data(@or_file, 'lbl_etiqueta_clave')
  @@btn_ingresar = get_or_data(@or_file, 'btn_ingresar')
  @@lbl_menu_principal = get_or_data(@or_file, 'lbl_menu_principal')
  @@txt_cargas = get_or_data(@or_file, 'txt_cargas')
  @@img_inventario= get_or_data(@or_file, 'img_inventario')
  @@lbl_presione_sincronizar= get_or_data(@or_file, 'lbl_presione_sincronizar')
  @@btn_sincronizar= get_or_data(@or_file, 'btn_sincronizar')
  @@lbl_lbl_configuracion_inicial= get_or_data(@or_file, 'lbl_lbl_configuracion_inicial')
  @@lbl_version_configuracion_inicial= get_or_data(@or_file, 'lbl_version_configuracion_inicial')
  @@txt_bodega_configuracion_inicial= get_or_data(@or_file, 'txt_bodega_configuracion_inicial')
  @@img_impresora= get_or_data(@or_file, 'img_impresora')
  @@btn_tercer_tcomm= get_or_data(@or_file, 'btn_tercer_tcomm')
  @@btn_1460_siguiente= get_or_data(@or_file, 'btn_1460_siguiente')
  @@logo_pepsi= get_or_data(@or_file, 'logo_pepsi')
  @@lbl_confirma_km= get_or_data(@or_file, 'lbl_confirma_km')
  @@lbl_economico= get_or_data(@or_file, 'lbl_economico')
  @@lbl_kilometraje= get_or_data(@or_file, 'lbl_kilometraje')
  @@btn_palomita= get_or_data(@or_file, 'btn_palomita')
  @@btn_etiqueta= get_or_data(@or_file, 'btn_etiqueta')
  @@btn_servicio_al_cliente= get_or_data(@or_file, 'btn_servicio_al_cliente')
  @@btn_popup= get_or_data(@or_file, 'btn_popup')
  @@btn_popup_no= get_or_data(@or_file, 'btn_popup_no')
  @@lbl_login= get_or_data(@or_file, 'lbl_login')
  #############################################################################
  # ACCIONES
  #############################################################################


  def llenar_formulario(tipo_usuario = nil)
    sleep 2
    or_exec_uielement(@@txt_usuario, $dt_row[:usuario].to_s.strip)
    press_user_action_button('search')
    or_exec_uielement(@@txt_password, $dt_row[:password].to_s.strip)



  rescue Exception => e
    raise("llenar_formulario => Error no se pudo llenar el formulario 'login'.
             \nException => #{e.message}")
  end

  def send_text_bodega_ruta
    #Se obtiene la bodega y ruta del csv correspondiente para el tipo de usuario, ya sea Tlane o Mixcoac
    or_exec_uielement(@@txt_bodega, $dt_row[:bodega].to_s.strip)
    or_exec_uielement(@@txt_ruta, $dt_row[:ruta].to_s.strip)
  rescue Exception => e
    raise("ingresar_campo_bodega => Error no se pudo ingresar la bodega. y ruta'")

    end

  def send_text_bodega
    #Se obtiene la bodega del csv correspondiente para el tipo de usuario, ya sea Tlane o Mixcoac
              or_exec_uielement(@@txt_bodega_configuracion_inicial, $dt_row[:bodega].to_s.strip)
    sleep 2
  rescue Exception => e
    raise("ingresar_campo_bodega => Error no se pudo ingresar la bodega.")

  end


  def touch_btn_tcomm_local
    assert_for_element_exist(@@btn_tcomm_local[:locator], 400)
    or_exec_uielement(@@btn_tcomm_local)
    sleep 120
  rescue Exception => e
    raise("touch_btn_tcomm => Error al hacer touch en el boton tcomm local")
  end

  def touch_txt_carga
    sleep 2
    or_exec_uielement(@@txt_cargas)
  rescue Exception => e
    raise("seleccionar_carga=> Error al seleccionar una carga")
  end


  def touch_btn_ingresar?
    or_exec_uielement(@@btn_ingresar)

  rescue Exception => e
    raise("touch_btn_ingresar? => Error al hacer clic en 'Siguiente'
             \nException => #{e.message}")

  end

  def aceptar_pop_up
    assert_for_element_exist(@@btn_popup[:locator], 50, "Error=>no se mostro la ventana emergente ")
    or_exec_uielement(@@btn_popup)

    sleep 2
  end
  def rechazar_pop_up
    assert_for_element_exist(@@btn_popup_no[:locator], 50,"Error=> no se mostro la ventana emergente")
    or_exec_uielement(@@btn_popup_no)
  end

  def touch_img_inventario
    assert_for_element_exist(@@img_inventario[:locator], 20,
                             "Error => no se cargo el inventario")
    sleep 1
    or_exec_uielement(@@img_inventario)
  rescue Exception => e
    raise("touch_img_inventario => Error al hacer touch en el inventario")
    end

  def touch_btn_sincroniazar
    sleep 1
    or_exec_uielement(@@btn_sincronizar)
  rescue Exception => e
    raise("touch_btn_sincroniazar => Error al hacer touch en el sincronizar")
  end

  def touch_img_impresora
    sleep 5
    or_exec_uielement(@@img_impresora)
  rescue Exception => e
    raise("touch_img_impresora => Error al imprimir inventario")
  end


  def touch_btn_tercer_tcomm
    assert_for_element_exist(@@btn_tercer_tcomm[:locator], 400)
    or_exec_uielement(@@btn_tercer_tcomm)
  rescue Exception => e
    raise("touch_btn_tercer_tcomm => Error al dar click en el tercer tcomm")
  end


  def touch_btn_siguiente_1460
    sleep 2
    assert_for_element_exist(@@btn_1460_siguiente[:locator], 350)
    or_exec_uielement(@@btn_1460_siguiente)
  rescue Exception => e
    raise("touch_btn_1460 => Error al dar click en siguiente")
  end

  def send_text_datos_kilometraje
    sleep 7
    or_exec_uielement(@@lbl_economico,$dt_row[:economico].to_s.strip)
    sleep 2
    or_exec_uielement(@@lbl_kilometraje,$dt_row[:kilometraje].to_s.strip )
  rescue Exception => e
    raise("send_text_datos_kilometraje => Error al ingresar los datos de control")

  end
  def send_text_confirmacion_km
    sleep 2
    or_exec_uielement(@@lbl_confirma_km,$dt_row[:kilometraje].to_s.strip )
  rescue Exception => e
    raise("send_text_datos_kilometraje => Error al ingresar los datos de control")
  end

  def touch_btn_palomita
    sleep 2
    or_exec_uielement(@@btn_palomita )
  rescue Exception => e
    raise("touch_btn_1460 => Error al dar click en siguiente")

  end

  def touch_btn_etiqueta
    sleep 2
    or_exec_uielement(@@btn_etiqueta)
  rescue Exception => e
    raise("touch_btn_1460 => Error al dar click en siguiente")
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_login_page_displayed?
    assert_for_element_exist(@@txt_usuario[:locator], 400,
                             "Error => no se cargo la pagina de login")
  end

  def assert_incio_page_displayed?
    assert_for_element_exist(@@txt_inicio_page[:locator], 200,
                             "Error => no se cargo la pagina 1420")
  end



  def assert_cargas_page_displayed?

    assert_for_element_exist(@@view_cargas_page[:locator], 400,
                             "Error => no se mostro la pagina de cargas")
  end

  def assert_atributos_inicio_displayed?
    r=Array.new
    puts("validando atributos de la pagina de incio")

    ##Se valida el logotipo Gepp
    r.push verity_for_element_exist(@@img_logo_gepp[:locator], 20,
                                    "Error => no se cargo el logotipo de gepp")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido el logo de Gepp"
    end

    #Se valida la fecha de entrega siguiente
    r.push verity_for_element_exist(@@fecha_entrega_siguiente[:locator], 20,
                                    "Error => no se cargo la fecha de entrega siguiente")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido la fecha de entrega siguiente "
    end

    #Se valida la version de la app
    r.push verity_for_element_exist(@@txt_version_app[:locator], 20,
                                    "Error => no se cargo la version de la app")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido la version de la app"
    end

  end






  def assert_atributos?
    r = Array.new
    puts("Validando atributos de la pagina de 01_mp_tecom_y_login")

    ##VALIDO VERSION
    r.push verity_for_element_exist(@@lbl_version[:locator], 200,
                                    "Error => No se encontro la version de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro la version de la aplicacion"
    end

    ##VALIDO LOGO
    r.push verity_for_element_exist(@@img_logo[:locator], 200,
                                    "Error => No se encontro el logo de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro el logo de la aplicacion"
    end



    ##FECHA DE LA OPERACION
    r.push verity_for_element_exist(@@lbl_fecha[:locator], 200,
                                    "Error => No se encontro la fecha de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro la fecha de la aplicacion"
    end


    ##VALIDO RUTA
    r.push verity_for_element_exist(@@lbl_ruta[:locator], 200,
                                    "Error => No se encontro la ruta de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro la ruta de la aplicacion"
    end


    ##VALIDO ENTRADA DE USUARIO
    r.push verity_for_element_exist(@@txt_usuario[:locator], 200,
                                    "Error => No se encontro el input de usuario de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro el input de usuario de la aplicacion"
    end


    ##VALIDO ENTRADA DE CONTRASEÑA
    r.push verity_for_element_exist(@@txt_password[:locator], 200,
                                    "Error => No se encontro el input de contraseña de la aplicacion")
    if r.to_s.include? 'true'
      puts "Se encontro el input de contraseña de la aplicacion"
    end


    ##VALIDO ETIQUETA INGRESA TU CLAVE DE EMPLEADO
    r.push verity_for_element_exist(@@lbl_etiqueta_clave[:locator], 200,
                                    "Error => No se encontro la etiqueta 'INGRESA TU CLAVE DE EMPLEADO' y banderas MOF")
    if r.to_s.include? 'true'
      puts "Se encontro la etiqueta 'INGRESA TU CLAVE DE EMPLEADO' y banderas MOF"
    end


    ##VALIDO BOTON SIGUIENTE
    r.push verity_for_element_exist(@@btn_ingresar[:locator], 200,
                                    "Error => No se encontro el boton 'Siguiente'")
    if r.to_s.include? 'true'
      puts "Se encontro el boton 'Siguiente'"
    end


  end

  def assert_menu_principal?
    sleep 5
    assert_for_element_exist(@@lbl_menu_principal[:locator], 20,
                             "Error => no se cargo la pagina del menu principal")
    end

  def assert_lbl_configuracion_inicial?
    sleep 2
    assert_for_element_exist(@@lbl_lbl_configuracion_inicial[:locator], 20,
                             "Error => no se cargo la etiqueta de Configuracion Inicial")
  end

  def assert_atributos_configuracion_inicial?
    r=Array.new
    puts("Atributos de la ventana Configuracion Inicial")

    ##Se valida el logotipo Gepp
    r.push verity_for_element_exist(@@img_logo_gepp[:locator], 20,
                                    "Error => no se cargo el logotipo de gepp")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido el logo de Gepp"
    end

    ##Se valida la version de la app
    r.push verity_for_element_exist(@@lbl_version_configuracion_inicial[:locator], 20,
                                    "Error => no se cargo la version de gepp")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido la version de Gepp"
    end

    ##Se valida la leyenda 'Presione sincronizar'
    r.push verity_for_element_exist(@@lbl_presione_sincronizar[:locator], 20,
                                    "Error => no se cargo la leyenda Presione sinronizar")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido la leyenda Presione Sincronizar"
    end

    ##Se valida que exista el campo de Bodega
    r.push verity_for_element_exist(@@txt_bodega_configuracion_inicial[:locator], 20,
                                    "Error => no se cargo el input para Bodega")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido el input para Bodega"
    end

    ##Se valida que exista el boton Sincronizar
    r.push verity_for_element_exist(@@btn_sincronizar[:locator], 20,
                                    "Error => no se cargo el boton Sincronizar")
    sleep 2
    if r.to_s.include? 'true'
      puts "Se valido el boton sincronizar"
    end

  end

  def assert_logo_pepsi_displayed?
    sleep 2
    assert_for_element_exist(@@logo_pepsi[:locator],500)

  end

  def assert_btn_servicio_al_cliente_displayed?
    sleep 2
    assert_for_element_exist(@@btn_servicio_al_cliente[:locator], 500)

    end

  def assert_lbl_login?
    sleep 2
    assert_for_element_exist(@@lbl_login[:locator], 500)

  end



end
