require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'



class FinDiaPage < Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_fin_dia'
  @@btn_liquidar_ruta=get_or_data(@or_file, 'btn_liquidar_ruta')
  @@lbl_6030_page=get_or_data(@or_file, 'lbl_6030_page')
  @@lbl_ventas_brutas=get_or_data(@or_file, 'lbl_ventas_brutas')
  @@lbl_descuentos=get_or_data(@or_file, 'lbl_descuentos')
  @@lbl_ventas_netas=get_or_data(@or_file, 'lbl_ventas_netas')
  @@lbl_creditos_cobrados=get_or_data(@or_file, 'lbl_creditos_cobrados')
  @@lbl_total_a_liquidar=get_or_data(@or_file, 'lbl_total_a_liquidar')
  @@lbl_credito_otorgado=get_or_data(@or_file, 'lbl_credito_otorgado')
  @@lbl_otras_monedas=get_or_data(@or_file, 'lbl_otras_monedas')
  @@lbl_cheques=get_or_data(@or_file, 'lbl_cheques')
  @@lbl_efectivo_a_liquidar=get_or_data(@or_file, 'lbl_efectivo_a_liquidar')
  @@txt_efectivo_a_entregar=get_or_data(@or_file, 'txt_efectivo_a_entregar')
  @@lbl_total_liquidado=get_or_data(@or_file, 'lbl_total_liquidado')
  @@lbl_diferencia=get_or_data(@or_file, 'lbl_diferencia')
  @@combo_motivo=get_or_data(@or_file, 'combo_motivo')
  @@btn_motivo=get_or_data(@or_file, 'btn_motivo')
  @@btn_seleccionar_todo=get_or_data(@or_file, 'btn_seleccionar_todo')
  @@btn_guardar=get_or_data(@or_file, 'btn_guardar')
  @@btn_siguiente=get_or_data(@or_file, 'btn_siguiente')
  @@lbl_resumen_de_ventas_diarias=get_or_data(@or_file, 'lbl_resumen_de_ventas_diarias')
  @@txt_total_ventas=get_or_data(@or_file, 'txt_total_ventas')
  @@btn_siguiente_resumen_ventas=get_or_data(@or_file, 'btn_siguiente_resumen_ventas')
  @@btn_tcomm_fin_de_dia=get_or_data(@or_file, 'btn_tcomm_fin_de_dia')



  #############################################################################
  # ACCIONES
  #############################################################################
  def assert_btn_liquidar_ruta_displayed?
    sleep 2
    assert_for_element_exist(@@btn_liquidar_ruta[:locator], 10)

    or_exec_uielement(@@btn_liquidar_ruta)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'INVENTARIO'.
             \nException => #{e.message}")
  end


  def assert_6030_page_displayed?
    sleep 2
    assert_for_element_exist(@@lbl_6030_page[:locator], 10, "Error=>No se mostro la pantalla 6030, Resumen del deposito")
  end

  def elimina_caracteres_y_compara_valores(query,valor_a_comparar,tipo_valor)
    query.map!{|q| q.delete('  $,')}
    r_query=query[0].to_f

    if r_query == valor_a_comparar
      puts "el valor #{tipo_valor} es correcto"
    else

      raise "Error=> el valor #{tipo_valor} es incorrecto , deberia ser #{valor_a_comparar} pero se muestra #{r_query}"
    end

  end

  def validar_resumen_de_deposito
    hash= get_csv_data 'dt_inventario'
    hash=hash[1..-1]
    #CALCULO EL VALOR DE LA S VENTAS OBTENIENDO LAS HASH DEL CSV
    #RESUMEN DE LIQUIDACION
    neto=hash.map{ |h| h[:neto]}
    ventas_brutas=neto.each.map(&:to_f).inject(0,:+)
    $ventas_brutas=ventas_brutas
    q_ventas_brutas=query(@@lbl_ventas_brutas[:locator],:text)
      elimina_caracteres_y_compara_valores(q_ventas_brutas,ventas_brutas,"ventas brutas")

    dto=hash.map{ |h| h[:descuento_total]}
    descuentos=dto.each.map(&:to_f).inject(0,:+)
    q_descuento=query(@@lbl_descuentos[:locator],:text)
    elimina_caracteres_y_compara_valores( q_descuento,descuentos,"descuentos")


    ventas_netas=ventas_brutas-descuentos
    q_ventas_netas=query(@@lbl_ventas_netas[:locator],:text)
    elimina_caracteres_y_compara_valores(q_ventas_netas,ventas_netas,"ventas netas")

    pago_cr=hash.map{ |h| h[:pagos_cr]}
    cred_cobrados=pago_cr.each.map(&:to_f).inject(0,:+)
    q_cred_cobrados=query(@@lbl_creditos_cobrados[:locator],:text)
    elimina_caracteres_y_compara_valores(q_cred_cobrados,cred_cobrados,"creditos cobrados")

    total_a_liquidar=ventas_netas+cred_cobrados
    q_total_a_liquidar=query(@@lbl_total_a_liquidar[:locator],:text)
    elimina_caracteres_y_compara_valores(q_total_a_liquidar,total_a_liquidar,"total a liquidar")


    #RESUMEN DE PAGOS
    credito_revolvente=hash.map{ |h| h[:monto_cr]}
    cred_otor=credito_revolvente.each.map(&:to_f).inject(0,:+)
    q_credito_otorgado=query(@@lbl_credito_otorgado[:locator],:text)
    elimina_caracteres_y_compara_valores(q_credito_otorgado,cred_otor,"credito otorgado")

    monedas=hash.map{ |h| h[:cupones]}
    otras_monedas=monedas.each.map(&:to_f).inject(0,:+)
    q_otras_monedas=query(@@lbl_otras_monedas[:locator],:text)
    elimina_caracteres_y_compara_valores(q_otras_monedas,otras_monedas,"otras monedas")

    cheque=hash.map{ |h| h[:cheques]}
    cheques=cheque.each.map(&:to_f).inject(0,:+)
    q_cheques=query(@@lbl_cheques[:locator],:text)
    elimina_caracteres_y_compara_valores(q_cheques,cheques,"cheques")

    efectivo=hash.map{ |h| h[:efectivo]}
    efectivo_a_liquidar=efectivo.each.map(&:to_f).inject(0,:+)
    $efectivo_pre_liquidacion=efectivo_a_liquidar
    q_efectivo=query(@@lbl_efectivo_a_liquidar[:locator],:text)
    elimina_caracteres_y_compara_valores(q_efectivo,efectivo_a_liquidar,"efectivo a liquidar")
    sleep 2
  end


  def ingresar_efectivo_para_la_pre_liquidacion_de_ruta
    tipo_numero= $efectivo_pre_liquidacion.is_a? Integer
    if tipo_numero == false
      efectivo=$efectivo_pre_liquidacion
      efectivo_entregado = '%.2f' % efectivo
      touch(@@txt_efectivo_a_entregar[:locator])
      keyboard_enter_text(efectivo_entregado)
      press_user_action_button('search')

    else
      or_exec_uielement(@@txt_efectivo_a_entregar,$efectivo_pre_liquidacion)
      keyboard_enter_text(efectivo_entregado)
      sleep 2
      press_user_action_button('search')
      sleep 2
    end

    q_total_liquidado=query(@@lbl_total_liquidado[:locator],:text)
    #elimina_caracteres_y_compara_valores(q_total_liquidado,$ventas_brutas,"total liquidado")
    q_diferencia=query(@@lbl_diferencia[:locator],:text)
    #elimina_caracteres_y_compara_valores(q_diferencia,0,"diferencia")



  end

  def seleccionarMotivoDeCuentasNoAtendidas()
    sleep 2
    assert_for_element_exist(@@combo_motivo[:locator], 10)
    or_exec_uielement(@@combo_motivo)

 # rescue Exception => e
 #   raise("seleccionarMotivoDeCuentasNoAtendidas => Error al hacer touch en el combo de motivos.
  #           \nException => #{e.message}")

      sleep 2
    motivo = $dt_row[:motivo_de_cuenta_no_atendida]
    puts "Motivo es: ", motivo
    sleep 2

    el = val_or_uielement(@@btn_motivo,motivo)
    # puts el["element"]
  #  scroll_down_to_uielement_adr el["element"]
    assert_for_element_exist(el["element"], 20,
                             "Error => no se encontro el motivo")
    sleep 2
    #elementoNombreCombo = el
    touch(el["element"])
    sleep 2
  end

  def seleccionarTodosLosClientesNoAtentidos()
    sleep 2
    assert_for_element_exist(@@btn_seleccionar_todo[:locator], 10)
    or_exec_uielement(@@btn_seleccionar_todo)

  end

  def clicEnBotonGuardar()
    sleep 2
    assert_for_element_exist(@@btn_guardar[:locator], 10)
    or_exec_uielement(@@btn_guardar)

    sleep 8
  end

  def clicEnSiguiente()
    sleep 2
    assert_for_element_exist(@@btn_siguiente[:locator], 10)
    or_exec_uielement(@@btn_siguiente)

  end

  def validarPantalla6010()
    sleep 2
    assert_for_element_exist(@@lbl_resumen_de_ventas_diarias[:locator], 10)
  end

  def validarTotalDeVentas()
    sleep 2

    totalVentas = query(@@txt_total_ventas[:locator], :text)
    totalVentas = totalVentas[0].delete("$,")
   # totalVentas.to_f
   # puts "Total de Ventas es: ", totalVentas

    hash= get_csv_data 'dt_inventario'
    hash=hash[1..-1]
    #CALCULO EL VALOR DE LA S VENTAS OBTENIENDO LAS HASH DEL CSV
    #RESUMEN DE LIQUIDACION
    neto=hash.map{ |h| h[:neto]}
    ventas_brutas=neto.each.map(&:to_f).inject(0,:+)
    #puts "ventas_brutas es: ", ventas_brutas
    ventas_brutas = '%.2f' % ventas_brutas


   # ventas_brutas = 2332.00
    if ventas_brutas == totalVentas
      puts "El total de las ventas es correcto. Total calculado: ", ventas_brutas , " total del dispositivo: ", totalVentas

    else
      raise "Error=> el total del dispositivo no es correcto #{totalVentas}, total calculado: #{ventas_brutas} "
    end

  end

  def siguienteEnResumenDeLasVentas()
    sleep 2
    assert_for_element_exist(@@btn_siguiente_resumen_ventas[:locator], 10)
    or_exec_uielement(@@btn_siguiente_resumen_ventas)

  end

  def clicTcommFinDeDia()
    sleep 2
    assert_for_element_exist(@@btn_tcomm_fin_de_dia[:locator], 10)
    or_exec_uielement(@@btn_tcomm_fin_de_dia)
  end

  def obtener_inventario_final
    hash= get_csv_data 'dt_inventario'
    hash=hash[1..-1]
    hash=hash[1..-1]
    hash.map do |hash|
      hash.tap{|h| h.delete(:id)}
      hash.tap{|h| h.delete(:inventario_inicial)}
      hash.tap{|h| h.delete(:monto_combo1)}
      hash.tap{|h| h.delete(:monto_combo2)}
      hash.tap{|h| h.delete(:descuento_total)}
      hash.tap{|h| h.delete(:importe)}
      hash.tap{|h| h.delete(:efectivo)}
      hash.tap{|h| h.delete(:cheques)}
      hash.tap{|h| h.delete(:cupones)}
      hash.tap{|h| h.delete(:pagos_cr)}
      hash.tap{|h| h.delete(:monto_cr)}
      hash.tap{|h| h.delete(:neto)}
      hash.tap{|h| h.delete(:nombre_ticket)}
      hash.tap{|h| h.delete(:devolucion_envase_grb)}
      hash.tap{|h| h.delete(:devolucion_envase_garrafon)}
      hash.tap{|h| h.delete(:prestamo_envase_grb)}
      hash.tap{|h| h.delete(:prestamo_envase_garrafon)}
      hash.tap{|h| h.delete(:cobro_cajilla)}
      hash.tap{|h| h.delete(:cobro_envase_garrafon)}
      hash.tap{|h| h.delete(:cobro_envase_grb)}
      hash.tap{|h| h.delete(:cobro_envase_grb_y_garrafon)}
      if hash[:sku]== nil
        hash[:sku]="-"
      end
    end
    hash.sort_by! { |h | h[:inventario_final] }
    hash.uniq! {|e| e[:sku] }
    hash=hash[1..-1]
    count=hash.length
    sleep 2

    i=0
    while i<count  do
      sku = hash[i][:sku].to_s
      inventario_final=hash[i][:inventario_final].to_s
      x="//*[contains(@text,'"+sku+"')]/following-sibling::*[contains(@resource-id,'inv_row_prod_header_Total')]"

      scroll_down_to_uielement(x,
                               15,
                               'mate')

      if mobile_element_exists(x)
        resultado_inventario=get_mobile_element_attribute(x,"text")
      else

        scroll_up_to_uielement(x,
                                 40,
                                 'mate')

        resultado_inventario=get_mobile_element_attribute(x,"text")
      end

      if resultado_inventario== inventario_final
        puts "el inventario del sku: #{sku} si cuadra"
      else
          puts "el inventario del sku: #{sku},no cuadra se muestra #{resultado_inventario} y debería ser #{inventario_final}"
      end

      i+=1
    end

  end
  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################
end

