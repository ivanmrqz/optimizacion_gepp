# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'

class RevisitasPage < Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_revisitas'
  @@btn_mas_opciones=get_or_data(@or_file, 'btn_mas_opciones')
  @@btn_visitados=get_or_data(@or_file, 'btn_visitados')
  @@lbl_page_1500=get_or_data(@or_file, 'lbl_page_1500')
  @@btn_cambiar_pedido=get_or_data(@or_file, 'btn_cambiar_pedido')
  @@btn_no_venta=get_or_data(@or_file, 'btn_no_venta')
  @@sl_departamento_rt=get_or_data(@or_file, 'sl_departamento_rt')
  @@sl_opcion_departamento_rt=get_or_data(@or_file, 'sl_opcion_departamento_rt')
  @@sl_motivo_rt=get_or_data(@or_file, 'sl_motivo_rt')
  @@sl_opcion_motivo_rt=get_or_data(@or_file, 'sl_opcion_motivo_rt')
  @@lbl_page_6000=get_or_data(@or_file, 'lbl_page_6000')

  #############################################################################
  # ACCIONES
  #############################################################################

  def touch_btn_mas_opciones
    assert_for_element_exist(@@btn_mas_opciones[:locator], 10)

    or_exec_uielement(@@btn_mas_opciones)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'mas opciones'.
             \nException => #{e.message}")
  end

  def touch_btn_visitados
    assert_for_element_exist(@@btn_visitados[:locator], 10)

    or_exec_uielement(@@btn_visitados)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'visitados'.
             \nException => #{e.message}")
  end

  def touch_btn_no_venta
    assert_for_element_exist(@@btn_no_venta[:locator], 10)

    or_exec_uielement(@@btn_no_venta)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'visitados'.
             \nException => #{e.message}")
  end


  def touch_btn_cambiar_pedido?
    assert_for_element_exist(@@btn_cambiar_pedido[:locator], 10)

    or_exec_uielement(@@btn_cambiar_pedido)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'visitados'.
             \nException => #{e.message}")
  end

  def validar_skus_venta_anterior

    hash=$DATOS

    bandera=hash.length
    i=0
    while i<bandera do
      sku=hash[i][:sku].to_s

      r_sku=sku.split("-")

      if r_sku == nil
        puts r_sku, "nil"
      else

        x="(//*[contains(@text,'"+r_sku[1].to_s+"')])[1]"
        puts x
        scroll_down_to_uielement(x,
                                 4,
                                 'mate')
        #assert_for_mobile_element(x,10,"Error=>no se encontro el producto")
        if mobile_element_exists(x)
          puts
        else
          scroll_up_to_uielement(x,4,'mate')
          end

        i+=1
      end

    end


  end

  def rechazo_parcial_revisita

    $DTH_INVENTARIO = {
        :id=>nil,
        :sku=> nil,
        :inventario_inicial=> nil,
        :inventario_final => nil,
        :monto_combo1=>nil,
        :monto_combo2=>nil,
        :descuento_total =>nil,
        :importe => nil,
        :efectivo=>nil,
        :cheques=>nil,
        :cupones=>nil,
        :pagos_cr=>nil,
        :monto_cr=>nil,
        :neto => nil,
        :nombre_ticket=>nil,
        :devolucion_envase_grb=>nil,
        :devolucion_garrafon=>nil,
        :prestamo_envase_grb=>nil,
        :prestamo_envase_garrafon=>nil,
        :cobro_envase_grb=>nil,
        :cobro_envase_garrafon=>nil,
        :cobro_envase_grb_y_garrafon=>nil,
        :cobro_cajilla=>nil,
    }
    sku=$dt_row_revisita[:sku_rechazo_parcial]
    cajas=$dt_row_revisita[:cajas_rechazo_parcial].to_i
    if sku == nil
      raise("Error=> no se agrego ningun sku en el csv")
    end
    x_menos_cajas="//*[contains(@text,'"+sku+"')]/preceding-sibling::*[@class='android.widget.Button']"
    i_inicial="//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.EditText']"
    x_sku="//*[contains(@text,'"+sku+"')]"
    scroll_down_to_uielement(x_menos_cajas,
                             10,
                             'mate')
    #assert_for_mobile_element(x_sku,"error=>No se encontro el articulo para rechazo parcial ")
    if mobile_element_exists(x_sku)
      puts
    else
      raise("Error=>")
    end
    cajas_agregadas= (get_mobile_element_attribute i_inicial,"text").to_i
    r_x_sku=get_mobile_element_attribute x_sku,"text"
    if cajas_agregadas < cajas
      raise("Error=>Debes agregar menos cajas para rechazo parcial en el csv el maximo a disminuir es #{cajas_agregadas}")
    end
    i=0
    while i<cajas  do
      tap_element(x_menos_cajas)
      i+=1
    end

    $DTH_INVENTARIO[:id]=$dt_row_revisita[:id].to_s.strip
    $DTH_INVENTARIO[:sku] =r_x_sku
    $DTH_INVENTARIO[:inventario_inicial]=cajas_agregadas
    $DTH_INVENTARIO[:inventario_final] =cajas_agregadas-cajas
    $DATOS.push($DTH_INVENTARIO)
    puts $DATOS


  end


  def modificar_valores_hash
   $DATOS.map! do |h|
     h[:id]=$dt_row_revisita[:id].to_s.strip
     h

   end
    puts $DATOS
  end


  def seleccionar_departamento_rt
    departamento_rt =$dt_row_revisita[:departamento_rt].to_s
    or_exec_uielement(@@sl_departamento_rt )
    sleep 3
    el = val_or_uielement @@sl_opcion_departamento_rt, departamento_rt
    puts "el = #{el['element']}"
    or_exec_uielement(@@sl_opcion_departamento_rt, departamento_rt )

  rescue Exception => e
    raise("seleccionar_motivo_credito_revolvente => Error al seleccionar 'Departamento Rechazo total'.
             \nException => #{e.message}")
  end

  def seleccionar_motivo_de_rechazo
   motivo_rt=$dt_row_revisita[:motivo_rechazo].to_s
   puts "motivo_rt"
    or_exec_uielement(@@sl_motivo_rt)
    sleep 3
    el = val_or_uielement @@sl_opcion_motivo_rt, motivo_rt
    puts "el = #{el['element']}"
    or_exec_uielement(@@sl_opcion_motivo_rt, motivo_rt )
   #Al ser rechazo Total Reinicio la hash $DATOS para que no contenga algun valor
    $DATOS=Array.new

  rescue Exception => e
    raise("seleccionar_motivo_credito_revolvente => Error al seleccionar 'motivo de rechazo total'.
             \nException => #{e.message}")
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_pantalla_visitados_displayed?
    assert_for_element_exist(@@btn_visitados[:locator], 10, "Error=> No se mostro la pantalla visitados")
  end

  def assert_pantalla_1500_displayed?
    assert_for_element_exist(@@lbl_page_1500[:locator], 10, "Error=> No se mostro la pantalla visitados")

  end

  def assert_pantalla_6000_displayed?
    assert_for_element_exist(@@lbl_page_6000[:locator], 10, "Error=> No se mostro la pantalla 6000")

  end



end