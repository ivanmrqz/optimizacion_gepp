# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class ArPage< Calabash::ABase

  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_ar'
  @@importe=get_or_data(@or_file, 'importe')
  @@lbl_page_3501=get_or_data(@or_file, 'lbl_page_3501')
  @@txt_folio_recibo=get_or_data(@or_file, 'txt_folio_recibo')
  @@txt_orden_de_compra=get_or_data(@or_file, 'txt_orden_de_compra')
  @@btn_next=get_or_data(@or_file, 'btn_next')
  @@btn_siguiente=get_or_data(@or_file, 'btn_siguiente')


  #############################################################################
  # METODOS
  #############################################################################
  $r_permanente=Array.new
  $r_especial=Array.new
  $importe_x_producto=Array.new
  $descuento_x_producto=Array.new
  $precio_final=Array.new
  $iva=Array.new
  def calcula_precio_sin_iva(precio_sku,impuesto)
    iva=impuesto.to_f/100
    iva=iva+1
    precio_sin_iva= (precio_sku.to_f/iva.to_f).round(2)
    return precio_sin_iva.to_f
  end

  def calcula_dto_permanente_porcentaje(precio_sin_iva,cantidad_dto_permanente)
    if cantidad_dto_permanente.to_i==0
      total_a_descontar=0
      $r_permanente.push(total_a_descontar)
      return precio_sin_iva
    else
      cantidad_dto_permanente=(100-cantidad_dto_permanente.to_f)/100
      dto_permanente= precio_sin_iva*cantidad_dto_permanente
      r_dto_permanente=(dto_permanente).round(2)
      total_a_descontar=precio_sin_iva-r_dto_permanente
      $r_permanente.push(total_a_descontar.to_f.round(2))
      return r_dto_permanente
    end
  end

  def calcula_dto_especial_porcentaje(precio_dto_permanente,cantidad_dto_especial)
    if cantidad_dto_especial.to_i==0
      total_a_descontar=0
      $r_especial.push(total_a_descontar)
      return precio_dto_permanente

    else
      cantidad_dto_especial=(100-cantidad_dto_especial.to_f)/100
      dto_especial= precio_dto_permanente.to_f*cantidad_dto_especial.to_f
      r_dto_especial=(dto_especial).round(2)
      total_a_descontar=precio_dto_permanente-r_dto_especial
      $r_especial.push(total_a_descontar.to_f.round(2))
      return r_dto_especial
    end
  end

  def agrega_iva_y_agrega_cajas(precio_dto_especial,impuesto,cajas)
    iva=impuesto.to_f/100
    iva=iva+1
    precio_con_iva=precio_dto_especial.to_f*iva
    r_precio_con_iva=precio_con_iva.round(2)

    precio_final=r_precio_con_iva*cajas
    $precio_final.push(precio_final)
    return precio_final

  end

  def calcula_dto_permanente_monto(precio_sin_iva,cantidad_dto_permanente)
    precio_sin_iva=precio_sin_iva.to_f
    cantidad_dto_permanente=cantidad_dto_permanente.to_f
    r_dto_permanente=precio_sin_iva-cantidad_dto_permanente
    puts"total a descontar",cantidad_dto_permanente
    $r_permanente.push(cantidad_dto_permanente.to_f.round(2))
    return r_dto_permanente.to_f.round(2)
  end

  def calcula_dto_especial_monto(precio_dto_permanente,cantidad_dto_especial)
    precio_dto_permanente=precio_dto_permanente.to_f
    cantidad_dto_especial=cantidad_dto_especial
    r_dto_especial=precio_dto_permanente-cantidad_dto_especial
    $r_especial.push(cantidad_dto_especial.to_f.round(2))
    return r_dto_especial.to_f.round(2)

  end




  #############################################################################
  # ACCIONES
  #############################################################################


  def calcula_importe_ar (numero_producto)
    numero_producto=numero_producto.to_s
    tipo_producto = case numero_producto
                      when"pet"
                        sku=$dt_row[:pet]
                        cajas=$dt_row[:cajas_1].to_f
                        precio_con_iva=$dt_row[:precio_con_iva_1].to_f
                        dto_permanente=$dt_row[:dto_permamente_1].to_f
                        tipo_permanente=$dt_row[:tipo_dto_permanente_1]
                        dto_especial=$dt_row[:dto_especial_1].to_f
                        tipo_especial=$dt_row[:tipo_dto_especial_1]
                        impuesto=$dt_row[:impuesto_1].to_f
                      when"garrafon"
                        sku=$dt_row[:garrafon]
                        cajas=$dt_row[:cajas_2].to_f
                        precio_con_iva=$dt_row[:precio_con_iva_2].to_f
                        dto_permanente=$dt_row[:dto_permamente_2].to_f
                        tipo_permanente=$dt_row[:tipo_dto_permanente_2]
                        dto_especial=$dt_row[:dto_especial_2].to_f
                        tipo_especial=$dt_row[:tipo_dto_especial_2]
                        impuesto=$dt_row[:impuesto_2].to_f
                      when"no_carbonatado"
                        sku=$dt_row[:no_carbonatado]
                        cajas=$dt_row[:cajas_3].to_f
                        precio_con_iva=$dt_row[:precio_con_iva_3].to_f
                        dto_permanente=$dt_row[:dto_permamente_3].to_f
                        tipo_permanente=$dt_row[:tipo_dto_permanente_3]
                        dto_especial=$dt_row[:dto_especial_3].to_f
                        tipo_especial=$dt_row[:tipo_dto_especial_3]
                        impuesto=$dt_row[:impuesto_3].to_f
                      when "agua_embotellada"
                        sku=$dt_row[:agua_embotellada]
                        cajas=$dt_row[:cajas_4].to_f
                        precio_con_iva=$dt_row[:precio_con_iva_4].to_f
                        dto_permanente=$dt_row[:dto_permamente_4].to_f
                        tipo_permanente=$dt_row[:tipo_dto_permanente_4]
                        dto_especial=$dt_row[:dto_especial_4].to_f
                        tipo_especial=$dt_row[:tipo_dto_especial_4]
                        impuesto=$dt_row[:impuesto_4].to_f
                    end
    if cajas==nil
      raise("Error=> no se encontro una caja a agregar")
    end

    if sku==nil
      raise("Error=> no se encontro un sku, favor de validar csv")
    end

    sku_sin_iva=calcula_precio_sin_iva(precio_con_iva,impuesto)
    if tipo_permanente== "%"
      sku_con_dto_permanente=calcula_dto_permanente_porcentaje(sku_sin_iva,dto_permanente)
    elsif tipo_permanente =="$"
      sku_con_dto_permanente=calcula_dto_permanente_monto(sku_sin_iva,dto_permanente)
    end

    if tipo_especial=="%"
      sku_con_dto_especial=calcula_dto_especial_porcentaje(sku_con_dto_permanente,dto_especial)
    elsif tipo_especial=="$"
      sku_con_dto_especial=calcula_dto_especial_monto(sku_con_dto_permanente,dto_especial)
    else
    end
    sku_con_iva_y_cajas=agrega_iva_y_agrega_cajas(sku_con_dto_especial,impuesto,cajas)
    importe_x_producto=sku_con_dto_especial*cajas
    $importe_x_producto.push(importe_x_producto.round(2))
    descuento_x_producto=$r_permanente.each.map(&:to_f).inject(0,:+)+$r_especial.each.map(&:to_f).inject(0,:+)
    $descuento_x_producto.push(descuento_x_producto.round(2))
    sleep 2
    if impuesto==0
      iva=0
      $iva.push(iva)
    else
      iva=impuesto.to_f/100
      iva=iva+1
      r_dto_con_iva = sku_con_dto_especial.to_f * iva
      r_dto_con_iva =r_dto_con_iva.round(2)
      iva=r_dto_con_iva-sku_con_dto_especial
      iva=iva*cajas
      $iva.push(iva.round(2))
      puts"iva!!!!",$iva
    end

  end

  def obten_importe_ar

    $DTH_INVENTARIO = {
        :id=>nil,
        :sku=> nil,
        :inventario_inicial=> nil,
        :inventario_final => nil,
        :monto_combo1=>nil,
        :monto_combo2=>nil,
        :descuento_total =>nil,
        :importe => nil,
        :efectivo=>nil,
        :cheques=>nil,
        :cupones=>nil,
        :pagos_cr=>nil,
        :monto_cr=>nil,
        :neto => nil,
        :iva=>nil,
        :nombre_ticket=>nil,
        :devolucion_envase_grb=>nil,
        :devolucion_garrafon=>nil,
        :prestamo_envase_grb=>nil,
        :prestamo_envase_garrafon=>nil,
        :cobro_envase_grb=>nil,
        :cobro_envase_garrafon=>nil,
        :cobro_envase_grb_y_garrafon=>nil,
        :cobro_cajilla=>nil,
    }
    $DTH_INVENTARIO[:id]=$dt_row[:id].to_s.strip
    $DTH_INVENTARIO[:sku] =nil
    $DTH_INVENTARIO[:inventario_inicial]=nil
    $DTH_INVENTARIO[:inventario_final] =nil
    #PUSHEAR PRODUCTO A DATA
    $DATOS.push($DTH_INVENTARIO)

    $descuento_x_producto=$descuento_x_producto.each.map(&:to_f).inject(0,:+)
    $precio_final=$precio_final.each.map(&:to_f).inject(0,:+)
    $precio_final=$precio_final.round(2)

   importe = query(@@importe[:locator],:text).map{|importe| importe.delete('$')}
   r_importe=importe.map{|r_importe| r_importe.delete(',')}
   $importe=$precio_final
   puts "El importe del pedido es: "
   puts r_importe[0]
    puts "el importe de la variable es"
    puts $importe
    iva=$iva.each.map(&:to_f).inject(0,:+)
    puts "el iva es",iva
    neto=$precio_final-iva
    puts "el descuento total es ",neto

   if r_importe[0].to_f ==$importe
     puts "el importe es correcto"
   else
     puts ("Error=> El importe no es correcto ")
   end
  sleep 2
  end


  def ingresar_folio(folio)
  recibo_folio=folio.to_s.strip
    if recibo_folio.empty?
      raise("Error=> no se agrego el primer folio de recibo.")
    end
    or_exec_uielement(@@txt_folio_recibo,recibo_folio)
    press_user_action_button('search')

  rescue Exception => e
    raise("Error al ingresar el folio de recibo
             \nException => #{e.message}")
  end
  
  def ingresar_orden_de_compra(orden)
    recibo_orden=orden.to_s.strip
    if recibo_orden.empty?
      raise("Error=> no se agrego la primer orden de compra.")
    end
    or_exec_uielement(@@txt_orden_de_compra,recibo_orden)
    press_user_action_button('search')
  rescue Exception => e
    raise("Error al ingresar la orden de compra
             \nException => #{e.message}")
  end

  def ingresar_folio_recibo_empresa_1
    recibo_folio=$dt_row[:folio_recibo_1].to_s.strip
    ingresar_folio(recibo_folio)
  end

  def ingresar_folio_recibo_empresa_2
    recibo_folio=$dt_row[:folio_recibo_2]
    if recibo_folio==nil
      puts "No se ingreso un folio de recibo para la empresa 2"
    else
      ingresar_folio(recibo_folio)
      sleep 2
      or_exec_uielement(@@btn_siguiente)
    end
  end


  def touch_btn_siguiente_ar
    recibo_folio=$dt_row[:folio_recibo_2]
    if recibo_folio != nil
      sleep 2
      or_exec_uielement(@@btn_siguiente)
    end

  end

  
  def ingresar_orden_de_compra_empresa_1
    orden_de_compra=$dt_row[:orden_de_compra_1].to_s.strip
    ingresar_orden_de_compra(orden_de_compra)
  end

  def ingresar_orden_de_compra_empresa_2
    orden_de_compra=$dt_row[:orden_de_compra_2]
    if orden_de_compra == nil
      puts "No se ingreso un recibo de orden para la empresa 2"
    else
      ingresar_orden_de_compra(orden_de_compra)
    end

  end


  def obtener_cobro_garrafon
   precio_envase=$dt_row[:precio_envase]
   cajas=$dt_row[:cajas_2].to_f

   if precio_envase== nil
      raise "Error=> no se ingreso un precio para el envase"

   else
     cobro_garrafon= precio_envase.to_f*cajas
     cobro_garrafon=cobro_garrafon.round(2)
     $importe=$importe+cobro_garrafon
   end

  end


#############################################################################
# ASSERTS/VERIFYS/VALIDACIONES
#############################################################################

def assert_page_folio_recibo_3501_displayed?
    assert_for_element_exist(@@lbl_page_3501[:locator], 10,
                             "Error => no se cargo la pantalla 'cantidad cheques'")
end

  def touch_btn_siguiente_page_folio
    iva=$iva.each.map(&:to_f).inject(0,:+)
    $DTH_INVENTARIO[:descuento_total]=$precio_final-iva
    $DTH_INVENTARIO[:importe] =$importe
    $DTH_INVENTARIO[:neto] =$precio_final
    $DTH_INVENTARIO[:iva] =iva
    $DTH_INVENTARIO[:nombre_ticket]=$nombre_ticket.to_s
    $DTH_INVENTARIO[:devolucion_envase_grb]=$dt_row[:devolucion_grb_1].to_i+$dt_row[:devolucion_grb_2].to_i
    $DTH_INVENTARIO[:devolucion_garrafon]=$dt_row[:devolucion_garrafon_1].to_i+$dt_row[:devolucion_garrafon_2].to_i
    dt= get_csv_data 'dt_ar_inventario'
    #Leer cada nuevo registro y agregarlo al CSV Original
    $DATOS.each_with_index do |fila, index|
      dt.push fila
      #ESCRIBIR CSV INVENTARIO
    end
    dt.map do |hash|
      hash.tap{|h| h.delete(:monto_combo1)}
      hash.tap{|h| h.delete(:monto_combo2)}
      hash.tap{|h| h.delete(:efectivo)}
      hash.tap{|h| h.delete(:cheques)}
      hash.tap{|h| h.delete(:cupones)}
      hash.tap{|h| h.delete(:pagos_cr)}
      hash.tap{|h| h.delete(:monto_cr)}
      hash.tap{|h| h.delete(:monto_cr)}
      hash.tap{|h| h.delete(:prestamo_envase_grb)}
      hash.tap{|h| h.delete(:prestamo_envase_garrafon)}
      hash.tap{|h| h.delete(:cobro_cajilla)}
    end
    exportHashToCsv(dt, 'dt_ar_inventario')
    or_exec_uielement(@@btn_next)
  rescue Exception => e
    raise("Error al hacer click en el boton siguiente de la pagina 3501
             \nException => #{e.message}")
  end




end