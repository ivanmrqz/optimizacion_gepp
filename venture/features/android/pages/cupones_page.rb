
# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


  class CuponesPage< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_cupones'
  @@view_page_cupones=get_or_data(@or_file, 'view_page_cupones')
  @@btn_descuento=get_or_data(@or_file, 'btn_descuento')
  @@txt_descuento=get_or_data(@or_file, 'txt_descuento')
  @@txt_importe_a_descontar=get_or_data(@or_file, 'txt_importe_a_descontar')
  @@txt_importe_a_comparar=get_or_data(@or_file, 'txt_importe_a_comparar')
  #############################################################################
  # ACCIONES
  #############################################################################




  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################
  #############################################################################
  def assert_cupones_page_displayed?

    assert_for_element_exist(@@view_page_cupones[:locator], 40,
                             "Error => no se mostro la pagina de cargas")
  end


  def validar_importe_con_descuento
    descuento= query(@@txt_descuento[:locator],:text).map{|descuento| descuento.delete('%')}
    #aqui obtengo el %
    r_descuento=(100-descuento[0].to_f)/100
    puts "el descuento a aplicar es de #{descuento[0]}%"
    puts r_descuento
    #aqui le quito el signo $
    importe_a_descontar=query(@@txt_importe_a_descontar[:locator],:text).map{|importe_a_descontar| importe_a_descontar.delete('$')}
    #aqui le quito las comas
    r_importe_a_descontar=importe_a_descontar.map{|r_importe_a_descontar| r_importe_a_descontar.delete(',')}
    #aqui sumo los elementos del array
    suma_descuentos_a_descontar=r_importe_a_descontar.each.map(&:to_f).inject(0,:+)
    puts "la suma de los descuentos es"
    puts suma_descuentos_a_descontar
    #aqui resto el importe menos la suma a descontar
    diferencia_importes=$importe-suma_descuentos_a_descontar
    puts "el importe es "
    puts $importe
    puts "la suma de descuentos es"
    puts suma_descuentos_a_descontar
    puts "la diferencia de los importes es"
    puts diferencia_importes


    importe_total=(suma_descuentos_a_descontar*r_descuento)+diferencia_importes
    puts "el importe total es"
    puts importe_total
    dto= $importe-importe_total
    puts "el descuento fue de #{dto}"


    sleep 2
    or_exec_uielement(@@btn_descuento )
    sleep 2
    importe_a_comparar=query(@@txt_importe_a_comparar[:locator],:text).map{|importe| importe.delete('$')}
    r_importe_a_comparar=importe_a_comparar.map{|r_importe| r_importe.delete(',')}
    $r_importe_a_comparar=r_importe_a_comparar[0].to_f
    $importe=$r_importe_a_comparar
   if importe_total == $importe
      puts true
      puts "si se aplico de manera correcta el descuento"
   else
     puts("no se aplico de manera correcta el descuento")
   end




  end


end