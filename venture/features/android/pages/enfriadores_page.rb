
# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'

class EnfriadoresPage< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_enfriadores'
  @@txt_reporte_enfriadores= get_or_data(@or_file, 'txt_reporte_enfriadores')
  @@btn_sin_etiqueta= get_or_data(@or_file, 'btn_sin_etiqueta')
  @@txt_telefono= get_or_data(@or_file, 'txt_telefono')
  @@btn_siguiente_enfriadores= get_or_data(@or_file, 'btn_siguiente_enfriadores')




  #############################################################################
  # ACCIONES
  #############################################################################



  def ingresar_numero_tel
    or_exec_uielement(@@txt_telefono, "5547321512")
    sleep 1
    adb_keys("KEYCODE_BACK")
    or_exec_uielement(@@btn_sin_etiqueta)
  rescue Exception => e
    raise("ingresar_numero_tel => Error al ingresar numero relefonico y seleccionar el boton sin etiqueta")
  end



  def touch_btn_siguiente_enfriadores
    or_exec_uielement(@@btn_siguiente_enfriadores)
  rescue Exception => e
    raise(" touch_btn_siguiente_enfriadores => Error al seleccionar el boton siguiente")
  end



  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################
  #############################################################################



  def assert_reporte_enfriadores_page_displayed?
    assert_for_element_exist(@@txt_reporte_enfriadores[:locator], 10,
                             "Error => no se cargo la pantalla 'indicadores de cliente'")
  end



end