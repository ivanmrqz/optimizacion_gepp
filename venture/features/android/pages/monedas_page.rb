# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'


class MonedasPage< Calabash::ABase

  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_monedas'
  @@txt_total_1=get_or_data(@or_file, 'txt_total_1')
  @@txt_total_2=get_or_data(@or_file, 'txt_total_2')
  @@txt_total=get_or_data(@or_file, 'txt_total')
  @@txt_disponible= get_or_data(@or_file, 'txt_disponible')
  @@txt_input_disponible_1=get_or_data(@or_file, 'txt_input_disponible_1')
  @@txt_input_disponible_2= get_or_data(@or_file, 'txt_input_disponible_2')
  @@nombre_cupon= get_or_data(@or_file, 'nombre_cupon')
  @@valor_cupon= get_or_data(@or_file, 'valor_cupon')
  @@input_cupon= get_or_data(@or_file, 'input_cupon')
  @@cupon_a_buscar=get_or_data(@or_file, 'cupon_a_buscar')






  #############################################################################
  # METODOS
  #############################################################################

  def agrega_moneda(empresa,nombre_moneda,cantidad_moneda)


      #x="(//*[contains(@text,'"+empresa+"')]/ancestor::*[contains(@resource-id, 'main_linearlayout')]/following-sibling::*//*[@text='"+nombre_moneda+"']/following-sibling::*[@class='android.widget.EditText'][1])"
        x="(//*[contains(@text,'"+empresa+"')]/ancestor::*[contains(@resource-id,'linearlayout')]/following-sibling::*//*[contains(@text,'"+nombre_moneda+"')]/following-sibling::*[@class='android.widget.EditText'])[1]"
      scroll_down_to_uielement(x,
                               10,
                               'mate')


      sleep 1
      tap_element(x)
      enter_text_uielement(x, cantidad_moneda,'mate')
      press_user_action_button('search')


  end


  #############################################################################
  # ACCIONES
  #############################################################################

  def    liquidar_total_con_monedas

    total=query(@@txt_total[:locator],:text).each.map(&:to_i)
    r_total=total.inject(0,:+)
    disponible=query(@@txt_disponible[:locator], :text)
    disponible[0].upcase!
    r_disponible=disponible.map{|r_disponible| r_disponible.delete('  $ABCDEFGHIJKLMNOPQRSTUVWXY,Z-')}

    if r_total== r_disponible[0].to_i
      puts "las cantidades coinciden, se puede liquidar el monto con monedas"
    else
      puts "las cantidades no coinciden"
    end

    puts"las monedas a agregar son "
    puts total[0]

    or_exec_uielement(@@txt_input_disponible_1,total[0])
    press_user_action_button('search')
    sleep 1
    if element_exists(@@txt_input_disponible_2[:locator])
      or_exec_uielement(@@txt_input_disponible_2,total[8])
    press_user_action_button('search')
    end


  end

  def agregar_monedas_parcialmente
    monedas_parciales=$dt_row[:cantidad_otras_monedas].to_i/2
    puts "las monedas parciales es "
    puts monedas_parciales
    or_exec_uielement(@@txt_cantidad_cupones, monedas_parciales)
    press_user_action_button('search')
    if element_exists(@@txt_cantidad_cupones_2[:locator])
      or_exec_uielement(@@txt_cantidad_cupones_2,monedas_parciales)
      press_user_action_button('search')
    end
  end




  def     buscar_primer_moneda(tipo_moneda)
    #OBTENGO EL PARAMETRO PARA VALIDAR QUE EMPRESA SE VA A BUSCAR
    tipo_moneda=tipo_moneda.to_s
    #MEDIANTE UN CASE SE VALIDAN LAS COLUMNAS DEL CSV
    monedas = case tipo_moneda
                when"primer moneda"
                  empresa=$dt_row[:nombre_empresa_1_m]
                  nombre_moneda=$dt_row[:nombre_moneda_1]
                  cantidad_moneda=$dt_row[:cantidad_moneda_1]
                  if empresa == nil
                    puts "no se agregaron monedas"

                  else
                    puts "si se agregan monedas"
                    agrega_moneda(empresa,nombre_moneda,cantidad_moneda)
                  end

                when"segunda moneda"
                  empresa=$dt_row[:nombre_empresa_2_m]
                  nombre_moneda=$dt_row[:nombre_moneda_2]
                  cantidad_moneda=$dt_row[:cantidad_moneda_2]
                  if empresa == nil
                    puts "no se agregaron monedas"
                  else
                    agrega_moneda(empresa,nombre_moneda,cantidad_moneda)
                  end
                when"tercer moneda"
                  empresa=$dt_row[:nombre_empresa_3_m]
                  nombre_moneda=$dt_row[:nombre_moneda_3]
                  cantidad_moneda=$dt_row[:cantidad_moneda_3]
                  if empresa == nil
                    puts "no se agregaron monedas"
                  else
                    agrega_moneda(empresa,nombre_moneda,cantidad_moneda)
                  end
              end

  end

    #x = "//*[@text=#{compania}]/ancestor::*[contains(@resource-id, 'main_linearlayout')]
     #     /following-sibling::*//*[@text=#{moneda}]
      #    /following-sibling::*[@class='android.widget.EditText']"


    #XPATH QUE ALMACENA EL NOMBRE DE LA EMPRESA Y LAS MONEDAS
    #empresa="//*[@text='"+empresa+"']/ancestor::*[contains(@resource-id, 'main_linearlayout')]"
    #moneda="//*[@text='"+nombre_moneda+"']/following-sibling::*[@class='android.widget.EditText']"
    #empresa="//*[@text='"+empresa+"']/ancestor::*[contains(@resource-id, 'main_linearlayout')]/following-sibling::*//*[@text='"+nombre_moneda+"']/following-sibling::*[@class='android.widget.EditText']"
    #empresa=" //*[@text='BEPUSA']/ancestor::*[contains(@resource-id, 'main_linearlayout')]/preceding-sibling::*//*[@text='Liq. Gratis $3']/following-sibling::*[@class='android.widget.EditText']"
    #PRIMERO SE REALIZA UN SCROLL HACIA ABAJO, PARA ENCONTRAR LA EMPRESA
    #empresa="//*[contains(@text,'"+empresa+"')]/ancestor::*[contains(@resource-id, 'main_linearlayout')]/following-sibling::*//*[@text='"+nombre_moneda+"']/following-sibling::*[@class='android.widget.EditText']"







  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################


end