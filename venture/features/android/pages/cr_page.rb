# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'

class CrPage < Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_cr'
  @@view_page_pagos=get_or_data(@or_file, 'view_page_pagos')
  @@btn_siguiente_pagos=get_or_data(@or_file, 'btn_siguiente_pagos')
  @@txt_importes_cr=get_or_data(@or_file, 'txt_importes_cr')
  @@txt_saldos_cr=get_or_data(@or_file, 'txt_saldos_cr')
  @@txt_saldo_total_cr=get_or_data(@or_file, 'txt_saldo_total_cr')
  @@txt_total_a_pagar_cr=get_or_data(@or_file, 'txt_total_a_pagar_cr')
  @@txt_nuevo_saldo_cr=get_or_data(@or_file, 'txt_nuevo_saldo_cr')
  @@ck_cantidad_a_pagar_cr=get_or_data(@or_file, 'ck_cantidad_a_pagar_cr')
  @@view_cr_pagos=get_or_data(@or_file, 'view_cr_pagos')
  @@txt_monto_credito = get_or_data(@or_file, 'txt_monto_credito')
  @@sl_motivo_credito_revolvente = get_or_data(@or_file, 'sl_motivo_credito_revolvente')
  @@sl_option_motivo_credito_revolvente = get_or_data(@or_file, 'sl_option_motivo_credito_revolvente')
  @@txt_g_total= get_or_data(@or_file, 'txt_g_total')
  @@btn_siguiente_cr= get_or_data(@or_file, 'btn_siguiente_cr')
  @@importe=get_or_data(@or_file, 'importe')


  #############################################################################
  # ACCIONES
  #############################################################################
  def liquidar_pagos_cr
    importes=query(@@txt_importes_cr[:locator],:text).map{|r_importe| r_importe.delete('  $,')}
    saldos=query(@@txt_saldos_cr[:locator],:text).map{|r_importe| r_importe.delete('  $,')}
    r_saldos=saldos.each.map(&:to_f)
    r_importes=importes.each.map(&:to_f)
    puts r_importes
    puts " salto"
    index_monto=r_importes.length
    sleep 2
    i=0
    while i<index_monto do

      touch("android.widget.EditText id:'pago_creditos_pago_saldo_edittext' index:#{i}")
      enter_text("android.widget.EditText id:'pago_creditos_pago_saldo_edittext' index:#{i}",r_importes[i])
      press_user_action_button('search')

      i+=1
    end
    sleep 2
    saldo_total_cr=query(@@txt_saldo_total_cr[:locator],:text).map{|r_importe| r_importe.delete('  $,')}[0]
    r_saldo_total_cr=saldo_total_cr.to_f
    suma_importes=r_importes.inject(0,:+)
    redondeo_suma_importes=suma_importes.round(2)
    $DTH_INVENTARIO[:pagos_cr] =redondeo_suma_importes.to_s.strip
    total_a_pagar_cr=query(@@txt_total_a_pagar_cr[:locator],:text).map{|r_importe| r_importe.delete('  $,')}[0]
    r_total_a_pagar_cr=total_a_pagar_cr.to_f
    $pagos_anteriores_cr=redondeo_suma_importes
    nuevo_saldo=query(@@txt_nuevo_saldo_cr[:locator],:text).map{|r_importe| r_importe.delete('  $,')}[0]
    r_nuevo_saldo=nuevo_saldo.to_f
    resta_total=r_saldo_total_cr-r_total_a_pagar_cr
    sleep 1
    puts "el saldo total cr es"
    puts saldo_total_cr
    puts "la suma de los importes es"
    puts redondeo_suma_importes
   if resta_total == r_nuevo_saldo
      puts "los montos son correctos"
   else
      raise("el saldo total es incorrecto")
   end

   if r_saldo_total_cr == redondeo_suma_importes
     puts "el saldo total es correcto"
   else
     raise("el saldo total es incorrecto")
   end


  end

  def touch_btn_siguiente_page_pagos
    or_exec_uielement(@@btn_siguiente_pagos, $importe)
  rescue Exception => e
    raise("agregar_monto_credito => Error al seleccionar el boton siguiente
             \nException => #{e.message}")
  end

  def seleccionar_motivo_credito_revolvente
    motivo =$dt_row[:motivo_credito_revolvente]
    or_exec_uielement(@@sl_motivo_credito_revolvente )
    sleep 3
    el = val_or_uielement @@sl_option_motivo_credito_revolvente, motivo
    puts "el = #{el['element']}"
    or_exec_uielement(@@sl_option_motivo_credito_revolvente, motivo )

  rescue Exception => e
    raise("seleccionar_motivo_credito_revolvente => Error al seleccionar 'motivo de credito revolvente'.
             \nException => #{e.message}")


  end

  def agregar_monto_credito(monto_credito)
    or_exec_uielement(@@txt_monto_credito, $dt_row[:monto_credito_revolvente] )
    press_user_action_button('search')

  rescue Exception => e
    raise("agregar_monto_credito => Error al ingresar 'MONTO DEL CREDITO'.
             \nException => #{e.message}")

  end
  def liquidar_cr
    or_exec_uielement(@@txt_monto_credito, $importe)
    press_user_action_button('search')
    $DTH_INVENTARIO[:monto_cr] =$importe.to_s.strip
    $DTH_INVENTARIO[:neto] = $importe
    dt= get_csv_data'dt_inventario'
    #Leer cada nuevo registro y agregarlo al CSV Original
    $DATOS.each_with_index do |fila, index|
      dt.push fila
    end

    #ESCRIBIR CSV INVENTARIO
    exportHashToCsv(dt, 'dt_inventario')

  rescue Exception => e
    raise("agregar_monto_credito => Error al ingresar 'MONTO DEL CREDITO'.
             \nException => #{e.message}")
  end
  def touch_btn_siguiente_cr
    or_exec_uielement(@@btn_siguiente_cr, $importe)
  rescue Exception => e
    raise("agregar_monto_credito => Error al seleccionar el boton siguiente
             \nException => #{e.message}")

  end

  def validar_gran_total
    g_total=query(@@txt_g_total[:locator],:text).map{|r_importe| r_importe.delete('  $,')}[0]
    r_g_total=g_total.to_f
    puts "el importe es "
    puts $importe
    puts "el gran total es"
    puts r_g_total
    if r_g_total == $importe
      puts "el gran total es correcto"
    else
      puts("error, el gran total no es correcto")
    end
  end
  def   validar_gran_total_con_descuento
    g_total=query(@@txt_g_total[:locator],:text).map{|r_importe| r_importe.delete('  $,')}[0]
    r_g_total=g_total.to_f
    if r_g_total == $importe
      puts "el gran total es correcto"
    else
      raise("error, el gran total no es correcto")
    end
  end


  def   liquidar_parcialmente_el_monto_cr
    or_exec_uielement(@@txt_monto_credito,  $saldo_cr/2)
    press_user_action_button('search')
    $DTH_INVENTARIO[:monto_cr]= $saldo_cr/2
  rescue Exception => e
    raise("agregar_monto_credito => Error al ingresar 'MONTO DEL CREDITO'.
             \nException => #{e.message}")
  end




  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def validar_pagos_anteriores_y_liquidar
    #obtengo la bandera del csv
    saldos_pendiente_cr=$dt_row[:saldo_pago_cr]
    pagos_cr = case saldos_pendiente_cr
                 when "si"
                   assert_for_element_exist(  @@view_page_pagos[:locator],10,"Error => no se cargo la pagina de credito revolvente")
                   liquidar_pagos_cr
                   touch_btn_siguiente_page_pagos

     end
  end




  def assert_cr_page_displayed?
    sleep 2
    assert_for_element_exist(@@view_cr_pagos[:locator],10,"Error => no se cargo la pagina de credito revolvente")
  end



  def obtnener_importe_sin_combo
    importe = query(@@importe[:locator],:text).map{|importe| importe.delete('$')}
    r_importe_sin_combo=importe.map{|r_importe| r_importe.delete(',')}
    $importe_sin_combo=r_importe[0].to_f
    puts "El importe del pedido sin combos es: "
    puts $importe_sin_combo

  end

  def obtener_importe_con_combo
    importe = query(@@importe[:locator],:text).map{|importe| importe.delete('$')}
    r_importe_con_combo=importe.map{|r_importe| r_importe.delete(',')}
    $importe_con_combo=r_importe[0].to_f
    puts "El importe del pedido con combos es: "
    puts $importe_con_combo
    $importe=$importe_con_combo
  end



end