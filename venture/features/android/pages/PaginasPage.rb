# encoding: utf-8
require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'

class PaginasPage < Calabash::ABase

  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_pages'
  @@btn_menu_principal = get_or_data(@or_file, 'btn_menu_principal')
  @@btn_buscar = get_or_data(@or_file, 'btn_buscar')
  @@btn_submenu_programados = get_or_data(@or_file, 'btn_submenu_programados')
  @@txt_buscar = get_or_data(@or_file, 'txt_buscar')
  @@btn_opcion_venta = get_or_data(@or_file, 'btn_opcion_venta')
  @@btn_opcion_pedido_dia = get_or_data(@or_file, 'btn_opcion_pedido_dia')
  @@pedido_del_dia = get_or_data(@or_file, 'pedido_del_dia')
  @@txt_cantidad_1 = get_or_data(@or_file, 'txt_cantidad_1')
  @@txt_cantidad_2 = get_or_data(@or_file, 'txt_cantidad_2')
  @@txt_cantidad_3 = get_or_data(@or_file, 'txt_cantidad_3')
  @@btn_agregar_productos_confirmar = get_or_data(@or_file, 'btn_agregar_productos_confirmar')
  @@btn_pretamo_caja = get_or_data(@or_file, 'btn_pretamo_caja')
  @@txt_cantidad_retornables = get_or_data(@or_file, 'txt_cantidad_retornables')
  @@txt_cantidad_cupones_2_f = get_or_data(@or_file, 'txt_cantidad_cupones_2_f')
  @@txt_cantidad_cupones = get_or_data(@or_file, 'txt_cantidad_cupones')
  @@txt_cantidad_cupones_2_i= get_or_data(@or_file, 'txt_cantidad_cupones_2_i')
  @@txt_pago_cantidad = get_or_data(@or_file, 'txt_pago_cantidad')
  @@txt_cantidad_devolucion_1 = get_or_data(@or_file, 'txt_cantidad_devolucion_1')
  @@txt_pedido_del_dia= get_or_data(@or_file, 'txt_pedido_del_dia')
  @@txt_indicadores_de_cliente= get_or_data(@or_file, 'txt_indicadores_de_cliente')
  @@txt_movimiento_del_dia= get_or_data(@or_file, 'txt_movimiento_del_dia')
  @@btn_sig_pedido_del_dia= get_or_data(@or_file, 'btn_sig_pedido_del_dia ')
  @@btn_sig_devolucion_de_retornables= get_or_data(@or_file, 'btn_sig_devolucion_de_retornables')
  @@btn_sig_cupones= get_or_data(@or_file, 'btn_sig_cupones')
  @@btn_sig_movimiento_del_dia= get_or_data(@or_file, 'btn_sig_movimiento_del_dia')
  @@btn_sig_indicadores_del_clientess= get_or_data(@or_file, 'btn_sig_indicadores_del_clientess')
  @@txt_auditoria_enfriadores= get_or_data(@or_file, 'txt_auditoria_enfriadores')
  @@txt_descuentos= get_or_data(@or_file, 'txt_descuentos')
  @@btn_siguiente_descuentos= get_or_data(@or_file, 'btn_siguiente_descuentos')
  @@btn_x= get_or_data(@or_file, 'btn_x')
  @@lbl_control_de_km= get_or_data(@or_file, 'lbl_control_de_km')
  #############################################################################
  # ACCIONES
  #############################################################################

  def seleccionar_menu(menu)
    #convertir string menu a utf8, compatibilidad con encoding
    menu = menu.to_s.force_encoding(Encoding::UTF_8)
    or_exec_uielement(@@btn_menu_principal, menu)

  rescue Exception => e
    raise("seleccionar_menu => Error no se pudo seleccionar el menu '#{menu}'.
             \nException => #{e.message}")
  end


  def buscar_por_nud
    nud=$dt_row[:nud]
    if nud==nil
      raise("Error=>no se Encontro un nud a buscar, validar csv")
    end

    or_exec_uielement(@@btn_buscar)
    or_exec_uielement(@@btn_submenu_programados , 'NUD')
    or_exec_uielement( @@txt_buscar,$dt_row[:nud].to_s)

  rescue Exception => e
    raise("buscar_por_nud => Error al buscar por 'NUD'.
             \nException => #{e.message}")
  end
  def buscar_cliente_por_nud
    nud=$dt_row[:nud]
    if nud==nil
      raise("Error=>no se Encontro un nud a buscar, validar csv")
    end
    or_exec_uielement(@@btn_x)
    or_exec_uielement( @@txt_buscar,$dt_row[:nud].to_s)

  rescue Exception => e
    raise("buscar_por_nud => Error al buscar por 'NUD'.
             \nException => #{e.message}")
  end

  def seleccionar_opcion_programados_venta
    or_exec_uielement(@@btn_opcion_venta)

  rescue Exception => e
    raise("seleccionar_opcion_programados_venta => Error no se pudo seleccionar opcion 'VENTA'.
             \nException => #{e.message}")
  end


  def seleccionar_opcion_pedidos_dia(opcion)
    opcion = opcion.to_s.force_encoding(Encoding::UTF_8)
    or_exec_uielement(@@btn_opcion_pedido_dia, opcion)
  sleep 3
  rescue Exception => e
    raise("seleccionar_opcion_pedidos_dia => Error no se pudo seleccionar opcion '#{opcion}'.
             \nException => #{e.message}")

    end

  def agregar_productos_cajas
    or_exec_uielement(@@txt_cantidad_1, $dt_row[:cantidad_cajas_1])
    sleep 2
    adb_keys("press_enter_key")
    or_exec_uielement(@@txt_cantidad_2, $dt_row[:cantidad_cajas_2])
    sleep 2
    adb_keys("press_enter_key")
    or_exec_uielement(@@txt_cantidad_3, $dt_row[:cantidad_cajas_3])
    sleep 2
    adb_keys("press_enter_key")
  rescue Exception => e
    raise("agregar_productos_cajas => Error no se pudo agregar productos (cajas).
             \nException => #{e.message}")
  end

  def confirmar_cajas
    or_exec_uielement(@@btn_agregar_productos_confirmar )

  rescue Exception => e
    raise("agregar_productos_cajas => Error no se pudo confirmar productos (cajas).
             \nException => #{e.message}")
  end



  def click_prestamo_caja
    or_exec_uielement(@@btn_pretamo_caja )

  rescue Exception => e
    raise("click_prestamo_caja => Error al hacer click en boton 'PRESTAMO CAJA'.
             \nException => #{e.message}")

  end




  def agregar_prestamo_retornables(cantidad)
    or_exec_uielement(@@txt_cantidad_retornables, $dt_row[:prestamo_retornables_cajas_prestadadas])
    press_user_action_button('search')

  rescue Exception => e
    raise("agregar_prestamo_retornables => Error al agregar 'cantidad de retornables'.
             \nException => #{e.message}")
  end




  def agregar_cantidad_cupones
    or_exec_uielement(@@txt_cantidad_cupones, $dt_row[:cantidad_otras_monedas])
    press_user_action_button('search')
    sleep 1
    if element_exists(@@txt_cantidad_cupones_2_i[:locator])
      or_exec_uielement(@@txt_cantidad_cupones_2_i)
     or_exec_uielement(@@txt_cantidad_cupones_2_f, $dt_row[:cantidad_otras_monedas])
      press_user_action_button('search')
    end

  end


  def agregar_cantidad_pago(pago)
    or_exec_uielement(@@txt_pago_cantidad, $dt_row[:pago_abono])
    press_user_action_button('search')

  rescue Exception => e
    raise("agregar_cantidad_cupones => Error no se pudo agregar cantidad cupones.
             \nException => #{e.message}")
  end



  def agregar_cantidad_envases_devoucion(cantidad_devolucion)
    or_exec_uielement(@@txt_cantidad_devolucion_1, $dt_row[:devolucion_envase_1])
    press_user_action_button('search')

  rescue Exception => e
    raise("agregar_cantidad_cupones => Error no se pudo agregar cantidad cupones.
             \nException => #{e.message}")
  end


  def touch_btn_siguiente_pedido_del_dia
    or_exec_uielement(@@btn_sig_pedido_del_dia)

  rescue Exception => e
    raise("touch_btn_siguiente_pedido_del_dia => Error no se pudo seleccionar el boton siguiente en la pantalla pedido del dia.
             \nException => #{e.message}")

  end

  def touch_btn_siguiente_devolucion_de_retornables
    sleep 2
    or_exec_uielement(@@btn_sig_devolucion_de_retornables)

  rescue Exception => e
    raise("touch_btn_siguiente_devolucion_de_retornables => Error no se pudo seleccionar el boton siguiente en la devolucion de retornables
             \nException => #{e.message}")
  end
  def touch_btn_siguiente_cupones
    or_exec_uielement(@@btn_sig_cupones)

  rescue Exception => e
    raise("touch_btn_siguiente_pedido_del_dia => Error no se pudo seleccionar el boton siguiente en la pantalla pedido del dia.
             \nException => #{e.message}")
  end
  def touch_btn_siguiente_movimiento_del_dia
    or_exec_uielement(@@btn_sig_movimiento_del_dia)

  rescue Exception => e
    raise("touch_btn_siguiente_movimiento_del_dia => Error no se pudo seleccionar el boton siguiente en la pantalla movimiento del dia
             \nException => #{e.message}")
  end
  def touch_btn_siguiente_indicadores_del_dia
    or_exec_uielement(@@btn_sig_indicadores_del_clientess)

  rescue Exception => e
    raise(" touch_btn_siguiente_indicadores_del_dia => Error no se pudo seleccionar el boton siguiente en la pantalla indicadores del dia
             \nException => #{e.message}")
  end


  def touch_btn_siguiente_descuentos
    or_exec_uielement(@@btn_siguiente_descuentos)

  rescue Exception => e
    raise("  touch_btn_siguiente_descuentos => Error no se pudo seleccionar el boton siguiente en la pantalla indicadores descuentos
             \nException => #{e.message}")
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_pagina_programados_display?
    assert_for_element_exist(@@btn_buscar[:locator], 40,
                             "Error => no se cargo la pantalla 'PROGRAMADOS'")
  end

  def assert_pagina_pedidos_dia_display?
    assert_for_element_exist(@@btn_opcion_pedido_dia[:locator], 10,
                             "Error => no se cargo la pantalla 'PEDIDOS DEL DIA'")
  end


  def assert_pagina_devolucion_retornables_display?
    assert_for_element_exist(@@btn_sig_devolucion_de_retornables[:locator], 10,
                             "Error => no se cargo la pantalla 'DEVOLUCION RETORNABLES'")
  end

  def assert_pagina_prestamo_retornables_display?
    assert_for_element_exist(@@txt_cantidad_retornables[:locator], 10,
                             "Error => no se cargo la pantalla 'PRESTAMO RETORNABLES'")
  end

  def assert_pagina_movimientos_dia_display?
    assert_for_element_exist(@@btn_sig_movimiento_del_dia[:locator], 10,
                             "Error => no se cargo la pantalla 'MOVIMIENTOS DEL DIA'")
  end


  def assert_paginapagos_display?
    assert_for_element_exist(@@txt_pago_cantidad[:locator], 10,
                             "Error => no se cargo la pantalla 'PAGOS'")
  end

  def assert_pedido_del_dia_display?
    assert_for_element_exist(@@txt_pedido_del_dia[:locator], 10,
                             "Error => no se cargo la pantalla 'Pedidos del dia'")

  end

  def assert_indicadores_cliente_page_displayed?
    assert_for_element_exist(@@txt_indicadores_de_cliente[:locator], 10,
                             "Error => no se cargo la pantalla 'indicadores de cliente'")
  end

  def assert_pagina_auditoria_de_enfriadores_display?
    assert_for_element_exist(@@txt_auditoria_enfriadores[:locator], 10,
                              "Error => no se cargo la pantalla 'auditoria de enfriadores'")
  end

  def assert_pagina_descuentos_displayed?
    assert_for_element_exist(@@txt_descuentos[:locator], 10,
                             "Error => no se cargo la pantalla 'descuentos'")
  end

  def assert_croc_page_display?
      assert_for_element_exist(@@lbl_control_de_km[:locator], 10,
                             "Error => no se cargo la pantalla 'Control de kilometraje'")
  end


end
