# encoding: utf-8

require 'fileutils'
require 'calabash-android/abase'
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/data_or'
require_relative '../../../../helpers/or_actions'
require_relative '../../../../helpers/mate'

class Recarga_inventarioPage< Calabash::ABase
  #############################################################################
  # OR - OBJECT REPOSITORY
  #############################################################################
  @or_file = 'or_recarga_inventario'
  @@btn_recarga=get_or_data(@or_file, 'btn_recarga')
  @@view_recarga_i=get_or_data(@or_file, 'view_recarga_i')
  @@btn_mas_producto=get_or_data(@or_file, 'btn_mas_producto')
  @@lbl_presentacion_sku=get_or_data(@or_file, 'lbl_presentacion_sku')
  @@lbl_sku_inventario=get_or_data(@or_file, 'lbl_sku_inventario')
  @@txt_agrega_cajas=get_or_data(@or_file, 'txt_agrega_cajas')
  @@btn_palomita_agrega_sku=get_or_data(@or_file, 'btn_palomita_agrega_sku')
  @@btn_impresora=get_or_data(@or_file, 'btn_impresora')
  @@btn_popup=get_or_data(@or_file, 'btn_popup')
  @@view_inicio_super=get_or_data(@or_file, 'view_inicio_super')
  @@txt_usuario=get_or_data(@or_file, 'txt_usuario')
  @@txt_password=get_or_data(@or_file, 'txt_password')
  @@txt_usuario_1=get_or_data(@or_file, 'txt_usuario_1')
  @@txt_password_1=get_or_data(@or_file, 'txt_password_1')
  @@btn_sig_super=get_or_data(@or_file, 'btn_sig_super')
  @@btn_inventario=get_or_data(@or_file, 'btn_inventario')

  #############################################################################
  # ACCIONES
  #############################################################################}
  def scroll_to_down_mate(drag_to = nil)

    #obtener 'mobile element' form xpath
    xpath = "(//node[./node/node])[last()]"
    drag = drag_to.nil? ? 100 : drag_to.to_s.to_i

    mobile_element = get_mobile_element xpath

    if mobile_element.nil? or mobile_element.to_s.empty?
    puts "no se encontro en la primera pantalla"
    end

    begin

      found_bounds = get_bounds_from_element(mobile_element) do |x1, y1, x2, y2|
        if (x1 + y1 + x2 + y2) == 0
          screen_size = get_screen_size
          xm = screen_size[:width] >> 1
          ym = screen_size[:height]  >> 1
          yf = ym - drag
        else
          xm = (x1 + x2) >> 1
          ym = (y1 + y2) >> 1
          yf = y1 - drag
        end

        adb_exec("shell input swipe #{xm} #{ym} #{xm} #{yf}")
        #puts("shell input swipe #{xm} #{ym} #{xm} #{yf}")
      end

      result = !found_bounds.nil?

      return result


    end

  end

  def scroll_down_for_elemento_mate(xpath, intentos = nil, scroll_size = nil)

    result = false
    no_scrols = intentos.nil? ? 10 : intentos

    for i in 0..no_scrols

      if mobile_element_exists xpath
        result = true
        break
      end
      scroll_to_down_mate scroll_size
    end

    return result
  end
  def agrega_inventario_en_recarga(sku,cajas)
    $DTH_RECARGA_INVENTARIO = {
        :sku=>nil,
        :i_final=>nil
    }
    x="(//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.TextView'])[2]"
    inventario_inicial=get_mobile_element_attribute(x,"text")
    i_final=inventario_inicial.to_i+cajas.to_i
     $DTH_RECARGA_INVENTARIO[:sku] =sku
    $DTH_RECARGA_INVENTARIO[:i_final]=i_final
    $DATA_RECARGA_INVENTARIO.push($DTH_RECARGA_INVENTARIO)

  end
  def descarga_inventario(sku,cajas)
    $DTH_RECARGA_INVENTARIO = {
        :sku=>nil,
        :i_final=>nil
    }
    x="(//*[contains(@text,'"+sku+"')]/following-sibling::*[@class='android.widget.TextView'])[2]"
    inventario_inicial=get_mobile_element_attribute(x,"text")
    i_final=inventario_inicial.to_i-cajas.to_i
    $DTH_RECARGA_INVENTARIO[:sku] =sku
    $DTH_RECARGA_INVENTARIO[:i_final]=i_final
    $DATA_RECARGA_INVENTARIO.push($DTH_RECARGA_INVENTARIO)


  end

  def agrega_inventario_en_2310(sku,cajas)
    $DTH_RECARGA_INVENTARIO = {
        :sku=>nil,
        :i_final=>nil
    }
    $DTH_RECARGA_INVENTARIO[:sku] =sku
    $DTH_RECARGA_INVENTARIO[:i_final]=cajas
    $DATA_RECARGA_INVENTARIO.push($DTH_RECARGA_INVENTARIO)
  end
  def agrega_productos_desde_2320(presentacion,sku,cajas)

  x="//*[contains(@text,'"+sku+"')]"

  el = val_or_uielement(@@lbl_presentacion_sku, presentacion)
  scroll_down_to_uielement_adr el["element"]
  assert_for_element_exist(el["element"], 20,
                           "Error => no se encontro el  producto")
  agrega_inventario_en_2310(sku,cajas)
  touch (el["element"])
  sleep 1
  scroll_down_to_uielement(x,
                           10,
                           'mate')

  q=query(@@lbl_sku_inventario[:locator],:text)
  cantidad=query(@@txt_agrega_cajas[:locator])
  regex=Regexp.new(sku)
  r_sku=q.find{|e|regex=~e}
  posicion= q.index(r_sku)

  touch(cantidad[posicion])
  enter_text("android.widget.EditText id:'act2310ProductoEdit' index:#{posicion}",cajas)
  press_user_action_button('search')
  sleep 1
  or_exec_uielement(@@btn_palomita_agrega_sku)
  or_exec_uielement(@@btn_popup)
end

  def touch_btn_impresora
    assert_for_element_exist(@@btn_impresora[:locator], 10)

    or_exec_uielement(@@btn_impresora)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'impresora'.
             \nException => #{e.message}")
  end

  def llenar_formulario_verificador
    sleep 2
    or_exec_uielement(@@txt_usuario_1, $dt_row[:usuario_verificador].to_s.strip)
    press_user_action_button('search')
    or_exec_uielement(@@txt_password_1, $dt_row[:password_verificador].to_s.strip)
    press_user_action_button('search')
    sleep 2
  end

  def llenar_formulario_super
    sleep 2
    or_exec_uielement(@@txt_usuario, $dt_row[:usuario_super].to_s.strip)
    press_user_action_button('search')
    or_exec_uielement(@@txt_password, $dt_row[:password_super].to_s.strip)
    press_user_action_button('search')
    sleep 2


  end


  def llenar_formulario_vendedor
    sleep 2
    or_exec_uielement(@@txt_usuario_1, $dt_row[:usuario_vendedor].to_s.strip)
    press_user_action_button('search')
    or_exec_uielement(@@txt_password_1, $dt_row[:usuario_vendedor].to_s.strip)
    press_user_action_button('search')
    sleep 2
  end


  def valida_producto_agregado(numero_sku)
    numero_sku =numero_sku.to_s
    tipo_producto = case numero_sku
                      when"1"
                        sku=$DATA_RECARGA_INVENTARIO[0][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[0][:i_final]
                      when"2"
                        sku=$DATA_RECARGA_INVENTARIO[1][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[1][:i_final]
                      when"3"
                        sku=$DATA_RECARGA_INVENTARIO[2][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[2][:i_final]
                      when "4"
                        sku=$DATA_RECARGA_INVENTARIO[3][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[3][:i_final]
                      when "5"
                        sku=$DATA_RECARGA_INVENTARIO[4][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[4][:i_final]
                      when "6"
                        sku=$DATA_RECARGA_INVENTARIO[5][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[5][:i_final]
                      when "7"
                        sku=$DATA_RECARGA_INVENTARIO[6][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[6][:i_final]
                      when "8"
                        sku=$DATA_RECARGA_INVENTARIO[7][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[7][:i_final]
                      when "9"
                        sku=$DATA_RECARGA_INVENTARIO[8][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[8][:i_final]
                      when "10"
                        sku=$DATA_RECARGA_INVENTARIO[9][:sku]
                        inventario=$DATA_RECARGA_INVENTARIO[9][:i_final]
                    end

    x="//*[contains(@text,'"+sku+"')]/following-sibling::*[contains(@resource-id,'inv_row_prod_header_Total')]"

    scroll_down_to_uielement(x,
                             10,
                             'mate')
    resultado_inventario=get_mobile_element_attribute(x,"text")

    if inventario.to_i == resultado_inventario.to_i
      puts "el producto y sus cajas se agregaron de manera correcta"
    else
      raise("Error=>El producto no se agrego de manera correcta se agregaron #{inventario.to_i} cajas, y se muestran #{resultado_inventario.to_i}cajas ")
    end
  end



  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################
  def assert_btn_recarga_displayed?
    sleep 2
    assert_for_element_exist(@@btn_recarga[:locator], 10)

    or_exec_uielement(@@btn_recarga)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'Recarga'.
             \nException => #{e.message}")
  end

  def assert_pantalla_recarga_displayed?
    sleep 2
    assert_for_element_exist(@@view_recarga_i[:locator], 500, "Error=> no se mostro la pantalla de descargas")
  end

  def busca_y_agrega_productos_para_inventario(sku)
    sku =sku.to_s
    tipo_producto = case sku
                      when"1"
                        presentacion=$dt_row[:presentacion_sku_1].to_s.strip
                        sku=$dt_row[:sku_1]
                        cajas=$dt_row[:cajas_sku_1].to_s.strip
                      when"2"
                        presentacion=$dt_row[:presentacion_sku_2].to_s.strip
                        sku=$dt_row[:sku_2]
                        cajas=$dt_row[:cajas_sku_2].to_s.strip
                      when"3"
                        presentacion=$dt_row[:presentacion_sku_3].to_s.strip
                        sku=$dt_row[:sku_3]
                        cajas=$dt_row[:cajas_sku_3].to_s.strip
                      when "4"
                        presentacion=$dt_row[:presentacion_sku_4].to_s.strip
                        sku=$dt_row[:sku_4]
                        cajas=$dt_row[:cajas_sku_4].to_s.strip
                      when "5"
                        presentacion=$dt_row[:presentacion_sku_5].to_s.strip
                        sku=$dt_row[:sku_5]
                        cajas=$dt_row[:cajas_sku_5].to_s.strip
                      when "6"
                        presentacion=$dt_row[:presentacion_sku_6].to_s.strip
                        sku=$dt_row[:sku_6]
                        cajas=$dt_row[:cajas_sku_6].to_s.strip
                      when "7"
                        presentacion=$dt_row[:presentacion_sku_7].to_s.strip
                        sku=$dt_row[:sku_7]
                        cajas=$dt_row[:cajas_sku_7].to_s.strip
                      when "8"
                        presentacion=$dt_row[:presentacion_sku_8].to_s.strip
                        sku=$dt_row[:sku_8]
                        cajas=$dt_row[:cajas_sku_8].to_s.strip
                      when "9"
                        presentacion=$dt_row[:presentacion_sku_9].to_s.strip
                        sku=$dt_row[:sku_9]
                        cajas=$dt_row[:cajas_sku_9].to_s.strip
                      when "10"
                        presentacion=$dt_row[:presentacion_sku_10].to_s.strip
                        sku=$dt_row[:sku_10]
                        cajas=$dt_row[:cajas_sku_10].to_s.strip
                      end
    #lo busco en la pantalla de inicio
    $DTH_RECARGA_INVENTARIO = {
        :sku=>nil,
        :i_final=>nil
    }

    sku_xpath="//*[contains(@text,'"+sku+"')]/following-sibling::*[contains(@resource-id,'cargas_prod_header_Recarga_TextView')]"
    scroll_down_for_elemento_mate(sku_xpath)
    if mobile_element_exists(sku_xpath)
    #SI EXISTE LO AGREGO
    sleep 1
    agrega_inventario_en_recarga(sku,cajas)
    tap_element(sku_xpath)
    tap_element(sku_xpath)
    clear_text(sku_xpath)
    sleep 1
    enter_text_uielement(sku_xpath, cajas,'mate')
    sleep 1
    press_enter_button
    sleep 1
    press_back_button
    sleep 2

    else
      or_exec_uielement(@@btn_mas_producto)
      #LO BUSCA EN MAS PRODUCTOS
      sleep 2
      agrega_productos_desde_2320(presentacion,sku,cajas)
      sleep 2

    end


    end

  def busca_y_descarga_productos_en_inventario(numero_descarga)
    numero =numero_descarga.to_s
    tipo_producto = case numero
                      when"1"
                        presentacion=$dt_row[:presentacion_sku_1].to_s.strip
                        sku=$dt_row[:sku_1]
                        cajas=$dt_row[:cajas_sku_1].to_s.strip
                      when"2"
                        presentacion=$dt_row[:presentacion_sku_2].to_s.strip
                        sku=$dt_row[:sku_2]
                        cajas=$dt_row[:cajas_sku_2].to_s.strip
                      when"3"
                        presentacion=$dt_row[:presentacion_sku_3].to_s.strip
                        sku=$dt_row[:sku_3]
                        cajas=$dt_row[:cajas_sku_3].to_s.strip
                      when "4"
                        presentacion=$dt_row[:presentacion_sku_4].to_s.strip
                        sku=$dt_row[:sku_4]
                        cajas=$dt_row[:cajas_sku_4].to_s.strip
                      when "5"
                        presentacion=$dt_row[:presentacion_sku_5].to_s.strip
                        sku=$dt_row[:sku_5]
                        cajas=$dt_row[:cajas_sku_5].to_s.strip
                      when "6"
                        presentacion=$dt_row[:presentacion_sku_6].to_s.strip
                        sku=$dt_row[:sku_6]
                        cajas=$dt_row[:cajas_sku_6].to_s.strip
                      when "7"
                        presentacion=$dt_row[:presentacion_sku_7].to_s.strip
                        sku=$dt_row[:sku_7]
                        cajas=$dt_row[:cajas_sku_7].to_s.strip
                      when "8"
                        presentacion=$dt_row[:presentacion_sku_8].to_s.strip
                        sku=$dt_row[:sku_8]
                        cajas=$dt_row[:cajas_sku_8].to_s.strip
                      when "9"
                        presentacion=$dt_row[:presentacion_sku_9].to_s.strip
                        sku=$dt_row[:sku_9]
                        cajas=$dt_row[:cajas_sku_9].to_s.strip
                      when "10"
                        presentacion=$dt_row[:presentacion_sku_10].to_s.strip
                        sku=$dt_row[:sku_10]
                        cajas=$dt_row[:cajas_sku_10].to_s.strip
                    end
    #lo busco en la pantalla de inicio
    $DTH_RECARGA_INVENTARIO = {
        :sku=>nil,
        :i_final=>nil
    }

    sku_xpath="//*[contains(@text,'"+sku+"')]/following-sibling::*[contains(@resource-id,'cargas_prod_header_Descarga_TextView')]"
    scroll_down_for_elemento_mate(sku_xpath)
      #SI EXISTE LO AGREGO
      sleep 1
      descarga_inventario(sku,cajas )
      tap_element(sku_xpath)
      tap_element(sku_xpath)
      clear_text(sku_xpath)
      sleep 1
      enter_text_uielement(sku_xpath, cajas,'mate')
      sleep 1
      press_enter_button
      sleep 1
      press_back_button
      sleep 2


  end


  def assert_pantalla_super_displayed?
    sleep 2
    assert_for_element_exist(@@view_inicio_super[:locator], 500, "Error=> no se mostro la pantalla de Inicio de Sesión para supervisor")
  end

  def assert_btn_inventario_displayed?
    sleep 2
    assert_for_element_exist(@@btn_inventario[:locator], 10)

    or_exec_uielement(@@btn_inventario)

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'INVENTARIO'.
             \nException => #{e.message}")
  end

  def btn_siguiente_supervisor
    sleep 2
    assert_for_element_exist(@@btn_sig_super[:locator], 10)

    or_exec_uielement(@@btn_sig_super)
    sleep 10

  rescue Exception => e
    raise("touch_btn_siguiente => Error al hacer touch en boton 'Recarga'.
             \nException => #{e.message}")
  end




end


  #############################################################################
