
require 'calabash-android/management/adb'
require 'calabash-android/operations'
require_relative '../../../../helpers/data_db/PersistenciaDTO'

Before do |scenario|
  #clear_app_data
  #puts "CLEAR APP DATA"
  reinstall_test_server
  puts "REINSTALL TEST SERVER"
  start_test_server_in_background
  puts "START TEST SERVER"


end

After do |scenario|

  if scenario.failed?
     if scenario.exception.message =="HTTPClient::KeepAliveDisconnected" || scenario.exception.message =="EOFError"
       puts "*********HTTP error!! attempting to restart test server!*************"
       reinstall_test_server
       start_test_server_in_background
     end
  end
  if scenario.failed?
    save_evidence(scenario)
  end
  #clear_app_data
  #puts "CLEAR APP DATA"
  shutdown_test_server
  puts "SHUTDOWN TEST SERVER"
end


# Before do |scenario|
#   if (scenario.source_tag_names.include?('@reset'))
#     clear_app_data
#   end
#   reinstall_test_server
#   start_test_server_in_background
#   $screen = Screen.new
#   $screen.rotate_to_proper_position()
# end
#
# After do |scenario|
#   if scenario.failed?
#     print "Test failed: #{scenario.name}\n"
#     if scenario.exception.message =="HTTPClient::KeepAliveDisconnected" || scenario.exception.message =="EOFError"
#       puts "*********HTTP error!! attempting to restart test server!*************"
#       reinstall_test_server
#       start_test_server_in_background
#     end
#     if scenario.failed?
#       screenshot_embed
#     end
#   end
#   shutdown_test_server
# end