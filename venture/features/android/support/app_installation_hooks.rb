require 'calabash-android/management/app_installation'

AfterConfiguration do |config|
  FeatureMemory.feature = nil
end

Before do |scenario|
  scenario = scenario.scenario_outline if scenario.respond_to?(:scenario_outline)

  feature = scenario.feature

  #Recupera el nombre del feautre, para tomar evidencia dentro de los step
  $feature_name = scenario.feature.name
  #Recupera el nombre del scenario, para tomar evidencia dentro de los step
  $scenario_name = scenario.name


  if FeatureMemory.feature != feature || ENV['RESET_BETWEEN_SCENARIOS'] == '1'

    if ENV['RESET_BETWEEN_SCENARIOS'] == '1'
      log 'New scenario - reinstalling apps'
    else
      log 'First scenario in feature - reinstalling apps'
    end

    #puts "UNISTALL APP INICIA"
    #descomentar *****
    #uninstall_apps
    #puts "UNISTALL APP OK"
    #puts "INSTALL APP EMPIEZA"
    # #descomentar *****
    #install_app(ENV['TEST_APP_PATH'])
    #install_app(ENV['APP_PATH'])
    #puts "INSTALL APP OK"

    FeatureMemory.feature = feature
    FeatureMemory.invocation = 1

  else
    FeatureMemory.invocation += 1

  end
end

FeatureMemory = Struct.new(:feature, :invocation).new

