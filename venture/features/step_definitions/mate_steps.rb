require_relative '../../../helpers/mate'
require_relative '../../../helpers/generic'

When(/^I goto home device$/) do
  sleep 1
  keyboard_send_keyevent "KEYCODE_HOME"
  save_evidence_execution
end

When(/^I drag element to other element$/) do
  sleep 1
  drag_element_to_element "//node[@text='Correo']", "//node[@text='Dev Tools']", 5000
  save_evidence_execution
end


When(/^I touch to phone option$/) do
  sleep 1
  tap_element "//node[contains(@text, 'Tel') and contains(@text, 'fono')]"
  save_evidence_execution
end

And(/^I touch to search contatos$/) do
  sleep 1
  tap_element "//node[@resource-id='com.android.dialer:id/search_box_start_search']"
  save_evidence_execution
end


When(/^I search "([^"]*)" contact$/) do |text|
  sleep 1
  set_text_element "//node[@resource-id='com.android.dialer:id/search_view']", text
  save_evidence_execution
end

When(/^I touch to dev\-tools option$/) do
  sleep 1
  tap_element "//node[@text='Dev Tools']"
end

When(/^I scroll to down$/) do
  sleep 1
  scroll_to_down 10
  scroll_to_down
  save_evidence_execution
  scroll_to_up
  save_evidence_execution
end

When(/^I open notifications bar$/) do
  sleep 1
  open_notifications
  save_evidence_execution
end

When(/^I clear all notifications$/) do
  begin
    tap_element "//node[@text='BORRAR TODO']"
  rescue
    puts "NO HAY NOTIFICACIONES PARA BORRAR"
  end
  save_evidence_execution
end

