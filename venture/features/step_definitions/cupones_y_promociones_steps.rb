# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$cupones_page= CuponesPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

When(/^se muestra la pantalla cantidad de cupones$/) do
 $cupones_page.assert_cupones_page_displayed?
  save_evidence_execution
end



When(/^selecciono el descuento y valido que se aplique$/) do
  $cupones_page.validar_importe_con_descuento
  save_evidence_execution
end