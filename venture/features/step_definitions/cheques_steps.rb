# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'



#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$cheques_page= ChequesPage.new(self)
$combos_page = CombosPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

When(/^se muestra la pantalla cantidad de cheques$/) do
  $cheques_page.assert_pagina_cheques_displayed?
  save_evidence_execution
end


When(/^obtenemos el importe del pedido$/) do
  $cheques_page.obtener_importe
  save_evidence_execution

end

When(/^ingresamos los datos de cheque para pago total con cheque$/) do
  $cheques_page.send_datos_de_cheque

  end

When(/^validar que se hayan incrementado los productos en Devolucion de Retornables$/) do
  $cheques_page.assert_producto_grb

  save_evidence_execution
end



When(/^Seleccionamos siguiente en la pagina cantidad de cheques$/) do
  $cheques_page.touch_btn_siguiente_cheque
end


When(/^buscar "([^"]*)"$/) do |retornable|
  $cheques_page.buscar_retornable(retornable)
  save_evidence_execution
  end



When(/^buscar el grb 2$/) do
  $cheques_page.buscar_grb_2
  save_evidence_execution
end

When(/^buscar el garrafon 2$/) do
  $cheques_page.buscar_garrafon_2
  save_evidence_execution
  end


#When(/^buscar cajilla como "([^"]*)"$/) do |cajilla|
#  $cheques_page.buscar_cajilla(cajilla)
 # save_evidence_execution
#end


When(/^buscar cajilla para prestamo$/) do
  $cheques_page.buscar_cajilla_prestamo
  save_evidence_execution
  end


When(/^ingresar envases que tengan saldo$/) do
  $cheques_page.ingresar_envases_saldo
  save_evidence_execution
end

When(/^que abro el ticket de venta$/) do
  $cheques_page.testTicket
  save_evidence_execution
end


When(/^ingresamos los datos de cheque para pago parcial con cheque y otras monedas$/) do
  $cheques_page.sed_datos_cheque_pago_parcial
  save_evidence_execution
end


When(/^ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1$/) do
 $cheques_page.pago_parcial_cheque_y_otras_monedas_empresa_1
  save_evidence_execution
end

When(/^ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2$/) do
  $cheques_page.pago_parcial_cheque_y_otras_monedas_empresa_2
  save_evidence_execution
end

When(/^ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3$/) do
  $cheques_page.pago_parcial_cheque_y_otras_monedas_empresa_3
  save_evidence_execution
end

When(/^ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1$/) do
  $cheques_page.pago_parcial_cheque_otras_monedas_efectivo_empresa_1
  save_evidence_execution
end
When(/^ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2$/) do
  $cheques_page.pago_parcial_cheque_otras_monedas_efectivo_empresa_2
  save_evidence_execution
end

When(/^ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3$/) do
  $cheques_page.pago_parcial_cheque_otras_monedas_efectivo_empresa_3
  save_evidence_execution
end
When(/^ingresamos los datos de cheque para pago parcial con cheque y cr$/) do
  $cheques_page.pago_parcial_cheque_y_cr
  save_evidence_execution
end


