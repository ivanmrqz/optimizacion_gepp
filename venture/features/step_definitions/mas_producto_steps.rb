# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require 'calabash-android/abase'
#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$pedido_dia_page = Pedido_diaPage.new(self)
$agregar_producto_page=Mas_producto_page.new(self)

#*****************************************************************************
# STEPS DEFINITIONS
#*****************************************************************************




When(/^busco y agrego productos "([^"]*)"$/) do |sku|
  $agregar_producto_page.busco_e_ingreso_productos(sku,cajas)
  save_evidence_execution
end




When(/^busco productos y agrego productos"([^"]*)"$/) do |tipo_sku|
  $agregar_producto_page.buscar_y_agregar_productos(tipo_sku)
  save_evidence_execution
end


When(/^busco un producto no vendible$/) do
  $agregar_producto_page.buscar_producto_no_vendible
  save_evidence_execution
end

