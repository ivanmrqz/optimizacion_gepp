# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'


#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$login_page = LoginPage.new(self)
$paginas_page = PaginasPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

Then(/^I press back button$/) do

  sleep 4
  adb_keys("KEYCODE_BACK")
end


Then(/^press_enter_button$/) do
  sleep 1
  adb_keys("press_enter_key")
end
When(/^abro la aplicacion$/) do
  true
end

When(/^presiono enter$/) do
  sleep 1
  press_user_action_button('search')
  end

#*****************************************************************************
#
#*****************************************************************************





When(/^Inicio sesion como usuario "([^"]*)"$/) do |tipo_usuario|
  $login_page.assert_login_page_displayed?
  save_evidence_execution
  $login_page.llenar_formulario(tipo_usuario)
  save_evidence_execution
end

When(/^Selecciono menu "([^"]*)"$/) do |menu|
  $paginas_page.seleccionar_menu(menu)
end

When(/^Se muestra la pantalla programados$/) do
  $paginas_page.assert_pagina_programados_display?
  save_evidence_execution
end

When(/^Busco un cliente por NUD$/) do
  $paginas_page.buscar_por_nud
  save_evidence_execution
  press_user_action_button('search')
  save_evidence_execution
end

When(/^Busco cliente por nud$/) do
  $paginas_page.buscar_cliente_por_nud
  save_evidence_execution
  press_user_action_button('search')
  save_evidence_execution
end
When(/^Selecciono el submenu "([^"]*)"$/) do |opcion_submenu|
  $paginas_page.seleccionar_opcion_programados_venta
  save_evidence_execution
end

When(/^Confirmo el mensaje emergente$/) do
  save_evidence_execution
  $generic_page.popup_confirmar_si
end

When(/^Se muestra el pedido del dia$/) do
  $paginas_page.assert_pedido_del_dia_display?
  save_evidence_execution
end

When(/^Seleccionamos la opcion "([^"]*)"$/) do |opcion|
  $paginas_page.seleccionar_opcion_pedidos_dia(opcion)
  save_evidence_execution
  end

When(/^Agregamos cajas$/) do
  $paginas_page.agregar_productos_cajas
  save_evidence_execution
end
When(/^confirmo cajas$/) do
  $paginas_page.confirmar_cajas
  save_evidence_execution
end
When(/^Seleccionamos siguiente$/) do
  $generic_page.touch_btn_siguiente
  save_evidence_execution
end

When(/^Seleccionamos >>siguiente$/) do
  $generic_page.touch_btn_sig
  save_evidence_execution
end
When(/^Se muestra la pantalla de Devolucion Retornables$/) do
 sleep 2
  $paginas_page.assert_pagina_devolucion_retornables_display?
  save_evidence_execution
end

When(/^Seleccionamos prestamo de caja$/) do
  $paginas_page.click_prestamo_caja
end

When(/^Se muestra la pantalla de Prestamos Retornables$/) do
  $paginas_page.assert_pagina_prestamo_retornables_display?
  save_evidence_execution
end

When(/^agrego el numero de cajas prestadas "([^"]*)"$/) do |numero_cajas|
  $paginas_page.agregar_prestamo_retornables numero_cajas
end



When(/^Se muestra la pantalla otras monedas$/) do
  $paginas_page.assert_pagina_movimientos_dia_display?
  save_evidence_execution
end

When(/^Agrego monedas$/) do
  $paginas_page.agregar_cantidad_cupones
end

When(/^Se muestra la pantalla movimientos del dia$/) do
  $paginas_page.assert_pagina_movimientos_dia_display?
  save_evidence_execution
end

When(/^Se muestra la pantalla de pagos$/) do
  $paginas_page.assert_paginapagos_display?
  save_evidence_execution
end

When(/^Realizo el pago$/) do
  $paginas_page.agregar_cantidad_pago nil
  save_evidence_execution
end


When(/^Agrego cantidad de envases para su devolucion$/) do
  $paginas_page.agregar_cantidad_envases_devoucion nil
  save_evidence_execution
end


When(/^valido estar en la pantalla de control de km$/) do
  $paginas_page.assert_croc_page_display?
  save_evidence_execution
end
#####################################################
#####################################################
#'STEP' que permite ejecutar varios 'STEPS DEFINITION' de manera iterativa,
#     correspondientes al numero de registros de un 'CSV DATA
#####################################################
#####################################################
# Permite iterar varios steps de acuerdo a los registros de un csv data o los filtros aplicados al data pool
# @params
#   :csv_data String, nombre del archivo datapool csv, este se debe ubicar en '..venture/config/csv_data'
#   :dt_filters String, filtros a aplicar al datapool csv. los filtros a columnas se delimitan por ',' (coma)
#       - delimitaodor: ','
#       - operadores: '=, !=, >, >=, <, <=, contains'
#       - ejemplo: 'columna1 = valor, columna2 != valor, columna3 > valor, columna4 <= valor, columna5 contains valor'
#   :step_strings String, 'steps definitions' a ejecutar, se ingresa un salto de linea entre cada step
When(/^I run records from "([^"]*)" data pool by filters "([^"]*)":$/) do |csv_data, dt_filters, step_strings|

  #bandera resultado
  result = true
  #separar el texto en cada uno de los steps
  steps = step_strings.to_s.split(/\n+/)

  #obtener data
  #   - si "dt_filters = 'all' se ejecutan todos los registros"
  csv = get_csv_data csv_data.to_s.strip
  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all'
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  $dt_data = csv

  #ITERAR CSV DATA
  csv.each_with_index do |record, index|

    #descargar fila 0 de la data. esta contiene nombre de columnas
    if index > 0

      #enviar data row a variable global '$dt_row'. esta se puede recuperar en cualqquier step y cualquier page object
      $dt_row = record
      result_it = "PASS"
      step_name = nil

      #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
      puts "ITERATION => #{index}"
      puts "DATA => #{record}"

      begin

        steps.each {|step_string|

          step_name = nil
          step = nil

          #'STEPS DEFINITIONS' que requieren parametrizacion:
          #   - desde el feature se pasa exprecion regular => ${dt_parametro}
          #   - se remplaza la expresion ${parametro} por  record[:nombre_columna].to_s
          if step_string.to_s.include? '{dt_'
            step =  String.new(step_string)
            step_name = get_step_dt_values(step, record)
          else
            step_name = step_string
          end

          step step_name

          #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
          puts "STEP => #{step_name} => PASS"

        }

      rescue Exception => e
        result = false
        result_it = "FAIL"
        #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
        puts "STEP => #{step_name} => FAIL"
        puts "MSJ ERROR => #{e.message}"
        save_evidence_execution_fail

      end

      #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
      puts "STATUS ITERATION => #{result_it}"
      puts "\n"


    end

  end

end



#####################################################
#'STEP' que permite obtener la data correspondientes desde un 'CSV DATA'
#     se puede filtrar y estos valores se almacenan en
#       varialbe global '$data'
#   "**NOTA": LOS VALORES OBTENEIDOS CORRESPONDEN SOLO A 1 FILA
#####################################################


When(/^Obtengo los datos de prueba desde "([^"]*)" data pool con filtros "([^"]*)"$/) do |csv_data, dt_filters|

  #obtener data
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all' or dt_filters.to_s.empty?
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  $dt_data = csv
  $dt_row = csv[1]
  puts "DATA => #{$dt_data}"
  sleep 3

  nud=$dt_row[:nud].to_s
  ticket=$dt_row[:ticket].to_s
  visita=$dt_row[:visita].to_s
  $nombre_ticket=nud+ticket+visita

end


When(/^Obtengo los segundos datos de prueba desde "([^"]*)" data pool con filtros  "([^"]*)"$/) do |csv_data, dt_filters|

  #obtener data
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all' or dt_filters.to_s.empty?
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  $dt_data_combo =csv
  $dt_row_combo =csv[1]
  puts "DATA => #{$dt_data_combo}"

end

When(/^Obtengo los terceros datos de prueba desde "([^"]*)" data pool con filtros  "([^"]*)"$/) do |csv_data, dt_filters|
  #obtener data
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all' or dt_filters.to_s.empty?
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  $dt_data_combo2 =csv
  $dt_row_combo2 =csv[1]
  puts "DATA => #{$dt_data_combo2}"
end

When(/^Obtengo los segundos datos de prueba para revisitas desde "([^"]*)" data pool con filtros "([^"]*)"$/) do |csv_data, dt_filters|

  # obtener data
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all' or dt_filters.to_s.empty?
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  $dt_data_revisita =csv
  $dt_row_revisita =csv[1]
  puts "DATA => #{$dt_data_revisita}"


  nud=$dt_row_revisita[:nud].to_s
  ticket=$dt_row_revisita[:ticket].to_s
  visita=$dt_row_revisita[:visita].to_s
  $nombre_ticket=nud+ticket+visita
end

When(/^Veo Login$/) do
  $login_page.assert_login_page_displayed?
  raise "ERROR LOGIN INVALIDO"

end

When(/^valido estar en la pantalla indicadores de cliente$/) do
  $paginas_page.assert_indicadores_cliente_page_displayed?
  save_evidence_execution
end


When(/^seleccionamos siguiente en la pantalla pedido del dia$/) do
  $paginas_page.touch_btn_siguiente_pedido_del_dia
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla devolucion de retornables$/) do
  $paginas_page.touch_btn_siguiente_devolucion_de_retornables
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla cupones$/) do
  $paginas_page.touch_btn_siguiente_cupones
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla movimientos del dia$/) do
  $paginas_page.touch_btn_siguiente_movimiento_del_dia
  save_evidence_execution
end

When(/^Seleccionamos siguiente en indicadores de cliente$/) do
  $paginas_page.touch_btn_siguiente_indicadores_del_dia
  save_evidence_execution
end

When(/^se muestra la auditoria de enfriadores$/) do
  $paginas_page.assert_pagina_auditoria_de_enfriadores_display?
  save_evidence_execution
end


When(/^valido estar en la pantalla de descuentos$/) do
 $paginas_page.assert_pagina_descuentos_displayed?
  save_evidence_execution
end

When(/^seleccionamos siguiente en la pantalla descuentos$/) do
  $paginas_page.touch_btn_siguiente_descuentos
  save_evidence_execution
end


When(/^obtener cobro de envase Garrafon$/) do
  $generic_page.obtener_cobro_embase_garrafon
  save_evidence_execution
  end

When(/^obtener cobro de cajilla$/) do
  $generic_page.obtener_cobro_cajilla
  save_evidence_execution
  end

When(/^sumar los cobros$/) do
  $generic_page.suma_cobros
  save_evidence_execution
end

When(/^acepto el mensaje de cobro de envase GRB$/) do
  $generic_page.btn_cobro_envase
  save_evidence_execution
  end

When(/^acepto el mensaje de cobro de envase Garrafon$/) do
  $generic_page.btn_cobro_envase_garrafon
  save_evidence_execution
  end

When(/^acepto el mensaje de cobro de cajilla$/) do
  $generic_page.btn_cobro_cajilla
  save_evidence_execution
  end

When(/^valido estar en la pantalla de Prestamo de Retornables$/) do
  $generic_page.lbl_prestamo_retornable
  save_evidence_execution
  end

When(/^valido datos de cobro de GRB$/) do
  $generic_page.txt_mensaje_grb
  save_evidence_execution
  end

When(/^valido datos de cobro de cajilla$/) do
  $generic_page.txt_mensaje_cajilla
  save_evidence_execution
  end

When(/^valido datos de cobro de garrafon$/) do
  $generic_page.txt_mensaje_garrafon
  save_evidence_execution
  end

When(/^valido datos de cobro de GRB y garrafon$/) do
  $generic_page.txt_mensaje_grb_y_garrafon
  save_evidence_execution
  end


When(/^valido la recuperación de retonables$/) do
  $generic_page.validar_recuperacion_retornable
  save_evidence_execution
end


