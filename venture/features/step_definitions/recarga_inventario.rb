# encoding: utf-8
require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$recarga_inventario_page=Recarga_inventarioPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************


When(/^valido y selecciono el boton Recarga$/) do
 $recarga_inventario_page.assert_btn_recarga_displayed?
  save_evidence_execution
end

When(/^valido estar en la pantalla de recarga y descarga$/) do
 $recarga_inventario_page.assert_pantalla_recarga_displayed?
 save_evidence_execution

end


When(/^busco y agrego el producto para inventario "([^"]*)"$/) do |sku|
 $recarga_inventario_page.busca_y_agrega_productos_para_inventario(sku)
 save_evidence_execution
end

When(/^imprimo la recarga de inventario$/) do
 $recarga_inventario_page.touch_btn_impresora
  save_evidence_execution
end

When(/^valido estar en la pagina de login para supervisor$/) do
 $recarga_inventario_page.assert_pantalla_super_displayed?
 save_evidence_execution
end


When(/^Inicio sesion como verificador$/) do
 $recarga_inventario_page.llenar_formulario_verificador
 save_evidence_execution
end

When(/^Inicio sesion como supervisor$/) do
 $recarga_inventario_page.llenar_formulario_super
 save_evidence_execution
end

When(/^Inicio sesion como vendedor$/) do
 $recarga_inventario_page.llenar_formulario_vendedor
 save_evidence_execution
end


When(/^valido y selecciono el boton inventario$/) do
 $recarga_inventario_page.assert_btn_inventario_displayed?
 save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla de supervisor$/) do
 $recarga_inventario_page.btn_siguiente_supervisor
  save_evidence_execution
end

When(/^valido el producto agregado en inventario "([^"]*)"$/) do |numero_sku|
 $recarga_inventario_page.valida_producto_agregado(numero_sku)
 save_evidence_execution
end

When(/^busco y descargo el producto para inventario "([^"]*)"$/) do |numero_descarga|
 $recarga_inventario_page.busca_y_descarga_productos_en_inventario(numero_descarga)
 save_evidence_execution
end

When(/^valido el producto descargado en inventario "([^"]*)"$/) do |numero_sku|
 $recarga_inventario_page.valida_producto_agregado(numero_sku)
 save_evidence_execution
end