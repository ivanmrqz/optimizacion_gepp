# encoding: utf-8
require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$movimiento_dia_page= Movimiento_diaPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

When(/^valido que el resumen de cuenta sea correcto$/) do
  $movimiento_dia_page.validar_resumen_de_cuenta
  save_evidence_execution
end


When(/^valido que el resumen de cuenta sea correcto con cr$/) do
  $movimiento_dia_page.validar_resumen_de_cuenta_con_cr
  save_evidence_execution
end


When(/^busco ticket$/) do
  sleep 1
  $movimiento_dia_page.validarTicket
  end

When(/^busco ticket ar$/) do
  sleep 1
  $movimiento_dia_page.validarTicketAR
end


When(/^regreso a la ruta principal$/) do
  sleep 3
  adb_keys("KEYCODE_BACK")
  sleep 3
  adb_keys("KEYCODE_BACK")
  sleep 3
  adb_keys("KEYCODE_BACK")
  sleep 3
  adb_keys("KEYCODE_BACK")
  sleep 3
  adb_keys("KEYCODE_BACK")
  sleep 1
  adb_keys("KEYCODE_BACK")
  sleep 1
  adb_keys("KEYCODE_BACK")



end