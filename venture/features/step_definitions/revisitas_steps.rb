# encoding: utf-8
require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$revisitas_page=RevisitasPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

When(/^Selecciono el submenu visitados$/) do
  $revisitas_page.touch_btn_visitados
  save_evidence_execution
end

When(/^selecciono el menu mas opciones$/) do
  $revisitas_page.touch_btn_mas_opciones
  save_evidence_execution
end

When(/^se muestra la pantalla visitados$/) do
  $revisitas_page.assert_pantalla_visitados_displayed?
  save_evidence_execution
end

When(/^se muestra la pantalla opciones de factura_1500$/) do
  $revisitas_page.assert_pantalla_1500_displayed?
  save_evidence_execution
end

When(/^selecciono el boton de cambiar pedido$/) do
  $revisitas_page.touch_btn_cambiar_pedido?
  save_evidence_execution
end

When(/^valido que se muestren los productos de la venta anterior$/) do
  $revisitas_page.validar_skus_venta_anterior
  save_evidence_execution
end

When(/^disminuyo productos para rechazo parcial$/) do
  $revisitas_page.rechazo_parcial_revisita
  save_evidence_execution
end

When(/^seleccionamos el boton de no venta$/) do
  $revisitas_page.touch_btn_no_venta
  save_evidence_execution
end

When(/^modifico valores hash$/) do
  $revisitas_page.modificar_valores_hash
  save_evidence_execution
end


When(/^se muestra la pantalla_6000 Razones de No Venta$/) do
  $revisitas_page.assert_pantalla_6000_displayed?
  save_evidence_execution
end

When(/^Selecciono el departamento$/) do
  $revisitas_page.seleccionar_departamento_rt
  save_evidence_execution
end

When(/^selecciono el motivo de rechazo$/) do
  $revisitas_page.seleccionar_motivo_de_rechazo
  save_evidence_execution
end