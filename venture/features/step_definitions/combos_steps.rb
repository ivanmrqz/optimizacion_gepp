# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require 'calabash-android/abase'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$combos_page = CombosPage.new(self)
$combos_page_2=CombosPage_2.new(self)


#*****************************************************************************
# STEPS DEFINITIONS
#*****************************************************************************

When(/^damos clic en el boton "([^"]*)"$/) do |opcion|
  $combos_page.seleccionar_btn(opcion)
  save_evidence_execution
end

When(/^damos clic en el boton de cajilla$/) do
  $combos_page.seleccionar_btn_cajilla
  save_evidence_execution
end

When(/^damos clic en el combo "([^"]*)"$/) do |combo|
  $combos_page.seleccionar_btn_combo(combo)
  save_evidence_execution
end

When(/^damos clic en el segundo combo "([^"]*)"$/) do |segundoCombo|
  $combos_page.seleccionar_btn_segundo_combo(segundoCombo)
  save_evidence_execution
end


When(/^seleccionamos el combo de descuentos escalera$/) do
  $combos_page.touch_btn_combo_escalera
  save_evidence_execution
end

When(/^seleccionamos el segundo combo de descuentos escalera$/) do
  $combos_page.touch_btn_segundo_combo_escalera
  save_evidence_execution
end


When(/^seleccionamos el combo de descuentos dinamico$/) do
  $combos_page.touch_btn_combo_dinamico
  save_evidence_execution
end


When(/^buscamos el combo$/) do
  $combos_page.buscar_combo
  save_evidence_execution
end

When(/^buscamos el segundo combo$/) do
  $combos_page.buscar_segundo_combo
  save_evidence_execution
end

When(/^buscamos combo dinamico$/) do
  $combos_page.buscar_combo_dinamico
  save_evidence_execution
end

When(/^buscamos segundo combo dinamico$/) do
  $combos_page.buscar_segundo_combo_dinamico
  save_evidence_execution
end

When(/^buscamos combo estatico$/) do
  $combos_page.buscar_combo_estatico
  save_evidence_execution
end

When(/^busco e ingreso el producto "([^"]*)"$/) do |sku|
  $combos_page.busco_producto(sku)
  save_evidence_execution
end

When(/^busco e ingreso el producto del segundo combo "([^"]*)"$/) do |segundoSku|
  $combos_page.busco_producto_segundo_combo(segundoSku)
  save_evidence_execution
end

When(/^busco el producto e ingreso cantidad "([^"]*)"$/) do |sku_combo|
  $combos_page.busco_producto_combo(sku_combo)
  save_evidence_execution
end

When(/^recupero el total y calculo el porcentaje$/) do
  $combos_page.calculo_combo
  save_evidence_execution
end

When(/^recupero el total y calculo el porcentaje del segundo combo$/) do
  $combos_page.calculo_segundo_combo
  save_evidence_execution
end

When(/^dar clic en la palomita para confirmar combo$/) do
  $combos_page.touch_confirmar_combo
  save_evidence_execution
end

When(/^ingresamos la cantidad de combos dinamicos$/) do
  $combos_page.ingresar_cantidad_combos
  save_evidence_execution
end

When(/^recupero el total y calculo el porcentaje del combo dinamico$/) do
  $combos_page.calculo_combo_dinamico
  save_evidence_execution
end

When(/^recupero el total y calculo el porcentaje del segundo combo dinamico$/) do
  $combos_page.calculo_segundo_combo_dinamico
  save_evidence_execution
end

When(/^busco combo estatico y realizo calculo$/) do
  $combos_page.calculo_combo_estatico
  save_evidence_execution
end

When(/^calculo descuento eventual$/) do
  $combos_page.calcularDescuentoEVT
  save_evidence_execution
end

When(/^calculo descuento del CSV$/) do
  $combos_page.validarDescuentosEnElCSV
  save_evidence_execution
end


When(/^busco tipo de combo$/) do
  $combos_page.buscar_tipo_combo
  save_evidence_execution
end


When(/^busco segundo combo$/) do
  $combos_page_2.buscar_tipo_combo2
  save_evidence_execution
end