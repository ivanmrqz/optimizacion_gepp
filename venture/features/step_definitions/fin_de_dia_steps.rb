# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$fin_dia_page= FinDiaPage.new(self)

#*****************************************************************************
# STEPS DEFINITIONS
#*****************************************************************************


When(/^valido y selecciono el boton Liquidar Ruta$/) do
  $fin_dia_page.assert_btn_liquidar_ruta_displayed?
  save_evidence_execution
end

When(/^Se muestra la pantalla 6030_deposito$/) do
  $fin_dia_page.assert_6030_page_displayed?
  save_evidence_execution
end

When(/^valido El resumen de deposito$/) do
  $fin_dia_page.validar_resumen_de_deposito
  save_evidence_execution
end

When(/^ingreso el efectivo para la pre liquidacion de ruta$/) do
  $fin_dia_page.ingresar_efectivo_para_la_pre_liquidacion_de_ruta
  save_evidence_execution
  end

When(/^Selecciono el motivo de cuentas no atendidas$/) do
  $fin_dia_page.seleccionarMotivoDeCuentasNoAtendidas
  save_evidence_execution
  end

When(/^Selecciono todos los clientes no atendidos$/) do
  $fin_dia_page.seleccionarTodosLosClientesNoAtentidos
  save_evidence_execution
  end

When(/^damos clic en el boton guardar$/) do
  $fin_dia_page.clicEnBotonGuardar
  save_evidence_execution
  end

When(/^doy clic en Siguiente en clientes no atendidos$/) do
  $fin_dia_page.clicEnSiguiente
  save_evidence_execution
  end

When(/^valido estar en la pantalla 6010 Resumen de las Ventas Diarias$/) do
  $fin_dia_page.validarPantalla6010
  save_evidence_execution
  end

When(/^validar el total de las ventas$/) do
  $fin_dia_page.validarTotalDeVentas
  save_evidence_execution
  end

When(/^doy clic en 'Siguiente' en resumen de las ventas diarias$/) do
  $fin_dia_page.siguienteEnResumenDeLasVentas
  save_evidence_execution
end

When(/^doy clic en el TCOMM para fin de dia$/) do
  $fin_dia_page.clicTcommFinDeDia
  save_evidence_execution
end

When(/^obtengo el inventario Final$/) do
  $fin_dia_page.obtener_inventario_final
  save_evidence_execution
end