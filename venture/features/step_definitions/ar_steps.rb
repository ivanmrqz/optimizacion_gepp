# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'



#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$ar_page= ArPage.new(self)

#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************


When(/^obtengo el importe de "([^"]*)"$/) do|numero_producto|
  $ar_page.calcula_importe_ar (numero_producto)
  save_evidence_execution
end

When(/^obtengo el importe total de ar$/) do
  $ar_page.obten_importe_ar
  save_evidence_execution
end

When(/^valido estar en la pantalla de folio recibo y orden de compra$/) do
  $ar_page.assert_page_folio_recibo_3501_displayed?
  save_evidence_execution
end

When(/^ingreso el folio de recibo para la empresa_1$/) do
  $ar_page.ingresar_folio_recibo_empresa_1
  save_evidence_execution
end

When(/^ingreso la orden de compra para la empresa_1$/) do
  $ar_page.ingresar_orden_de_compra_empresa_1
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla de folio y orden de compra$/) do
  $ar_page.touch_btn_siguiente_page_folio
  save_evidence_execution
end

When(/^confirmo el folio de recibo$/) do
  $ar_page.ingresar_folio_recibo_empresa_1
  save_evidence_execution
end

When(/^obtengo el cobro de garrafon para ar$/) do
  $ar_page.obtener_cobro_garrafon
  save_evidence_execution
end

When(/^Confirmo la orden de compra$/) do
  $ar_page.ingresar_orden_de_compra_empresa_1
  save_evidence_execution
end

When(/^ingreso el folio de recibo para la empresa_2$/) do
  $ar_page.ingresar_folio_recibo_empresa_2
  save_evidence_execution
end

When(/^ingreso la orden de compra para la empresa1_2$/) do
  $ar_page.ingresar_orden_de_compra_empresa_2
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2$/) do
  $ar_page.touch_btn_siguiente_ar
  save_evidence_execution
end

When(/^confirmo el folio de recibo_2$/) do
  sleep 2
  $ar_page.ingresar_folio_recibo_empresa_2
  save_evidence_execution
end