# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$login_page = LoginPage.new(self)


#*****************************************************************************
# STEPS DEFINITIONS
#*****************************************************************************



When(/^doy click en el boton permitir$/) do
  $generic_page.touch_btn_permitir
  save_evidence_execution
end


When(/^que estoy en la pagina de inicio '1420'$/) do
  $login_page.assert_incio_page_displayed?
  save_evidence_execution
end

When(/^valido atributos de inicio$/) do
  $login_page.assert_atributos_inicio_displayed?
  save_evidence_execution
end

When(/^ingreso bodega y ruta$/) do
  $login_page.send_text_bodega_ruta
  save_evidence_execution
end

When(/^selecciono el boton tcomm local$/) do
  $login_page.touch_btn_tcomm_local
  save_evidence_execution
end


When(/^valido estar en la pantalla de cargas$/) do
  $login_page.assert_cargas_page_displayed?
  save_evidence_execution
end

When(/^selecciono una carga disponible$/) do
  $login_page.touch_txt_carga
  save_evidence_execution
end


When(/^acepto la popup$/) do
  $login_page.aceptar_pop_up
  save_evidence_execution
end

When(/^rechazo la pop up$/) do
  $login_page.rechazar_pop_up
  save_evidence_execution
end


When(/^valido atributos$/) do
  $login_page.assert_atributos?
end

When(/^ingreso el usuario y contraseña$/) do
  $login_page.llenar_formulario?
end

When(/^doy clic en 'Siguiente'$/) do
  $login_page.touch_btn_ingresar?
end

When(/^validar estar en la ventana 1000 Menu$/) do
  $login_page.assert_menu_principal?
end


When(/^doy click en inventario$/) do
  $login_page.touch_img_inventario
  save_evidence_execution
end


When(/^valido estar en la pantalla de Configuracion Inicial$/) do
  $login_page.assert_lbl_configuracion_inicial?

end

When(/^valido atributos de la primer configuracion$/) do
  $login_page.assert_atributos_configuracion_inicial?
  end

When(/^ingreso bodega$/) do
  $login_page.send_text_bodega
  end

When(/^dar clic en el boton sincronizar$/) do
  $login_page.touch_btn_sincroniazar
end


When(/^imprimo el inventario$/) do
  $login_page.touch_img_impresora
  save_evidence_execution
end

When(/^doy click en el tercer tcomm$/) do
  $login_page.touch_btn_tercer_tcomm
  save_evidence_execution
end

When(/^Seleccionamos siguiente en la pagina "1507"$/) do
  $login_page.touch_btn_siguiente_1460
  save_evidence_execution
end

When(/^valido el icono de pepsi$/) do
  $login_page.assert_logo_pepsi_displayed?
  save_evidence_execution
end

When(/^ingreso datos de control de km$/) do
  $login_page.send_text_datos_kilometraje
  save_evidence_execution
end

When(/^confirmo kilometraje$/) do
  $login_page.send_text_confirmacion_km
  save_evidence_execution
end

When(/^doy click en el icono de confirmacion$/) do
  $login_page.touch_btn_palomita
  save_evidence_execution
end

When(/^doy click en el boton etiqueta$/) do
  $login_page.touch_btn_etiqueta
  save_evidence_execution
end



When(/^valido el boton servicio al cliente$/) do
  $login_page.assert_btn_servicio_al_cliente_displayed?
  save_evidence_execution
  end

When(/^valido estar en la pantalla de Login$/) do
  $login_page.assert_lbl_login?
  save_evidence_execution
  end

