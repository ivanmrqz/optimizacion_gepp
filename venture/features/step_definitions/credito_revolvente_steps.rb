# encoding: utf-8

require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'


#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$credito_revolvente_page= CrPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************



When(/^liquido y verifico que se aplicaron los pagos$/) do
$credito_revolvente_page.liquidar_pagos_cr
  save_evidence_execution
end

When(/^doy click en siguiente de la pantalla pagos$/) do
 $credito_revolvente_page.touch_btn_siguiente_page_pagos
  save_evidence_execution
end



When(/^valido pagos y si existen los liquido$/) do
  $credito_revolvente_page.validar_pagos_anteriores_y_liquidar
  save_evidence_execution
end


When(/^valido estar en la pantalla de credito revolvente$/) do
  $credito_revolvente_page.assert_cr_page_displayed?
  save_evidence_execution
end
When(/^Selecciono el motivo de credito revolvente$/) do
  $credito_revolvente_page.seleccionar_motivo_credito_revolvente
  save_evidence_execution
end


When(/^Agrego el monto del credito revolvente "([^"]*)"$/) do |monto_credito|
  $credito_revolvente_page.agregar_monto_credito(monto_credito)
  save_evidence_execution
end


When(/^verifico que el importe sea correcto$/) do
  $credito_revolvente_page.validar_gran_total
  save_evidence_execution
end

When(/^verifico que el importe sea correcto con descuento$/) do
  $credito_revolvente_page.validar_gran_total_con_descuento
  save_evidence_execution
end

When(/^doy click en siguiente de la pantalla cr$/) do
  $credito_revolvente_page.touch_btn_siguiente_cr
  save_evidence_execution
end


When(/^liquido el monto de credito revolvente$/) do
  $credito_revolvente_page.liquidar_cr
  save_evidence_execution
end

When(/^liquido parcialmente el monto de credito revolvente$/) do
  $credito_revolvente_page.liquidar_parcialmente_el_monto_cr
  save_evidence_execution

end

When(/^obtengo el importe sin combos$/) do
 $credito_revolvente_page.obtnener_importe_sin_combo
  save_evidence_execution
end

When(/^obtengo el importe con combos$/) do
  $credito_revolvente_page.obtener_importe_con_combo
  save_evidence_execution
end

