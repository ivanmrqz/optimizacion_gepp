# encoding: utf-8
require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'


#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$monedas_page= MonedasPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************

When(/^liquido el total con cupones$/) do
  $monedas_page.liquidar_total_con_monedas
  save_evidence_execution
end

When(/^Agrego monedas de manera parcial$/) do
  $monedas_page.agregar_monedas_parcialmente
  save_evidence_execution
end

When(/^busco la moneda "([^"]*)"$/) do |tipo_moneda|
  $monedas_page.buscar_primer_moneda(tipo_moneda)
  save_evidence_execution
end

