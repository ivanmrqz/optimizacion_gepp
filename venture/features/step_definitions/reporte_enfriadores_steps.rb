# encoding: utf-8
require 'calabash-android/calabash_steps'
require_relative '../../../helpers/generic.rb'
require_relative '../../../helpers/csv_data'

#*****************************************************************************
# INSTANCIA CLASES
#*****************************************************************************
$generic_page = GenericPage.new(self)
$enfriadores_page= EnfriadoresPage.new(self)


#*****************************************************************************
#STEPS DEFINITIONS
#*****************************************************************************


When(/^valido estar en la pantalla de enfriadores$/) do
  $enfriadores_page.assert_reporte_enfriadores_page_displayed?
  save_evidence_execution
end


When(/^ingreso un numero telefonico y selecciono el boton sin etiqueta$/) do
  $enfriadores_page.ingresar_numero_tel
  save_evidence_execution
end


When(/^selecciono el boton siguiente de la pagina de enfriadores$/) do
  $enfriadores_page.touch_btn_siguiente_enfriadores
  save_evidence_execution
end

