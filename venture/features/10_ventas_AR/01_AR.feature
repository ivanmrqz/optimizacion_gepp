# language: es
# encoding: utf-8
Característica: VENTAS AR

  @regresion @Ar @CP0001AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=1"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up



  @regresion @Ar @CP004AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=4"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
      #Entonces se muestra la auditoria de enfriadores
      #Y selecciono el boton siguiente de la pagina de enfriadores
      #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y buscar "GRB"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @Ar @CP008AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=8"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
        #Entonces se muestra la auditoria de enfriadores
        #Y selecciono el boton siguiente de la pagina de enfriadores
        #Entonces acepto la popup
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Y buscar "garrafon"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    #hay popup cuando se muestra el recibo de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0015AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=15"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    ####
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Cuando ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Y Seleccionamos siguiente
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0020AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=20"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    #Entonces acepto la popup
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Y  buscar "GRB"
    Y buscar "garrafon"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Cuando ingreso el folio de recibo para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @Ar @CP0022AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=22"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
     #Entonces se muestra la auditoria de enfriadores
     #Y selecciono el boton siguiente de la pagina de enfriadores
     #Entonces acepto la popup
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0026AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=26"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
       #Entonces se muestra la auditoria de enfriadores
       #Y selecciono el boton siguiente de la pagina de enfriadores
       #Entonces acepto la popup
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "garrafon"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0027AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=27"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   #Entonces se muestra la auditoria de enfriadores
   #Y selecciono el boton siguiente de la pagina de enfriadores
   #Entonces acepto la popup
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "GRB"
    Entonces buscar "garrafon"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0030AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=30"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
   #Entonces se muestra la auditoria de enfriadores
   #Y selecciono el boton siguiente de la pagina de enfriadores
   #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @Ar @CP0034AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=34"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
 #Entonces se muestra la auditoria de enfriadores
 #Y selecciono el boton siguiente de la pagina de enfriadores
 #Entonces acepto la popup
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "garrafon"
    Entonces buscar "cajilla"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
     #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @Ar @CP0035AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=35"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    #Entonces acepto la popup
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "garrafon"
    Entonces buscar "GRB"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @AR @CP0036AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=36"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
 #Entonces se muestra la auditoria de enfriadores
 #Y selecciono el boton siguiente de la pagina de enfriadores
 #Entonces acepto la popup
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


     @regresion @Ar @CP0045AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=45"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    #Entonces se muestra la auditoria de enfriadores
    #Y selecciono el boton siguiente de la pagina de enfriadores
    # Entonces acepto la popup
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up



  @regresion @Ar @CP0047AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=47"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
  #Entonces se muestra la auditoria de enfriadores
  #Y selecciono el boton siguiente de la pagina de enfriadores
  # Entonces acepto la popup
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "GRB"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0049AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=49"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
  #Entonces se muestra la auditoria de enfriadores
  #Y selecciono el boton siguiente de la pagina de enfriadores
  # Entonces acepto la popup
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "GRB"
    Entonces buscar "cajilla"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0053AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=53"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
  #Entonces se muestra la auditoria de enfriadores
  #Y selecciono el boton siguiente de la pagina de enfriadores
  # Entonces acepto la popup
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up

  @regresion @Ar @CP0054AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=54"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
#Entonces se muestra la auditoria de enfriadores
#Y selecciono el boton siguiente de la pagina de enfriadores
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "cajilla"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
        #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0056AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=56"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
 #Entonces se muestra la auditoria de enfriadores
 #Y selecciono el boton siguiente de la pagina de enfriadores
 #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Y obtengo el cobro de garrafon para ar
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up


  @regresion @Ar @CP0057AR
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_ar" data pool con filtros "id=57"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
 #Entonces se muestra la auditoria de enfriadores
 #Y selecciono el boton siguiente de la pagina de enfriadores
 #Entonces acepto la popup
    Y busco productos y agrego productos"pet"
    Entonces obtengo el importe de "pet"
    Y busco productos y agrego productos"garrafon"
    Entonces obtengo el importe de "garrafon"
    Y busco productos y agrego productos"no_carbonatado"
    Entonces obtengo el importe de "no_carbonatado"
    Y busco productos y agrego productos"agua_embotellada"
    Entonces obtengo el importe de "agua_embotellada"
    Cuando obtengo el importe total de ar
    Y seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "cajilla"
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
 #hay popup cuando se muestra el resumen de orden
    Cuando acepto la popup
    Y valido estar en la pantalla de folio recibo y orden de compra
    Entonces ingreso el folio de recibo para la empresa_1
    Entonces ingreso la orden de compra para la empresa_1
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra
    Cuando confirmo el folio de recibo
    Y Seleccionamos siguiente
    Entonces ingreso el folio de recibo para la empresa_2
    Entonces ingreso la orden de compra para la empresa1_2
    Y Seleccionamos siguiente en la pantalla de folio y orden de compra para la empresa_2
    Cuando confirmo el folio de recibo_2
    Cuando acepto la popup
    Entonces valido estar en la pantalla indicadores de cliente
    Y Seleccionamos siguiente
    Entonces rechazo la pop up
