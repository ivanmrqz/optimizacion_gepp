# language: es
# encoding: utf-8
Característica: VENTAS - OTRAS MONEDAS GRB 0

  @regresion @ruta19 @ventas @CP069 @Otras_Monedas @grb0
  Escenario: Venta con otras monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=69"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=69"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=69"
    Entonces busco segundo combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces  buscar "GRB"
    Entonces buscar el grb 2
    Entonces buscar "garrafon"
    Entonces buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ruta19 @ventas @CP071 @Otras_Monedas @grb0
  Escenario: Venta con otras monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=71"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=71"
    Entonces busco tipo de combo
    Cuando Obtengo los terceros datos de prueba desde "dt_combos" data pool con filtros  "id=71 -1"
    Entonces busco segundo combo
    Entonces Se muestra el pedido del dia
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "GRB"
    Entonces buscar el grb 2
    Entonces buscar "garrafon"
    Entonces buscar el garrafon 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup


  @regresion @ruta19 @ventas @CP077 @Otras_Monedas @grb0
  Escenario: Venta con otras monedas
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_test" data pool con filtros "id=77"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Entonces Se muestra el pedido del dia
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=77"
    Entonces busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Entonces acepto la popup
    Y Se muestra la pantalla de Devolucion Retornables
    Entonces buscar "GRB"
    Entonces buscar el grb 2
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup

