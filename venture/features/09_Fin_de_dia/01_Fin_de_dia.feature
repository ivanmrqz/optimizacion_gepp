# language: es
# encoding: utf-8
Característica: FIN DE DIA


  @regresion @Fin_De_Dia @CP001Fin_de_Dia
  Escenario: Escaneo de etiqueta de manera exitosa
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=1"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y doy clic en 'Siguiente'
    Entonces validar estar en la ventana 1000 Menu
    Y doy click en el boton etiqueta
    Y acepto la popup
    Y acepto la popup


  @regresion @Fin_De_Dia @CP002Fin_de_Dia
  Escenario: Verificación del fin del día exitosa
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=2"
    Cuando Inicio sesion como verificador
    Y Seleccionamos siguiente
    Cuando doy click en inventario
    Y acepto la popup
    Entonces valido estar en la pantalla de control de km
    Entonces ingreso datos de control de km
    Y I press back button
    Entonces confirmo kilometraje
    Y I press back button
    Entonces doy click en el icono de confirmacion
    Cuando imprimo el inventario
    Y acepto la popup
    Y acepto la popup
    Entonces acepto la popup
    Entonces valido estar en la pantalla de Login


  @regresion @Fin_De_Dia @CP010Fin_de_Dia
  Escenario: Cuentas no atendidas Cliente No atendido- agregar motivo
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=10"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton Liquidar Ruta
    Cuando Selecciono el motivo de cuentas no atendidas
    Entonces Selecciono todos los clientes no atendidos
    Y damos clic en el boton guardar
    Y doy clic en Siguiente en clientes no atendidos
    Entonces valido estar en la pantalla 6010 Resumen de las Ventas Diarias
    Y validar el total de las ventas
    Entonces doy clic en 'Siguiente' en resumen de las ventas diarias


  @regresion @Fin_De_Dia @CP012Fin_de_Dia
  Escenario: Resumen de liquidación
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=2"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton Liquidar Ruta
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla 6030_deposito
    Y valido El resumen de deposito


  @regresion @Fin_De_Dia @CP013Fin_de_Dia
  Escenario: Ingreso de efectivo, preliquidación capturando la cantidad correcta solicitada
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=13"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Entonces valido y selecciono el boton Liquidar Ruta
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla 6030_deposito
    Y valido El resumen de deposito
    Entonces ingreso el efectivo para la pre liquidacion de ruta
    Y Seleccionamos siguiente
    Cuando acepto la popup
    Y acepto la popup
    Entonces validar estar en la ventana 1000 Menu


  @regresion @Fin_De_Dia @CP017Fin_de_Dia
  Escenario: Realizar el TCOMM de fin de día con el vendedor de manera exitosa
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_Fin_de_dia" data pool con filtros "id=17"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando doy clic en el TCOMM para fin de dia
    Entonces selecciono el boton tcomm local
    Y acepto la popup
    #Y acepto la popup
   # Entonces valido estar en la pantalla de cargas
   # Y selecciono una carga disponible
   # Cuando Seleccionamos siguiente
   # Entonces valido estar en la pantalla de Login





































