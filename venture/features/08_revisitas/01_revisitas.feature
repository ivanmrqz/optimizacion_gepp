# language: es
# encoding: utf-8
Característica: REVISITAS

  @regresion @Revisita_grb2 @CP005_Revisita
  Escenario: Descarga de inventario
  Dado abro la aplicacion
  Entonces valido estar en la pantalla de Login
  Entonces valido atributos
  Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=5"
  Cuando Inicio sesion como usuario "valido"
  Y press_enter_button

  #### pantalla descuentos

  Y busco productos y agrego productos"1485"
  Y busco productos y agrego productos"1458"
  Y busco productos y agrego productos"tanque C02"
  Y busco productos y agrego productos"pepsi1"
  Y busco productos y agrego productos"tanque C02"

  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5-R-1"
  Y busco tipo de combo
  Y obtenemos el importe del pedido
  Cuando seleccionamos siguiente en la pantalla pedido del dia
  Entonces valido estar en la pantalla de descuentos
  Entonces calculo descuento del CSV
  Y seleccionamos siguiente en la pantalla descuentos
  Entonces Se muestra la pantalla de Devolucion Retornables
    #PRESTAMO CAJILLA.3
  Cuando damos clic en el boton de cajilla
  Y valido datos de cobro de GRB
  Cuando acepto el mensaje de cobro de envase GRB
  Y  buscar cajilla para prestamo
  Entonces Seleccionamos siguiente
  Y valido pagos y si existen los liquido
  Entonces se muestra la pantalla cantidad de cheques
  Y Seleccionamos siguiente en la pagina cantidad de cheques
  Entonces se muestra la pantalla cantidad de cupones
  Y Seleccionamos siguiente en la pantalla cupones
  Cuando Se muestra la pantalla movimientos del dia
  Entonces valido que el resumen de cuenta sea correcto
  Y Seleccionamos siguiente en la pantalla movimientos del dia
  Cuando acepto la popup
  Entonces  valido estar en la pantalla indicadores de cliente
  Y I press back button
  Y selecciono el menu mas opciones
  Cuando Selecciono el submenu visitados
  Entonces se muestra la pantalla visitados
  Cuando Busco un cliente por NUD
  Y Selecciono el submenu "VENTA"
  Y Confirmo el mensaje emergente
  Entonces se muestra la pantalla opciones de factura_1500
  Y valido que se muestren los productos de la venta anterior
  Y selecciono el boton de cambiar pedido
  Cuando acepto la popup
  Entonces se muestra la auditoria de enfriadores
  Y selecciono el boton siguiente de la pagina de enfriadores
  Cuando acepto la popup
  #REVISITA 1
  Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=5_2"
  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5-R-2"
  Y busco productos y agrego productos"revisita"
  Y obtenemos el importe del pedido
    Entonces seleccionamos siguiente en la pantalla pedido del dia
  Cuando acepto la popup
  #Y acepto la popup
  Entonces valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
  Y seleccionamos siguiente en la pantalla descuentos
  Entonces Se muestra la pantalla de Devolucion Retornables
  Y valido datos de cobro de GRB
  Cuando acepto el mensaje de cobro de envase GRB
  Y buscar cajilla para prestamo
  Entonces Seleccionamos siguiente
  Y valido pagos y si existen los liquido
  Entonces se muestra la pantalla cantidad de cheques
  Y Seleccionamos siguiente en la pagina cantidad de cheques
  Entonces se muestra la pantalla cantidad de cupones
  Y Seleccionamos siguiente en la pantalla cupones
  Cuando Se muestra la pantalla movimientos del dia
  Entonces valido que el resumen de cuenta sea correcto
  Y Seleccionamos siguiente en la pantalla movimientos del dia
  Cuando acepto la popup
  Entonces  valido estar en la pantalla indicadores de cliente
  Y I press back button
  Y selecciono el menu mas opciones
  Cuando Selecciono el submenu visitados
  Entonces se muestra la pantalla visitados
  Cuando Busco un cliente por NUD
  Y Selecciono el submenu "VENTA"
  Y Confirmo el mensaje emergente
  Entonces se muestra la pantalla opciones de factura_1500
  Y valido que se muestren los productos de la venta anterior
  Y selecciono el boton de cambiar pedido
  Cuando acepto la popup
  Entonces se muestra la auditoria de enfriadores
  Y selecciono el boton siguiente de la pagina de enfriadores
  Cuando acepto la popup
   #REVISITA 1
  Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=5_3"
  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5-R-3"
  Y busco productos y agrego productos"revisita"
  Y obtenemos el importe del pedido
  Entonces seleccionamos siguiente en la pantalla pedido del dia
  Y acepto la popup
  Entonces valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
  Y seleccionamos siguiente en la pantalla descuentos
  Entonces Se muestra la pantalla de Devolucion Retornables
    #PRESTAMO CAJILLA
  Cuando damos clic en el boton de cajilla
  Y valido datos de cobro de GRB
  Cuando acepto el mensaje de cobro de envase GRB
  Y  buscar cajilla para prestamo
  Entonces Seleccionamos siguiente
  Y valido pagos y si existen los liquido
  Entonces se muestra la pantalla cantidad de cheques
  Y Seleccionamos siguiente en la pagina cantidad de cheques
  Entonces se muestra la pantalla cantidad de cupones
  Y busco la moneda "primer moneda"
  Y busco la moneda "segunda moneda"
  Y busco la moneda "tercer moneda"
  Y Seleccionamos siguiente en la pantalla cupones
  Cuando Se muestra la pantalla movimientos del dia
  Cuando modifico valores hash
  Entonces valido que el resumen de cuenta sea correcto
  Y Seleccionamos siguiente en la pantalla movimientos del dia
  Cuando acepto la popup
  Entonces  valido estar en la pantalla indicadores de cliente
  Y Seleccionamos siguiente en indicadores de cliente
  Y rechazo la pop up
  Y I press back button
  Y selecciono el menu mas opciones
  Cuando Selecciono el submenu visitados
  Entonces se muestra la pantalla visitados
  Cuando Busco un cliente por NUD
  Y Selecciono el submenu "VENTA"
  Y Confirmo el mensaje emergente
  Entonces se muestra la pantalla opciones de factura_1500
  Y valido que se muestren los productos de la venta anterior
  Y selecciono el boton de cambiar pedido
  Cuando acepto la popup
  Entonces se muestra la auditoria de enfriadores
  Y selecciono el boton siguiente de la pagina de enfriadores
  Cuando acepto la popup
  #REVISITA 3
  Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=5_4"
  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5-R-4"
  Y busco productos y agrego productos"revisita"
    Y obtenemos el importe del pedido
    Entonces seleccionamos siguiente en la pantalla pedido del dia
  Cuando acepto la popup
  #Y acepto la popup
  Entonces valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
  Y seleccionamos siguiente en la pantalla descuentos
  Entonces Se muestra la pantalla de Devolucion Retornables
  #PRESTAMO CAJILLA
  Cuando damos clic en el boton de cajilla
  Y valido datos de cobro de GRB
  Cuando acepto el mensaje de cobro de envase GRB
  Y  buscar cajilla para prestamo
  Entonces Seleccionamos siguiente
  Y valido pagos y si existen los liquido
  Entonces se muestra la pantalla cantidad de cheques
  Y Seleccionamos siguiente en la pagina cantidad de cheques
  Y busco la moneda "primer moneda"
  Y busco la moneda "segunda moneda"
  Y busco la moneda "tercer moneda"
  Y Seleccionamos siguiente en la pantalla cupones
  Cuando Se muestra la pantalla movimientos del dia
  Cuando modifico valores hash
  Entonces valido que el resumen de cuenta sea correcto
  Y Seleccionamos siguiente en la pantalla movimientos del dia
  Cuando acepto la popup
  Entonces  valido estar en la pantalla indicadores de cliente
  Y Seleccionamos siguiente en indicadores de cliente
  Y rechazo la pop up
  Y I press back button
  Y selecciono el menu mas opciones
  Cuando Selecciono el submenu visitados
  Entonces se muestra la pantalla visitados
  Cuando Busco un cliente por NUD
  Y Selecciono el submenu "VENTA"
  Y Confirmo el mensaje emergente
  Entonces se muestra la pantalla opciones de factura_1500
  Y valido que se muestren los productos de la venta anterior
  Y selecciono el boton de cambiar pedido
  Cuando acepto la popup
  Entonces se muestra la auditoria de enfriadores
  Y selecciono el boton siguiente de la pagina de enfriadores
  Cuando acepto la popup
  Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=5_5"
  Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=5-R-5"
  Entonces Se muestra el pedido del dia
  Y seleccionamos el boton de no venta
  Entonces se muestra la pantalla_6000 Razones de No Venta
  Cuando Selecciono el departamento
  Y selecciono el motivo de rechazo
  Y Seleccionamos siguiente
  Entonces valido estar en la pantalla indicadores de cliente
  Cuando Seleccionamos siguiente
  Y rechazo la pop up
  Entonces se muestra la pantalla visitados


  @regresion @Revisita_grb2 @CP020_Revisita
    #VISITA 1 Transferencia
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=20"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 20-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    Entonces  valido estar en la pantalla indicadores de cliente
    Y I press back button
    #REVISITA 2 V+Cheque
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=20_2"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 20-R-2"
    Y busco productos y agrego productos"revisita"
    Y obtenemos el importe del pedido
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Y acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    #PRESTAMO CAJILLA
    Cuando damos clic en el boton de cajilla
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente en la pantalla cupones
    Cuando Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Entonces valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    Entonces  valido estar en la pantalla indicadores de cliente
    Y I press back button
    #REVISITA 3 V+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=20_3"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 20-R-3"
    Y busco productos y agrego productos"revisita"
    Y obtenemos el importe del pedido
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
    Y Seleccionamos siguiente en indicadores de cliente
    Y rechazo la pop up
    Y I press back button
    #REVISITA 4 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=20_4"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
    Cuando acepto la popup
    Y se muestra la pantalla_6000 Razones de No Venta
    Entonces Selecciono el departamento
    Y selecciono el motivo de rechazo
    Cuando Seleccionamos siguiente
    Entonces  valido estar en la pantalla indicadores de cliente
    #REVISITA 5 V+Cheque+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=20_5"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 20-R-5"
    Y busco productos y agrego productos"revisita"
    Y obtenemos el importe del pedido
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces Seleccionamos siguiente
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  Seleccionamos siguiente en la pantalla cupones
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Cuando modifico valores hash
    Entonces  valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup


  @regresion @Revisita_grb2 @CP012_Revisita
    #VISITA 1 Otras Monedas
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=12"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    Entonces  valido estar en la pantalla indicadores de cliente
    Y I press back button
  #REVISITA 2 RP+OTRAS MONEDAS
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=12_2"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12-R-2"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
   Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
  #REVISITA 3 V+Efectivo+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=12_3"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12-R-3"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
  #REVISITA 4 RP+OTRAS MONEDAS
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=12_4"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12-R-4"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Entonces liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
#REVISITA 5 V+OTRAS MONEDAS
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=12_5"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 12-R-5"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Cuando valido estar en la pantalla de credito revolvente
    Y doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup



  @regresion @Revisita_grb2 @CP018_Revisita
      #VISITA 1 Efectivo+Transferencia+Crédito Revolvente+Otras Monedas
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=18"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id=18-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Cuando valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 2 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=18_2"
    Entonces Se muestra el pedido del dia
    Entonces seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados
    #REVISITA 3 V+Efectivo+Transferencia
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=18_3"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 18-R-3"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  seleccionamos siguiente en la pantalla descuentos
    Y Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Entonces  valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    #REVISITA 4 RP+Cheque+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=18_4"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 18-R-4"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    #REVISITA 5 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=18_5"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados


  @regresion @Revisita_grb2 @CP010_Revisita
    #VISITA 1 Cheque+Transferencia
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=10"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Entonces calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto
    Entonces acepto la popup
    #REVISITA 2 RP+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=10_2"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10-R-2"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
    #REVISITA 3 V+Transferencia
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=10_3"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10-R-3"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago total con cheque
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 4 RP+Transferencia+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=10_4"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10-R-4"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 5 RP+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=10_5"
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 10-R-5"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup


  @regresion @Revisita_grb2 @CP011_Revisita
 #VISITA 1 Efectivo+Cheque+Crédito Revolvente+Otras Monedas
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=11"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 11-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Y liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 2 RP+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=11_2"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente
    Entonces acepto la popup
    #REVISITA 3 V+Transferencia+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=11_3"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 4 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=11_4"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados
    #REVISITAS 5 V+Cheque+Efectivo
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=11_5"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB
    Cuando acepto el mensaje de cobro de envase GRB
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas
    Entonces Seleccionamos siguiente en la pagina cantidad de cheques
    Y se muestra la pantalla cantidad de cupones
    Entonces  seleccionamos siguiente en la pantalla descuentos
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Cuando modifico valores hash
    Entonces  valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup

     @regresion @Revisita_grb2 @CP007_Revisita
 #VISITA 1 Efectivo+Cheque+CréditoRevolvente
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=7"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 7-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de GRB y garrafon
    Cuando acepto el mensaje de cobro de envase GRB
       Entonces valido pagos y si existen los liquido
       Entonces valido estar en la pantalla de credito revolvente
       Y verifico que el importe sea correcto
       Cuando liquido parcialmente el monto de credito revolvente
       Y Selecciono el motivo de credito revolvente
       Entonces doy click en siguiente de la pantalla cr
       Entonces se muestra la pantalla cantidad de cheques
       Y ingresamos los datos de cheque para pago parcial con cheque y cr
       Y Seleccionamos siguiente en la pagina cantidad de cheques
       Entonces se muestra la pantalla cantidad de cupones
       Y Seleccionamos siguiente en la pantalla cupones
       Entonces Se muestra la pantalla movimientos del dia
       Y valido que el resumen de cuenta sea correcto con cr
       Y Seleccionamos siguiente en la pantalla movimientos del dia
       Entonces acepto la popup
       #REVISITA 2 V+Crédito Revolvente
       Y selecciono el menu mas opciones
       Cuando Selecciono el submenu visitados
       Entonces se muestra la pantalla visitados
       Cuando Busco un cliente por NUD
       Y Selecciono el submenu "VENTA"
       Y Confirmo el mensaje emergente
       Entonces se muestra la pantalla opciones de factura_1500
       Y valido que se muestren los productos de la venta anterior
       Y selecciono el boton de cambiar pedido
       Cuando acepto la popup
       Entonces se muestra la auditoria de enfriadores
       Y selecciono el boton siguiente de la pagina de enfriadores
       Cuando acepto la popup
       Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=7_2"
       Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 7-R-2"
       Y busco productos y agrego productos"revisita"
       Entonces seleccionamos siguiente en la pantalla pedido del dia
       Cuando acepto la popup
       Entonces valido estar en la pantalla de descuentos
       Y calculo descuento del CSV
       Y seleccionamos siguiente en la pantalla descuentos
       Entonces Se muestra la pantalla de Devolucion Retornables
       Y Seleccionamos siguiente en la pantalla devolucion de retornables
       Y valido datos de cobro de GRB y garrafon
       Cuando acepto el mensaje de cobro de envase GRB
       Entonces valido pagos y si existen los liquido
       Entonces valido estar en la pantalla de credito revolvente
       Y verifico que el importe sea correcto
       Cuando liquido el monto de credito revolvente
       Y Selecciono el motivo de credito revolvente
       Entonces doy click en siguiente de la pantalla cr
       Y acepto la popup
       #REVISITA 3 RT
       Y selecciono el menu mas opciones
       Cuando Selecciono el submenu visitados
       Entonces se muestra la pantalla visitados
       Cuando Busco un cliente por NUD
       Y Selecciono el submenu "VENTA"
       Y Confirmo el mensaje emergente
       Entonces se muestra la pantalla opciones de factura_1500
       Y valido que se muestren los productos de la venta anterior
       Y selecciono el boton de cambiar pedido
       Cuando acepto la popup
       Entonces se muestra la auditoria de enfriadores
       Y selecciono el boton siguiente de la pagina de enfriadores
       Cuando acepto la popup
       Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=7_3"
       Entonces Se muestra el pedido del dia
       Y seleccionamos el boton de no venta
       Entonces se muestra la pantalla_6000 Razones de No Venta
       Cuando Selecciono el departamento
       Y selecciono el motivo de rechazo
       Y Seleccionamos siguiente
       Entonces valido estar en la pantalla indicadores de cliente
       Cuando Seleccionamos siguiente
       Y rechazo la pop up
       Entonces se muestra la pantalla visitados
       #REVISITA 4 RP+Otras Monedas
       Y selecciono el menu mas opciones
       Cuando Selecciono el submenu visitados
       Entonces se muestra la pantalla visitados
       Cuando Busco un cliente por NUD
       Y Selecciono el submenu "VENTA"
       Y Confirmo el mensaje emergente
       Entonces se muestra la pantalla opciones de factura_1500
       Y valido que se muestren los productos de la venta anterior
       Y selecciono el boton de cambiar pedido
       Cuando acepto la popup
       Entonces se muestra la auditoria de enfriadores
       Y selecciono el boton siguiente de la pagina de enfriadores
       Cuando acepto la popup
       Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=7_4"
       Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 7-R-4"
       Entonces disminuyo productos para rechazo parcial
       Entonces seleccionamos siguiente en la pantalla pedido del dia
       Entonces valido estar en la pantalla de descuentos
       Y calculo descuento del CSV
       Y seleccionamos siguiente en la pantalla descuentos
       Entonces Se muestra la pantalla de Devolucion Retornables
       Y Seleccionamos siguiente en la pantalla devolucion de retornables
       Y valido datos de cobro de GRB y garrafon
       Cuando acepto el mensaje de cobro de envase GRB
       Entonces valido pagos y si existen los liquido
       Entonces se muestra la pantalla cantidad de cupones
       Y liquido el total con cupones
       Cuando Seleccionamos siguiente
       Entonces Se muestra la pantalla movimientos del dia
       Cuando modifico valores hash
       Y valido que el resumen de cuenta sea correcto
       Y Seleccionamos siguiente en la pantalla movimientos del dia
       Entonces acepto la popup
       #REVISITA 5 RT
       Y selecciono el menu mas opciones
       Cuando Selecciono el submenu visitados
       Entonces se muestra la pantalla visitados
       Cuando Busco un cliente por NUD
       Y Selecciono el submenu "VENTA"
       Y Confirmo el mensaje emergente
       Entonces se muestra la pantalla opciones de factura_1500
       Y valido que se muestren los productos de la venta anterior
       Y selecciono el boton de cambiar pedido
       Cuando acepto la popup
       Entonces se muestra la auditoria de enfriadores
       Y selecciono el boton siguiente de la pagina de enfriadores
       Cuando acepto la popup
       Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=7_5"
       Entonces Se muestra el pedido del dia
       Y seleccionamos el boton de no venta
       Entonces se muestra la pantalla_6000 Razones de No Venta
       Cuando Selecciono el departamento
       Y selecciono el motivo de rechazo
       Y Seleccionamos siguiente
       Entonces valido estar en la pantalla indicadores de cliente
       Cuando Seleccionamos siguiente
       Y rechazo la pop up
       Entonces se muestra la pantalla visitados


  @regresion @Revisita_grb2 @CP014_Revisita
 #VISITA 1 Efectivo+Cheque+CréditoRevolvente
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=14"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 14-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces se muestra la pantalla cantidad de cheques
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa1
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa2
    Y ingresamos los datos de cheque para pago parcial con cheque y otras monedas empresa3
    Y Seleccionamos siguiente en la pagina cantidad de cheques
    Entonces  se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Y valido que el resumen de cuenta sea correcto con cr
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
      #REVISITAS 2 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=14_2"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados
     #REVISITA 3 V+Crédito Revolvente+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=14_3"
    Y busco productos y agrego productos"revisita"
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Cuando acepto la popup
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido parcialmente el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Entonces  se muestra la pantalla cantidad de cupones
    Y busco la moneda "primer moneda"
    Y busco la moneda "segunda moneda"
    Y busco la moneda "tercer moneda"
    Y Seleccionamos siguiente en la pantalla cupones
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto con cr
    Entonces Seleccionamos siguiente en la pantalla movimientos del dia
    Y acepto la popup
    #REVISITA 4 RP+Efectivo+Otras Monedas
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=14_4"
    Entonces disminuyo productos para rechazo parcial
    Entonces seleccionamos siguiente en la pantalla pedido del dia
    Entonces valido estar en la pantalla de descuentos
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
    Y valido datos de cobro de garrafon
    Cuando acepto el mensaje de cobro de envase Garrafon
    Entonces valido pagos y si existen los liquido
    Entonces se muestra la pantalla cantidad de cupones
    Y liquido el total con cupones
    Cuando Seleccionamos siguiente
    Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
    Y valido que el resumen de cuenta sea correcto
    Y Seleccionamos siguiente en la pantalla movimientos del dia
    Entonces acepto la popup
    #REVISITA 5 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=14_5"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados


  @regresion @Revisita_grb2 @CP019_Revisita
 #VISITA 1 Transferencia+Crédito Revolvente
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=19"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 19-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
      Y valido datos de cobro de GRB
      Cuando acepto el mensaje de cobro de envase GRB
      Entonces valido pagos y si existen los liquido
      Entonces valido estar en la pantalla de credito revolvente
      Y verifico que el importe sea correcto
      Cuando liquido parcialmente el monto de credito revolvente
      Y Selecciono el motivo de credito revolvente
      Entonces doy click en siguiente de la pantalla cr
      Entonces se muestra la pantalla cantidad de cheques
      Y ingresamos los datos de cheque para pago total con cheque
      Y Seleccionamos siguiente en la pagina cantidad de cheques
      Entonces Se muestra la pantalla movimientos del dia
      Y valido que el resumen de cuenta sea correcto con cr
      Y Seleccionamos siguiente en la pantalla movimientos del dia
      Entonces acepto la popup
      #REVISITA 2 V+Cheque
      Y selecciono el menu mas opciones
      Cuando Selecciono el submenu visitados
      Entonces se muestra la pantalla visitados
      Cuando Busco un cliente por NUD
      Y Selecciono el submenu "VENTA"
      Y Confirmo el mensaje emergente
      Entonces se muestra la pantalla opciones de factura_1500
      Y valido que se muestren los productos de la venta anterior
      Y selecciono el boton de cambiar pedido
      Cuando acepto la popup
      Entonces se muestra la auditoria de enfriadores
      Y selecciono el boton siguiente de la pagina de enfriadores
      Cuando acepto la popup
      Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=19_2"
      Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 19-R-2"
      Y busco productos y agrego productos"revisita"
      Entonces seleccionamos siguiente en la pantalla pedido del dia
      Cuando acepto la popup
      Entonces valido estar en la pantalla de descuentos
      Y calculo descuento del CSV
      Y seleccionamos siguiente en la pantalla descuentos
      Entonces Se muestra la pantalla de Devolucion Retornables
      Y Seleccionamos siguiente en la pantalla devolucion de retornables
      Y valido datos de cobro de GRB
      Cuando acepto el mensaje de cobro de envase GRB
      Entonces valido pagos y si existen los liquido
      Entonces se muestra la pantalla cantidad de cheques
      Y ingresamos los datos de cheque para pago total con cheque
      Y Seleccionamos siguiente en la pagina cantidad de cheques
      Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
      Y valido que el resumen de cuenta sea correcto
      Y Seleccionamos siguiente en la pantalla movimientos del dia
      Entonces acepto la popup
      #REVISITA 3 RP+Efectivo
      Y selecciono el menu mas opciones
      Cuando Selecciono el submenu visitados
      Entonces se muestra la pantalla visitados
      Cuando Busco un cliente por NUD
      Y Selecciono el submenu "VENTA"
      Y Confirmo el mensaje emergente
      Entonces se muestra la pantalla opciones de factura_1500
      Y valido que se muestren los productos de la venta anterior
      Y selecciono el boton de cambiar pedido
      Cuando acepto la popup
      Entonces se muestra la auditoria de enfriadores
      Y selecciono el boton siguiente de la pagina de enfriadores
      Cuando acepto la popup
      Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=19_3"
      Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 19-R-3"
      Entonces disminuyo productos para rechazo parcial
      Entonces seleccionamos siguiente en la pantalla pedido del dia
      Entonces valido estar en la pantalla de descuentos
      Y calculo descuento del CSV
      Y seleccionamos siguiente en la pantalla descuentos
      Entonces Se muestra la pantalla de Devolucion Retornables
      Y Seleccionamos siguiente en la pantalla devolucion de retornables
      Y valido datos de cobro de GRB
      Cuando acepto el mensaje de cobro de envase GRB
      Entonces valido pagos y si existen los liquido
      Entonces se muestra la pantalla cantidad de cupones
      Y Seleccionamos siguiente
      Entonces Se muestra la pantalla movimientos del dia
    Cuando modifico valores hash
      Y valido que el resumen de cuenta sea correcto
      Y Seleccionamos siguiente
      Entonces acepto la popup
      #REVISITA 4 RT
      Y selecciono el menu mas opciones
      Cuando Selecciono el submenu visitados
      Entonces se muestra la pantalla visitados
      Cuando Busco un cliente por NUD
      Y Selecciono el submenu "VENTA"
      Y Confirmo el mensaje emergente
      Entonces se muestra la pantalla opciones de factura_1500
      Y valido que se muestren los productos de la venta anterior
      Y selecciono el boton de cambiar pedido
      Cuando acepto la popup
      Entonces se muestra la auditoria de enfriadores
      Y selecciono el boton siguiente de la pagina de enfriadores
      Cuando acepto la popup
      Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=19_4"
      Entonces Se muestra el pedido del dia
      Y seleccionamos el boton de no venta
    Entonces se muestra la pantalla_6000 Razones de No Venta
    Cuando Selecciono el departamento
    Y selecciono el motivo de rechazo
    Y Seleccionamos siguiente
    Entonces valido estar en la pantalla indicadores de cliente
    Cuando Seleccionamos siguiente
    Y rechazo la pop up
    Entonces se muestra la pantalla visitados
      #REVISITA 5 V+Efectivo+Cheque+Crédito Revolvente+Otras Monedas
      Y selecciono el menu mas opciones
      Cuando Selecciono el submenu visitados
      Entonces se muestra la pantalla visitados
      Cuando Busco un cliente por NUD
      Y Selecciono el submenu "VENTA"
      Y Confirmo el mensaje emergente
      Entonces se muestra la pantalla opciones de factura_1500
      Y valido que se muestren los productos de la venta anterior
      Y selecciono el boton de cambiar pedido
      Cuando acepto la popup
      Entonces se muestra la auditoria de enfriadores
      Y selecciono el boton siguiente de la pagina de enfriadores
      Cuando acepto la popup
      Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=19_5"
      Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 19-R-5"
      Y busco productos y agrego productos"revisita"
      Entonces seleccionamos siguiente en la pantalla pedido del dia
      Cuando acepto la popup
      Entonces valido estar en la pantalla de descuentos
      Y calculo descuento del CSV
      Y seleccionamos siguiente en la pantalla descuentos
      Entonces Se muestra la pantalla de Devolucion Retornables
      Y Seleccionamos siguiente en la pantalla devolucion de retornables
      Y valido datos de cobro de GRB
      Cuando acepto el mensaje de cobro de envase GRB



       @regresion @Revisita_grb2 @CP017_Revisita
 #VISITA 1 Efectivo+Cheque+Crédito Revolvente+Otras Monedas
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=17"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 17-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos siguiente en la pantalla devolucion de retornables
      Y valido datos de cobro de GRB
      Cuando acepto el mensaje de cobro de envase GRB
         Entonces valido pagos y si existen los liquido
         Entonces valido estar en la pantalla de credito revolvente
         Y verifico que el importe sea correcto
         Y liquido parcialmente el monto de credito revolvente
         Y Selecciono el motivo de credito revolvente
         Entonces doy click en siguiente de la pantalla cr
         Entonces se muestra la pantalla cantidad de cheques
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
         Y Seleccionamos siguiente en la pagina cantidad de cheques
         Entonces se muestra la pantalla cantidad de cupones
         Y busco la moneda "primer moneda"
         Y busco la moneda "segunda moneda"
         Y busco la moneda "tercer moneda"
         Y Seleccionamos siguiente en la pantalla cupones
         Entonces Se muestra la pantalla movimientos del dia
         Y valido que el resumen de cuenta sea correcto con cr
         Y Seleccionamos siguiente en la pantalla movimientos del dia
         Entonces acepto la popup
         #REVISITA 2 RP+Crédito Revolvente+Otras Monedas
         Y selecciono el menu mas opciones
         Cuando Selecciono el submenu visitados
         Entonces se muestra la pantalla visitados
         Cuando Busco un cliente por NUD
         Y Selecciono el submenu "VENTA"
         Y Confirmo el mensaje emergente
         Entonces se muestra la pantalla opciones de factura_1500
         Y valido que se muestren los productos de la venta anterior
         Y selecciono el boton de cambiar pedido
         Cuando acepto la popup
         Entonces se muestra la auditoria de enfriadores
         Y selecciono el boton siguiente de la pagina de enfriadores
         Cuando acepto la popup
         Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=17_2"
         Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 17-R-2"
         Entonces disminuyo productos para rechazo parcial
         Entonces seleccionamos siguiente en la pantalla pedido del dia
         Entonces valido estar en la pantalla de descuentos
         Y calculo descuento del CSV
         Y seleccionamos siguiente en la pantalla descuentos
         Entonces Se muestra la pantalla de Devolucion Retornables
         Y Seleccionamos siguiente en la pantalla devolucion de retornables
         Y valido datos de cobro de GRB
         Cuando acepto el mensaje de cobro de envase GRB
         Entonces valido pagos y si existen los liquido
         Entonces valido estar en la pantalla de credito revolvente
         Y verifico que el importe sea correcto
         Cuando liquido parcialmente el monto de credito revolvente
         Y Selecciono el motivo de credito revolvente
         Entonces doy click en siguiente de la pantalla cr
         Entonces  se muestra la pantalla cantidad de cupones
         Y busco la moneda "primer moneda"
         Y busco la moneda "segunda moneda"
         Y busco la moneda "tercer moneda"
         Y Seleccionamos siguiente en la pantalla cupones
         Entonces Se muestra la pantalla movimientos del dia
         Cuando modifico valores hash
         Y valido que el resumen de cuenta sea correcto con cr
         Entonces Seleccionamos siguiente en la pantalla movimientos del dia
         Y acepto la popup
         #REVISITA 3 V+Cheque+Crédito Revolvente+Otras Monedas
         Y selecciono el menu mas opciones
         Cuando Selecciono el submenu visitados
         Entonces se muestra la pantalla visitados
         Cuando Busco un cliente por NUD
         Y Selecciono el submenu "VENTA"
         Y Confirmo el mensaje emergente
         Entonces se muestra la pantalla opciones de factura_1500
         Y valido que se muestren los productos de la venta anterior
         Y selecciono el boton de cambiar pedido
         Cuando acepto la popup
         Entonces se muestra la auditoria de enfriadores
         Y selecciono el boton siguiente de la pagina de enfriadores
         Cuando acepto la popup
         Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=17_3"
         Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 17-R-3"
         Y busco productos y agrego productos"revisita"
         Entonces seleccionamos siguiente en la pantalla pedido del dia
         Cuando acepto la popup
         Entonces valido estar en la pantalla de descuentos
         Y seleccionamos siguiente en la pantalla descuentos
         Entonces Se muestra la pantalla de Devolucion Retornables
         Y Seleccionamos siguiente en la pantalla devolucion de retornables
         Y valido datos de cobro de GRB
         Cuando acepto el mensaje de cobro de envase GRB
         Entonces valido pagos y si existen los liquido
         Entonces valido estar en la pantalla de credito revolvente
         Y verifico que el importe sea correcto
         Y liquido parcialmente el monto de credito revolvente
         Y Selecciono el motivo de credito revolvente
         Entonces doy click en siguiente de la pantalla cr
         Entonces se muestra la pantalla cantidad de cheques
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
         Y Seleccionamos siguiente en la pagina cantidad de cheques
         Entonces se muestra la pantalla cantidad de cupones
         Y busco la moneda "primer moneda"
         Y busco la moneda "segunda moneda"
         Y busco la moneda "tercer moneda"
         Y Seleccionamos siguiente en la pantalla cupones
         Entonces Se muestra la pantalla movimientos del dia
         Cuando modifico valores hash
         Y valido que el resumen de cuenta sea correcto con cr
         Y Seleccionamos siguiente en la pantalla movimientos del dia
         Entonces acepto la popup
         #REVISITA 4 RT
         Y selecciono el menu mas opciones
         Cuando Selecciono el submenu visitados
         Entonces se muestra la pantalla visitados
         Cuando Busco un cliente por NUD
         Y Selecciono el submenu "VENTA"
         Y Confirmo el mensaje emergente
         Entonces se muestra la pantalla opciones de factura_1500
         Y valido que se muestren los productos de la venta anterior
         Y selecciono el boton de cambiar pedido
         Cuando acepto la popup
         Entonces se muestra la auditoria de enfriadores
         Y selecciono el boton siguiente de la pagina de enfriadores
         Cuando acepto la popup
         Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=17_4"
         Entonces Se muestra el pedido del dia
         Y seleccionamos el boton de no venta
         Entonces se muestra la pantalla_6000 Razones de No Venta
         Cuando Selecciono el departamento
         Y selecciono el motivo de rechazo
         Y Seleccionamos siguiente
         Entonces valido estar en la pantalla indicadores de cliente
         Cuando Seleccionamos siguiente
         Y rechazo la pop up
         Entonces se muestra la pantalla visitados
         #REVISITA 5 V+Efectivo+Cheque+Crédito Revolvente+Otras Monedas
         Y selecciono el menu mas opciones
         Cuando Selecciono el submenu visitados
         Entonces se muestra la pantalla visitados
         Cuando Busco un cliente por NUD
         Y Selecciono el submenu "VENTA"
         Y Confirmo el mensaje emergente
         Entonces se muestra la pantalla opciones de factura_1500
         Y valido que se muestren los productos de la venta anterior
         Y selecciono el boton de cambiar pedido
         Cuando acepto la popup
         Entonces se muestra la auditoria de enfriadores
         Y selecciono el boton siguiente de la pagina de enfriadores
         Cuando acepto la popup
         Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=17_5"
         Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 17-R-5"
         Y busco productos y agrego productos"revisita"
         Entonces seleccionamos siguiente en la pantalla pedido del dia
         Cuando acepto la popup
         Entonces valido estar en la pantalla de descuentos
         Y seleccionamos siguiente en la pantalla descuentos
         Entonces Se muestra la pantalla de Devolucion Retornables
         Y Seleccionamos siguiente en la pantalla devolucion de retornables
         Y valido datos de cobro de GRB
         Cuando acepto el mensaje de cobro de envase GRB
         Entonces valido pagos y si existen los liquido
         Entonces valido estar en la pantalla de credito revolvente
         Y verifico que el importe sea correcto
         Y liquido parcialmente el monto de credito revolvente
         Y Selecciono el motivo de credito revolvente
         Entonces doy click en siguiente de la pantalla cr
         Entonces se muestra la pantalla cantidad de cheques
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa1
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa2
         Y ingresamos los datos de cheque para pago parcial con cheque otras monedas y efectivo empresa3
         Y Seleccionamos siguiente en la pagina cantidad de cheques
         Entonces se muestra la pantalla cantidad de cupones
         Y busco la moneda "primer moneda"
         Y busco la moneda "segunda moneda"
         Y busco la moneda "tercer moneda"
         Y Seleccionamos siguiente en la pantalla cupones
         Entonces Se muestra la pantalla movimientos del dia
         Cuando modifico valores hash
         Y valido que el resumen de cuenta sea correcto con cr
         Y Seleccionamos siguiente en la pantalla movimientos del dia
         Entonces acepto la popup



   @regresion @Revisita_grb2 @CP038_Revisita
 #VISITA 1 Crédito Revolvente
  Escenario: Descarga de inventario
    Dado abro la aplicacion
    Entonces valido estar en la pantalla de Login
    Entonces valido atributos
    Cuando Obtengo los datos de prueba desde "dt_revisitas" data pool con filtros "id=38"
    Cuando Inicio sesion como usuario "valido"
    Y press_enter_button
    Y Seleccionamos siguiente
    Cuando Selecciono menu "Servicio Cliente"
    Entonces Se muestra la pantalla programados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Entonces acepto la popup
    Entonces Se muestra el pedido del dia
    Y busco productos y agrego productos"primer pet"
    Y busco productos y agrego productos"segundo pet"
    Y busco productos y agrego productos"primer grb"
    Y busco productos y agrego productos"segundo grb"
    Y busco productos y agrego productos"primer garrafon"
    Y busco productos y agrego productos"segundo garrafon"
    Y busco productos y agrego productos"primer no carbonatado"
    Y busco productos y agrego productos"segundo no carbonatado"
    Y busco productos y agrego productos"primer agua embotellada"
    Y busco productos y agrego productos"segunda agua embotellada"
    Y busco un producto no vendible
    Cuando Obtengo los segundos datos de prueba desde "dt_combos" data pool con filtros  "id= 38-R-1"
    Y busco tipo de combo
    Y obtenemos el importe del pedido
    Cuando seleccionamos siguiente en la pantalla pedido del dia
    Cuando valido estar en la pantalla de descuentos
    Y calculo descuento del CSV
    Y seleccionamos siguiente en la pantalla descuentos
    Entonces Se muestra la pantalla de Devolucion Retornables
    Y Seleccionamos prestamo de caja
    Y buscar cajilla para prestamo
    Y Seleccionamos siguiente
    Y valido datos de cobro de cajilla
    Cuando acepto el mensaje de cobro de cajilla
    Entonces valido pagos y si existen los liquido
    Entonces valido estar en la pantalla de credito revolvente
    Y verifico que el importe sea correcto
    Cuando liquido el monto de credito revolvente
    Y Selecciono el motivo de credito revolvente
    Entonces doy click en siguiente de la pantalla cr
    Y acepto la popup
    #REVISITA 2 RT
    Y selecciono el menu mas opciones
    Cuando Selecciono el submenu visitados
    Entonces se muestra la pantalla visitados
    Cuando Busco un cliente por NUD
    Y Selecciono el submenu "VENTA"
    Y Confirmo el mensaje emergente
    Entonces se muestra la pantalla opciones de factura_1500
    Y valido que se muestren los productos de la venta anterior
    Y selecciono el boton de cambiar pedido
    Cuando acepto la popup
    Entonces se muestra la auditoria de enfriadores
    Y selecciono el boton siguiente de la pagina de enfriadores
    Cuando acepto la popup
    Cuando Obtengo los segundos datos de prueba para revisitas desde "dt_revisitas" data pool con filtros "id=38-2"
    Entonces Se muestra el pedido del dia
    Y seleccionamos el boton de no venta
     Entonces se muestra la pantalla_6000 Razones de No Venta
     Cuando Selecciono el departamento
     Y selecciono el motivo de rechazo
     Y Seleccionamos siguiente
     Entonces valido estar en la pantalla indicadores de cliente
     Cuando Seleccionamos siguiente
     Y rechazo la pop up
     Entonces se muestra la pantalla visitados


