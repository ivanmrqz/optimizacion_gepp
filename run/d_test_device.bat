@ECHO OFF

REM SET VARS PATH PROYECT
set unidad_disco=C:
set cucumber_proyect_run="C:\ambiente\automation\geep_android\run"
set jenkins_home="C:\ambiente"
set path_proyect="geep_android"
set app_so=android
set app="gepp.apk"
set device_name="d_test_device"
set device="19135B57F9"
set cucumber_profile="android"
set feature="04_efectivo/02_efectivo_grb_0.feature"
set cucumber_tags="\"@CP086\""
set file_name=""
%unidad_disco%

REM -----------------------------------------------------------------------------------------------------
REM UNINSTALL/REINSTALL GEMS Y SW
REM -----------------------------------------------------------------------------------------------------
REM gem uninstall rubyzip -a -I -x
REM gem install rubyzip -v '1.2.1'

cd %cucumber_proyect_run%

sh run_tests.sh --app_so=%app_so% --app=%app% --device_name=%device_name% --device=%device% --cucumber_profile=%cucumber_profile% --feature=%feature% --file_name=%file_name% --path_proyect=%path_proyect% --jenkins_home=%jenkins_home% --cucumber_tags=%cucumber_tags%

PAUSE

exit
