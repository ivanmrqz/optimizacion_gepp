#!/bin/bash

# PARAMETROS
# -jenkins_home    =>  JENKINS
# -path_proyect    =>  PATH_PROYECT
# -feature    =>  FEATURE OR FEATURE:SCENARIO
# -file_name    =>  PARAMETRO ADICIONAL PARA NOMBRE DEL JSON Y RERUN

# SH PARAMETROS
# --app = apk_file.apk
# --driver = chrome/phantom/selenium/safari
# --browser = navegador => inyecta el user agent, util para dispositivos moviles, debe estar declarado en ../config/csv_data/devices.csv
# --device = dispositivo, debe estar declarado en ../config/csv_data/devices.csv
# --cucumber_profile = nombre del profile de cucumber.yml
# --feature = opcional, si se decea ejecutar 1 solo feature o algunos escenarios, usar sintaxis de cucumber ej. feature.feature:5:10
# --file_name = opcional, si se desea pasar 1 nombre adicional para la creacion de reportes html, json, etc
# --path_proyect = path relativo donde esta el proyeto de jenkins ej. cm_web_chrome_suscrito
# --jenkins_home = path donde se encuentra jenkins_home
# --cucumber_tags = tags que se ejecutaran por cucumber

# EJEMPLO DE EJECUCION
# sh run_tests.sh --app='app.apk' --driver='chrome' --browser='chrome' --device='desktop' --cucumber_profile='lv_movile' --feature='' --file_name='' --path_proyect='' --jenkins_home=''


# OBTIENE LOS PARAMETROS SETEADOS AL SH
while [ $# -gt 0 ]; do
  case "$1" in
    --jenkins_home=*)
      jenkins_home="${1#*=}"
      #jenkins_home="${jenkins_home,,}"
      ;;
    --path_proyect=*)
      path_proyect="${1#*=}"
      #path_proyect="${path_proyect,,}"
      ;;
    --feature=*)
      feature="${1#*=}"
      ;;
    --file_name=*)
      file_name="${1#*=}"
      #file_name="${file_name,,}"
      ;;
    --app=*)
      app="${1#*=}"
      #environment="${environment,,}"
      ;;
    --device_name=*)
      device_name="${1#*=}"
      #browser="${browser,,}"
      ;;
    --device=*)
      device="${1#*=}"
      #device="${device,,}"
      ;;
    --country=*)
      country="${1#*=}"
      #country="${country,,}"
      ;;
    --user_type=*)
      user_type="${1#*=}"
      #user_type="${user_type,,}"
      ;;
    --cucumber_profile=*)
      cucumber_profile="${1#*=}"
      ;;
    --app_so=*)
      app_so="${1#*=}"
      ;;
    --cucumber_tags=*)
      cucumber_tags="${1#*=}"
      ;;
    --winium_host=*)
      winium_host="${1#*=}"
      ;;
    --winium_port=*)
      winium_port="${1#*=}"
      ;;
    --path_jenkins_proyect=*)
      path_jenkins_proyect="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done

echo "********************************************************"
#printf "Argument country   =>   %s\n" "$country"
#printf "Argument user   =>   %s\n" "$user_type"
printf "Argument app_so   =>   %s\n" "$app_so"
printf "Argument device_name   =>   %s\n" "$device_name"
printf "Argument device   =>   %s\n" "$device"
printf "Argument app   =>   %s\n" "$app"
printf "Argument cucumber_profile   =>   %s\n" "$cucumber_profile"
printf "Argument feature   =>   %s\n" "$feature"
printf "Argument file_name   =>   %s\n" "$file_name"
printf "Argument jenkins_home   =>   %s\n" "$jenkins_home"
printf "Argument path_proyect   =>   %s\n" "$path_proyect"
printf "Argument cucumber_tags   =>   %s\n" "$cucumber_tags"
printf "Argument winium_host   =>   %s\n" "$winium_host"
printf "Argument winium_port   =>   %s\n" "$winium_port"
echo "********************************************************"

echo "********************************************************"
echo "SET DEVICE ID... DEVICE OVERRIDE..."
echo "********************************************************"
NUM=`expr $(adb devices | wc -l) - 1`
DEVICES_LIST=`adb devices | tail -$NUM | awk -F " " '{print $1}'`
echo DEVICES
for DEVICEID in $DEVICES_LIST; do \
  DEVICE_ID=$DEVICEID
  echo $DEVICEID
done
echo "DEVICE = $DEVICEID"
export DEVICE=$DEVICEID
echo "********************************************************"


# EXPORT VARS JENKINS & PATH PROYECT
export JENKINS=$jenkins_home
export PATH_PROYECT=$path_proyect
export PATH_JENKINS_PROYECT=$path_jenkins_proyect

# MOONT DISK
# net use X: %JENKINS%

echo "********************************************************"
echo "VARIABLES PARA RESULTADOS / REPORTES"
echo "********************************************************"
# EXPORT VARS PARA CONFIGURACIONES
export COUNTRY=$country
export USER_TYPE=$user_type
#export ENVIRONMENT=$environment
#export DRIVER=$driver
#export BROWSER=$browser
export APP_SO=$app_so
export DEVICE_NAME=$device_name
#export DEVICE=$device
export APP=$app
export FILE_NAME=$file_name
export CUCUMBER_TAGS=$cucumber_tags
export WINIUM_HOST=$winium_host
export WINIUM_PORT=$winium_port

# EXPORT VARS PARA EVICENCIA/REPORTES
export EXEC_DATE=$(date +"%y%m%d")
export EXEC_TIME=$(date +"%H%M")
export EXEC_TYPE=single
export PATH_RESULTS=evidence/"$EXEC_DATE"/"$EXEC_TIME"/"$APP_SO"_"$DEVICE_NAME"_"$DEVICE"
export PATH_RESULTS_SINGLE="$PATH_RESULTS"/single
export PATH_RESULTS_RERUN="$PATH_RESULTS"/rerun
export REPORT_NAME="$APP_SO"_"$DEVICE_NAME"_"$DEVICE"
export REPORT_NAME="results"
echo "********************************************************"


echo "********************************************************"
echo "CREAR DIRECTORIOS EVIDENCIAS / REPORTES..."
echo "********************************************************"
cd ../venture
echo mkdir -p "$PATH_RESULTS"
mkdir -p "$PATH_RESULTS"
echo mkdir -p "$PATH_RESULTS_SINGLE"
mkdir -p "$PATH_RESULTS_SINGLE"
echo mkdir -p "$PATH_RESULTS_RERUN"
mkdir -p "$PATH_RESULTS_RERUN"
echo "********************************************************"


echo "********************************************************"
echo "BUNDLE INSTALL..."
echo "********************************************************"
cd ../
echo "gem uninstall rubyzip -a -I -x "
gem uninstall rubyzip -a -I -x <<EOFGEM
EOFGEM
echo "gem install rubyzip -v 1.2.1"
gem install rubyzip -v "1.2.1" <<EOFGEM2
EOFGEM2
## bundle install
echo "********************************************************"

echo "********************************************************"
echo "CALABASH - FIRMAR APK..."
echo "********************************************************"
cd venture
echo "calabash-android resign $APP"
calabash-android resign "$APP" <<EOFFIRMAAPP
EOFFIRMAAPP
echo "calabash-android build $APP"
calabash-android build "$APP" <<EOFFIRMAAPP2
EOFFIRMAAPP2
echo "********************************************************"

echo "********************************************************"
echo "EJECUCION TEST SINGLE"
echo "********************************************************"
# cd venture
# EXECUTE TEST SINGLE
echo calabash-android run "$APP" ADB_DEVICE_ARG="$DEVICE" -p $cucumber_profile -t "$CUCUMBER_TAGS" features/$feature -f pretty -f html --out "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html -f pretty -f rerun --out "$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f json -o "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json
calabash-android run "$APP" SCREENSHOT_VIA_USB="false" ADB_DEVICE_ARG="$DEVICE" -p $cucumber_profile -t "$CUCUMBER_TAGS" features/$feature -f pretty -f html --out "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html -f pretty -f rerun --out "$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f json -o "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json <<EOF
EOF
echo "FIN EJECUCION SINGLE"
echo "********************************************************"


echo "********************************************************"
echo "CUSTOM REPORT"
echo "********************************************************"
echo ""$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html"
cd ..
cd helpers
ruby custom_report.rb ""$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html" <<EOF_CUSTOMREPORT
EOF_CUSTOMREPORT
echo "********************************************************"


echo "********************************************************"
echo "EJECUCION TEST RERUN"
echo "********************************************************"
# EXECUTE TEST RERUN
cd ..
cd venture
export EXEC_TYPE=rerun
echo calabash-android run "$APP" ADB_DEVICE_ARG="$DEVICE" @"$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f html --out "$PATH_RESULTS_RERUN"/"$REPORT_NAME".html -f pretty -f json -o "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json
calabash-android run "$APP" SCREENSHOT_VIA_USB="false" ADB_DEVICE_ARG="$DEVICE" @"$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f html --out "$PATH_RESULTS_RERUN"/"$REPORT_NAME".html -f pretty -f json -o "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json <<EOF2
EOF2
echo "FIN EJECUCION RERUN"
echo "********************************************************"


echo "********************************************************"
echo "CUSTOM REPORT"
echo "********************************************************"
echo ""$PATH_RESULTS_RERUN"/"$REPORT_NAME".html"
cd ..
cd helpers
ruby custom_report.rb ""$PATH_RESULTS_RERUN"/"$REPORT_NAME".html" <<EOF_CUSTOMREPORT2
EOF_CUSTOMREPORT2
echo "********************************************************"


echo "********************************************************"
echo "MERGE JSON SINGLE => JSON RERUN"
echo "********************************************************"
cd ../helpers
ruby cucumber_json.rb "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json "$PATH_RESULTS"/"$REPORT_NAME".json <<EOF3
EOF3
echo "FIN MERGE JSON SINGLE => JSON RERUN"
echo "********************************************************"

echo "********************************************************"
echo "COPIANDO CUCUMBER JSON => WORKSPACE"
echo "********************************************************"
cd ../venture
echo cp -f "$PATH_RESULTS"/"$REPORT_NAME".json \""$JENKINS"/"$PATH_JENKINS_PROYECT"/"$REPORT_NAME".json\"
cp -f "$PATH_RESULTS"/"$REPORT_NAME".json "$JENKINS"/"$PATH_JENKINS_PROYECT"/"$REPORT_NAME".json
echo "FIN COPIANDO CUCUMBER JSON => WORKSPACE"
echo "********************************************************"


echo "********************************************************"
echo "FIN EJECUCION..............."
echo "............................"
echo "********************************************************"

