#!/bin/bash

# PARAMETROS
# -is_emulator = 1 si es emulador, 0/otro valor si no lo es
# -avd_name = nombre de la avd creada en avd manager de sdk android
# -port = puerto en el cual se inicia la avd manager


# OBTIENE LOS PARAMETROS SETEADOS AL SH
while [ $# -gt 0 ]; do
  case "$1" in
    --is_emulator=*)
      is_emulator="${1#*=}"
      ;;
    --avd_name=*)
      avd_name="${1#*=}"
      ;;
    --port=*)
      port="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done


echo "********************************************************"
printf "Argument is_emulator   =>   %s\n" "$is_emulator"
printf "Argument avd_name   =>   %s\n" "$avd_name"
printf "Argument port   =>   %s\n" "$port"
echo "********************************************************"

# EXPORT VARS JENKINS & PATH PROYECT
export AVD_NAME=$avd_name
export PORT=$port

echo "********************************************************"
printf "DEVICE AVD EMULATOR"
echo "********************************************************"

echo "********************************************************"
printf "RUN EMULATOR:"
printf "********************************************************"
echo emulator -avd "$AVD_NAME" -port "$PORT"
emulator -avd "$AVD_NAME" -port "$PORT"
echo "********************************************************"

# PAUSA PARA CONSOLA DE UNIX, HASTA QUE SE PRESIONE TECLA
# read -p "Press enter to continue"
