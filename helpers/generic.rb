require 'fileutils'
require_relative 'config.rb'


##################################################################################
##################################################################################
##################################################################################
### UTILERIAS
##################################################################################
##################################################################################
##################################################################################


#Método que obtiene un numero unico apartir del timestamps
def timestamps
  (Time.now.to_f * 1000).to_i
end

#Método que obtiene la fecha en formato HORA,MINUTO,SEGUNDO
def datetime
  Time.now.strftime('%H%M%S').to_s
end

#Método que genera un email de forma random, apartir de la fecha
def email_random
  return "test.#{timestamps}@test.com"
end

#Método que obtiene el nombre de los meses en español
# @params
# * :month_number numero del mes va del 1 - 10
# @return
# * :String nombre del mes
def obtener_mes_nombre_es(month_number)
  month_names = [nil, 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Novimebre', 'Diciembre']
  return month_names[month_number]
end

#Método que obtiene el nombre abreviado de los meses en español
# @params
# * :month_number numero del mes va del 1 - 10
# @return
# * :String nombre abreviado del mes
def obtener_mes_abbr_es(month_number)
  abbr_month_names = [nil, 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  return abbr_month_names[month_number]
end


#Método que obtiene el nombre del sistema operativo
# @return
# * :String nombre Sistema Operativo
def os
  require 'rbconfig'

  @os ||= (
  host_os = RbConfig::CONFIG['host_os']
  case host_os
    when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
      :windows
    when /darwin|mac os/
      :macosx
    when /linux/
      :linux
    when /solaris|bsd/
      :unix
    else
      raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
  end
  )
end


##################################################################################
##################################################################################
##################################################################################
### EVIDENCIA
##################################################################################
##################################################################################
##################################################################################

#Método para guardar la evidencia, al finalizar el scenario, esto permite obtener si el scenario es fail o pass
# @params
# * :scenario datos del escenario, se obtiene desde los hooks
def save_evidence(scenario)
  begin
    feature_name = special_chars_replace(scenario.feature.name)
    scenario_name = scenario_name_remove_tc(scenario.name)
    scenario_name = special_chars_replace(scenario_name)
    device = special_chars_replace(Config.device_name.to_s.downcase.strip)

    #evidence_dir = "#{$evidence_path.to_s.downcase}/#{$device_name.to_s.downcase}/#{feature_name}/#{scenario_name}/"
    evidence_dir = "#{Config.path_results}/screenshots/#{device}/#{feature_name}/#{scenario_name.to_s[0..20].strip}/"
    evidence_name = "#{datetime}_#{scenario.failed? ? 'Fail' : 'Pass'}.png"

    #evidence_dir = "evidence/#{Time.now.strftime('%F')}/#{ENV['CUCUMBER_MOBILE_EXECUTION']}"
    FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir
    #screenshot_embed({:prefix=>evidence_dir, :name=>"#{timestamp}_#{scenario.feature.name}_#{scenario.name}_#{scenario.failed? ? 'Fail' : 'Pass'}.png"})
    screenshot_embed({:prefix=>evidence_dir, :name=>"#{evidence_name}"})

  rescue Exception => e
    puts "Fail => screenshot => #{e.message}"
  end

end


#Método para guardar la evidencia de ejecucion al momento de invocar este metodo, en este punto toda la evidencia es pass
def save_evidence_execution
  begin
    feature_name = special_chars_replace(Config.current_feature.to_s.strip)
    scenario_name = scenario_name_remove_tc(Config.current_scenario.to_s.strip)
    scenario_name = special_chars_replace(scenario_name)
    device = special_chars_replace(Config.device_name.to_s.downcase.strip)

    evidence_dir = "#{Config.path_results}/screenshots/#{device}/#{feature_name}/#{scenario_name.to_s[0..20].strip}/"
    evidence_name = "#{datetime}_Pass.png"
    #puts "evidence_dir => #{evidence_dir}"
    #puts "evidence_name => #{evidence_name}"

    #evidence_dir = "evidence/#{Time.now.strftime('%F')}/#{ENV['CUCUMBER_MOBILE_EXECUTION']}"
    FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir
    #screenshot_embed({:prefix=>evidence_dir, :name=>"#{timestamp}_evidence.png"})
    screenshot_embed({:prefix=>evidence_dir, :name=>"#{evidence_name}"})

  rescue Exception => e
    puts "Fail => screenshot => #{e.message}"

  end

end


#Método para guardar la evidencia de ejecucion al momento de invocar este metodo, en este punto toda la evidencia es pass
def save_evidence_execution_fail
  begin
    feature_name = special_chars_replace(Config.current_feature.to_s.strip)
    scenario_name = scenario_name_remove_tc(Config.current_scenario.to_s.strip)
    scenario_name = special_chars_replace(scenario_name)
    device = special_chars_replace(Config.device_name.to_s.downcase.strip)

    evidence_dir = "#{Config.path_results}/screenshots/#{device}/#{feature_name}/#{scenario_name.to_s[0..20].strip}/"
    evidence_name = "#{datetime}_Fail.png"

    FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir
    screenshot_embed({:prefix=>evidence_dir, :name=>"#{evidence_name}"})

  rescue Exception => e
    puts "Fail => screenshot => #{e.message}"

  end
end


#Método para obtener el nombre del scenario, esto permite extraerlo y pasarlo al nombre de la evidencia
#   se quita del nombre todo a la izquierda de "(TC"
# @params
# * :scenario nombre del escenario
# @return
# * :String nombre del escenario
def scenario_name_remove_tc(scenario)
  a = scenario.to_s.split('(TC')
  return a[0].to_s.downcase.strip
end


#Método que quita caracteres especiales a un string
def special_chars_replace(string_value)
  string_value = string_value.to_s.tr(':', '')
  string_value = string_value.to_s.tr('-', '')
  string_value = string_value.to_s.tr(',', '') # ,
  string_value = string_value.to_s.tr('.', '')
  string_value = string_value.to_s.tr('"', '')
  string_value = string_value.to_s.tr( ';', '' )
  string_value = string_value.to_s.tr( '(', '' )
  string_value = string_value.to_s.tr( ')', '' )
  string_value = string_value.to_s.tr( '{', '' )
  string_value = string_value.to_s.tr( '}', '' )
  string_value = string_value.to_s.tr( '[', '' )
  string_value = string_value.to_s.tr( ']', '' )
  string_value = string_value.to_s.tr( '!', '' )
  string_value = string_value.to_s.tr( '@', '' )
  string_value = string_value.to_s.tr( '#', '' )
  string_value = string_value.to_s.tr( '$', '' )
  string_value = string_value.to_s.tr( '%', '' )
  string_value = string_value.to_s.tr( '&', '' )
  string_value = string_value.to_s.tr( '/', '' )
  string_value = string_value.to_s.tr( '?', '' )
  string_value = string_value.to_s.tr( '¿', '' )
  string_value = string_value.to_s.tr( '=', '' )
  string_value = string_value.to_s.tr( '>', '' )
  string_value = string_value.to_s.tr( 'á', 'a' )
  string_value = string_value.to_s.tr( 'é', 'e' )
  string_value = string_value.to_s.tr( 'í', 'i' )
  string_value = string_value.to_s.tr( 'ó', 'o' )
  string_value = string_value.to_s.tr( 'ú', 'u' )
  string_value = string_value.to_s.tr( ' ', '_' )
  return string_value.to_s.downcase.strip
end





def is_iphone?
  iphone?
rescue Exception => e
  return false
end

def is_ipad?
  ipad?
rescue Exception => e
  return false
end
