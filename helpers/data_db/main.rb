require 'active_record'
require_relative './models/movie'
require 'pg'
require 'json'
require 'pp'

#conectar con YML 'cucumber', donde ser cargan configuraciones genericas de proyecto
def db_configuration
  db_configuration_file = File.join(File.expand_path('../../venture/config', __FILE__), '..', 'db', 'cucumber.yml')
  YAML.load(File.read(db_configuration_file))
end

#Parametros de conexion BD
@username = db_configuration['username']
@password = db_configuration['password']
@database = db_configuration['database']
@host = db_configuration['host']
@port = db_configuration['port']

#conexion BD
def connection
  begin
    #Conecion a BD
    con = PG.connect(@host, @port, '', '', @database, @username, @password)
    #puts con.server_version
    
  rescue Exception => e
    raise "connection => Error al conectar con DB => #{query}.\nException => #{e.message}"

  rescue Exception => ex
    raise "connection => Error al conectar con DB => #{query}.\nException => #{ex.message}"
 
  end
  
  return con
end


#Metodo para obtener el resultado del query
# @params
#   :query String query a ejecutar
# @return
#   HASHMAP con los datos del query
def select_query(query)
  begin
    hash = nil
    #Obtenemos el resultado de la consulta
    rs = connection.exec(query)
    #lo convertimos a array
    rs_hasmap = rs.to_a
    #de array a has map
    hash = Hash[rs_hasmap.collect { |item| [item, ""] } ]
    pp hash

  rescue PG::Error => e
    raise "select_query => Error al obtener la query => #{query}.\nException => #{e.message}"

  rescue Exception => ex
    raise "select_query => Error al obtener la query => #{query}.\nException => #{ex.message}"

  ensure
    connection.close if connection

  end

  return hash

end


def deep_find(query, &block)
  flat_map do |key, value|
    if key == connection.exec(query)
      yield value if block_given?
      [value]
    elsif value.is_a? Hash
      value.deep_find(query, &block)
    elsif value.is_a? Array
      value.select { |i| i.is_a? Hash }.flat_map { |h| h.deep_find(query, &block) }
    end
  end
end


