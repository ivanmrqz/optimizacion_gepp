#********************************************************
# CONEXION DB POSTGRESQL
#********************************************************

#require 'active_record'
#require_relative './models/movie'
require 'pg'
#require 'json'
#require 'pp'
require 'yaml'


#conectar con YML 'cucumber', donde ser cargan configuraciones genericas de proyecto
def db_configuration
  #db_configuration_file = File.join(File.expand_path('..', __FILE__), 'config.yml')
  db_configuration_file = File.join(File.expand_path('../../../venture/config', __FILE__), 'cucumber.yml')
  YAML.load(File.read(db_configuration_file))
end

#Parametros de conexion BD

#conexion BD
def connection
  begin
    #Conecion a BD
    #con = PG.connect("localhost", "5432", '', '', "PWA", "postgres", "root")
    con = PG.connect( :hostaddr => $db_host, :port => $db_port, :dbname => $db_database,
                      :user => $db_username, :password => $db_password,
                      :connect_timeout => $db_timeout
                    )
  rescue Exception => e
    raise "connection => Error al conectar con DB => \nException => #{e.message}"

  rescue Exception => ex
    raise "connection => Error al conectar con DB => \nException => #{ex.message}"

  end

  return con
end


#Metodo para obtener el resultado del query
# @params
#   :query String query a ejecutar
# @return
#   HASHMAP con los datos del query
def exec_query(query)
  begin
    #Obtenemos el resultado de la consulta
    cnn = connection
    rs = cnn.exec(query)
    #lo convertimos a array
    rs_a = Array.new
    rs_a = rs.to_a

  rescue PG::Error => e
    raise "select_query => Error al obtener la query => #{query}.\nException => #{e.message}"

  rescue Exception => ex
    raise "select_query => Error al obtener la query => #{query}.\nException => #{ex.message}"

  ensure
    cnn.close if cnn

  end

  return rs_a

end



