# encoding: utf-8

class PersistenciaDTO

  #############################################################################
  # ATRIBUTOS
  #############################################################################

  #-------------------------------------------------------
  # BOLSA
  #-------------------------------------------------------
  @bolsa_id_bolsa
  @bolsa_id_plp
  @bolsa_id_pdp
  @bolsa_nombre_sku
  @bolsa_sku
  @bolsa_raiting
  @bolsa_color
  @bolsa_talla
  @bolsa_material
  @bolsa_textura
  @bolsa_fecha_de_entrega
  @bolsa_precio_lista
  @bolsa_precio_venta
  @bolsa_cantidad


  #-------------------------------------------------------
  # EJECUCION
  #-------------------------------------------------------
  @ejecucion_id_ejecucion
  @ejecucion_fecha_inicio
  @ejecucion_id_dispositivo
  @ejecucion_nombre_dispositivo
  @ejecucion_ip_dispositivo
  @ejecucion_hostname
  @ejecucion_plataforma
  @ejecucion_navegador


  #-------------------------------------------------------
  # ENTREGA
  #-------------------------------------------------------
  @entrega_id_entrega
  @entrega_id_ejecucion
  @entrega_nombre_corto
  @entrega_direccion


  #-------------------------------------------------------
  # ENTREGA PASO2
  #-------------------------------------------------------
  @entrega_paso2_id_entrega_paso2
  @entrega_paso2_id_entrega
  @entrega_paso2_id_pago
  @entrega_paso2_nombre_corto
  @entrega_paso2_direccion


  #-------------------------------------------------------
  # ENTREGA PASO3
  #-------------------------------------------------------
  @entrega_paso3_id_entrega_paso3
  @entrega_paso3_id_entrega
  @entrega_paso3_id_pago
  @entrega_paso3_nombre_corto
  @entrega_paso3_direccion


  #-------------------------------------------------------
  # FORMA DE PAGO
  #-------------------------------------------------------
  @forma_de_pago_id_pago
  @forma_de_pago_id_entrega
  @forma_de_pago_id_ejecucion
  @forma_de_pago_nombre_tarjeta
  @forma_de_pago_numero_de_tarjeta
  @forma_de_pago_correo_paypal
  @forma_de_pago_efectivo
  @forma_de_pago_establecimiento
  @forma_de_pago_spei


  #-------------------------------------------------------
  # FORMA DE PAGO 3
  #-------------------------------------------------------
  @forma_de_pago3_id_pagop3
  @forma_de_pago3_id_pago
  @forma_de_pago3_id_promos
  @forma_de_pago3_metodo_p
  @forma_de_pago3_bin_target


  #-------------------------------------------------------
  # PDP
  #-------------------------------------------------------
  @pdp_id_pdp
  @pdp_id_plp
  @pdp_articulo
  @pdp_sku
  @pdp_cantidad
  @pdp_p_lista
  @pdp_p_venta
  @pdp_color
  @pdp_talla
  @pdp_material
  @pdp_textura
  @pdp_video
  @pdp_raiting
  @pdp_fecha_de_entrega
  @pdp_promociones
  #atributos extra a la DB
  @pdp_img_bandera
  @pdp_img_raiting
  @pdp_img_contador


  #-------------------------------------------------------
  # PLP
  #-------------------------------------------------------
  @plp_id_plp
  @plp_nombre_sku
  @plp_imagen
  @plp_precio_lista
  @plp_precio_venta
  @plp_raiting
  @plp_id_ejecucion
  @plp_img_bandera
  @plp_img_raiting
  @plp_img_contador


  #-------------------------------------------------------
  # PRODUCTOS
  #-------------------------------------------------------
  @productos_id_productos
  @productos_id_entrega
  @productos_id_bolsa
  @productos_nombre_sku
  @productos_color
  @productos_talla
  @productos_material
  @productos_cantidad
  @productos_fecha_de_entrega


  #-------------------------------------------------------
  # RESUMEN BOLSA
  #-------------------------------------------------------
  @resumen_bolsa_id_resumen_bolsa
  @resumen_bolsa_id_bolsa
  @resumen_bolsa_subtotal
  @resumen_bolsa_descuento
  @resumen_bolsa_promocion
  @resumen_bolsa_direccion_envio
  @resumen_bolsa_total


  #-------------------------------------------------------
  # RESUMEN BOLSA PASO2
  #-------------------------------------------------------
  @resumen_bolsa_paso2_id_resumen_paso2
  @resumen_bolsa_paso2_id_pago
  @resumen_bolsa_paso2_id_resumen
  @resumen_bolsa_paso2_subtotal
  @resumen_bolsa_paso2_descuento
  @resumen_bolsa_paso2_codigo_promo
  @resumen_bolsa_paso2_direccion
  @resumen_bolsa_paso2_total


  #-------------------------------------------------------
  # PROMOCIONES
  #-------------------------------------------------------
  @promociones_id_promocion
  @promociones_descripcion_promocion


  #-------------------------------------------------------
  # RESUMEN BOLDA PASO3
  #-------------------------------------------------------
  @resumen_bolsa_paso3_id_resumen_paso3
  @resumen_bolsa_paso3_id_pago
  @resumen_bolsa_paso3_id_resumen
  @resumen_bolsa_paso3_id_promociones
  @resumen_bolsa_paso3_subtotal
  @resumen_bolsa_paso3_descuento
  @resumen_bolsa_paso3_codigo_promo
  @resumen_bolsa_paso3_direccion
  @resumen_bolsa_paso3_total


  #-------------------------------------------------------
  # TICKET
  #-------------------------------------------------------
  @ticket_id_ticket
  @ticket_id_ejecucion
  @ticket_id_pdp
  @ticket_id_entrega
  @ticket_id_promos
  @ticket_sku_pdp
  @ticket_promociones
  @ticket_nombre_sku
  @ticket_color_sku
  @ticket_talla_sku
  @ticket_material_sku
  @ticket_textuta_sku
  @ticket_precio_sku
  @ticket_total_sku
  @ticket_detalle_de_la_compra
  @ticket_usuario
  @ticket_correo_compra
  @ticket_numero_de_boleta
  @ticket_numero_terminal
  @ticket_tienda


  #############################################################################
  # GETTERS & SETTERS
  #############################################################################
  def bolsa_id_bolsa
    @bolsa_id_bolsa
  end
  def set_bolsa_id_bolsa(bolsa_id_bolsa)
    @bolsa_id_bolsa = bolsa_id_bolsa
  end

  def bolsa_id_plp
    @bolsa_id_plp
  end
  def set_bolsa_id_plp(bolsa_id_plp)
    @bolsa_id_plp = bolsa_id_plp
  end

  def bolsa_id_pdp
    @bolsa_id_pdp
  end
  def set_bolsa_id_pdp(bolsa_id_pdp)
    @bolsa_id_pdp = bolsa_id_pdp
  end

  def bolsa_nombre_sku
    @bolsa_nombre_sku
  end
  def set_bolsa_nombre_sku(bolsa_nombre_sku)
    @bolsa_nombre_sku = bolsa_nombre_sku
  end

  def bolsa_sku
    @bolsa_sku
  end
  def set_bolsa_sku(bolsa_sku)
    @bolsa_sku = bolsa_sku
  end

  def bolsa_raiting
    @bolsa_raiting
  end
  def set_bolsa_raiting(bolsa_raiting)
    @bolsa_raiting = bolsa_raiting
  end

  def bolsa_color
    @bolsa_color
  end
  def set_bolsa_color(bolsa_color)
    @bolsa_color = bolsa_color
  end

  def bolsa_talla
    @bolsa_talla
  end
  def set_bolsa_talla(bolsa_talla)
    @bolsa_talla = bolsa_talla
  end

  def bolsa_material
    @bolsa_material
  end
  def set_bolsa_material(bolsa_material)
    @bolsa_material = bolsa_material
  end

  def bolsa_textura
    @bolsa_textura
  end
  def set_bolsa_textura(bolsa_textura)
    @bolsa_textura = bolsa_textura
  end

  def bolsa_fecha_de_entrega
    @bolsa_fecha_de_entrega
  end
  def set_bolsa_fecha_de_entrega(bolsa_fecha_de_entrega)
    @bolsa_fecha_de_entrega = bolsa_fecha_de_entrega
  end

  def bolsa_precio_lista
    @bolsa_precio_lista
  end
  def set_bolsa_precio_lista(bolsa_precio_lista)
    @bolsa_precio_lista = bolsa_precio_lista
  end

  def bolsa_precio_venta
    @bolsa_precio_venta
  end
  def set_bolsa_precio_venta(bolsa_precio_venta)
    @bolsa_precio_venta = bolsa_precio_venta
  end

  def bolsa_cantidad
    @bolsa_cantidad
  end
  def set_bolsa_cantidad(bolsa_cantidad)
    @bolsa_cantidad = bolsa_cantidad
  end

  def ejecucion_id_ejecucion
    @ejecucion_id_ejecucion
  end
  def set_ejecucion_id_ejecucion(ejecucion_id_ejecucion)
    @ejecucion_id_ejecucion = ejecucion_id_ejecucion
  end

  def ejecucion_fecha_inicio
    @ejecucion_fecha_inicio
  end
  def set_ejecucion_fecha_inicio(ejecucion_fecha_inicio)
    @ejecucion_fecha_inicio = ejecucion_fecha_inicio
  end

  def ejecucion_id_dispositivo
    @ejecucion_id_dispositivo
  end
  def set_ejecucion_id_dispositivo(ejecucion_id_dispositivo)
    @ejecucion_id_dispositivo = ejecucion_id_dispositivo
  end

  def ejecucion_nombre_dispositivo
    @ejecucion_nombre_dispositivo
  end
  def set_ejecucion_nombre_dispositivo(ejecucion_nombre_dispositivo)
    @ejecucion_nombre_dispositivo = ejecucion_nombre_dispositivo
  end

  def ejecucion_ip_dispositivo
    @ejecucion_ip_dispositivo
  end
  def set_ejecucion_ip_dispositivo(ejecucion_ip_dispositivo)
    @ejecucion_ip_dispositivo = ejecucion_ip_dispositivo
  end

  def ejecucion_hostname
    @ejecucion_hostname
  end
  def set_ejecucion_hostname(ejecucion_hostname)
    @ejecucion_hostname = ejecucion_hostname
  end

  def ejecucion_plataforma
    @ejecucion_plataforma
  end
  def set_ejecucion_plataforma(ejecucion_plataforma)
    @ejecucion_plataforma = ejecucion_plataforma
  end

  def ejecucion_navegador
    @ejecucion_navegador
  end
  def set_ejecucion_navegador(ejecucion_navegador)
    @ejecucion_navegador = ejecucion_navegador
  end

  def entrega_id_entrega
    @entrega_id_entrega
  end
  def set_entrega_id_entrega(entrega_id_entrega)
    @entrega_id_entrega = entrega_id_entrega
  end

  def entrega_id_ejecucion
    @entrega_id_ejecucion
  end
  def set_entrega_id_ejecucion(entrega_id_ejecucion)
    @entrega_id_ejecucion = entrega_id_ejecucion
  end

  def entrega_nombre_corto
    @entrega_nombre_corto
  end
  def set_entrega_nombre_corto(entrega_nombre_corto)
    @entrega_nombre_corto = entrega_nombre_corto
  end

  def entrega_direccion
    @entrega_direccion
  end
  def set_entrega_direccion(entrega_direccion)
    @entrega_direccion = entrega_direccion
  end

  def entrega_paso2_id_entrega_paso2
    @entrega_paso2_id_entrega_paso2
  end
  def set_entrega_paso2_id_entrega_paso2(entrega_paso2_id_entrega_paso2)
    @entrega_paso2_id_entrega_paso2 = entrega_paso2_id_entrega_paso2
  end

  def entrega_paso2_id_entrega
    @entrega_paso2_id_entrega
  end
  def set_entrega_paso2_id_entrega(entrega_paso2_id_entrega)
    @entrega_paso2_id_entrega = entrega_paso2_id_entrega
  end

  def entrega_paso2_id_pago
    @entrega_paso2_id_pago
  end
  def set_entrega_paso2_id_pago(entrega_paso2_id_pago)
    @entrega_paso2_id_pago = entrega_paso2_id_pago
  end

  def entrega_paso2_nombre_corto
    @entrega_paso2_nombre_corto
  end
  def set_entrega_paso2_nombre_corto(entrega_paso2_nombre_corto)
    @entrega_paso2_nombre_corto = entrega_paso2_nombre_corto
  end

  def entrega_paso2_direccion
    @entrega_paso2_direccion
  end
  def set_entrega_paso2_direccion(entrega_paso2_direccion)
    @entrega_paso2_direccion = entrega_paso2_direccion
  end

  def entrega_paso3_id_entrega_paso3
    @entrega_paso3_id_entrega_paso3
  end
  def set_entrega_paso3_id_entrega_paso3(entrega_paso3_id_entrega_paso3)
    @entrega_paso3_id_entrega_paso3 = entrega_paso3_id_entrega_paso3
  end

  def entrega_paso3_id_entrega
    @entrega_paso3_id_entrega
  end
  def set_entrega_paso3_id_entrega(entrega_paso3_id_entrega)
    @entrega_paso3_id_entrega = entrega_paso3_id_entrega
  end

  def entrega_paso3_id_pago
    @entrega_paso3_id_pago
  end
  def set_entrega_paso3_id_pago(entrega_paso3_id_pago)
    @entrega_paso3_id_pago = entrega_paso3_id_pago
  end

  def entrega_paso3_nombre_corto
    @entrega_paso3_nombre_corto
  end
  def set_entrega_paso3_nombre_corto(entrega_paso3_nombre_corto)
    @entrega_paso3_nombre_corto = entrega_paso3_nombre_corto
  end

  def entrega_paso3_direccion
    @entrega_paso3_direccion
  end
  def set_entrega_paso3_direccion(entrega_paso3_direccion)
    @entrega_paso3_direccion = entrega_paso3_direccion
  end

  def forma_de_pago_id_pago
    @forma_de_pago_id_pago
  end
  def set_forma_de_pago_id_pago(forma_de_pago_id_pago)
    @forma_de_pago_id_pago = forma_de_pago_id_pago
  end

  def forma_de_pago_id_entrega
    @forma_de_pago_id_entrega
  end
  def set_forma_de_pago_id_entrega(forma_de_pago_id_entrega)
    @forma_de_pago_id_entrega = forma_de_pago_id_entrega
  end

  def forma_de_pago_id_ejecucion
    @forma_de_pago_id_ejecucion
  end
  def set_forma_de_pago_id_ejecucion(forma_de_pago_id_ejecucion)
    @forma_de_pago_id_ejecucion = forma_de_pago_id_ejecucion
  end

  def forma_de_pago_nombre_tarjeta
    @forma_de_pago_nombre_tarjeta
  end
  def set_forma_de_pago_nombre_tarjeta(forma_de_pago_nombre_tarjeta)
    @forma_de_pago_nombre_tarjeta = forma_de_pago_nombre_tarjeta
  end

  def forma_de_pago_numero_de_tarjeta
    @forma_de_pago_numero_de_tarjeta
  end
  def set_forma_de_pago_numero_de_tarjeta(forma_de_pago_numero_de_tarjeta)
    @forma_de_pago_numero_de_tarjeta = forma_de_pago_numero_de_tarjeta
  end

  def forma_de_pago_correo_paypal
    @forma_de_pago_correo_paypal
  end
  def set_forma_de_pago_correo_paypal(forma_de_pago_correo_paypal)
    @forma_de_pago_correo_paypal = forma_de_pago_correo_paypal
  end

  def forma_de_pago_efectivo
    @forma_de_pago_efectivo
  end
  def set_forma_de_pago_efectivo(forma_de_pago_efectivo)
    @forma_de_pago_efectivo = forma_de_pago_efectivo
  end

  def forma_de_pago_establecimiento
    @forma_de_pago_establecimiento
  end
  def set_forma_de_pago_establecimiento(forma_de_pago_establecimiento)
    @forma_de_pago_establecimiento = forma_de_pago_establecimiento
  end

  def forma_de_pago_spei
    @forma_de_pago_spei
  end
  def set_forma_de_pago_spei(forma_de_pago_spei)
    @forma_de_pago_spei = forma_de_pago_spei
  end

  def forma_de_pago3_id_pagop3
    @forma_de_pago3_id_pagop3
  end
  def set_forma_de_pago3_id_pagop3(forma_de_pago3_id_pagop3)
    @forma_de_pago3_id_pagop3 = forma_de_pago3_id_pagop3
  end

  def forma_de_pago3_id_pago
    @forma_de_pago3_id_pago
  end
  def set_forma_de_pago3_id_pago(forma_de_pago3_id_pago)
    @forma_de_pago3_id_pago = forma_de_pago3_id_pago
  end

  def forma_de_pago3_id_promos
    @forma_de_pago3_id_promos
  end
  def set_forma_de_pago3_id_promos(forma_de_pago3_id_promos)
    @forma_de_pago3_id_promos = forma_de_pago3_id_promos
  end

  def forma_de_pago3_metodo_p
    @forma_de_pago3_metodo_p
  end
  def set_forma_de_pago3_metodo_p(forma_de_pago3_metodo_p)
    @forma_de_pago3_metodo_p = forma_de_pago3_metodo_p
  end

  def forma_de_pago3_bin_target
    @forma_de_pago3_bin_target
  end
  def set_forma_de_pago3_bin_target(forma_de_pago3_bin_target)
    @forma_de_pago3_bin_target = forma_de_pago3_bin_target
  end

  def pdp_id_pdp
    @pdp_id_pdp
  end
  def set_pdp_id_pdp(pdp_id_pdp)
    @pdp_id_pdp = pdp_id_pdp
  end

  def pdp_id_plp
    @pdp_id_plp
  end
  def set_pdp_id_plp(pdp_id_plp)
    @pdp_id_plp = pdp_id_plp
  end

  def pdp_articulo
    @pdp_articulo
  end
  def set_pdp_articulo(pdp_articulo)
    @pdp_articulo = pdp_articulo
  end

  def pdp_sku
    @pdp_sku
  end
  def set_pdp_sku(pdp_sku)
    @pdp_sku = pdp_sku
  end

  def pdp_cantidad
    @pdp_cantidad
  end
  def set_pdp_cantidad(pdp_cantidad)
    @pdp_cantidad = pdp_cantidad
  end

  def pdp_p_lista
    @pdp_p_lista
  end
  def set_pdp_p_lista(pdp_p_lista)
    @pdp_p_lista = pdp_p_lista
  end

  def pdp_p_venta
    @pdp_p_venta
  end
  def set_pdp_p_venta(pdp_p_venta)
    @pdp_p_venta = pdp_p_venta
  end

  def pdp_color
    @pdp_color
  end
  def set_pdp_color(pdp_color)
    @pdp_color = pdp_color
  end

  def pdp_talla
    @pdp_talla
  end
  def set_pdp_talla(pdp_talla)
    @pdp_talla = pdp_talla
  end

  def pdp_material
    @pdp_material
  end
  def set_pdp_material(pdp_material)
    @pdp_material = pdp_material
  end

  def pdp_textura
    @pdp_textura
  end
  def set_pdp_textura(pdp_textura)
    @pdp_textura = pdp_textura
  end

  def pdp_video
    @pdp_video
  end
  def set_pdp_video(pdp_video)
    @pdp_video = pdp_video
  end

  def pdp_raiting
    @pdp_raiting
  end
  def set_pdp_raiting(pdp_raiting)
    @pdp_raiting = pdp_raiting
  end

  def pdp_fecha_de_entrega
    @pdp_fecha_de_entrega
  end
  def set_pdp_fecha_de_entrega(pdp_fecha_de_entrega)
    @pdp_fecha_de_entrega = pdp_fecha_de_entrega
  end

  def pdp_promociones
    @pdp_promociones
  end
  def set_pdp_promociones(pdp_promociones)
    @pdp_promociones = pdp_promociones
  end

  def pdp_img_bandera
    @pdp_img_bandera
  end
  def set_pdp_img_bandera(pdp_img_bandera)
    @pdp_img_bandera = pdp_img_bandera
  end

  def pdp_img_raiting
    @pdp_img_raiting
  end
  def set_pdp_img_raiting(pdp_img_raiting)
    @pdp_img_raiting = pdp_img_raiting
  end

  def pdp_img_contador
    @pdp_img_contador
  end
  def set_pdp_img_contador(pdp_img_contador)
    @pdp_img_contador = pdp_img_contador
  end

  def plp_id_plp
    @plp_id_plp
  end
  def set_plp_id_plp(plp_id_plp)
    @plp_id_plp = plp_id_plp
  end

  def plp_nombre_sku
    @plp_nombre_sku
  end
  def set_plp_nombre_sku(plp_nombre_sku)
    @plp_nombre_sku = plp_nombre_sku
  end

  def plp_imagen
    @plp_imagen
  end
  def set_plp_imagen(plp_imagen)
    @plp_imagen = plp_imagen
  end

  def plp_precio_lista
    @plp_precio_lista
  end
  def set_plp_precio_lista(plp_precio_lista)
    @plp_precio_lista = plp_precio_lista
  end

  def plp_precio_venta
    @plp_precio_venta
  end
  def set_plp_precio_venta(plp_precio_venta)
    @plp_precio_venta = plp_precio_venta
  end

  def plp_raiting
    @plp_raiting
  end
  def set_plp_raiting(plp_raiting)
    @plp_raiting = plp_raiting
  end

  def plp_id_ejecucion
    @plp_id_ejecucion
  end
  def set_plp_id_ejecucion(plp_id_ejecucion)
    @plp_id_ejecucion = plp_id_ejecucion
  end

  def plp_img_bandera
    @plp_img_bandera
  end
  def set_plp_img_bandera(plp_img_bandera)
    @plp_img_bandera = plp_img_bandera
  end

  def plp_img_raiting
    @plp_img_raiting
  end
  def set_plp_img_raiting(plp_img_raiting)
    @plp_img_raiting = plp_img_raiting
  end

  def plp_img_contador
    @plp_img_contador
  end
  def set_plp_img_contador(plp_img_contador)
    @plp_img_contador = plp_img_contador
  end

  def productos_id_productos
    @productos_id_productos
  end
  def set_productos_id_productos(productos_id_productos)
    @productos_id_productos = productos_id_productos
  end

  def productos_id_entrega
    @productos_id_entrega
  end
  def set_productos_id_entrega(productos_id_entrega)
    @productos_id_entrega = productos_id_entrega
  end

  def productos_id_bolsa
    @productos_id_bolsa
  end
  def set_productos_id_bolsa(productos_id_bolsa)
    @productos_id_bolsa = productos_id_bolsa
  end

  def productos_nombre_sku
    @productos_nombre_sku
  end
  def set_productos_nombre_sku(productos_nombre_sku)
    @productos_nombre_sku = productos_nombre_sku
  end

  def productos_color
    @productos_color
  end
  def set_productos_color(productos_color)
    @productos_color = productos_color
  end

  def productos_talla
    @productos_talla
  end
  def set_productos_talla(productos_talla)
    @productos_talla = productos_talla
  end

  def productos_material
    @productos_material
  end
  def set_productos_material(productos_material)
    @productos_material = productos_material
  end

  def productos_cantidad
    @productos_cantidad
  end
  def set_productos_cantidad(productos_cantidad)
    @productos_cantidad = productos_cantidad
  end

  def productos_fecha_de_entrega
    @productos_fecha_de_entrega
  end
  def set_productos_fecha_de_entrega(productos_fecha_de_entrega)
    @productos_fecha_de_entrega = productos_fecha_de_entrega
  end

  def resumen_bolsa_id_resumen_bolsa
    @resumen_bolsa_id_resumen_bolsa
  end
  def set_resumen_bolsa_id_resumen_bolsa(resumen_bolsa_id_resumen_bolsa)
    @resumen_bolsa_id_resumen_bolsa = resumen_bolsa_id_resumen_bolsa
  end

  def resumen_bolsa_id_bolsa
    @resumen_bolsa_id_bolsa
  end
  def set_resumen_bolsa_id_bolsa(resumen_bolsa_id_bolsa)
    @resumen_bolsa_id_bolsa = resumen_bolsa_id_bolsa
  end

  def resumen_bolsa_subtotal
    @resumen_bolsa_subtotal
  end
  def set_resumen_bolsa_subtotal(resumen_bolsa_subtotal)
    @resumen_bolsa_subtotal = resumen_bolsa_subtotal
  end

  def resumen_bolsa_descuento
    @resumen_bolsa_descuento
  end
  def set_resumen_bolsa_descuento(resumen_bolsa_descuento)
    @resumen_bolsa_descuento = resumen_bolsa_descuento
  end

  def resumen_bolsa_promocion
    @resumen_bolsa_promocion
  end
  def set_resumen_bolsa_promocion(resumen_bolsa_promocion)
    @resumen_bolsa_promocion = resumen_bolsa_promocion
  end

  def resumen_bolsa_direccion_envio
    @resumen_bolsa_direccion_envio
  end
  def set_resumen_bolsa_direccion_envio(resumen_bolsa_direccion_envio)
    @resumen_bolsa_direccion_envio = resumen_bolsa_direccion_envio
  end

  def resumen_bolsa_total
    @resumen_bolsa_total
  end
  def set_resumen_bolsa_total(resumen_bolsa_total)
    @resumen_bolsa_total = resumen_bolsa_total
  end

  def resumen_bolsa_paso2_id_resumen_paso2
    @resumen_bolsa_paso2_id_resumen_paso2
  end
  def set_resumen_bolsa_paso2_id_resumen_paso2(resumen_bolsa_paso2_id_resumen_paso2)
    @resumen_bolsa_paso2_id_resumen_paso2 = resumen_bolsa_paso2_id_resumen_paso2
  end

  def resumen_bolsa_paso2_id_pago
    @resumen_bolsa_paso2_id_pago
  end
  def set_resumen_bolsa_paso2_id_pago(resumen_bolsa_paso2_id_pago)
    @resumen_bolsa_paso2_id_pago = resumen_bolsa_paso2_id_pago
  end

  def resumen_bolsa_paso2_id_resumen
    @resumen_bolsa_paso2_id_resumen
  end
  def set_resumen_bolsa_paso2_id_resumen(resumen_bolsa_paso2_id_resumen)
    @resumen_bolsa_paso2_id_resumen = resumen_bolsa_paso2_id_resumen
  end

  def resumen_bolsa_paso2_subtotal
    @resumen_bolsa_paso2_subtotal
  end
  def set_resumen_bolsa_paso2_subtotal(resumen_bolsa_paso2_subtotal)
    @resumen_bolsa_paso2_subtotal = resumen_bolsa_paso2_subtotal
  end

  def resumen_bolsa_paso2_descuento
    @resumen_bolsa_paso2_descuento
  end
  def set_resumen_bolsa_paso2_descuento(resumen_bolsa_paso2_descuento)
    @resumen_bolsa_paso2_descuento = resumen_bolsa_paso2_descuento
  end

  def resumen_bolsa_paso2_codigo_promo
    @resumen_bolsa_paso2_codigo_promo
  end
  def set_resumen_bolsa_paso2_codigo_promo(resumen_bolsa_paso2_codigo_promo)
    @resumen_bolsa_paso2_codigo_promo = resumen_bolsa_paso2_codigo_promo
  end

  def resumen_bolsa_paso2_direccion
    @resumen_bolsa_paso2_direccion
  end
  def set_resumen_bolsa_paso2_direccion(resumen_bolsa_paso2_direccion)
    @resumen_bolsa_paso2_direccion = resumen_bolsa_paso2_direccion
  end

  def resumen_bolsa_paso2_total
    @resumen_bolsa_paso2_total
  end
  def set_resumen_bolsa_paso2_total(resumen_bolsa_paso2_total)
    @resumen_bolsa_paso2_total = resumen_bolsa_paso2_total
  end

  def promociones_id_promocion
    @promociones_id_promocion
  end
  def set_promociones_id_promocion(promociones_id_promocion)
    @promociones_id_promocion = promociones_id_promocion
  end

  def promociones_descripcion_promocion
    @promociones_descripcion_promocion
  end
  def set_promociones_descripcion_promocion(promociones_descripcion_promocion)
    @promociones_descripcion_promocion = promociones_descripcion_promocion
  end

  def resumen_bolsa_paso3_id_resumen_paso3
    @resumen_bolsa_paso3_id_resumen_paso3
  end
  def set_resumen_bolsa_paso3_id_resumen_paso3(resumen_bolsa_paso3_id_resumen_paso3)
    @resumen_bolsa_paso3_id_resumen_paso3 = resumen_bolsa_paso3_id_resumen_paso3
  end

  def resumen_bolsa_paso3_id_pago
    @resumen_bolsa_paso3_id_pago
  end
  def set_resumen_bolsa_paso3_id_pago(resumen_bolsa_paso3_id_pago)
    @resumen_bolsa_paso3_id_pago = resumen_bolsa_paso3_id_pago
  end

  def resumen_bolsa_paso3_id_resumen
    @resumen_bolsa_paso3_id_resumen
  end
  def set_resumen_bolsa_paso3_id_resumen(resumen_bolsa_paso3_id_resumen)
    @resumen_bolsa_paso3_id_resumen = resumen_bolsa_paso3_id_resumen
  end

  def resumen_bolsa_paso3_id_promociones
    @resumen_bolsa_paso3_id_promociones
  end
  def set_resumen_bolsa_paso3_id_promociones(resumen_bolsa_paso3_id_promociones)
    @resumen_bolsa_paso3_id_promociones = resumen_bolsa_paso3_id_promociones
  end

  def resumen_bolsa_paso3_subtotal
    @resumen_bolsa_paso3_subtotal
  end
  def set_resumen_bolsa_paso3_subtotal(resumen_bolsa_paso3_subtotal)
    @resumen_bolsa_paso3_subtotal = resumen_bolsa_paso3_subtotal
  end

  def resumen_bolsa_paso3_descuento
    @resumen_bolsa_paso3_descuento
  end
  def set_resumen_bolsa_paso3_descuento(resumen_bolsa_paso3_descuento)
    @resumen_bolsa_paso3_descuento = resumen_bolsa_paso3_descuento
  end

  def resumen_bolsa_paso3_codigo_promo
    @resumen_bolsa_paso3_codigo_promo
  end
  def set_resumen_bolsa_paso3_codigo_promo(resumen_bolsa_paso3_codigo_promo)
    @resumen_bolsa_paso3_codigo_promo = resumen_bolsa_paso3_codigo_promo
  end

  def resumen_bolsa_paso3_direccion
    @resumen_bolsa_paso3_direccion
  end
  def set_resumen_bolsa_paso3_direccion(resumen_bolsa_paso3_direccion)
    @resumen_bolsa_paso3_direccion = resumen_bolsa_paso3_direccion
  end

  def resumen_bolsa_paso3_total
    @resumen_bolsa_paso3_total
  end
  def set_resumen_bolsa_paso3_total(resumen_bolsa_paso3_total)
    @resumen_bolsa_paso3_total = resumen_bolsa_paso3_total
  end

  def ticket_id_ticket
    @ticket_id_ticket
  end
  def set_ticket_id_ticket(ticket_id_ticket)
    @ticket_id_ticket = ticket_id_ticket
  end

  def ticket_id_ejecucion
    @ticket_id_ejecucion
  end
  def set_ticket_id_ejecucion(ticket_id_ejecucion)
    @ticket_id_ejecucion = ticket_id_ejecucion
  end

  def ticket_id_pdp
    @ticket_id_pdp
  end
  def set_ticket_id_pdp(ticket_id_pdp)
    @ticket_id_pdp = ticket_id_pdp
  end

  def ticket_id_entrega
    @ticket_id_entrega
  end
  def set_ticket_id_entrega(ticket_id_entrega)
    @ticket_id_entrega = ticket_id_entrega
  end

  def ticket_id_promos
    @ticket_id_promos
  end
  def set_ticket_id_promos(ticket_id_promos)
    @ticket_id_promos = ticket_id_promos
  end

  def ticket_sku_pdp
    @ticket_sku_pdp
  end
  def set_ticket_sku_pdp(ticket_sku_pdp)
    @ticket_sku_pdp = ticket_sku_pdp
  end

  def ticket_promociones
    @ticket_promociones
  end
  def set_ticket_promociones(ticket_promociones)
    @ticket_promociones = ticket_promociones
  end

  def ticket_nombre_sku
    @ticket_nombre_sku
  end
  def set_ticket_nombre_sku(ticket_nombre_sku)
    @ticket_nombre_sku = ticket_nombre_sku
  end

  def ticket_color_sku
    @ticket_color_sku
  end
  def set_ticket_color_sku(ticket_color_sku)
    @ticket_color_sku = ticket_color_sku
  end

  def ticket_talla_sku
    @ticket_talla_sku
  end
  def set_ticket_talla_sku(ticket_talla_sku)
    @ticket_talla_sku = ticket_talla_sku
  end

  def ticket_material_sku
    @ticket_material_sku
  end
  def set_ticket_material_sku(ticket_material_sku)
    @ticket_material_sku = ticket_material_sku
  end

  def ticket_textuta_sku
    @ticket_textuta_sku
  end
  def set_ticket_textuta_sku(ticket_textuta_sku)
    @ticket_textuta_sku = ticket_textuta_sku
  end

  def ticket_precio_sku
    @ticket_precio_sku
  end
  def set_ticket_precio_sku(ticket_precio_sku)
    @ticket_precio_sku = ticket_precio_sku
  end

  def ticket_total_sku
    @ticket_total_sku
  end
  def set_ticket_total_sku(ticket_total_sku)
    @ticket_total_sku = ticket_total_sku
  end

  def ticket_detalle_de_la_compra
    @ticket_detalle_de_la_compra
  end
  def set_ticket_detalle_de_la_compra(ticket_detalle_de_la_compra)
    @ticket_detalle_de_la_compra = ticket_detalle_de_la_compra
  end

  def ticket_usuario
    @ticket_usuario
  end
  def set_ticket_usuario(ticket_usuario)
    @ticket_usuario = ticket_usuario
  end

  def ticket_correo_compra
    @ticket_correo_compra
  end
  def set_ticket_correo_compra(ticket_correo_compra)
    @ticket_correo_compra = ticket_correo_compra
  end

  def ticket_numero_de_boleta
    @ticket_numero_de_boleta
  end
  def set_ticket_numero_de_boleta(ticket_numero_de_boleta)
    @ticket_numero_de_boleta = ticket_numero_de_boleta
  end

  def ticket_numero_terminal
    @ticket_numero_terminal
  end
  def set_ticket_numero_terminal(ticket_numero_terminal)
    @ticket_numero_terminal = ticket_numero_terminal
  end

  def ticket_tienda
    @ticket_tienda
  end
  def set_ticket_tienda(ticket_tienda)
    @ticket_tienda = ticket_tienda
  end



  #############################################################################
  # METODOS
  #############################################################################

  def to_s
    s = StringIO.new
    s << "bolsa_id_bolsa = #{@bolsa_id_bolsa}, "
    s << "bolsa_id_plp = #{@bolsa_id_plp}, "
    s << "bolsa_id_pdp = #{@bolsa_id_pdp}, "
    s << "bolsa_nombre_sku = #{@bolsa_nombre_sku}, "
    s << "bolsa_sku = #{@bolsa_sku}, "
    s << "bolsa_raiting = #{@bolsa_raiting}, "
    s << "bolsa_color = #{@bolsa_color}, "
    s << "bolsa_talla = #{@bolsa_talla}, "
    s << "bolsa_material = #{@bolsa_material}, "
    s << "bolsa_textura = #{@bolsa_textura}, "
    s << "bolsa_fecha_de_entrega = #{@bolsa_fecha_de_entrega}, "
    s << "bolsa_precio_lista = #{@bolsa_precio_lista}, "
    s << "bolsa_precio_venta = #{@bolsa_precio_venta}, "
    s << "bolsa_cantidad = #{@bolsa_cantidad}, "
    s << "ejecucion_id_ejecucion = #{@ejecucion_id_ejecucion}, "
    s << "ejecucion_fecha_inicio = #{@ejecucion_fecha_inicio}, "
    s << "ejecucion_id_dispositivo = #{@ejecucion_id_dispositivo}, "
    s << "ejecucion_nombre_dispositivo = #{@ejecucion_nombre_dispositivo}, "
    s << "ejecucion_ip_dispositivo = #{@ejecucion_ip_dispositivo}, "
    s << "ejecucion_hostname = #{@ejecucion_hostname}, "
    s << "ejecucion_plataforma = #{@ejecucion_plataforma}, "
    s << "ejecucion_navegador = #{@ejecucion_navegador}, "
    s << "entrega_id_entrega = #{@entrega_id_entrega}, "
    s << "entrega_id_ejecucion = #{@entrega_id_ejecucion}, "
    s << "entrega_nombre_corto = #{@entrega_nombre_corto}, "
    s << "entrega_direccion = #{@entrega_direccion}, "
    s << "entrega_paso2_id_entrega_paso2 = #{@entrega_paso2_id_entrega_paso2}, "
    s << "entrega_paso2_id_entrega = #{@entrega_paso2_id_entrega}, "
    s << "entrega_paso2_id_pago = #{@entrega_paso2_id_pago}, "
    s << "entrega_paso2_nombre_corto = #{@entrega_paso2_nombre_corto}, "
    s << "entrega_paso2_direccion = #{@entrega_paso2_direccion}, "
    s << "entrega_paso3_id_entrega_paso3 = #{@entrega_paso3_id_entrega_paso3}, "
    s << "entrega_paso3_id_entrega = #{@entrega_paso3_id_entrega}, "
    s << "entrega_paso3_id_pago = #{@entrega_paso3_id_pago}, "
    s << "entrega_paso3_nombre_corto = #{@entrega_paso3_nombre_corto}, "
    s << "entrega_paso3_direccion = #{@entrega_paso3_direccion}, "
    s << "forma_de_pago_id_pago = #{@forma_de_pago_id_pago}, "
    s << "forma_de_pago_id_entrega = #{@forma_de_pago_id_entrega}, "
    s << "forma_de_pago_id_ejecucion = #{@forma_de_pago_id_ejecucion}, "
    s << "forma_de_pago_nombre_tarjeta = #{@forma_de_pago_nombre_tarjeta}, "
    s << "forma_de_pago_numero_de_tarjeta = #{@forma_de_pago_numero_de_tarjeta}, "
    s << "forma_de_pago_correo_paypal = #{@forma_de_pago_correo_paypal}, "
    s << "forma_de_pago_efectivo = #{@forma_de_pago_efectivo}, "
    s << "forma_de_pago_establecimiento = #{@forma_de_pago_establecimiento}, "
    s << "forma_de_pago_spei = #{@forma_de_pago_spei}, "
    s << "forma_de_pago3_id_pagop3 = #{@forma_de_pago3_id_pagop3}, "
    s << "forma_de_pago3_id_pago = #{@forma_de_pago3_id_pago}, "
    s << "forma_de_pago3_id_promos = #{@forma_de_pago3_id_promos}, "
    s << "forma_de_pago3_metodo_p = #{@forma_de_pago3_metodo_p}, "
    s << "forma_de_pago3_bin_target = #{@forma_de_pago3_bin_target}, "
    s << "pdp_id_pdp = #{@pdp_id_pdp}, "
    s << "pdp_id_plp = #{@pdp_id_plp}, "
    s << "pdp_articulo = #{@pdp_articulo}, "
    s << "pdp_sku = #{@pdp_sku}, "
    s << "pdp_cantidad = #{@pdp_cantidad}, "
    s << "pdp_p_lista = #{@pdp_p_lista}, "
    s << "pdp_p_venta = #{@pdp_p_venta}, "
    s << "pdp_color = #{@pdp_color}, "
    s << "pdp_talla = #{@pdp_talla}, "
    s << "pdp_material = #{@pdp_material}, "
    s << "pdp_textura = #{@pdp_textura}, "
    s << "pdp_video = #{@pdp_video}, "
    s << "pdp_raiting = #{@pdp_raiting}, "
    s << "pdp_fecha_de_entrega = #{@pdp_fecha_de_entrega}, "
    s << "pdp_promociones = #{@pdp_promociones}, "
    s << "pdp_img_bandera = #{@pdp_img_bandera}, "
    s << "pdp_img_raiting = #{@pdp_img_raiting}, "
    s << "pdp_img_contador = #{@pdp_img_contador}, "
    s << "plp_id_plp = #{@plp_id_plp}, "
    s << "plp_nombre_sku = #{@plp_nombre_sku}, "
    s << "plp_imagen = #{@plp_imagen}, "
    s << "plp_precio_lista = #{@plp_precio_lista}, "
    s << "plp_precio_venta = #{@plp_precio_venta}, "
    s << "plp_raiting = #{@plp_raiting}, "
    s << "plp_id_ejecucion = #{@plp_id_ejecucion}, "
    s << "plp_img_bandera = #{@plp_img_bandera}, "
    s << "plp_img_raiting = #{@plp_img_raiting}, "
    s << "plp_img_contador = #{@plp_img_contador}, "
    s << "productos_id_productos = #{@productos_id_productos}, "
    s << "productos_id_entrega = #{@productos_id_entrega}, "
    s << "productos_id_bolsa = #{@productos_id_bolsa}, "
    s << "productos_nombre_sku = #{@productos_nombre_sku}, "
    s << "productos_color = #{@productos_color}, "
    s << "productos_talla = #{@productos_talla}, "
    s << "productos_material = #{@productos_material}, "
    s << "productos_cantidad = #{@productos_cantidad}, "
    s << "productos_fecha_de_entrega = #{@productos_fecha_de_entrega}, "
    s << "resumen_bolsa_id_resumen_bolsa = #{@resumen_bolsa_id_resumen_bolsa}, "
    s << "resumen_bolsa_id_bolsa = #{@resumen_bolsa_id_bolsa}, "
    s << "resumen_bolsa_subtotal = #{@resumen_bolsa_subtotal}, "
    s << "resumen_bolsa_descuento = #{@resumen_bolsa_descuento}, "
    s << "resumen_bolsa_promocion = #{@resumen_bolsa_promocion}, "
    s << "resumen_bolsa_direccion_envio = #{@resumen_bolsa_direccion_envio}, "
    s << "resumen_bolsa_total = #{@resumen_bolsa_total}, "
    s << "resumen_bolsa_paso2_id_resumen_paso2 = #{@resumen_bolsa_paso2_id_resumen_paso2}, "
    s << "resumen_bolsa_paso2_id_pago = #{@resumen_bolsa_paso2_id_pago}, "
    s << "resumen_bolsa_paso2_id_resumen = #{@resumen_bolsa_paso2_id_resumen}, "
    s << "resumen_bolsa_paso2_subtotal = #{@resumen_bolsa_paso2_subtotal}, "
    s << "resumen_bolsa_paso2_descuento = #{@resumen_bolsa_paso2_descuento}, "
    s << "resumen_bolsa_paso2_codigo_promo = #{@resumen_bolsa_paso2_codigo_promo}, "
    s << "resumen_bolsa_paso2_direccion = #{@resumen_bolsa_paso2_direccion}, "
    s << "resumen_bolsa_paso2_total = #{@resumen_bolsa_paso2_total}, "
    s << "promociones_id_promocion = #{@promociones_id_promocion}, "
    s << "promociones_descripcion_promocion = #{@promociones_descripcion_promocion}, "
    s << "resumen_bolsa_paso3_id_resumen_paso3 = #{@resumen_bolsa_paso3_id_resumen_paso3}, "
    s << "resumen_bolsa_paso3_id_pago = #{@resumen_bolsa_paso3_id_pago}, "
    s << "resumen_bolsa_paso3_id_resumen = #{@resumen_bolsa_paso3_id_resumen}, "
    s << "resumen_bolsa_paso3_id_promociones = #{@resumen_bolsa_paso3_id_promociones}, "
    s << "resumen_bolsa_paso3_subtotal = #{@resumen_bolsa_paso3_subtotal}, "
    s << "resumen_bolsa_paso3_descuento = #{@resumen_bolsa_paso3_descuento}, "
    s << "resumen_bolsa_paso3_codigo_promo = #{@resumen_bolsa_paso3_codigo_promo}, "
    s << "resumen_bolsa_paso3_direccion = #{@resumen_bolsa_paso3_direccion}, "
    s << "resumen_bolsa_paso3_total = #{@resumen_bolsa_paso3_total}, "
    s << "ticket_id_ticket = #{@ticket_id_ticket}, "
    s << "ticket_id_ejecucion = #{@ticket_id_ejecucion}, "
    s << "ticket_id_pdp = #{@ticket_id_pdp}, "
    s << "ticket_id_entrega = #{@ticket_id_entrega}, "
    s << "ticket_id_promos = #{@ticket_id_promos}, "
    s << "ticket_sku_pdp = #{@ticket_sku_pdp}, "
    s << "ticket_promociones = #{@ticket_promociones}, "
    s << "ticket_nombre_sku = #{@ticket_nombre_sku}, "
    s << "ticket_color_sku = #{@ticket_color_sku}, "
    s << "ticket_talla_sku = #{@ticket_talla_sku}, "
    s << "ticket_material_sku = #{@ticket_material_sku}, "
    s << "ticket_textuta_sku = #{@ticket_textuta_sku}, "
    s << "ticket_precio_sku = #{@ticket_precio_sku}, "
    s << "ticket_total_sku = #{@ticket_total_sku}, "
    s << "ticket_detalle_de_la_compra = #{@ticket_detalle_de_la_compra}, "
    s << "ticket_usuario = #{@ticket_usuario}, "
    s << "ticket_correo_compra = #{@ticket_correo_compra}, "
    s << "ticket_numero_de_boleta = #{@ticket_numero_de_boleta}, "
    s << "ticket_numero_terminal = #{@ticket_numero_terminal}, "
    s << "ticket_tienda = #{@ticket_tienda}"
    s.string.to_s
  end

  def to_h
    {
        :bolsa_id_bolsa => @bolsa_id_bolsa,
        :bolsa_id_plp => @bolsa_id_plp,
        :bolsa_id_pdp => @bolsa_id_pdp,
        :bolsa_nombre_sku => @bolsa_nombre_sku,
        :bolsa_sku => @bolsa_sku,
        :bolsa_raiting => @bolsa_raiting,
        :bolsa_color => @bolsa_color,
        :bolsa_talla => @bolsa_talla,
        :bolsa_material => @bolsa_material,
        :bolsa_textura => @bolsa_textura,
        :bolsa_fecha_de_entrega => @bolsa_fecha_de_entrega,
        :bolsa_precio_lista => @bolsa_precio_lista,
        :bolsa_precio_venta => @bolsa_precio_venta,
        :bolsa_cantidad => @bolsa_cantidad,
        :ejecucion_id_ejecucion => @ejecucion_id_ejecucion,
        :ejecucion_fecha_inicio => @ejecucion_fecha_inicio,
        :ejecucion_id_dispositivo => @ejecucion_id_dispositivo,
        :ejecucion_nombre_dispositivo => @ejecucion_nombre_dispositivo,
        :ejecucion_ip_dispositivo => @ejecucion_ip_dispositivo,
        :ejecucion_hostname => @ejecucion_hostname,
        :ejecucion_plataforma => @ejecucion_plataforma,
        :ejecucion_navegador => @ejecucion_navegador,
        :entrega_id_entrega => @entrega_id_entrega,
        :entrega_id_ejecucion => @entrega_id_ejecucion,
        :entrega_nombre_corto => @entrega_nombre_corto,
        :entrega_direccion => @entrega_direccion,
        :entrega_paso2_id_entrega_paso2 => @entrega_paso2_id_entrega_paso2,
        :entrega_paso2_id_entrega => @entrega_paso2_id_entrega,
        :entrega_paso2_id_pago => @entrega_paso2_id_pago,
        :entrega_paso2_nombre_corto => @entrega_paso2_nombre_corto,
        :entrega_paso2_direccion => @entrega_paso2_direccion,
        :entrega_paso3_id_entrega_paso3 => @entrega_paso3_id_entrega_paso3,
        :entrega_paso3_id_entrega => @entrega_paso3_id_entrega,
        :entrega_paso3_id_pago => @entrega_paso3_id_pago,
        :entrega_paso3_nombre_corto => @entrega_paso3_nombre_corto,
        :entrega_paso3_direccion => @entrega_paso3_direccion,
        :forma_de_pago_id_pago => @forma_de_pago_id_pago,
        :forma_de_pago_id_entrega => @forma_de_pago_id_entrega,
        :forma_de_pago_id_ejecucion => @forma_de_pago_id_ejecucion,
        :forma_de_pago_nombre_tarjeta => @forma_de_pago_nombre_tarjeta,
        :forma_de_pago_numero_de_tarjeta => @forma_de_pago_numero_de_tarjeta,
        :forma_de_pago_correo_paypal => @forma_de_pago_correo_paypal,
        :forma_de_pago_efectivo => @forma_de_pago_efectivo,
        :forma_de_pago_establecimiento => @forma_de_pago_establecimiento,
        :forma_de_pago_spei => @forma_de_pago_spei,
        :forma_de_pago3_id_pagop3 => @forma_de_pago3_id_pagop3,
        :forma_de_pago3_id_pago => @forma_de_pago3_id_pago,
        :forma_de_pago3_id_promos => @forma_de_pago3_id_promos,
        :forma_de_pago3_metodo_p => @forma_de_pago3_metodo_p,
        :forma_de_pago3_bin_target => @forma_de_pago3_bin_target,
        :pdp_id_pdp => @pdp_id_pdp,
        :pdp_id_plp => @pdp_id_plp,
        :pdp_articulo => @pdp_articulo,
        :pdp_sku => @pdp_sku,
        :pdp_cantidad => @pdp_cantidad,
        :pdp_p_lista => @pdp_p_lista,
        :pdp_p_venta => @pdp_p_venta,
        :pdp_color => @pdp_color,
        :pdp_talla => @pdp_talla,
        :pdp_material => @pdp_material,
        :pdp_textura => @pdp_textura,
        :pdp_video => @pdp_video,
        :pdp_raiting => @pdp_raiting,
        :pdp_fecha_de_entrega => @pdp_fecha_de_entrega,
        :pdp_promociones => @pdp_promociones,
        :pdp_img_bandera => @pdp_img_bandera,
        :pdp_img_raiting => @pdp_img_raiting,
        :pdp_img_contador => @pdp_img_contador,
        :plp_id_plp => @plp_id_plp,
        :plp_nombre_sku => @plp_nombre_sku,
        :plp_imagen => @plp_imagen,
        :plp_precio_lista => @plp_precio_lista,
        :plp_precio_venta => @plp_precio_venta,
        :plp_raiting => @plp_raiting,
        :plp_id_ejecucion => @plp_id_ejecucion,
        :plp_img_bandera => @plp_img_bandera,
        :plp_img_raiting => @plp_img_raiting,
        :plp_img_contador => @plp_img_contador,
        :productos_id_productos => @productos_id_productos,
        :productos_id_entrega => @productos_id_entrega,
        :productos_id_bolsa => @productos_id_bolsa,
        :productos_nombre_sku => @productos_nombre_sku,
        :productos_color => @productos_color,
        :productos_talla => @productos_talla,
        :productos_material => @productos_material,
        :productos_cantidad => @productos_cantidad,
        :productos_fecha_de_entrega => @productos_fecha_de_entrega,
        :resumen_bolsa_id_resumen_bolsa => @resumen_bolsa_id_resumen_bolsa,
        :resumen_bolsa_id_bolsa => @resumen_bolsa_id_bolsa,
        :resumen_bolsa_subtotal => @resumen_bolsa_subtotal,
        :resumen_bolsa_descuento => @resumen_bolsa_descuento,
        :resumen_bolsa_promocion => @resumen_bolsa_promocion,
        :resumen_bolsa_direccion_envio => @resumen_bolsa_direccion_envio,
        :resumen_bolsa_total => @resumen_bolsa_total,
        :resumen_bolsa_paso2_id_resumen_paso2 => @resumen_bolsa_paso2_id_resumen_paso2,
        :resumen_bolsa_paso2_id_pago => @resumen_bolsa_paso2_id_pago,
        :resumen_bolsa_paso2_id_resumen => @resumen_bolsa_paso2_id_resumen,
        :resumen_bolsa_paso2_subtotal => @resumen_bolsa_paso2_subtotal,
        :resumen_bolsa_paso2_descuento => @resumen_bolsa_paso2_descuento,
        :resumen_bolsa_paso2_codigo_promo => @resumen_bolsa_paso2_codigo_promo,
        :resumen_bolsa_paso2_direccion => @resumen_bolsa_paso2_direccion,
        :resumen_bolsa_paso2_total => @resumen_bolsa_paso2_total,
        :promociones_id_promocion => @promociones_id_promocion,
        :promociones_descripcion_promocion => @promociones_descripcion_promocion,
        :resumen_bolsa_paso3_id_resumen_paso3 => @resumen_bolsa_paso3_id_resumen_paso3,
        :resumen_bolsa_paso3_id_pago => @resumen_bolsa_paso3_id_pago,
        :resumen_bolsa_paso3_id_resumen => @resumen_bolsa_paso3_id_resumen,
        :resumen_bolsa_paso3_id_promociones => @resumen_bolsa_paso3_id_promociones,
        :resumen_bolsa_paso3_subtotal => @resumen_bolsa_paso3_subtotal,
        :resumen_bolsa_paso3_descuento => @resumen_bolsa_paso3_descuento,
        :resumen_bolsa_paso3_codigo_promo => @resumen_bolsa_paso3_codigo_promo,
        :resumen_bolsa_paso3_direccion => @resumen_bolsa_paso3_direccion,
        :resumen_bolsa_paso3_total => @resumen_bolsa_paso3_total,
        :ticket_id_ticket => @ticket_id_ticket,
        :ticket_id_ejecucion => @ticket_id_ejecucion,
        :ticket_id_pdp => @ticket_id_pdp,
        :ticket_id_entrega => @ticket_id_entrega,
        :ticket_id_promos => @ticket_id_promos,
        :ticket_sku_pdp => @ticket_sku_pdp,
        :ticket_promociones => @ticket_promociones,
        :ticket_nombre_sku => @ticket_nombre_sku,
        :ticket_color_sku => @ticket_color_sku,
        :ticket_talla_sku => @ticket_talla_sku,
        :ticket_material_sku => @ticket_material_sku,
        :ticket_textuta_sku => @ticket_textuta_sku,
        :ticket_precio_sku => @ticket_precio_sku,
        :ticket_total_sku => @ticket_total_sku,
        :ticket_detalle_de_la_compra => @ticket_detalle_de_la_compra,
        :ticket_usuario => @ticket_usuario,
        :ticket_correo_compra => @ticket_correo_compra,
        :ticket_numero_de_boleta => @ticket_numero_de_boleta,
        :ticket_numero_terminal => @ticket_numero_terminal,
        :ticket_tienda => @ticket_tienda
    }
  end




  #############################################################################
  # CONSTRUCTOR
  #############################################################################

  #def initialize
  #end

end










