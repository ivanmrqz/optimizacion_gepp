#********************************************************
# CONEXION DB POSTGRESQL
#********************************************************

require_relative 'sql_postgres'
require_relative 'querys'


#Método que obtiene el query string de el archivo querys.rb
# @params
# * :query_name nombre del METODO QUERY, se obtiene del archivo 'querys.rb'
# @return
# * :String query sql
def obtener_query(query_name)
  #return self.send(query_name)
  query = String.new

  case query_name.to_s.downcase.strip
    when 'nombre producto random'
      filtro_busqueda = ['sofline', 'optica', 'bigticket', 'digital', 'h sofline', 'marketplace bigticket']
      producto_random = rand(0..(filtro_busqueda.size - 1))
      query = obtener_query(filtro_busqueda[producto_random])

    when 'marca'
      query = q_marca_random

    when 'sofline'
      query = q_skus_sl

    when 'optica'
      query = q_skus_optica

    when 'bigticket'
      query = q_skus_bt

    when 'digital'
      query = q_skus_digital

    when 'h sofline'
      query = q_skus_h_sl

    when 'marketplace bigticket'
      query = q_skus_mkp_bt

    when 'marketplace sofline'
      query = q_skus_mkp_slm

    when 'usuario login'
      query = q_usuario

    when 'usuario antifraude'
      query = q_usuario_antifraude

    else
      raise("obtener_query => Opcion Query Invalida => #{query_name}")

  end

  #puts "query STRING => #{query}"
  return query

rescue Exception => e
  raise("obtener_query => Error al obtener query string '#{query_name}'.\nException => #{e.message}")
end


#Método que obtiene los resultados de un query en array
# @params
# * :query_name nombre del METODO QUERY, se obtiene del archivo 'querys.rb'
# @return
# * :Array array con la data, cada registro dentro del array es 1 hash map con las columnas que retorna la consulta
def obtener_sql_data(query_name)
  query = obtener_query query_name
  return exec_query query
rescue Exception => e
  raise("obtener_sql_data => Error al ejecutar query '#{query_name}'.\nException => #{e.message}")
end



#*********************************************************************************************************
#*********************************************************************************************************
#*********************************************************************************************************
# OBTENER DATOS
#*********************************************************************************************************
#*********************************************************************************************************
#*********************************************************************************************************

#Método que obtiene la data para la busqueda
# @params
# * :query_name nombre del METODO QUERY, se obtiene del archivo 'querys.rb'
def obtener_busqueda_data(query_name)
  data = Array.new

  begin
    data = obtener_sql_data query_name
    #puts "data => #{data}"
    data_h = Hash.new

    if data.size > 0
      #obtener un index random para el producto
      sku_index = rand(0..(data.size - 1))
      #obtener la data en base al index random
      data_h = data[sku_index]

      if data_h['skus']
        data_h['sku'] = data_h.delete('skus')
      end
      if data_h['descripcion']
        data_h['nombre_sku'] = data_h.delete('descripcion')
      end

      $DT_SKU = data_h

    else
      raise("obtener_busqueda_data => Error no se obtuvieron datos de la consulta. QUERY VACIA.\nQuery => #{query_name}")
    end

  rescue Exception => e
    raise("obtener_busqueda_data => Exception al obtener producto/marca random' #{query_name}'.\nException => #{e.message}")

  end

end



#Método que obtiene la data para login
# @params
# * :query_name nombre del METODO QUERY, se obtiene del archivo 'querys.rb'
def obtener_usuario_data(query_name)
  data = Array.new

  begin
    data = obtener_sql_data query_name
    data_h = Hash.new

    if data.size > 0
      #obtener un index random para el producto
      dt_random = rand(0..(data.size - 1))
      #obtener la data en base al index random
      data_h = data[dt_random]

      $DT_USUARIO = data_h

    else
      raise("obtener_usuario_data => Error no se obtuvieron datos de la consulta. QUERY VACIA. \nQuery => #{query_name}")

    end

  rescue Exception => e
    raise("obtener_usuario_data => Exception al obtener datos del usuario'#{query_name}'.\nException => #{e.message}")

  end

end