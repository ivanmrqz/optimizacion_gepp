#********************************************************
# CONEXION DB POSTGRESQL
#********************************************************

@@no_skus = 10


#Query Generica, para obtener productos softline
def q_skus_sl
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_sku_sl(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos optica
def q_skus_optica
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_172_con_optica(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos bt
def q_skus_bt
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_bt(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos digital
def q_skus_digital
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_digital(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos h sl
def q_skus_h_sl
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_h_sl(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos mkp bt
def q_skus_mkp_bt
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_mkp_bt(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener productos mkp sl
def q_skus_mkp_sl
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_skus_mkp_sl(#{@@no_skus})"
  return q.string.to_s.strip
end


#Query Generica, para obtener marca
def q_marca_random
  q = StringIO.new
  q << "SELECT * FROM "
  q << "fn_liverpool_obtener_marcas('')"
  return q.string.to_s.strip
end


#Query Generica, para obtener marca
def q_usuario_antifraude
  q = StringIO.new
  q << "SELECT * FROM "
  q << "usuario_antifraude"
  return q.string.to_s.strip
end


#Query Generica, para obtener marca
def q_usuario
  q = StringIO.new
  q << "SELECT * FROM "
  q << "usuario"
  return q.string.to_s.strip
end









