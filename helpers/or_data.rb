#####################################################
#Autor: Testingit                                   #
#Descripcion:  Helper para obtener los datos de     #
# los usuarios                                      #
#####################################################

require 'fileutils'

# Método para obtener datos de un elemento desde OR
# @params
#   :or_data_file nombre del archivo csv que contiene el OR
#   :element_name nombre del elemento a obtener
# @return
#   array con datos del dispocitivo
def get_or_data(or_data_file, element_name = nil)

  #se obtiene el datapool del object repository
  or_data = get_or_data_file(or_data_file)

  #hash a retornar
  get_or = Hash.new

  #si no se envia element_name se recupera todo el data pool
  if element_name.nil?

    get_or = or_data

  #de lo contrario se obtiene solo el registro para el elemento correspondiente (element_name)
  else

    or_data.each do |elem|
      if elem[:element_name].to_s.downcase.strip == element_name.to_s.downcase.strip
        get_or = elem
      end
    end

  end



  #puts "get_or => #{get_or}"
  return get_or

end


#Metodo obtener la data de un csv en hash map para los OR
# @params:
#   :csv_data nobre del archivo csv que se va cargar
def get_or_data_file(csv_data)
  csv_file = nil

  #obtiene el path y nombre del archivo csv
  if csv_data.to_s.include? '.csv'
    csv_file =  File.join(File.dirname(__FILE__), "../venture/config/object_repository/#{csv_data}")
  elsif (
  csv_file = File.join(File.dirname(__FILE__), "../venture/config/object_repository/#{csv_data}.csv")
  )
  end


  csv_arr = CSV.read( csv_file,  {:headers => true, :header_converters => :symbol, :encoding => 'ISO-8859-1'} )
  keys = csv_arr.headers.to_a

  # read attribute example => csv[index][:column1]
  return csv_arr.to_a.map {|row| Hash[ keys.zip(row) ] }
end
