#####################################################
#Autor: Testingit                                   #
#Descripcion:  Helper de configuracion general      #
#####################################################
require 'yaml'
require_relative 'csv_data'

class Config

  #************************************************************************************
  # VARIABLES ENTORNO
  #************************************************************************************
  #Driver de la ejecucion selenium / phantom / chromedriver / appium / winium
  @driver_name = ENV['DRIVER'].to_s.strip.downcase
  #tipo de plataforma del app mobile
  @app_platform = ENV['APP_SO'].nil? ? 'android' : ENV['APP_SO'].to_s.strip.downcase
  #ENVIRONMENT LIVE / STAGE
  @env = ENV['APP']
  #Dispositivo
  #   - web/web mobile: desktop(web escritorio)/tablet,smartphone,etc.
  #       en vace a este valor se debera recuperar del pool de datos 'devices', la data correspondiente por el cambo 'DEVICE_NAME'.
  #   - apps mobile: id dispositivo adb devices, xcode uiid
  @device = ENV['DEVICE']
  #Nombre del dispositivo
  @device_name = ENV['DEVICE_NAME'].nil? ? 'ANY_DEVICE' : ENV['DEVICE_NAME']
  #Fecha de ejecucion este valor podria ser usado para crear la carpeta que contenga reporte y evidencia.
  @exec_date = ENV['EXEC_DATE'].nil? ? Time.now.strftime("%Y%m%d") : ENV['EXEC_DATE']
  @exec_time = ENV['EXEC_TIME'].nil? ? Time.now.strftime("%H%M") : ENV['EXEC_TIME']
  #Tipo de ejecucion: 'regular'/'rerun'
  @exec_type = ENV['EXEC_TYPE'].nil? ? 'single' : ENV['EXEC_TYPE']
  #obtiene el directorio de resultados de la ejecucion en curso
  @path_results = ENV['PATH_RESULTS'].nil? ? "#{@exec_type}" : "#{ENV['PATH_RESULTS']}/#{@exec_type}"
  #pat de la evidencia
  @evidence_path = ENV['EVIDENCE_PATH'].nil? ? 'evidence/default' : ENV['EVIDENCE_PATH']
  #puerto por el cual escucha winium - para apps escritorio (wpf, win forms)
  @winium_host = ENV["WINIUM_HOST"]
  @winium_port = ENV["WINIUM_PORT"]


  #************************************************************************************
  # VARIABLES CUCUMBER YML
  #************************************************************************************
  #Obtener YML
  @config = YAML.load(File.read("../venture/config/cucumber.yml"))
  #Timeout por default para Capybara.default_max_wait_time = $to
  #@to = @config["to"]
  #nombre del proyecto, para reporte
  @report_proyect_name = @config["report_proyect_name"]
  #logo del proyecto, para reporte
  @report_logo = @config["report_logo"]


  #************************************************************************************
  # VARIABLES TEST
  #************************************************************************************
  @driver = nil
  @current_feature = nil
  @current_scenario = nil
  @dt_login = nil
  @dt_data = Array.new
  @dt_row = nil



  #****************************************************
  #CLASS SELF
  #****************************************************
  class << self
    attr_reader :app_platform, :env, :exec_time, :exec_type, :winium_port, :driver_name, :device_name,
                :exec_date, :evidence_path, :path_results, :winium_host, :device,
                :driver, :current_feature, :current_scenario,
                :dt_login, :dt_data, :dt_row,
                :report_proyect_name, :report_logo

    attr_writer :app_platform, :env, :exec_time, :exec_type, :winium_port, :driver_name, :device_name,
                :exec_date, :evidence_path, :path_results, :winium_host, :device,
                :driver, :current_feature, :current_scenario,
                :dt_login, :dt_data, :dt_row,
                :report_proyect_name, :report_logo


  end







end

