require 'fileutils'
require_relative 'mate'
#require_relative 'config'


##################################################################################
##################################################################################
##################################################################################
### CALABASH/ADB - ACCIONES/EVENTOS
##################################################################################
##################################################################################
##################################################################################


##################################################################################
### ADB/ADB SHELL
##################################################################################

# LISTA TODOS LOS SERVICIOS DISPONIBLES
#     https://qiita.com/aremokoremo/items/6ea8094d8e7477791944
#exec_adb("shell cmd -l")


#Método para ejecutar comando ADB dentro del device (adb shell)
# @params
# * :cmd comando de shell (cmd, shell, etc)
# @return
# * :Hash hash con outs de la terminal => {:stdout => stdout, :stderr => stderr, :status => status}
def exec_adb(cmd)

  adb_cmd = "#{default_device.adb_command} #{cmd}"
  stdout, stderr, status = Open3.capture3(adb_cmd)

  unless status.success?
    raise "Adb failed: #{adb_cmd} Returned #{stdout} :: #{stderr}"
  end

  #[stdout, stderr, status]
  {:stdout => stdout, :stderr => stderr, :status => status}

end


#Método para ejecutar comando ADB dentro del device (adb shell)
# @params
# * :keyevent key a ejecutar en el dispositivo. ver "../helpers/adb_keyevents_keys"
def keyboard_enter_keyevent(keyevent)
  exec_adb("shell input keyevent #{keyevent}")
end


#Metodo que permite enviar secuencia de teclado de android al dispositivo desde adb
# @params
# * :key key a ejecutar en el dispositivo. ver "../helpers/adb_keyevents_keys"
# * :id_device (OPCIONAL) ID DEVICE, si no se envia valor, se toma por default de variable de entorno del "../run/run_test.sh"y que se recibe en "../helpers/config"
def adb_keys(key, id_device = nil)

  #device = id_device.nil? ? $device : id_device
  #system("adb -s #{device} shell input keyevent #{key}")

  require 'open3'

  if !ENV['DEVICE'].nil?
    #si tenemos la 'variable de entorno DEVICE ' entonces ejecutaremos ADB SHELL desde consola
    #   ESTO EN CASO DE NO ESTAR USANDO CALABASH
    device = id_device.nil? ? $device : id_device
    adb_cmd = "adb -s #{device} shell input keyevent #{key}"

  elsif !default_device.nil?
    #si default device no es nulo, entonces se esta usando 'CALABASH'
    adb_cmd = "#{default_device.adb_command} shell input keyevent #{key}"

  else
    raise "Se necesita definir la VARIABLE DE ENTORNO 'DEVICE'. O ejecutar desde 'CALABASH CONSOLE'"

  end

  #puts "adb_cmd => #{adb_cmd}"
  stdout, stderr, status = Open3.capture3(adb_cmd)

  unless status.success?
    raise "Adb failed: #{adb_cmd} Returned #{stdout} :: #{stderr}"
  end

  [stdout, stderr, status]

end




##################################################################################
##################################################################################
##################################################################################
### CALABASH ANDROID/IOS/MATE.RB - ACCIONES
##################################################################################
##################################################################################
##################################################################################


#Metodo que hace touch sobre un uielement - OR ACCION
# @params
# *  :uielement String locator element
# * :android_lib libreria android valores permitidos => "mate/calabash"
def touch_uielement(uielement, android_lib = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'

  if adr_lib.to_s.strip.include? 'mate'
    tap_element(uielement.to_s.strip)
  else
    # touch para calabash android y ios
    touch(uielement.to_s.strip)
  end
end


#Metodo que introduce texto en uielement - OR ACCION
# @params
# *  :uielement String locator element
# * :text texto a escribir en el input
# * :android_lib libreria android valores permitidos => "mate/calabash"
def enter_text_uielement(uielement, text, android_lib = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'

  case Config.app_platform
    when 'android'
      if adr_lib.to_s.strip.include? 'mate'
        set_text_element(uielement.to_s.strip, text)
      else
        # touch para calabash android
        clear_text_in(uielement.to_s.strip)
        enter_text(uielement.to_s.strip, text)
      end

    when 'ios'
      # touch para calabash ios
      touch(uielement.to_s.strip)
      keyboard_enter_text text

    else
      raise("uielement_enter_text => Fail. opcion invalida => #{Config.app_platform}")
  end
end

#Metodo que introduce texto en uielement - OR ACCION
# @params
# *  :uielement String locator element
# * :text texto a escribir en el input
# * :android_lib libreria android valores permitidos => "mate/calabash"
def enter_text_and_dimiss_keyboard_uielement(uielement, text, android_lib = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'

  case Config.app_platform
    when 'android'
      if adr_lib.to_s.strip.include? 'mate'
        set_text_element(uielement.to_s.strip, text)
      else
        # touch para calabash android
        clear_text_in(uielement.to_s.strip)
        enter_text(uielement.to_s.strip, text)
      end

      #ocultar teclado andorid
      hide_soft_keyboard

    when 'ios'
      # touch para calabash ios
      touch(uielement.to_s.strip)
      keyboard_enter_text text
      #ocultar teclado ios
      dismiss_ipad_keyboard

    else
      raise("uielement_enter_text_and_dimiss_keyboard => Fail. opcion invalida => #{Config.app_platform}")
  end
end





##################################################################################
### SCROLLS
##################################################################################

#-----------------------------------------------------------
# SCROLL TO
#-----------------------------------------------------------

#Metodo que hace scroll a un elemento, incluso si no esta visible en la pantalla
# @params
# *  :uielement String locator element
def scroll_to_uielement(uielement)
  begin
    uielement = uielement.to_s.strip
    scroll_to(uielement)

  rescue Exception => e

    if e.message.to_s.include? "undefined method `embed'"
      puts("Warning al hacer scroll_to elmement => '#{uielement}' => Exeption => #{e.message.to_s}")
    else
      raise("scroll_to_uielement_adr => Error al hacer scroll_to elmement => '#{uielement}' => Exeption => #{e.message.to_s}")
    end

  end
end


#-----------------------------------------------------------
# SCROLL DOWN
#-----------------------------------------------------------

#Metodo que hace scroll down N intentos, hasta que aparesca un mobile elmement - OR ACCION
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# * :android_lib libreria android valores permitidos => "mate/calabash"
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_down_to_uielement(element, intentos = nil, android_lib = nil, scroll_size = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'
  element = element.to_s.strip

  case Config.app_platform
    when 'android'
      if adr_lib.to_s.strip.include? 'mate'
        scroll_down_for_element_exists(element, intentos, scroll_size)

      else
        # calabash android
        scroll_down_to_uielement_adr(element, intentos, scroll_size)

      end

    when 'ios'
      # calabash ios
      scroll_down_to_uielement_ios(element, intentos)

    else
      raise("scroll_down_to_uielement => Fail. opcion invalida => #{Config.app_platform}")
  end
end


#Metodo que hace scroll down N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_down_to_uielement_adr(element, intentos = nil, scroll_size = nil)
  element =  element.to_s.strip
  count = 0
  result = false
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos

    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll si el elemento no existe
    #scroll_down
    scroll_to_down scroll_size

    count =count + 1

  end

  unless result
    puts "scroll_down_to_element_adr => Intentos fallidos al scrollear: #{element}."
  end

  return result

end


#Metodo que hace scroll down N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_down_to_uielement_ios(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = true
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos
    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end
    #scroll si el elemento no existe
    swipe(:down)
    count =count + 1
  end

  unless result
    puts "scroll_down_to_element_ios => Intentos fallidos al scrollear: #{element}."
  end

  return result

end


#-----------------------------------------------------------
# SCROLL UP
#-----------------------------------------------------------

#Metodo que hace scroll up N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# * :android_lib libreria android valores permitidos => "mate/calabash"
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_up_to_uielement(element, intentos = nil, android_lib = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'
  element =  element.to_s.strip

  case Config.app_platform
    when 'android'
      if adr_lib.to_s.strip.include? 'mate'
        scroll_up_for_element_exists(element, intentos)

      else
        # calabash android
        scroll_up_to_element_adr(element, intentos)

      end

    when 'ios'
      # calabash ios
      scroll_up_to_uielement_ios(element, intentos)

    else
      raise("scroll_up_to_uielement => Fail. opcion invalida => #{Config.app_platform}")
  end
end


#Metodo que hace scroll up N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_up_to_element_adr(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = false
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos

    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll si el elemento no existe
    #scroll_to_up 200
    scroll_up

    count =count + 1

  end

  unless result
    puts "scroll_up_to_element_adr => Intentos fallidos al scrollear: #{element}."
  end

  return result

end

#Metodo que hace scroll up N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_up_to_element_adr_dropdown(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = false
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos

    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll si el elemento no existe
    #scroll_to_down 50
    scroll_up

    count =count + 1

  end

  unless result
    puts "scroll_up_to_element_adr => Intentos fallidos al scrollear: #{element}."
  end

  return result

end

#Metodo que hace scroll up N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_up_to_uielement_ios(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = true
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos
    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end
    #scroll si el elemento no existe
    swipe(:up)
    count =count + 1
  end

  unless result
    puts "scroll_up_to_uielement_ios => Intentos fallidos al scrollear: #{element}."
  end

  return result

end


#-----------------------------------------------------------
# SCROLL LEFT
#-----------------------------------------------------------

#Metodo que hace scroll left N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_left_to_uielement(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = false
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos

    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll dependiendo el SO
    case Config.app_platform
      when 'android'
        # calabash android
        performAction('swipe', 'left')
        #pan(element, :left)
      when 'ios'
        # calabash ios
        swipe(:left)
        #scroll(element, :left)
      else
        raise("scroll_left_to_uielement => Fail. opcion invalida => #{Config.app_platform}")
    end
  end

  count = count + 1

  unless result
    puts "scroll_left_to_element_adr => Intentos fallidos al scrollear: #{element}."
  end

  return result
end



#Metodo que hace scroll left N intentos, hasta que aparesca un mobile elmement
# @params
# * :element string query que identifica un mobile element
# * :intentos numero de intentos de scroll hasta ver el elemento. si no se envia el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "intentos"
def scroll_right_to_uielement(element, intentos = nil)
  element =  element.to_s.strip
  count = 0
  result = false
  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos

    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll dependiendo el SO
    case Config.app_platform
      when 'android'
        # calabash android
        performAction('swipe', 'right')
        #pan(element, :right)
      when 'ios'
        # calabash ios
        swipe(:right)
        #scroll(element, :right)
      else
        raise("scroll_right_to_uielement => Fail. opcion invalida => #{Config.app_platform}")
    end
  end

  count = count + 1

  unless result
    puts "scroll_left_to_element_adr => Intentos fallidos al scrollear: #{element}."
  end

  return result
end


#-----------------------------------------------------------
# SCROLL A ELEMENTO DE CALABASH, USANDO EL SCROLL DE MATE
#-----------------------------------------------------------

#Metodo para validar si un elemento existe - OR ACCION
# @params
# * :uielement string query que identifica un mobile element (query de calabash)
# * :intentos numero de intentos que hara scroll para econtrar al elemento, el default es 5
# * :scroll_drag numero de pixeles que recorrera en pantalla, por default si es nil, mate envia 100
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en N intentos
def scroll_down_mate_to_calabash_uielement(uielement, intentos = nil, scroll_drag = nil)
  element =  uielement.to_s.strip
  count = 0
  result = false

  #si no se manda timeout por default se envia 5
  intentos = intentos.nil? ? 5 : intentos.to_i

  while count < intentos
    if element_exists(element)
      #puts "element exist => #{element}"
      el = query element
      if el.first["visible"]
        result = true
        break
      end
    end

    #scroll si el elemento no existe
    scroll_to_down scroll_drag.to_i

    count = count + 1
  end

  unless result
    puts "scroll_down_mate_to_calabash_uielement => Intentos fallidos al scrollear: #{element}."
  end

  return result

end

##################################################################################
### VALIDACIONES DE ELEMENTOS (ASSERTS/VERIFYS/WAIT ELEMENT)
##################################################################################

#Metodo para validar si un elemento existe - OR ACCION
# @params
# * :uielement string query que identifica un mobile element
# * :to time out para esperar que exista el elemento. si no se manda to, el default es 5
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "to"
def wait_element_exist(uielement, to = nil, android_lib = nil)
  adr_lib = android_lib.to_s.downcase.strip == 'mate' ? android_lib.to_s.downcase.strip : 'calabash'

  res = false
  if adr_lib.to_s.strip.include? 'mate'
    res = wait_for_mobile_element(uielement, to)
  else
    res = wait_element_exist_adr_ios(uielement, to)
  end

  return res
end


#Metodo para validar si un elemento existe
# @params
# * :element string query que identifica un mobile element
# * :to time out para esperar en segundos que exista el elemento. si no se manda to, el default es 1s
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "to"
def wait_element_exist_adr_ios(element, to = nil)
  element =  element.to_s.strip
  result = false
  #si no se manda timeout por default se envia 5
  to = to.nil? ? 5 : to
  start = Time.new

  while (start.to_i + to.to_i) > Time.new.to_i
    if element_exists(element)
      #puts "element exist => #{element}"
      result = true
      break
    end
  end

  unless result
    puts "wait_element_exist => TimeOut al buscar: #{element}."
  end

  return result

end


#Método que espera que exista un elemento. Si no existe el testcase es 'fail'
# @params
# * :uielement string query que identifica un mobile element
# * :timeout tiempo máximo de espera
# * :mjs_error en caso de un error mensaje a mostrar en la consola
# * :android_lib libreria android valores permitidos => "mate/calabash"
# @return
# * :Boolean true => si existe el elemento, EXCEPTION => si no se encontro el elemento en el tiempo de espera "timeout"
def assert_for_element_exist(uielement, timeout = nil, mjs_error = nil, android_lib = nil)
  element =  uielement.to_s.strip
  result = wait_element_exist(element, timeout, android_lib)

  unless result
    raise("assert_for_element_exist => TimeOut al buscar: #{uielement}. #{mjs_error}")
  end

  return result

end


#Método que espera que exista un elemento. Si no existe se retorna el el error
# @params
# * :element string query que identifica un mobile element
# * :timeout tiempo máximo de espera
# * :mjs_error en caso de un error mensaje a mostrar en la consola
# * :android_lib libreria android valores permitidos => "mate/calabash"
# @return
# * :Boolean true => si existe el elemento, false => si no se encontro el elemento en el tiempo de espera "timeout"
def verity_for_element_exist(uielement, timeout = nil, mjs_error = nil, android_lib = nil)
  element =  uielement.to_s.strip
  result = wait_element_exist(element, timeout, android_lib)

  unless result
    puts("verity_for_element_exist => TimeOut al buscar: #{uielement}. #{mjs_error}")
  end

  return result

end




##################################################################################
### PUSH NOTIFICATIONS - ANDROID
##################################################################################

#Método que abre la barra de notificaciones de androis
def open_notifications_bar
  begin
    exec_adb("shell service call statusbar 1")
      #exec_adb("shell cmd statusbar expand-notifications")

  rescue
    exec_adb("shell input swipe 0 0 0 300")
  end
end


#Método que abre la barra de notificaciones de androis
def close_notifications_bar
  exec_adb("shell service call statusbar 2")
  #exec_adb("shell cmd statusbar collapse")
end


#Metodo que permite obtener el numero de notificaciones que hay presentes en la "notification bar"
# @return
# * :INT Numero de notificaciones detectadas en el dispositivo (al momento de ejecutar el metodo)
def count_notifications
  #ADB SHELL GET NUMERO DE NOTIFICACIONES
  count = exec_adb("shell dumpsys notification | grep NotificationRecord | wc -l")
  count = count[:stdout].to_s.to_i
end


#Metodo que permite datos de todas las notificaciones
# @return
# * :Hash hash del "exec_adb" con todos los datos de consola de consultar las notificaciones
def get_all_notifications
  #ADB SHELL GET NUMERO DE NOTIFICACIONES
  exec_adb("shell dumpsys notification --noredact")

end


#Metodo que permite datos de todas las notificaciones:
#   - TITULO
#   - TEXTO
#OBTIENE BUSQUEDA DE 2 O MAS VALORES SE DEBE SEPARAR POR ESPACIO '|'
#   adb -s ce0417146055b07d0d shell dumpsys notification --noredact | grep -i "android.title=|android.text="
#OBTIENE BUSQUEDA DE 2 O MAS VALORES SE DEBE SEPARAR POR ESPACIO ' '
#   adb -s ce0417146055b07d0d shell dumpsys notification --noredact | findstr "android.title= android.text="
#OTROS EJEMPLOS:
#   adb -s ce0417146055b07d0d shell dumpsys notification --noredact | grep NotificationRecord
#   adb -s ce0417146055b07d0d shell dumpsys notification --noredact | grep android.title=
#   adb -s ce0417146055b07d0d shell dumpsys notification --noredact | grep android.text=
# @return
# * :Array Array de hash: => "{ :title => notification_titulo, :text => notification_texto }"
def get_notifications_data

  cmd_data = nil
  notifications = nil
  notification_titulo = nil
  notification_texto = nil
  notifications_a = []


  if os.to_s == 'windows'
    cmd_data = exec_adb 'shell dumpsys notification --noredact | findstr "android.title= android.text="'
  else
    cmd_data = exec_adb 'shell dumpsys notification --noredact | grep -i "android.title=|android.text="'
  end

  #si el output de error no es vacio, hubo un error
  unless cmd_data[:stderr].to_s.empty?
    raise "get_notifications_data => Hubo un error => #{cmd_data[:stderr].to_s}"
  end

  notifications = cmd_data[:stdout].to_s.strip
  notifications = notifications.gsub! 'String (', ''
  notifications = notifications.gsub! ')', ''
  notifications = notifications.split('android.title=')

  for i in 0..( notifications.size - 1 )

    row = nil
    cols = nil
    notification_titulo = nil
    notification_texto = nil
    notifications_h = Hash.new

    row = notifications[i].to_s.strip
    #puts "row => #{row}"
    cols = row.split('android.text=')

    notification_titulo = cols[0]
    notification_texto = cols[1]

    notifications_h = { :title => notification_titulo, :text => notification_texto }

    notifications_a.push(notifications_h)

  end

  #puts "notifications_a => #{notifications_a}"
  return notifications_a

end


#Metodo que permite validar si existe una notificacion
# @params
# * :notification_title String contenido en el titulo de la notificacion,
# * :notification_text String contenido en el texto de la notificacion,
# @return
# * :Bolean True => si existe la notificacion, False => si no existe la notificacion
def notification_exists(notification_title = nil, notification_text = nil)

  result = false

  if notification_title.nil? and notification_text.nil?
    raise('Titulo y Texto de la notificacion son null. Se espera al menos un valor.')
    exit
  end

  get_notifications = get_notifications_data

  for i in 0..( get_notifications.size - 1 )
    if get_notifications[i][:title].to_s.downcase.strip.include? notification_title.to_s.downcase.strip and  get_notifications[i][:text].to_s.downcase.strip.include? notification_text.to_s.downcase.strip
      result = true
      puts "notification exists => #{get_notifications[i]}"
      break
    end
  end

  return result

end


#Metodo que permite realizar un assert a una notificacion
# @params
# * :notification_title String contenido en el titulo de la notificacion,
# * :notification_text String contenido en el texto de la notificacion,
# @return
# * :Bolean True => si existe la notificacion, RAISE(EXCEPTION) => si no existe la notificacion
def assert_notification_exists(notification_title = nil, notification_text = nil)

  unless notification_exists(notification_title, notification_text)
    raise "No se encontro la notificacion => Titulo => #{notification_title}, Texto => #{notification_text}"
  end

  return true

end

def clear_all_notifications
  cmd = "shell input swipe 0 0 0 300
          num=$(shell dumpsys notification | grep NotificationRecord | wc -l)
          echo $num
          while [ $num -gt 0 ]; do
              shell input swipe 0 400 300 400
              num=$(( $num - 1 ))
          done"

  exec_adb("shell dumpsys notification -e")

  exec_adb("shell input")

end

