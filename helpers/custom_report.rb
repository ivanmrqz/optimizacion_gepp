require 'fileutils'
require_relative 'config'

#Path relativo y nombre del archivo de ejecucion single | ARGV[0] => nombre archivo json 'ejecucion single'
@file_report  = File.join(File.dirname(__FILE__), "../venture/#{ARGV[0]}")
@path_logo  = File.join(File.dirname(__FILE__), "../../../../venture")

#@file_report = File.join(File.dirname(__FILE__), "#{ARGV[0]}")
#$report_proyect_name = 'PoC Automation Test'
#$report_logo = 'logo.png'


def override_html

	puts "Archivo de reporte => #{@file_report}"

	report = File.read(@file_report.to_s.strip)

	#sobreescribimos el fondo de color del HEADER del html
	report['background: #65c400;'] = 'background: blue;'

	#sobreescribimos el alto 'height' del HEADER del html
	report['height: 6em;'] = 'height: 16em;'

	#copiamos el logo al directorio de ejecucion en curso
	begin
		FileUtils.cp("../venture/config/#{Config.report_logo}", "../venture/#{Config.path_results}/#{Config.report_logo}")
	rescue
		puts "override_html => no se pudo copiar logo"
	end

	#sobreescribimos el titulo 'Cucumber Features' por la variable que contiene
	#	tambien agregamos la imagen de logo
	report['<h1>Cucumber Features</h1>'] = "<img src=\"#{Config.report_logo}\"><h1>#{Config.report_proyect_name}</h1>"

	#SOBREESCRIBIMOS EL ARCHIVO HTML
	File.open("#{@file_report.to_s.strip}", 'w') do |f|
		f.write(report.to_s)
		f.close
	end

	puts "REPORTE CUSTOMED TERMINADO.... "

end
  

#EJECUTAR EL OVERRIDE AL REPORTE
override_html