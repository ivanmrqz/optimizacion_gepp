# COMANDO QUE PERMITE OBTENER NOTIFICACIONES DEL DISPOSITIVO TITULO Y TEXTO DE UNA NOTIFICACION
# adb -s emulator-5554 shell dumpsys notification --noredact

#**********************************************
#UIAutomator
#Retrieving the UIAutomator dump requires a couple of calls: one to create the dump, and one to get it off the phone. I’m using adb shell cat instead of adb pull because it saves managing temporary files on the host machine.
#**********************************************

#**********************************************
# FEATURE STEPS
#**********************************************
# Given I am on the Android home screen
# And there are no notifications with "You got an award on Badoo!"
# And the server sends me an "award" notification
# When I click a notification with "Badoo Award" and "You got an award on Badoo!"
# Then I verify I am on the award screen

#**********************************************
# STEPS DEFINITIONS
#**********************************************
# Given(/^I am on the Android home screen/)
# # Shouldn't really *do* something in a 'given', but...
# keyboard_enter_keyevent('KEYCODE_HOME')
# end
#
# Given(/^There are no notifications with "([^"]*?)"(:? and "([^"]*)")?(?: within (\d+) seconds?)?$/) do |text1, text2, timeout|
#   # Shouldn't really *do* something in a 'given', but...
#   dismiss_notification_matched_by_full_text(
#       (timeout || 1).to_f * 1000, text1, text2)
# end
#
# When(/^I click a notification with "([^"]*?)"(:? and "([^"]*)")?(?: within (\d+) seconds?)?$/) do |text1, text2, timeout|
#   click_notification_matched_by_full_text(
#       (timeout || 1).to_f * 1000, text1, text2)
# end


require 'nokogiri'


def uiautomator_dump
  stdout, stderr, status = exec_adb('shell uiautomator dump')
  unless /dumped to: (?<file>\S*)/ =~ stdout
    fail "uiautomator dump failed? Returned #{stdout} :: #{stderr}"
  end
  stdout, stderr, status = exec_adb("shell cat #{file}")
  [stdout, stderr, status]
end


def xpath_for_full_path_texts(params)
  texts = params.keys.grep(/^notification.full./)
  clauses = texts.collect { |k| "./node/node[@text='#{params[k]}']" }
  puts "xpath_for_full_path_texts => //node[#{clauses.join('][')}]"
  "//node[#{clauses.join('][')}]"
end

def extract_integer_bounds(set)
  return nil if set.empty?
  match = (set.attr('bounds').to_s.match(/\[(\d+),(\d+)\]\[(\d+),(\d+)\]/))
  puts "extract_integer_bounds => match =>   #{match}"
  puts "extract_integer_bounds => match.captures =>   #{match.captures.collect(&:to_i)}"
  match.captures.collect(&:to_i)
end

def bounds_from_xpath(xpath)
  stdout, _stderr, _status = uiautomator_dump
  set = Nokogiri::XML(stdout).xpath(xpath)
  puts "bounds_from_xpath => set => #{set}"
  if (bounds = extract_integer_bounds(set))
    puts "bounds_from_xpath => yield bounds => #{yield bounds}"
    return yield bounds
  else
    return nil
  end
end

def open_notification_shutter
  bounds_from_xpath('//node[1]') do |x1, y1, x2, y2|
    xm = (x1 + x2) >> 1
    exec_adb("shell input swipe #{xm} #{y1} #{xm} #{y2}")
  end
end

def tap_notification(xpath)
  found_bounds = bounds_from_xpath(xpath) do |x1, y1, x2, y2|
    ym = (y1 + y2) >> 1
    xm = (x1 + x2) >> 1
    puts "tap_notification => ym => #{ym}"
    puts "tap_notification => xm => #{xm}"
    exec_adb("shell input tap #{xm} #{ym}")
  end
  dismissed = !found_bounds.nil?
  keyboard_enter_keyevent('KEYCODE_BACK') unless dismissed
  puts "tap_notification => dismissed => #{dismissed}"
  return dismissed
end

def dismiss_notification(xpath)
  found_bounds = bounds_from_xpath(xpath) do |x1, y1, _x2, y2|
    puts "found_bounds => #{found_bounds}"
    ym = (y1 + y2) >> 1
    exec_adb("shell input swipe #{x1} #{ym} 10000 #{ym}")
    puts "EXEC SHELL => shell input swipe #{x1} #{ym} 10000 #{ym}"
  end
  found_bounds.nil?
end


def handle_notification(params)
  xpath = xpath_for_full_path_texts(params)
  puts "XPATH = #{xpath}"

  timeout = params['timeout'].to_i
  start = Time.new

  tap_notification(xpath)

  while start + timeout / 1000 > Time.new
    open_notification_shutter
    if params['action.click']
      puts "ENTRO A IF PARAMS ACTION.CLICK"
      break if tap_notification(xpath)
    else
      puts "ENTRO A ELSE PARAMS ACTION.CLICK"
      break if dismiss_notification(xpath)
    end
  end
end

def click_notification_matched_by_full_text(timeout, *strings)
  h = { 'timeout' => timeout.to_s, 'action.click' => 'true' }
  strings.map.with_index { |v, i| h["notification.full.#{i}"] = v if v }
  handle_notification(h)
end

def dismiss_notification_matched_by_full_text(timeout, *strings)
  h = { 'timeout' => timeout.to_s, 'action.dismiss' => 'true' }
  strings.map.with_index { |v, i| h["notification.full.#{i}"] = v if v }
  puts "params => #{h}"
  handle_notification(h)
end


#**********************************************
#For completeness, the adb functions:
#**********************************************

def exec_adb(cmd)
  adb_cmd = "#{default_device.adb_command} #{cmd}"
  puts "adb_cmd => #{adb_cmd}"
  stdout, stderr, status = Open3.capture3(adb_cmd)
  unless status.success?
    fail "Adb failed: #{adb_cmd} Returned #{stdout} :: #{stderr}"
  end
  [stdout, stderr, status]
end

def keyboard_enter_keyevent(keyevent)
  exec_adb("shell input keyevent #{keyevent}")
end


