#####################################################
#Autor: Testingit                                   #
#Descripcion:  Helper para obtener los datos de     #
# los usuarios                                      #
#####################################################
require 'fileutils'
require 'yaml'
require_relative 'config'


# Método para obtener datos login generico
# @params
#   :user_type tipo de usuario
# @return
#   se retorna un objeto hash map, que contiene el user y password
def get_data_user(user_type)
  #obtener archivo csv
  csv = get_csv_data 'dt_login'
  result = nil
  #obtener el tipo de usuario para el navegador
  user_type = user_type == 'web' ? "#{user_type.to_s.strip.downcase}_#{Config.driver_name}" : user_type

  #ITERAR CSV DATA
  csv.each_with_index do |(record), index|
    if user_type.to_s.strip.downcase == record[:tipo_usuario].to_s.strip.downcase
      result = record
      break
    end
  end

  return result

end

