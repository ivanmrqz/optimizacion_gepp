require 'fileutils'
require_relative 'actions_mobile'

##################################################################################
##################################################################################
##################################################################################
### OR ACTIONS
##################################################################################
##################################################################################
##################################################################################

#Metodo que ejecuta validacion y acciones sobre un web element
# @params
#   :element HASH, contiene todos los datos del elemento OR del data pool
#   :override_dt_fiel valor a ingresar. si es un campo de texto (el valor viene desde el datapool o puede ser i<ngresado directo en la funcion)
def or_exec_uielement(element, override_dt_fiel = nil)
  #valida elemento y obtiene hash => "result = { 'or_element' => element, 'element' => '', 'or_dt_field' => '', 'result' => false }"
  web_elem = val_or_uielement(element, override_dt_fiel)
  #puts "web_elem => #{web_elem}"

  #evalua si el resultado de la validacion es 'true'
  if web_elem['result']
    action_1 = element[:action_1].to_s.empty? ? 'na' : element[:action_1].to_s.downcase.strip
    action_2 = element[:action_2].to_s.empty? ? 'na' : element[:action_2].to_s.downcase.strip
    action_3 = element[:action_3].to_s.empty? ? 'na' : element[:action_3].to_s.downcase.strip
    or_uielement_action( web_elem, action_1  )
    or_uielement_action( web_elem, action_2  )
    or_uielement_action( web_elem, action_3 )
  end

end


#Metodo que ejecuta acciones a un elemento, este metodo se debe implementar usando OR DATA POOL
# @params
#   :uielement HASH OBJECT =>  "val_or_element = { 'or_element' => element, element' => '', 'or_dt_field' => '', 'result' => false }"
#   :action accion a realizar
def or_uielement_action(uielement, action)
  #OBJECT UIELEMENT
  #puts "uielement => #{uielement}"
  or_element = uielement['or_element']
  data = uielement['or_dt_field']
  ui_element = uielement['element'].to_s.strip
  scroll_intentos = or_element[:scroll_intentos].to_s.strip
  android_lib = or_element[:selector].to_s.strip

  #valor a ingresar. si es un campo de texto (el valor viene desde el datapool o puede ser ingresado directo en la funcion)
  #		tambien define logica para elementos, select option, replace substring en el locator etc
  or_td_field = uielement['or_dt_field'].to_s.strip

  case action.to_s.downcase.strip
    when 'na' then

    when 'touch' then
      touch_uielement(ui_element, android_lib)

    when 'enter_text' then
      enter_text_uielement(ui_element, or_td_field, android_lib)

    when 'enter_text_dismiss_keyboard' then
      enter_text_and_dimiss_keyboard_uielement(ui_element, or_td_field, android_lib)

    when 'scroll_to_uielement'
      scroll_to_uielement(ui_element)

    when 'scroll_down_to_element'
      scroll_down_to_uielement(ui_element, scroll_intentos, android_lib)

    when 'scroll_up_to_uielement'
      scroll_up_to_uielement(ui_element, scroll_intentos, android_lib)

    when 'scroll_right_to_uielement'
      scroll_right_to_uielement(ui_element, scroll_intentos)

    when 'scroll_left_to_uielement'
      scroll_left_to_uielement(ui_element, scroll_intentos)

  when 'scroll_down_mate_to_calabash_uielement'
      puts "entro a scroll_down_mate_to_calabash_uielement"
      scroll_down_mate_to_calabash_uielement(ui_element, scroll_intentos.to_i, 200)

    else
      raise("or_element_action => opción invalida => #{action.to_s.downcase.strip}")
  end

end

#Metodo que valida que el campo de data pool a usar no este vacio y el elemento existan
# @params
#   :or_dt nombre del archivo del data pool.
# @return
#   hash con elemento, dato a ingresar, resultado de la validacion.
# 		Ej. "result = { 'or_element' => element, element' => '', 'or_dt_field' => '', 'result' => false }"
def val_or_uielement(element, override_dt_fiel = nil)

  #resultado a retornar, de entrada damos por echo que el elemento no existe y el campo del data pool es vacio,
  # 	para retornar "result=false"
  result = Hash.new
  result = { 'or_element' => element, 'element' => nil, 'or_dt_field' => nil, 'result' => true }

  ##################################################################################
  #SE EVALUA EL DATA POOL / FIELD OVERRIDE
  ##################################################################################

  #--------------------------------------
  # CAMBIO PARA ANDROID
  #--------------------------------------
  result['or_dt_field'] = override_dt_fiel.to_s.strip
  result['result'] = true

  ##################################################################################
  #SE EVALUA EL WEB ELEMENT
  ##################################################################################

    if element[:locator].to_s.empty? or element[:locator].nil?
      #setea el resultado a false
      result['result'] = false
      raise("val_element => No existe LOCATOR para el UIELEMENT => #{element}")
    end

    #OVERRIDE 'LOCATOR' CUANDO  'ACTION = dt_field'
    if element[:locator_override].to_s.strip.downcase == '1'
      loc_overr = nil
      loc_overr = element[:locator].to_s
      #loc_overr =  String.new(element[:locator].to_s)
      loc_overr["${DT_VALUE}"] = result['or_dt_field'].to_s
      element[:locator] = loc_overr
      #puts "locator override => #{element[:locator].to_s}"
    end

    #Enviar el elemento la cadena string final para localizar el elemento
    result['element'] = element[:locator].to_s.strip

    #OVERRIDE DEJAR ORIGINAL
    if element[:locator_override].to_s.strip.downcase == '1'
      loc_overr = nil
      loc_overr = element[:locator].to_s
      loc_overr[result['or_dt_field'].to_s] = "${DT_VALUE}"
      element[:locator] = loc_overr
      #puts "locator override => #{element[:locator].to_s}"
    end

  #SE RETORNA EL VALOR EN FORMA DE HASH
  #puts "result => #{result}"
  return result

end


#Metodo que recupera el registro que se va usar del data pool, siempre y cuando "data_pool[:status_record] <> 0"
# @params
#   :or_dt nombre del archivo del data pool.
def get_dt_for_or_elements(or_data_file)

  #hash a retornar
  get_dt = Hash.new

  if or_data_file.nil? == false && or_data_file.to_s.empty? == false
    #OBTIENE EL CSV del datapool (completo)
    dt = get_csv_data(or_data_file)

    #SE LEE TODO EL DATA POOL Y SE OBTIENE EL primero diferene de 0, este sera el REGISTRO a TRABAJAR
    dt.each_with_index do |record, index|
      #print "key: #{record}, index: #{index}\n"
      #se valida solo se recupera el primer registro! cuando "status_record <> 0"
      #		y el index mayor a '0' ya que este es las columnas
      if record[:status_record].to_s.downcase.strip != '0' && index > 0
        get_dt = record
        break
      end

    end

  end

  return get_dt

end


#Metodo que permite cambiar el 'status_record = 0'. esto cuando 1 dato se quema
# @params
#   :or_data_file nombre del archivo data pool asociado al O.R
def dt_status_record_change(or_data_file)

  #OBTIENE EL CSV del datapool (completo)
  dt = get_csv_data(or_data_file)

  #SE LEE TODO EL DATA POOL Y SE OBTIENE EL primero diferene de 0, este sera el REGISTRO a TRABAJAR
  dt.each_with_index do |record, index|
    #print "key: #{record}, index: #{index}\n"
    #se valida solo se recupera el primer registro! cuando "status_record <> 0"
    #		y el index mayor a '0' ya que este es las columnas
    if record[:status_record].to_s.downcase.strip != '0' && index > 0
      record[:status_record] = '0'
      break
    end

  end

  exportHashToCsv(dt, or_data_file)

end
